package eu.lasersenigma.nms.v1_16_R1;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.session.ClipboardHolder;
import com.sk89q.worldedit.util.io.Closer;
import com.sk89q.worldedit.world.World;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicInvalidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.nms.IWEPaste;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public class NMSWEPaste extends ANMSWE implements IWEPaste {

    public static final int NB_MAXIMUM_BLOCK = 1000000000;

    private final Player player;
    private final Location playerLocation;
    private final byte[] worldEditSchematic;
    WorldEditPlugin worldEditPlugin;
    private World world;
    private Clipboard clipboard;

    public NMSWEPaste(Player player, byte[] worldEditSchematic) {
        this.player = player;
        this.worldEditSchematic = worldEditSchematic;
        this.playerLocation = player.getLocation();
    }

    @Override
    public Location getPlayerLocation() {
        return this.playerLocation;
    }

    @Override
    public boolean initializePaste() throws WorldEditNotAvailableException, InvalidSelectionException, SchematicInvalidException {
        worldEditPlugin = getWorldEditPlugin();
        com.sk89q.worldedit.entity.Player worldEditPlayer = worldEditPlugin.wrapPlayer(player);
        ClipboardFormat format = BuiltInClipboardFormat.SPONGE_SCHEMATIC;
        world = worldEditPlayer.getWorld();
        Closer closer = Closer.create();
        try {
            ByteArrayInputStream bas = closer.register(new ByteArrayInputStream(worldEditSchematic));
            GZIPInputStream gis = closer.register(new GZIPInputStream(bas));
            BufferedInputStream bis = closer.register(new BufferedInputStream(gis));
            ClipboardReader reader = format.getReader(bis);
            clipboard = reader.read();
            closer.close();
            return true;
        } catch (IOException e) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "An error occurred while restoring schematic.", e);
            try {
                closer.close();
            } catch (IOException ex) {
                Logger.getLogger(NMSWEPaste.class.getName()).log(Level.SEVERE, null, ex);
            }
            throw new SchematicInvalidException();
        }
    }

    @Override
    public void finalizePaste() throws SchematicInvalidException {

        try (EditSession editSession = worldEditPlugin.getWorldEdit().newEditSession(world)) {
            Operation operation = new ClipboardHolder(clipboard)
                    .createPaste(editSession)
                    .copyEntities(false)
                    .to(BlockVector3.at(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()))
                    // configure here
                    .build();
            Operations.complete(operation);
        } catch (WorldEditException ex) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.WARNING, "Could not paste schematic {0}", new Object[]{ex.getMessage()});
            throw new SchematicInvalidException();
        }
    }
}
