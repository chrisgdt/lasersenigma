/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.nms.v1_17_R1;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_17_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author Benjamin
 */
public class NMSItems {

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final HashSet<Material> CROSSABLE_MATERIALS = new HashSet<>();

    public static ItemStack getItemStack(Item item, FileConfiguration translationConfigurationFile) {
        String name = null;
        String lore = null;
        ItemStack itemStack = null;
        if (item == null) {
            throw new UnsupportedOperationException("Error during item initialization");
        }
        boolean hasNameAndLore = item.hasNameAndLore();
        if (hasNameAndLore) {
            name = translationConfigurationFile.getString(Item.BASE_PATH + item.getConfigPath() + Item.NAME_PATH, item.getConfigPath());
            lore = translationConfigurationFile.getString(Item.BASE_PATH + item.getConfigPath() + Item.DESCRIPTION_PATH, "");
        }
        Boolean enchant = (Boolean) item.getAttributes().get(ItemAttribute.ENCHANT);
        switch (item.getType()) {
            case HEAD:
                String texture = (String) item.getAttributes().get(ItemAttribute.TEXTURE);
                if (item.hasNameAndLore()) {
                    return ItemUtils.getHead(texture, name, lore);
                } else {
                    return ItemUtils.getHead(texture, "", "");
                }
            case BLOCK:
                Material material = getItemMaterial(item);

                if (!hasNameAndLore) {
                    itemStack = new ItemStack(material, 1);
                } else {
                    itemStack = ItemUtils.getItem(material, name, lore, 1);
                }
                break;
        }
        if (enchant != null && enchant) {
            itemStack = ItemUtils.addEnchantement(itemStack);
        }
        return itemStack;

    }

    public static String getSkullMetaStr(ItemMeta itemMeta) {
        if (!(itemMeta instanceof SkullMeta)) {
            return "";
        }

        String skullMetaStr = "";
        try {
            Field profileField;
            profileField = itemMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            Object profileObject = profileField.get(itemMeta);
            if (profileObject instanceof GameProfile) {
                GameProfile profile = (GameProfile) profileObject;
                Collection<Property> properties = profile.getProperties().get("textures");
                skullMetaStr = properties.stream().map(Property::getValue).reduce(skullMetaStr, String::concat);
            }
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e2) {
        }
        return skullMetaStr;
    }

    public static ItemStack getPlayerSkull(UUID key, String loreStr) {
        OfflinePlayer player = Bukkit.getOfflinePlayer(key);
        ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(player.getName());
        ArrayList<String> lore = new ArrayList<>(Arrays.asList(loreStr.split("\n")));
        skull.setLore(lore);
        skull.setOwningPlayer(player);
        item.setItemMeta(skull);
        return item;
    }

    public static Material getItemMaterial(Item item) {
        LasersColor color = (LasersColor) item.getAttributes().get(ItemAttribute.COLOR);
        byte colorDatavalue = 0;
        if (color != null) {
            colorDatavalue = (byte) color.getDatavalue();
        }
        try {
            String itemName = (String) item.getAttributes().get(ItemAttribute.MATERIAL);
            if (itemName == null) {
                throw new IllegalArgumentException(item.name() + " has no ItemAttribute.Material.");
            }
            MaterialMapping materialMapping = MaterialMapping.requestMaterials(itemName, colorDatavalue);
            if (materialMapping == null) {
                throw new IllegalArgumentException(item.name() + " (datavalue: " + colorDatavalue + ") couldn't be found within the MaterialMapping.");
            }
            return Material.getMaterial(materialMapping.name());
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(item.name() + "couldn't be initialized.", e);
        }
    }

    public static void setBlock(Block block, Material material, LasersColor color) {
        Material newMaterial = null;
        switch (material) {
            case WHITE_STAINED_GLASS:
                switch (color) {
                    case WHITE:
                        newMaterial = Material.WHITE_STAINED_GLASS;
                        break;
                    case RED:
                        newMaterial = Material.RED_STAINED_GLASS;
                        break;
                    case GREEN:
                        newMaterial = Material.GREEN_STAINED_GLASS;
                        break;
                    case BLUE:
                        newMaterial = Material.BLUE_STAINED_GLASS;
                        break;
                    case YELLOW:
                        newMaterial = Material.YELLOW_STAINED_GLASS;
                        break;
                    case MAGENTA:
                        newMaterial = Material.MAGENTA_STAINED_GLASS;
                        break;
                    case CYAN:
                        newMaterial = Material.LIGHT_BLUE_STAINED_GLASS;
                        break;
                }
                break;
            case WHITE_STAINED_GLASS_PANE:
                switch (color) {
                    case WHITE:
                        newMaterial = Material.WHITE_STAINED_GLASS_PANE;
                        break;
                    case RED:
                        newMaterial = Material.RED_STAINED_GLASS_PANE;
                        break;
                    case GREEN:
                        newMaterial = Material.GREEN_STAINED_GLASS_PANE;
                        break;
                    case BLUE:
                        newMaterial = Material.BLUE_STAINED_GLASS_PANE;
                        break;
                    case YELLOW:
                        newMaterial = Material.YELLOW_STAINED_GLASS_PANE;
                        break;
                    case MAGENTA:
                        newMaterial = Material.MAGENTA_STAINED_GLASS_PANE;
                        break;
                    case CYAN:
                        newMaterial = Material.LIGHT_BLUE_STAINED_GLASS_PANE;
                        break;
                }
                break;

            case WHITE_CONCRETE_POWDER:
                switch (color) {
                    case WHITE:
                        newMaterial = Material.WHITE_CONCRETE_POWDER;
                        break;
                    case RED:
                        newMaterial = Material.RED_CONCRETE_POWDER;
                        break;
                    case GREEN:
                        newMaterial = Material.GREEN_CONCRETE_POWDER;
                        break;
                    case BLUE:
                        newMaterial = Material.BLUE_CONCRETE_POWDER;
                        break;
                    case YELLOW:
                        newMaterial = Material.YELLOW_CONCRETE_POWDER;
                        break;
                    case MAGENTA:
                        newMaterial = Material.MAGENTA_CONCRETE_POWDER;
                        break;
                    case CYAN:
                        newMaterial = Material.LIGHT_BLUE_CONCRETE_POWDER;
                        break;
                }
                break;
        }
        if (newMaterial != null) {
            block.setType(newMaterial);
            block.getState().update();
        }
    }

    public static ItemStack getArmor(String tags) {
        ItemStack itemStack = ItemUtils.getItem(Material.DIAMOND_CHESTPLATE, 1);
        net.minecraft.world.item.ItemStack chestPlateNMS = CraftItemStack.asNMSCopy(itemStack);
        NBTTagCompound rootNBTtagCompound = new NBTTagCompound();
        NBTTagList tagsList = new NBTTagList();
        tagsList.add(NBTTagString.a(tags));
        rootNBTtagCompound.set("Tags", tagsList);
        chestPlateNMS.setTag(rootNBTtagCompound);
        return CraftItemStack.asBukkitCopy(CraftItemStack.copyNMSStack(chestPlateNMS, 1));
    }

    static void appendToItemLore(ItemStack itemStack, String txtToAdd) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        List<String> loreLines = itemMeta.getLore();
        loreLines.add(txtToAdd);
        itemMeta.setLore(loreLines);
        itemStack.setItemMeta(itemMeta);
    }
}
