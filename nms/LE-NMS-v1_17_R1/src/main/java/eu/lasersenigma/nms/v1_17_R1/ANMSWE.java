package eu.lasersenigma.nms.v1_17_R1;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import org.bukkit.Bukkit;

public abstract class ANMSWE {

    protected final WorldEditPlugin getWorldEditPlugin() throws WorldEditNotAvailableException {
        WorldEditPlugin worldEditPlugin = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        if (worldEditPlugin == null) {
            throw new WorldEditNotAvailableException();
        }
        return worldEditPlugin;
    }
}
