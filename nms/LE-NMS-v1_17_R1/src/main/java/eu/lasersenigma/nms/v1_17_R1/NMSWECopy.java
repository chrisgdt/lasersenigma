package eu.lasersenigma.nms.v1_17_R1;

import com.sk89q.worldedit.*;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.clipboard.BlockArrayClipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardWriter;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.world.World;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicSaveException;
import eu.lasersenigma.exceptions.SelectionNotCuboidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.nms.IWECopy;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;

public class NMSWECopy extends ANMSWE implements IWECopy {

    private final Player player;

    private Location min;
    private Location max;
    private Location playerLocation;
    private byte[] bytes;

    public NMSWECopy(Player player) throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicSaveException {
        this.player = player;
        this.bytes = new byte[0];
        initializeClipboardCopy();
    }

    @Override
    public Location getMin() {
        return min;
    }

    @Override
    public Location getMax() {
        return max;
    }

    @Override
    public Location getPlayerLocation() {
        return playerLocation;
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }

    private void initializeClipboardCopy() throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicSaveException {
        WorldEditPlugin worldEditPlugin = this.getWorldEditPlugin();
        LocalSession session = worldEditPlugin.getSession(player);
        this.playerLocation = player.getLocation();
        World world = session.getSelectionWorld();
        Region selectedRegion;

        try {
            selectedRegion = session.getSelection(world);
        } catch (IncompleteRegionException e) {
            throw new InvalidSelectionException();
        }
        if (!(selectedRegion instanceof CuboidRegion)) {
            throw new SelectionNotCuboidException();
        }
        this.min = new Location(player.getWorld(), selectedRegion.getMinimumPoint().getBlockX(), selectedRegion.getMinimumPoint().getBlockY(), selectedRegion.getMinimumPoint().getBlockZ());
        this.max = new Location(player.getWorld(), selectedRegion.getMaximumPoint().getBlockX(), selectedRegion.getMaximumPoint().getBlockY(), selectedRegion.getMaximumPoint().getBlockZ());
        BlockArrayClipboard clipboard = new BlockArrayClipboard(selectedRegion);
        EditSession editSession = worldEditPlugin.getWorldEdit().getEditSessionFactory().getEditSession(selectedRegion.getWorld(), -1);
        clipboard.setOrigin(BlockVector3.at(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()));
        ForwardExtentCopy copy = new ForwardExtentCopy(editSession, selectedRegion, clipboard, selectedRegion.getMinimumPoint());
        copy.setRemovingEntities(true);
        copy.setCopyingBiomes(false);
        try {
            Operations.complete(copy);
        } catch (MaxChangedBlocksException e) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, null, e);
            throw new SchematicSaveException();
        } catch (WorldEditException ex) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, null, ex);
            throw new SchematicSaveException();
        }

        ByteArrayOutputStream baos = null;
        ClipboardWriter writer = null;
        GZIPOutputStream gzos = null;
        BufferedOutputStream bos = null;
        boolean error = false;
        try {
            baos = new ByteArrayOutputStream();
            gzos = new GZIPOutputStream(baos);
            bos = new BufferedOutputStream(gzos);
            writer = BuiltInClipboardFormat.SPONGE_SCHEMATIC.getWriter(gzos);
            writer.write(clipboard);
            writer.close();
            bos.close();
            gzos.close();
            baos.close();
            this.bytes = baos.toByteArray();
        } catch (IOException e) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "An error occured while saving schematic.", e);
            error = true;
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ex) {
                    Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }
            if (gzos != null) {
                try {
                    gzos.close();
                } catch (IOException ex) {
                    Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException ex) {
                    Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ex) {
                    Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "A second error appeared while closing the streams.", ex);
                    error = true;
                }
            }
        }
        if (this.bytes.length == 0) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "An error occured while saving schematic.");
            throw new SchematicSaveException();
        }
        if (error) {
            throw new SchematicSaveException();
        }
    }
}
