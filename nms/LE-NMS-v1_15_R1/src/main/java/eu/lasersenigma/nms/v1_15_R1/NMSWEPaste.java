package eu.lasersenigma.nms.v1_15_R1;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.BuiltInClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.util.io.Closer;
import com.sk89q.worldedit.world.World;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicInvalidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.nms.IWEPaste;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public class NMSWEPaste extends ANMSWE implements IWEPaste {

    public static final int NB_MAXIMUM_BLOCK = 1000000000;

    private final Player player;
    private final Location playerLocation;
    private final byte[] worldEditSchematic;
    WorldEditPlugin worldEditPlugin;
    private Region region;
    private Extent targetExtent;
    private Clipboard clipboard;

    public NMSWEPaste(Player player, byte[] worldEditSchematic) {
        this.player = player;
        this.worldEditSchematic = worldEditSchematic;
        this.playerLocation = player.getLocation();
    }

    @Override
    public Location getPlayerLocation() {
        return this.playerLocation;
    }

    @Override
    public boolean initializePaste() throws WorldEditNotAvailableException, InvalidSelectionException, SchematicInvalidException {
        worldEditPlugin = getWorldEditPlugin();
        com.sk89q.worldedit.entity.Player worldEditPlayer = worldEditPlugin.wrapPlayer(player);
        ClipboardFormat format = BuiltInClipboardFormat.SPONGE_SCHEMATIC;
        World world = worldEditPlayer.getWorld();
        Closer closer = Closer.create();
        try {
            ByteArrayInputStream bas = closer.register(new ByteArrayInputStream(worldEditSchematic));
            GZIPInputStream gis = closer.register(new GZIPInputStream(bas));
            BufferedInputStream bis = closer.register(new BufferedInputStream(gis));
            ClipboardReader reader = format.getReader(bis);
            clipboard = reader.read();
            region = clipboard.getRegion();

            targetExtent = world;
            closer.close();
            return true;
        } catch (IOException e) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "An error occured while restoring schematic.", e);
            try {
                closer.close();
            } catch (IOException ex) {
                Logger.getLogger(NMSWEPaste.class.getName()).log(Level.SEVERE, null, ex);
            }
            throw new SchematicInvalidException();
        }
    }

    @Override
    public void finalizePaste() throws SchematicInvalidException {

        try (EditSession editSession = worldEditPlugin.getWorldEdit().getEditSessionFactory().getEditSession(region.getWorld(), NB_MAXIMUM_BLOCK)) {
            ForwardExtentCopy copy = new ForwardExtentCopy(clipboard, region, clipboard.getOrigin(), targetExtent, BlockVector3.at(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ()));
            copy.setCopyingEntities(false);
            Operations.completeLegacy(copy);
            editSession.flushSession();
        } catch (MaxChangedBlocksException ex) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.WARNING, "Exceeded the block limit while restoring schematic. limit in exception: {0}, limit passed by plugin: {1}", new Object[]{ex.getBlockLimit(), NB_MAXIMUM_BLOCK});
            throw new SchematicInvalidException();
        }
    }
}
