package eu.lasersenigma.nms;

import org.bukkit.Location;

public interface IWECopy {

    Location getMin();

    Location getMax();

    Location getPlayerLocation();

    byte[] getBytes();

}
