package eu.lasersenigma.nms;

import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicSaveException;
import eu.lasersenigma.exceptions.SelectionNotCuboidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.items.AArmorActionProcessor;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.items.Item;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.HashSet;
import java.util.UUID;

public interface ICoreNMS {

    void playSound(Location loc, String soundName, String soundCategory, float volume, float pitch);

    void playEffect(Location loc, String effect, int nbParticles);

    void playEffect(HashSet<Player> players, Location location, String effect, Color lasersColor);

    void playEffect(HashSet<Player> players, Location location, String effect, double xOffset, double yOffSet, double zOffSet, double speed, int count);

    ItemStack getItemStack(Item item, FileConfiguration translationConfigurationFile);

    void setBlock(Block block, Item item);

    void powerBlock(Block block, byte power);

    String getSkullMetaStr(ItemMeta itemMeta);

    ItemStack getPlayerSkull(UUID key, String loreStr);

    LasersColor getColorFromBlock(Block block);

    DyeColor getDyeColor(LasersColor color);

    boolean isConcretePowder(Material type);

    boolean isGlassOrStainedGlass(Material type);

    HashSet<Material> getCrossableMaterials(String[] crossableMaterialsStr);

    boolean isInsideEntityHitbox(LivingEntity lentity, Location location);

    IWECopy worldEditCopy(Player player) throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicSaveException;

    IWEPaste worldEditPaste(Player player, byte[] worldeditSchematic);

    AItemStackFlagsProcessor getItemStackFlagsProcessor();

    Inventory inventoryFromBase64(String data) throws IOException;

    String base64FromInventory(Inventory inventory);

    Material getItemMaterial(Item item);

    void setBlock(Block block, Material material, LasersColor color);

    AArmorActionProcessor getArmorActionProcessor(Player bukkitPlayer, ArmorAction selectedArmorAction, Location newMaterialLocation, boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber, String armor_no_knockback_tag, String armor_no_burn_tag, String armor_no_damage_tag, String armor_reflect_tag, String armor_prism_tag, String armor_focus_tag, String armor_lasers_durability_regex);

    void appendToItemLore(ItemStack itemStack, String txtToAdd);

}
