package eu.lasersenigma.nms;

import org.bukkit.inventory.ItemStack;

public abstract class AItemStackFlagsProcessor {

    protected static final short BASE_DURABILITY_DAMAGES = 1;

    protected String armor_no_knockback_tag;
    protected String armor_no_burn_tag;
    protected String armor_no_damage_tag;
    protected String armor_reflect_tag;
    protected String armor_prism_tag;
    protected String armor_focus_tag;
    protected String armor_lasers_durability_regex;

    protected boolean no_knockback = false;
    protected boolean no_burn = false;
    protected boolean no_damage = false;
    protected boolean reflect = false;
    protected boolean prism = false;
    protected boolean focus = false;
    protected int durabilityModifier = 7;

    public void setNoKnockbackFlag(String noKnockbackFlag) {
        this.armor_no_knockback_tag = noKnockbackFlag;
    }

    public void setNoBurnFlag(String noBurnFlag) {
        this.armor_no_burn_tag = noBurnFlag;
    }

    public void setNoDamageFlag(String noDamageFlag) {
        this.armor_no_damage_tag = noDamageFlag;
    }

    public void setReflectFlag(String reflectFlag) {
        this.armor_reflect_tag = reflectFlag;
    }

    public void setPrismFlag(String prismFlag) {
        this.armor_prism_tag = prismFlag;
    }

    public void setFocusFlag(String noFocusFlag) {
        this.armor_focus_tag = noFocusFlag;
    }

    public void setDurabilityFlag(String durabilityFlag) {
        this.armor_lasers_durability_regex = durabilityFlag;
    }

    public boolean hasNoKnockback() {
        return this.no_knockback;
    }

    public boolean hasNoBurn() {
        return this.no_burn;
    }

    public boolean hasNoDamage() {
        return this.no_damage;
    }

    public boolean hasReflect() {
        return this.reflect;
    }

    public boolean hasPrism() {
        return this.prism;
    }

    public boolean hasFocus() {
        return this.focus;
    }

    public int getDurabilityModifier() {
        return this.durabilityModifier;
    }


    public abstract void process(ItemStack itemStack);
}
