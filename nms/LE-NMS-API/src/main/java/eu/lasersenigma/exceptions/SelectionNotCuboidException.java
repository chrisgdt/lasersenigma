package eu.lasersenigma.exceptions;

public class SelectionNotCuboidException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "selection_not_cuboid";

    /**
     * Constructor
     */
    public SelectionNotCuboidException() {
        super(TRANSLATION_CODE);
    }

}
