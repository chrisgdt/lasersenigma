package eu.lasersenigma.exceptions;

public class SchematicSaveException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    public static final String TRANSLATION_CODE = "schematic_saving_error";

    /**
     * Constructor
     */
    public SchematicSaveException() {
        super(TRANSLATION_CODE);
    }

}
