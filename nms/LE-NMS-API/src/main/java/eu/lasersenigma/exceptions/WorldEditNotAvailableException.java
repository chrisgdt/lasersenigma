package eu.lasersenigma.exceptions;

public class WorldEditNotAvailableException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "worldedit_not_available";

    /**
     * Constructor
     */
    public WorldEditNotAvailableException() {
        super(TRANSLATION_CODE);
    }

}
