package eu.lasersenigma.exceptions;

public class InvalidSelectionException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    public static final String TRANSLATION_CODE = "invalid_selection";

    /**
     * Constructor
     */
    public InvalidSelectionException() {
        super(TRANSLATION_CODE);
    }

}
