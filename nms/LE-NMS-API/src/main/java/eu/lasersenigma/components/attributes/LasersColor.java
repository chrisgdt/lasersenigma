package eu.lasersenigma.components.attributes;

import org.bukkit.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * The colors supported by the plugin
 */
public enum LasersColor {
    WHITE(Color.WHITE, (byte) 0),
    RED(Color.RED, (byte) 14),
    GREEN(Color.LIME, (byte) 5),
    BLUE(Color.BLUE, (byte) 11),
    YELLOW(Color.YELLOW, (byte) 4),
    MAGENTA(Color.FUCHSIA, (byte) 2),
    CYAN(Color.AQUA, (byte) 3),
    BLACK(Color.BLACK, (byte) 15);

    /*
    WHITE(Color.fromRGB(255,255,255), (byte) 0),
    ORANGE(Color.fromRGB(249, 128, 29), (byte) 1),
    MAGENTA(Color.fromRGB(198, 79, 189),(byte)2),
    LIGHT_BLUE(Color.fromRGB(58, 179, 218),(byte)3),
    YELLOW(Color.fromRGB(255, 216, 61), (byte) 4),
    LIME(Color.fromRGB(128, 199, 31),(byte)5),
    PINK(Color.fromRGB(243, 140, 170),(byte)6),
    GRAY(Color.fromRGB(71, 79, 82),(byte)7),
    LIGHT_GRAY(Color.fromRGB(156, 157, 151),(byte)8),
    CYAN(Color.fromRGB(22, 156, 157),(byte)9),
    PURPLE(Color.PURPLE, (byte) 10),
    BLUE(Color.BLUE, (byte) 11),
    BROWN(Color.fromRGB(130, 84, 50),(byte)12),
    GREEN(Color.GREEN, (byte) 13),
    RED(Color.RED, (byte) 14),
    BLACK(Color.BLACK, (byte) 15);
    
     */
    /**
     * The path to the boolean "only_needed_colors" in the configuration
     */
    public static String ONLY_NEED_COLORS_CONFIG_PATH = "only_needed_colors";
    /**
     * The bukkit color
     */
    private final Color bukkitColor;
    /**
     * The ordinal value
     */
    private final int ordinal;
    /**
     * the byte datavalue for this DyeColor
     */
    private final byte datavalue;
    /**
     * Constructor
     *
     * @param bukkitColor the bukkit color
     * @param datavalue   the byte datavalue
     */
    LasersColor(Color bukkitColor, byte datavalue) {
        this.bukkitColor = bukkitColor;
        this.ordinal = ordinal();
        this.datavalue = datavalue;
    }

    /**
     * Retrives a color from its integer value (used for database loading)
     *
     * @param ordinal the integer value of this color
     * @return the corresponding color
     */
    public static LasersColor fromInt(int ordinal) {
        return values()[ordinal];
    }

    public static LasersColor merge(Set<LasersColor> colors) {
        if (colors != null && !colors.isEmpty()) {
            boolean red = colors.contains(RED);
            boolean yellow = colors.contains(GREEN);
            boolean blue = colors.contains(BLUE);
            if (colors.contains(WHITE)) {
                red = true;
                yellow = true;
                blue = true;
            } else {
                if (colors.contains(YELLOW)) {
                    red = true;
                    yellow = true;
                }
                if (colors.contains(MAGENTA)) {
                    red = true;
                    blue = true;
                }
                if (colors.contains(CYAN)) {
                    yellow = true;
                    blue = true;
                }
            }
            if (red && yellow && blue) {
                return WHITE;
            }
            if (red && yellow) {
                return YELLOW;
            }
            if (red && blue) {
                return MAGENTA;
            }
            if (yellow && blue) {
                return CYAN;
            }
            if (red) {
                return RED;
            }
            if (yellow) {
                return GREEN;
            }
            if (blue) {
                return BLUE;
            }
        }
        return null;
    }

    /**
     * Checks if the current color is (AT LEAST or ONLY) composed of given
     * colors the fact the test is bad on "AT LEAST" or "ONLY" depends from the
     * value of the "only_needed_color" boolean in configuration
     *
     * @param colors           The colors the current color may be composed of
     * @param onlyNeededColors (from configuration)
     * @return true is the color is (AT LEAST or ONLY) composed of the given
     * colors
     */
    public boolean isComposedOf(Set<LasersColor> colors, boolean onlyNeededColors) {
        if (onlyNeededColors) {
            int size = colors.size();
            switch (this) {
                case RED:
                    return (colors.contains(RED) && size == 1);
                case GREEN:
                    return (colors.contains(GREEN) && size == 1);
                case BLUE:
                    return (colors.contains(BLUE) && size == 1);
                case YELLOW:
                    if (size == 2) {
                        return (colors.contains(RED) && colors.contains(GREEN));
                    } else if (size == 1) {
                        return colors.contains(YELLOW);
                    }
                    return false;
                case MAGENTA:
                    if (size == 2) {
                        return (colors.contains(RED) && colors.contains(BLUE));
                    } else if (size == 1) {
                        return colors.contains(MAGENTA);
                    }
                    return false;
                case CYAN:
                    if (size == 2) {
                        return (colors.contains(BLUE) && colors.contains(GREEN));
                    } else if (size == 1) {
                        return colors.contains(CYAN);
                    }
                    return false;
                case WHITE:
                    switch (size) {
                        case 1:
                            return colors.contains(WHITE);
                        case 2:
                            return ((colors.contains(RED) && colors.contains(CYAN))
                                    || (colors.contains(GREEN) && colors.contains(MAGENTA))
                                    || (colors.contains(BLUE) && colors.contains(YELLOW)));
                        case 3:
                            return (colors.contains(RED) && colors.contains(GREEN) && colors.contains(BLUE));
                        default:
                            return false;
                    }
                default:
                    return false;
            }
        } else {
            int parts = 0;
            switch (this) {
                case RED:
                    return (colors.contains(RED) || colors.contains(YELLOW) || colors.contains(MAGENTA) || colors.contains(WHITE));
                case GREEN:
                    return (colors.contains(GREEN) || colors.contains(YELLOW) || colors.contains(CYAN) || colors.contains(WHITE));
                case BLUE:
                    return (colors.contains(BLUE) || colors.contains(CYAN) || colors.contains(MAGENTA) || colors.contains(WHITE));
                case YELLOW:
                    if (colors.contains(YELLOW) || colors.contains(WHITE)) {
                        return true;
                    }
                    if (colors.contains(GREEN) || colors.contains(CYAN)) {
                        parts++;
                    }
                    if (colors.contains(RED) || colors.contains(MAGENTA)) {
                        parts++;
                    }
                    return (parts >= 2);
                case MAGENTA:
                    if (colors.contains(MAGENTA) || colors.contains(WHITE)) {
                        return true;
                    }
                    if (colors.contains(BLUE) || colors.contains(CYAN)) {
                        parts++;
                    }
                    if (colors.contains(RED) || colors.contains(YELLOW)) {
                        parts++;
                    }
                    return (parts >= 2);
                case CYAN:
                    if (colors.contains(CYAN) || colors.contains(WHITE)) {
                        return true;
                    }
                    if (colors.contains(BLUE) || colors.contains(MAGENTA)) {
                        parts++;
                    }
                    if (colors.contains(GREEN) || colors.contains(YELLOW)) {
                        parts++;
                    }
                    return (parts >= 2);
                case WHITE:
                    if (colors.contains(WHITE)) {
                        return true;
                    }
                    if (colors.contains(RED) && colors.contains(GREEN) && colors.contains(BLUE)) {
                        return true;
                    }
                    if (colors.contains(RED) && colors.contains(CYAN)) {
                        return true;
                    }
                    if (colors.contains(GREEN) && colors.contains(MAGENTA)) {
                        return true;
                    }
                    if (colors.contains(BLUE) && colors.contains(YELLOW)) {
                        return true;
                    }
                default:
                    return false;
            }

        }
    }

    /**
     * gets the bukkit color
     *
     * @return the bukkit color
     */
    public Color getBukkitColor() {
        return this.bukkitColor;
    }

    /**
     * gets the datavalue
     *
     * @return the datavalue for this color
     */
    public short getDatavalue() {
        return this.datavalue;
    }

    /**
     * gets the integer value for this color
     *
     * @return the integer value for this color
     */
    public int getInt() {
        return this.ordinal;
    }

    /**
     * gets the next color for changeColor loops purposes
     *
     * @param withBlack true if black is accepted in the color rotation
     * @return the next color
     */
    public LasersColor getNextColor(boolean withBlack) {
        LasersColor[] colors = LasersColor.values();
        LasersColor newColor = colors[((ordinal + 1) % colors.length)];
        if (!withBlack && newColor == BLACK) {
            return newColor.getNextColor(withBlack);
        }
        return newColor;
    }

    /**
     * Filter the current color with a colored filter
     *
     * @param filterColor the color of the filter
     * @return a map containing 2 colors value with the following keys:
     * LasersColor.FilterResult.REFLECTED and
     * LasersColor.FilterResult.PASS_THROUGH
     */
    public HashMap<FilterResult, LasersColor> filterBy(LasersColor filterColor) {
        HashMap<FilterResult, LasersColor> result = new HashMap<>();
        result.put(FilterResult.REFLECTED, null);
        result.put(FilterResult.PASS_THROUGH, null);
        if (filterColor == WHITE) {
            result.put(FilterResult.PASS_THROUGH, this);
            return result;
        } else if (filterColor == BLACK) {
            result.put(FilterResult.REFLECTED, this);
            result.put(FilterResult.PASS_THROUGH, this);
            return result;
        }
        if (this == filterColor) {
            result.put(FilterResult.REFLECTED, this);
            return result;
        }
        switch (this) {
            case RED:
                switch (filterColor) {
                    case BLUE:
                    case GREEN:
                    case CYAN:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                    default:
                        result.put(FilterResult.REFLECTED, this);
                        break;
                }
                break;
            case GREEN:
                switch (filterColor) {
                    case RED:
                    case BLUE:
                    case MAGENTA:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                    default:
                        result.put(FilterResult.REFLECTED, this);
                        break;
                }
                break;
            case BLUE:
                switch (filterColor) {
                    case RED:
                    case GREEN:
                    case YELLOW:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                    default:
                        result.put(FilterResult.REFLECTED, this);
                        break;
                }
                break;
            case YELLOW:
                switch (filterColor) {
                    case GREEN:
                    case CYAN:
                        result.put(FilterResult.REFLECTED, GREEN);
                        result.put(FilterResult.PASS_THROUGH, RED);
                        break;
                    case RED:
                    case MAGENTA:
                        result.put(FilterResult.REFLECTED, RED);
                        result.put(FilterResult.PASS_THROUGH, GREEN);
                        break;
                    default:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                }
                break;
            case MAGENTA:
                switch (filterColor) {
                    case RED:
                    case YELLOW:
                        result.put(FilterResult.REFLECTED, RED);
                        result.put(FilterResult.PASS_THROUGH, BLUE);
                        break;
                    case BLUE:
                    case CYAN:
                        result.put(FilterResult.REFLECTED, BLUE);
                        result.put(FilterResult.PASS_THROUGH, RED);
                        break;
                    default:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                }
                break;
            case CYAN:
                switch (filterColor) {
                    case GREEN:
                    case YELLOW:
                        result.put(FilterResult.REFLECTED, GREEN);
                        result.put(FilterResult.PASS_THROUGH, BLUE);
                        break;
                    case BLUE:
                    case MAGENTA:
                        result.put(FilterResult.REFLECTED, BLUE);
                        result.put(FilterResult.PASS_THROUGH, GREEN);
                        break;
                    default:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                }
                break;
            case WHITE:
                switch (filterColor) {
                    case RED:
                        result.put(FilterResult.REFLECTED, RED);
                        result.put(FilterResult.PASS_THROUGH, CYAN);
                        break;
                    case GREEN:
                        result.put(FilterResult.REFLECTED, GREEN);
                        result.put(FilterResult.PASS_THROUGH, MAGENTA);
                        break;
                    case BLUE:
                        result.put(FilterResult.REFLECTED, BLUE);
                        result.put(FilterResult.PASS_THROUGH, YELLOW);
                        break;
                    case YELLOW:
                        result.put(FilterResult.REFLECTED, YELLOW);
                        result.put(FilterResult.PASS_THROUGH, BLUE);
                        break;
                    case MAGENTA:
                        result.put(FilterResult.REFLECTED, MAGENTA);
                        result.put(FilterResult.PASS_THROUGH, GREEN);
                        break;
                    case CYAN:
                        result.put(FilterResult.REFLECTED, CYAN);
                        result.put(FilterResult.PASS_THROUGH, RED);
                        break;
                    default:
                        result.put(FilterResult.PASS_THROUGH, this);
                        break;
                }
                break;
            default:
                result.put(FilterResult.PASS_THROUGH, this);
                break;
        }
        return result;
    }

    public ArrayList<LasersColor> decompose() {
        ArrayList<LasersColor> res = new ArrayList<>();
        switch (this) {
            case WHITE:
                res.add(MAGENTA);
                res.add(BLUE);
                res.add(CYAN);
                res.add(GREEN);
                res.add(YELLOW);
                res.add(RED);
                break;
            case YELLOW:
                res.add(GREEN);
                res.add(RED);
                break;
            case CYAN:
                res.add(BLUE);
                res.add(GREEN);
                break;
            case MAGENTA:
                res.add(BLUE);
                res.add(RED);
                break;
            default:
                res.add(this);
                break;
        }
        return res;
    }

    /**
     * The filter result enums.
     *
     * @see LasersColor#filterBy
     */
    public enum FilterResult {
        REFLECTED, PASS_THROUGH
    }
}
