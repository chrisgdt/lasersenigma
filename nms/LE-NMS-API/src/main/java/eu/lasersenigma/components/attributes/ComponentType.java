package eu.lasersenigma.components.attributes;

/**
 * The component types
 */
public enum ComponentType {
    LASER_SENDER,
    LASER_RECEIVER,
    MIRROR_SUPPORT,
    MIRROR_CHEST,
    REDSTONE_WINNER_BLOCK,
    DISAPPEARING_WINNER_BLOCK,
    APPEARING_WINNER_BLOCK,
    MELTABLE_CLAY,
    PRISM,
    CONCENTRATOR,
    REFLECTING_SPHERE,
    FILTERING_SPHERE,
    ATTRACTION_REPULSION_SPHERE,
    MUSIC_BLOCK,
    REDSTONE_SENSOR,
    LOCK,
    KEY_CHEST,
    ELEVATOR,
    CALL_BUTTON
}
