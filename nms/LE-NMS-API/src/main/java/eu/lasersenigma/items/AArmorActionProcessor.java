/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.items;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Comparator;

/**
 * @author Benjamin
 */
public abstract class AArmorActionProcessor {

    protected final Player player;
    protected final ArmorAction selectedArmorAction;
    protected final Location newMaterialLocation;
    protected final boolean noBurnCheckbox;
    protected final boolean noDamageCheckbox;
    protected final boolean noKnockbackCheckbox;
    protected final boolean reflectionCheckbox;
    protected final boolean focusCheckbox;
    protected final boolean prismCheckbox;
    protected final int durabilityNumber;

    protected final String armor_no_knockback_tag;
    protected final String armor_no_burn_tag;
    protected final String armor_no_damage_tag;
    protected final String armor_reflect_tag;
    protected final String armor_prism_tag;
    protected final String armor_focus_tag;
    protected final String armor_lasers_durability_regex;

    public AArmorActionProcessor(Player player, ArmorAction selectedArmorAction, Location newMaterialLocation,
                                 boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber,
                                 String armor_no_knockback_tag, String armor_no_burn_tag, String armor_no_damage_tag, String armor_reflect_tag, String armor_prism_tag, String armor_focus_tag, String armor_lasers_durability_regex
    ) {
        this.player = player;
        this.selectedArmorAction = selectedArmorAction;
        this.newMaterialLocation = newMaterialLocation;

        this.noBurnCheckbox = noBurnCheckbox;
        this.noDamageCheckbox = noDamageCheckbox;
        this.noKnockbackCheckbox = noKnockbackCheckbox;
        this.reflectionCheckbox = reflectionCheckbox;
        this.focusCheckbox = focusCheckbox;
        this.prismCheckbox = prismCheckbox;
        this.durabilityNumber = durabilityNumber;

        this.armor_no_knockback_tag = armor_no_knockback_tag;
        this.armor_no_burn_tag = armor_no_burn_tag;
        this.armor_no_damage_tag = armor_no_damage_tag;
        this.armor_reflect_tag = armor_reflect_tag;
        this.armor_prism_tag = armor_prism_tag;
        this.armor_focus_tag = armor_focus_tag;
        this.armor_lasers_durability_regex = armor_lasers_durability_regex;
    }

    protected String getTagsString() {
        StringBuilder sb = new StringBuilder();
        if (noBurnCheckbox) {
            sb.append(armor_no_burn_tag);
        }
        if (noDamageCheckbox) {
            sb.append(armor_no_damage_tag);
        }
        if (noKnockbackCheckbox) {
            sb.append(armor_no_knockback_tag);
        }
        if (reflectionCheckbox) {
            sb.append(armor_reflect_tag);
        }
        if (focusCheckbox) {
            sb.append(armor_focus_tag);
        }
        if (prismCheckbox) {
            sb.append(armor_prism_tag);
        }
        sb.append(armor_lasers_durability_regex.replace("X", "" + durabilityNumber));
        return sb.toString();
    }

    public Player getTargetedPlayer() {
        switch (selectedArmorAction) {
            case GIVE_TO_SELF:
                return player;
            case GIVE_TO_NEAREST:
                final Location playerLocation = player.getLocation();

                return playerLocation.getWorld().getPlayers().stream()
                        .filter((p) -> p.getUniqueId() != player.getUniqueId())
                        .min(Comparator.comparingDouble((p) -> {
                            return playerLocation.distance(p.getLocation());
                        })).orElse(null);
            default:
                return null;
        }
    }

    public abstract void process();

    public abstract ItemStack getArmorItemStack();
}
