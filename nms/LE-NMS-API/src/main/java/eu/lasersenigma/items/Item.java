package eu.lasersenigma.items;

import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LasersColor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * The items used in this plugin
 */
public enum Item {
    EMPTY(ItemType.BLOCK, null, true, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "AIR");
        }
    }),
    /**
     * ************************** MAIN SHORTCUTBAR ***************************
     */
    MAIN_SHORTCUTBAR_PLACE_COMPONENT(ItemType.HEAD, "place_component", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "18ee67d7a5b675b57d1b7c7ae79280d2c19828e582f07339cfb3ed9b90ff1236");
        }
    }),
    MAIN_SHORTCUTBAR_AREA_CONFIG(ItemType.HEAD, "area_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d8dae062b58056e5d6b7c3b121f6c788957f20adefee622d8cf4012dcc83865d");
        }
    }),
    MAIN_SHORTCUTBAR_STATS_MENU(ItemType.HEAD, "stats_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "532a5838fd9cd4c977f15071d6997ff5c7f956074a2da571a19ccefb03c57");
        }
    }),
    MAIN_SHORTCUTBAR_CREATE_AREA(ItemType.HEAD, "create_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8e88ead488ff68d35b61910c927a890ebc2c7605d76a1d13b5edb715e68a2cf3");
        }
    }),
    MAIN_SHORTCUTBAR_DELETE_AREA(ItemType.HEAD, "delete_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "4027ad3fefdcc69be5cea8db596f6cfc963d1af7f9007d62bbf569f522ee77d9");
        }
    }),
    MAIN_SHORTCUTBAR_ARMOR_MENU(ItemType.BLOCK, "armor_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "DIAMOND_CHESTPLATE");
        }
    }),
    MAIN_SHORTCUTBAR_CLOSE(ItemType.HEAD, "close_edition_mode", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    MIRROR_SUPPORT(ItemType.HEAD, "mirror_support", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52baeb4a35da8a85d14bdccf7184f5545088f954da55144f235c2983fdb8e05b");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MIRROR_SUPPORT);
        }
    }),
    PRISM(ItemType.HEAD, "prism", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "707ce43734cbefbb3da03f9ac1f01a3df456cee231550bd2dd42585594f9");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.PRISM);
        }
    }),
    REFLECTING_SPHERE(ItemType.HEAD, "reflecting_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d8c53bce8ae58dc692493481909b70e11ab7e9422d9d87635123d076c7133e");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.REFLECTING_SPHERE);
        }
    }),
    FILTERING_SPHERE(ItemType.HEAD, "filtering_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3e1e5c81fb9d64b74996fd171489deadbb8cb772d52cf8b566e3bc102301044");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.FILTERING_SPHERE);
        }
    }),
    ATTRACTION_SPHERE(ItemType.HEAD, "attraction_repulsion_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "89375d805291bac7d045c5c0448ab7f5599fa6d9a8caeae4bd251d586e2c8");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.ATTRACTION_REPULSION_SPHERE);
        }
    }),
    WHITE_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    //COMPONENT Detection items
    REDSTONE_SENSOR_ACTIVATED(ItemType.HEAD, "redstone_sensor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfd12f99a72bbbd19b37e1f328d9e09718066159374b6a1414b8698ce37032db");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.REDSTONE_SENSOR);
        }
    }),
    REDSTONE_WINNER_BLOCK(ItemType.BLOCK, "winner_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "REDSTONE_BLOCK");
            put(ItemAttribute.ENCHANT, true);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.REDSTONE_WINNER_BLOCK);
        }
    }),
    DISAPPEARING_WINNER_BLOCK(ItemType.BLOCK, "disappearing_winner_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "OBSIDIAN");
            put(ItemAttribute.ENCHANT, true);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.DISAPPEARING_WINNER_BLOCK);
        }
    }),
    APPEARING_WINNER_BLOCK(ItemType.BLOCK, "appearing_winner_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "BARRIER");
            put(ItemAttribute.ENCHANT, true);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.APPEARING_WINNER_BLOCK);
        }
    }),
    MUSIC_BLOCK(ItemType.HEAD, "music_block", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52c3b21ad36723d0aa3b75a239d427b34e1ac24c57c6a515c703ae47ce48485");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MUSIC_BLOCK);
        }
    }),
    ELEVATOR(ItemType.HEAD, "elevator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2f6557ef29e13481ef0e977720657adb241505601414a32c4d276a3a017e750");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.ELEVATOR);
        }
    }),
    ELEVATOR_SECOND_LOCATION(ItemType.HEAD, "elevator_second_location", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2f6557ef29e13481ef0e977720657adb241505601414a32c4d276a3a017e750");
        }
    }),
    CALL_BUTTON(ItemType.HEAD, "elevator_call_button", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9c928553e6780294c307cc4a16d32cc07b57ec57c7ffe3d2358a530f13441ce9");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.CALL_BUTTON);
        }
    }),
    //Mirrors
    MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.CYAN);
        }
    }),
    BLACK_MIRROR(ItemType.BLOCK, "mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    MIRROR_CHEST(ItemType.BLOCK, "mirror_chest", false, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "TRAPPED_CHEST");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MIRROR_CHEST);
        }
    }),
    //MISC Reflecting sphere
    REFLECTING_SPHERE_WHITE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ce236e3de8c164d2dff1c4a4ee38ddd6f7f0c0b0472e6abe14213429896a34e7");
        }
    }),
    REFLECTING_SPHERE_RED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "884e92487c6749995b79737b8a9eb4c43954797a6dd6cd9b4efce17cf475846");
        }
    }),
    REFLECTING_SPHERE_GREEN(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "83a2768de2af52b27c7bc1416dd535197b65158422673da9d3b0e61d49f33662");
        }
    }),
    REFLECTING_SPHERE_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8c8a824b9b94939af2e8ce188482618dad933ae5dd671a7b9ac999d3a3b61a5");
        }
    }),
    REFLECTING_SPHERE_YELLOW(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "211ab3a1132c9d1ef835ea81d972ed9b5cd8ddff0a07c55a749bcfcf8df5");
        }
    }),
    REFLECTING_SPHERE_MAGENTA(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "70cd83be2b45225497325d958543465fd56bd65cd94a97e8fdfb1c1ec93b08f");
        }
    }),
    REFLECTING_SPHERE_LIGHT_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "126e346287a21dbfca5b58c142d8d5712bdc84f5b75d4314ed2a83b222effa");
        }
    }),
    //MISC Filtering sphere
    FILTERING_SPHERE_WHITE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "caf039bec1fc1fb75196092b26e631f37a87dff143fc18297798d47c5eaaf");
        }
    }),
    FILTERING_SPHERE_RED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "533a5bfc8a2a3a152d646a5bea694a425ab79db694b214f156c37c7183aa");
        }
    }),
    FILTERING_SPHERE_GREEN(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c607cffcd7864ff27c78b29a2c5955131a677bef6371f88988d3dcc37ef8873");
        }
    }),
    FILTERING_SPHERE_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d1137b9bf435c4b6b88faeaf2e41d8fd04e1d9663d6f63ed3c68cc16fc724");
        }
    }),
    FILTERING_SPHERE_YELLOW(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "41139b3ef2e4c44a4c983f114cbe948d8ab5d4f879a5c665bb820e7386ac2f");
        }
    }),
    FILTERING_SPHERE_MAGENTA(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3dd0115e7d84b11b67a4c6176521a0e79d8ba7628587ae38159e828852bfd");
        }
    }),
    FILTERING_SPHERE_LIGHT_BLUE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cb9b2a4d59781d1bec2d8278f23985e749c881b72d7876c979e71fda5bd3c");
        }
    }),
    //MISC Repulsion sphere
    REPULSION_SPHERE(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5bfa6eab81e21bdbfbb987ed695a4596ed33e77510908dadf696912f9423f75");
        }
    }),
    //MISC Music block
    MUSIC_BLOCK_LOOP_N_KEEP_PLAYING_ON_EXIT(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3be46fe5bd5c9a55836b3cecac430b99c77929f1d6ca81907d343d6ee297");
        }
    }),
    MUSIC_BLOCK_KEEP_PLAYING_ON_EXIT(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "607a6971d0cba23e409ec5283186e3b29982dc5913174c37afd916022ee6b");
        }
    }),
    MUSIC_BLOCK_LOOP(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dcc1de7b06fd5ebf4dae636abf04b1b1f28a58c176e6ce4f177a8a2c512c4d");
        }
    }),
    //MISC Mirror Support
    MIRROR_SUPPORT_BLOCKED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8eff7a46d3b15eebd7106a6b7f91c78718c7886b0abd3cd0505892499ca37daf");
        }
    }),
    //MISC Redstone sensor
    REDSTONE_SENSOR_DEACTIVATED(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d6df83c2c3a9ffca434f1a86a697618dcdda09427597c7638155858cc348a7");
        }
    }),
    //MISC KeyChest
    KEY_CHEST_1(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2"); //WHITE
        }
    }),
    KEY_CHEST_2(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a89b4c5e119a61773dd52d36b572bc0f9548d984ce0841417a301a65351a768d"); //RED
        }
    }),
    KEY_CHEST_3(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfcffe1445e7505446ba4c07575407122a835128a992a77646c032451b94b81b"); //YELLOW
        }
    }),
    KEY_CHEST_4(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "be4baf7377e13742f04407167e2130378c518cbbe7c3e0f98437bf5357dc3db8"); //BLUE
        }
    }),
    KEY_CHEST_5(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a16d8abc0114c6d6e4592a84296bd09bd73356e98889905b527137f479340b57"); //ORANGE
        }
    }),
    KEY_CHEST_6(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "19d8f04d6501c2fb82f83996580c67d605f31f2e8340da32fc1596837c124641"); //PURPLE
        }
    }),
    KEY_CHEST_7(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9a29bb965d1064c5624caae7f7afb4d7882a78458dd3d6998c7512e01118539d"); //GREEN
        }
    }),
    KEY_CHEST_8(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a0e6af393ad3a80158e6ed235480b0bfc06ff4dfa0ddd0f9d057c532614a4a9"); //BLACK
        }
    }),
    KEY_CHEST_9(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c2b4b1712bce87ce30c64ae7ba2cac985e1e5a174ad1e0915ee0b2e9eb03d07f"); //RED 2
        }
    }),
    KEY_CHEST_10(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2ca6883e09c94d2d0efe343fc089814947d5b16bcb90a9255d3d47c750e8639b"); //RED 3
        }
    }),
    KEY_CHEST_11(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "67b9315eec646c01639007498060f639f21897a4990a562476a8991b9f93b93b"); //RED 4
        }
    }),
    KEY_CHEST_12(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "afdc21340ed4887ed1cc59d9025a0bcbe62524d94a581f69932e8c9b6196a665"); //YELLOW 2
        }
    }),
    KEY_CHEST_13(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a20c561fc895425fe4534ef932c1f03340505872f69e24c8c548467b86647602"); //YELLOW 3
        }
    }),
    KEY_CHEST_14(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6fcb6c6d342d96b8fc6e50be2ee8f3bfffb57779e100fc0bf46e0b03b17d0681"); //BLUE 2
        }
    }),
    KEY_CHEST_15(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f4d3e99197a8080248c0abf0a75e616285d489ad2bb5bd1088c6828b9b20be58"); //BLUE 3
        }
    }),
    KEY_CHEST_16(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "561c46bf6e5d5ebc31654dfc4fef2f6bfb4981bef8c1008a7e7ab425b9c49813"); // BLUE 4
        }
    }),
    KEY_CHEST_17(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "65d44f08b95edf08982c68f0f6b056fb3b57a9cb02265bbb393f75fe50457fcb"); //BLUE 5
        }
    }),
    KEY_CHEST_18(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c9ca87a3319d7628442cd2e1a22594487e3cbeb05de31bd3bea89591d82b52c2"); //ORANGE 2
        }
    }),
    KEY_CHEST_19(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "39b8bee816fead9ef1ea70ba580327734f94c58b231d57737ca75a9dcdee10d9"); //PURPLE 2
        }
    }),
    KEY_CHEST_20(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c2a2459c8c2bed9e1d54b9d2d918a4e0d7384b3cd90bd657aa728c80ec4bfed0"); //PURPLE 3
        }
    }),
    KEY_CHEST_21(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e233a0e752fad0560570588de50b0d951ba0038af871ec4fe6b4a8ec01d5ed08"); //PURPLE 4
        }
    }),
    KEY_CHEST_22(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5eac9392ebaab58b88c7528cdb3a62678e4dcb4d0dcc24cf287fb8178feaed62"); //GREEN 2
        }
    }),
    KEY_CHEST_23(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5f1d806d1c4cd0b1ddd0bea39f48e8d7475069a6d79d8e06becc12a93a3f3512"); //GREEN 3
        }
    }),
    KEY_CHEST_24(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "982bada50efa8cbd8758c226c7712158c6fca03eaa87e19c5c231d4feda8a2c6"); //GREEN 4
        }
    }),
    KEY_CHEST_25(ItemType.HEAD, null, false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dda260fdc05722eebaab390aebc2f9392134440f3b1da5f30e5fe671238a8abb"); //GREY
        }
    }),
    //MISC Meltable clay
    RED_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.RED);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    GREEN_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    BLUE_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    YELLOW_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    MAGENTA_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    LIGHT_BLUE_MELTABLE_CLAY(ItemType.BLOCK, "meltable_clay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_CLAY");
            put(ItemAttribute.COLOR, LasersColor.CYAN);
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.MELTABLE_CLAY);
        }
    }),
    //Glasses
    WHITE_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.CYAN);
        }
    }),
    BLACK_GLASS(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    //Glass panes
    WHITE_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.CYAN);
        }
    }),
    BLACK_GLASS_PANE(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    //CONCRETE POWDER
    WHITE_CONCRETE_POWDER(ItemType.BLOCK, "mirror_block", true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    RED_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.CYAN);
        }
    }),
    BLACK_CONCRETE_POWDER(ItemType.BLOCK, null, true, true, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "CONCRETE_POWDER");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    /**
     * ************************ COMPONENT SHORTCUTBAR *************************
     */
    COMPONENT_SHORTCUTBAR_COLOR_LOOPER(ItemType.HEAD, "color_looper", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c227670d148794915304827b0eb03eff273ca153f874db5e9094d1cdbb6258a2");
        }
    }),
    COMPONENT_SHORTCUTBAR_ANY_COLOR_TOGGLE(ItemType.HEAD, "any_color_toggle", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1e33821edb80516405a8749d94c7ff43dce4359fb0b885263e0ae0786f56e17d");
        }
    }),
    COMPONENT_SHORTCUTBAR_RETRIEVE_MIRROR(ItemType.HEAD, "retrieve_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_SHORTCUTBAR_PLACE_MIRROR(ItemType.HEAD, "place_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE(ItemType.HEAD, "increase_sphere_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3213c1a420d11a06a74c3db98b21332b7493a8c5622d558003186882a1b99f56");
        }
    }),
    COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE(ItemType.HEAD, "decrease_sphere_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "10d4aeb26aa17345f9bdab1f199c27daf6efb1fd71546f23d35229f612f37328");
        }
    }),
    COMPONENT_SHORTCUTBAR_MOD_LOOPER(ItemType.HEAD, "mod_looper", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8ae52ae8c98ac19fd07637a469ffa256ab0b3b10ece6243186188ba38df154");
        }
    }),
    COMPONENT_SHORTCUTBAR_INCREASE_MIRROR_NB(ItemType.HEAD, "increase_mirror_nb", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_SHORTCUTBAR_DECREASE_MIRROR_NB(ItemType.HEAD, "decrease_mirror_nb", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_SHORTCUTBAR_PREV_SONG(ItemType.HEAD, "previous_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c4de72d37c2f5ba374bf36fd1d6b4789c9dcb3b8279ba037ff33684282859c89");
        }
    }),
    COMPONENT_SHORTCUTBAR_NEXT_SONG(ItemType.HEAD, "next_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "28f8139707f4fb341d52d2c3e077b2314617d754985784fc13fd8a978a3f74d2");
        }
    }),
    COMPONENT_SHORTCUTBAR_SONG_LOOP(ItemType.HEAD, "song_loop_toggle", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "aba40e063fae1ba19599a72426aa0d5ec347c70b8ebe4390dcf7da7fbc5ca5ec");
        }
    }),
    COMPONENT_SHORTCUTBAR_SONG_STOPS_ON_PLAYER_EXIT(ItemType.HEAD, "song_stop_on_exit_toggle", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5a4c4ad32980ebd1dd975b9c2ba57ade092f5ae6e29534ebbfd1c9de19e69ebb");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_LEFT(ItemType.HEAD, "rotation_left", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_RIGHT(ItemType.HEAD, "rotation_right", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_UP(ItemType.HEAD, "rotation_up", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ccbf9883dd359fdf2385c90a459d737765382ec4117b04895ac4dc4b60fc");
        }
    }),
    COMPONENT_SHORTCUTBAR_ROTATION_DOWN(ItemType.HEAD, "rotation_down", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "72431911f4178b4d2b413aa7f5c78ae4447fe9246943c31df31163c0e043e0d6");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MIN(ItemType.HEAD, "range_increase_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "47456b506ee3dcab215f33cb93a9edd9749f3e2ff8a4893426e1784692de8b5");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MIN(ItemType.HEAD, "range_decrease_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_INCREASE_MAX(ItemType.HEAD, "range_increase_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458");
        }
    }),
    COMPONENT_SHORTCUTBAR_RANGE_DECREASE_MAX(ItemType.HEAD, "range_decrease_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dbe6713f20a9c2de43803b90aefa4db9a6bf89915d7c1590f77a7e10df3a3e54");
        }
    }),
    COMPONENT_SHORTCUTBAR_ADD_KEYCHEST(ItemType.HEAD, "add_key_chest", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2");
        }
    }),
    COMPONENT_SHORTCUTBAR_TELEPORT_TO_KEYCHEST_MENU(ItemType.HEAD, "teleport_to_key_chests_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_SHORTCUTBAR_SELECT_KEY_NUMBER(ItemType.HEAD, "select_key_number_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    COMPONENT_SHORTCUTBAR_TELEPORT_TO_LOCK(ItemType.HEAD, "teleport_to_lock", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_SHORTCUTBAR_MENU(ItemType.HEAD, "menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "532a5838fd9cd4c977f15071d6997ff5c7f956074a2da571a19ccefb03c57");
        }
    }),
    COMPONENT_SHORTCUTBAR_CLOSE(ItemType.HEAD, "close_component_edition", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    COMPONENT_SHORTCUTBAR_CREATE_FLOOR(ItemType.HEAD, "create_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "7d4acfbd59bdfec5fb7a08adb735206e6db64c1ddb6441a3b5216e2a82d87563");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_GROUND_FLOOR(ItemType.HEAD, "go_to_ground_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "adb0a7a3fe5bc3ab66deedf9eb7ef632c403016f9a7e4473786aa8fe50c157ac");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_PREVIOUS_FLOOR(ItemType.HEAD, "go_to_previous_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b6d0fce17905926dad6d7ca9ddf2f03b4b943384f351011a5597ecb2a9dca230");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_NEXT_FLOOR(ItemType.HEAD, "go_to_next_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "57190addde4ccc5246980ad4869344c22361183492e0b1c7809cc30e3c0e1cc9");
        }
    }),
    COMPONENT_SHORTCUTBAR_GO_TO_TOP_FLOOR(ItemType.HEAD, "go_to_top_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1f7e77b19f9e30fd8519ec85eb7ef2b71c28ecbc3864b88c2f7228ca4dc907fe");
        }
    }),
    COMPONENT_SHORTCUTBAR_CREATE_CALL_BUTTONS(ItemType.HEAD, "create_call_buttons", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9c928553e6780294c307cc4a16d32cc07b57ec57c7ffe3d2358a530f13441ce9");
        }
    }),
    /**
     * ************************** COLOR MENU ***************************
     */
    WHITE(ItemType.HEAD, "white", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "366a5c98928fa5d4b5d5b8efb490155b4dda3956bcaa9371177814532cfc");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    BLACK(ItemType.HEAD, "black", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "967a2f218a6e6e38f2b545f6c17733f4ef9bbb288e75402949c052189ee");
            put(ItemAttribute.COLOR, LasersColor.BLACK);
        }
    }),
    RED(ItemType.HEAD, "red", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5fde3bfce2d8cb724de8556e5ec21b7f15f584684ab785214add164be7624b");
            put(ItemAttribute.COLOR, LasersColor.RED);
        }
    }),
    GREEN(ItemType.HEAD, "green", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "a7695f96dda626faaa010f4a5f28a53cd66f77de0cc280e7c5825ad65eedc72e");
            put(ItemAttribute.COLOR, LasersColor.GREEN);
        }
    }),
    BLUE(ItemType.HEAD, "blue", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b8afa1555e9f876481e3c4299ec6e91d22b4075e67e58ef80dcd190ace6519f");
            put(ItemAttribute.COLOR, LasersColor.BLUE);
        }
    }),
    YELLOW(ItemType.HEAD, "yellow", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c641682f43606c5c9ad26bc7ea8a30ee47547c9dfd3c6cda49e1c1a2816cf0ba");
            put(ItemAttribute.COLOR, LasersColor.YELLOW);
        }
    }),
    MAGENTA(ItemType.HEAD, "magenta", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "205c17650e5d747010e8b69a6f2363fd11eb93f81c6ce99bf03895cefb92baa");
            put(ItemAttribute.COLOR, LasersColor.MAGENTA);
        }
    }),
    LIGHT_BLUE(ItemType.HEAD, "light_blue", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "07c78f3ee783feecd2692eba54851da5c4323055ebd2f683cd3e8302fea7c");
            put(ItemAttribute.COLOR, LasersColor.CYAN);
        }
    }),
    /**
     * *********************** ARMOR CONFIGURATION MENU ***********************
     */
    ARMOR_CONFIG_REFLECTION_HEADER(ItemType.BLOCK, "armor_config_reflection_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "STAINED_GLASS_PANE");
            put(ItemAttribute.COLOR, LasersColor.WHITE);
        }
    }),
    ARMOR_CONFIG_REFLECTION_UNCHECKED(ItemType.HEAD, "amour_config_reflection_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_REFLECTION_CHECKED(ItemType.HEAD, "armor_config_reflection_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_FOCUS_HEADER(ItemType.HEAD, "armor_config_focus_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6973f5f3b602437ef784d4c6a1752e194beb1b1737b11a33e27488d54de6a"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_FOCUS_UNCHECKED(ItemType.HEAD, "armor_config_focus_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_FOCUS_CHECKED(ItemType.HEAD, "armor_config_focus_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_PRISM_HEADER(ItemType.HEAD, "armor_config_prism_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "707ce43734cbefbb3da03f9ac1f01a3df456cee231550bd2dd42585594f9"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_PRISM_UNCHECKED(ItemType.HEAD, "armor_config_prism_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_PRISM_CHECKED(ItemType.HEAD, "armor_config_prism_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_DAMAGE_HEADER(ItemType.BLOCK, "armor_config_no_damage_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "DIAMOND_HELMET");
        }
    }),
    ARMOR_CONFIG_NO_DAMAGE_UNCHECKED(ItemType.HEAD, "armor_config_no_damage_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_DAMAGE_CHECKED(ItemType.HEAD, "armor_config_no_damage_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_BURNING_HEADER(ItemType.HEAD, "armor_config_no_burning_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "70a44d51ecc79f694cfd60228c88428848ca618e36a659a416e9246d841aec8");
        }
    }),
    ARMOR_CONFIG_NO_BURNING_UNCHECKED(ItemType.HEAD, "armor_config_no_burning_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_BURNING_CHECKED(ItemType.HEAD, "armor_config_no_burning_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_KNOCKBACK_HEADER(ItemType.BLOCK, "armor_config_no_knockback_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "PISTON_BASE");
        }
    }),
    ARMOR_CONFIG_NO_KNOCKBACK_UNCHECKED(ItemType.HEAD, "armor_config_no_knockback_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_NO_KNOCKBACK_CHECKED(ItemType.HEAD, "armor_config_no_knockback_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_HEADER(ItemType.BLOCK, "armor_config_durability_header", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "ANVIL"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_UNCHECKED(ItemType.HEAD, "armor_config_durability_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_CHECKED(ItemType.HEAD, "armor_config_durability_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_DECREMENT(ItemType.HEAD, "armor_config_durability_decrement", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_DURABILITY_INCREMENT(ItemType.HEAD, "armor_config_durability_increment", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458"); // TODO: Need to change
        }
    }),
    ARMOR_CONFIG_VALIDATE(ItemType.HEAD, "armor_config_validate", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6"); // TODO: Need to change
        }
    }),
    /**
     * *********************** AREA CONFIGURATION MENU ************************
     */
    AREA_CONFIG_MIRROR_SUPPORT(ItemType.HEAD, "area_config_mirror_support", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52baeb4a35da8a85d14bdccf7184f5545088f954da55144f235c2983fdb8e05b");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_mirror_support_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_mirror_support_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_mirror_support_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_mirror_support_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_CONCENTRATOR(ItemType.HEAD, "area_config_concentrator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6973f5f3b602437ef784d4c6a1752e194beb1b1737b11a33e27488d54de6a");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_concentrator_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_concentrator_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_concentrator_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_concentrator_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_SENDER(ItemType.HEAD, "area_config_laser_sender", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "18ee67d7a5b675b57d1b7c7ae79280d2c19828e582f07339cfb3ed9b90ff1236");
        }
    }),
    AREA_CONFIG_LASER_SENDER_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_sender_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_SENDER_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_sender_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_SENDER_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_sender_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_SENDER_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_sender_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER(ItemType.HEAD, "area_config_laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_receiver_h_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_receiver_h_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_laser_receiver_v_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_laser_receiver_v_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_GRAVITY_SPHERE(ItemType.HEAD, "area_config_gravity_sphere", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "89375d805291bac7d045c5c0448ab7f5599fa6d9a8caeae4bd251d586e2c8");
        }
    }),
    AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_CHECKED(ItemType.HEAD, "area_config_gravity_sphere_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_UNCHECKED(ItemType.HEAD, "area_config_gravity_sphere_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_HORIZONTAL(ItemType.HEAD, "area_config_horizontal", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
        }
    }),
    AREA_CONFIG_VERTICAL(ItemType.HEAD, "area_config_vertical", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ccbf9883dd359fdf2385c90a459d737765382ec4117b04895ac4dc4b60fc");
        }
    }),
    AREA_CONFIG_VICTORY_AREA_ADD(ItemType.HEAD, "area_config_victory_area_add", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "271c13a347c57aaca5b81aab47cc9ce093f3eab77f23b3c8aad0d20cca1a9a18");
        }
    }),
    AREA_CONFIG_VICTORY_AREA_DELETE(ItemType.HEAD, "area_config_victory_area_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "77ff15546e9aa76e4d813998e73339830dfd444f28a3422cf95143abfbbab79");
        }
    }),
    AREA_CONFIG_CHECKPOINT_DEFINE(ItemType.HEAD, "area_config_checkpoint_set", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ffc0caab7753228b4719a731901ece69a99b55b8478a72543f268d43f67c676c");
        }
    }),
    AREA_CONFIG_CHECKPOINT_DELETE(ItemType.HEAD, "area_config_checkpoint_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb6d4db4847abafea73333f891e7619b23ae42c14fd93d853cda4112449e7343");
        }
    }),
    AREA_CONFIG_MOD_VICTORY_AREA(ItemType.HEAD, "area_config_mod_victory_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "271c13a347c57aaca5b81aab47cc9ce093f3eab77f23b3c8aad0d20cca1a9a18");
        }
    }),
    AREA_CONFIG_MOD_VICTORY_AREA_CHECKED(ItemType.HEAD, "area_config_mod_victory_area_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_VICTORY_AREA_UNCHECKED(ItemType.HEAD, "area_config_mod_victory_area_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_LASER_RECEIVERS(ItemType.HEAD, "area_config_mod_laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
        }
    }),
    AREA_CONFIG_MOD_LASER_RECEIVERS_CHECKED(ItemType.HEAD, "area_config_mod_laser_receiver_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_LASER_RECEIVERS_UNCHECKED(ItemType.HEAD, "area_config_mod_laser_receiver_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_CHECKED(ItemType.HEAD, "area_config_mod_redstone_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_UNCHECKED(ItemType.HEAD, "area_config_mod_redstone_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_PLAYERS(ItemType.HEAD, "area_config_mod_players", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "eb7af9e4411217c7de9c60acbd3c3fd6519783332a1b3bc56fbfce90721ef35");
        }
    }),
    AREA_CONFIG_MOD_PLAYERS_CHECKED(ItemType.HEAD, "area_config_mod_players_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_PLAYERS_UNCHECKED(ItemType.HEAD, "area_config_mod_players_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_SENSORS(ItemType.HEAD, "area_config_mod_redstone_sensors", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfd12f99a72bbbd19b37e1f328d9e09718066159374b6a1414b8698ce37032db");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_SENSORS_CHECKED(ItemType.HEAD, "area_config_mod_redstone_sensors_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_REDSTONE_SENSORS_UNCHECKED(ItemType.HEAD, "area_config_mod_redstone_sensors_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    AREA_CONFIG_MOD_LOCKS(ItemType.HEAD, "area_config_mod_locks", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    AREA_CONFIG_MOD_LOCKS_CHECKED(ItemType.HEAD, "area_config_mod_locks_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    AREA_CONFIG_MOD_LOCKS_UNCHECKED(ItemType.HEAD, "area_config_mod_locks_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    /**
     * ********************* VICTORY AREA SHORTCUT BAR *********************
     */
    VICTORY_AREA_SHORTCUTBAR_SELECT(ItemType.HEAD, "victory_area_shortcutbar_select", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "271c13a347c57aaca5b81aab47cc9ce093f3eab77f23b3c8aad0d20cca1a9a18");
        }
    }),
    VICTORY_AREA_SHORTCUTBAR_CLOSE(ItemType.HEAD, "victory_area_shortcutbar_close", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    /**
     * ************************** COMPONENT MENU ***************************
     */
    COMPONENT_MENU_COLOR(ItemType.HEAD, "component_menu_color", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c227670d148794915304827b0eb03eff273ca153f874db5e9094d1cdbb6258a2");
        }
    }),
    COMPONENT_MENU_RETRIEVE_MIRROR(ItemType.HEAD, "component_menu_retrieve_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_MENU_PLACE_MIRROR(ItemType.HEAD, "component_menu_place_mirror", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_MENU_MIRROR_FREE(ItemType.HEAD, "component_menu_mirror_support_free", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "52baeb4a35da8a85d14bdccf7184f5545088f954da55144f235c2983fdb8e05b");
        }
    }),
    COMPONENT_MENU_MIRROR_FREE_CHECKED(ItemType.HEAD, "component_menu_mirror_support_free_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MIRROR_FREE_UNCHECKED(ItemType.HEAD, "component_menu_mirror_support_free_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MIRROR_BLOCKED(ItemType.HEAD, "component_menu_mirror_support_blocked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8eff7a46d3b15eebd7106a6b7f91c78718c7886b0abd3cd0505892499ca37daf");
        }
    }),
    COMPONENT_MENU_MIRROR_BLOCKED_CHECKED(ItemType.HEAD, "component_menu_mirror_support_blocked_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MIRROR_BLOCKED_UNCHECKED(ItemType.HEAD, "component_menu_mirror_support_blocked_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_INCREASE_MIRROR_CHEST(ItemType.HEAD, "component_menu_mirror_chest_increase", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "187baa4767234c01c04b8bbeb518a053dce739f4a04358a424302fb4a0172f8");
        }
    }),
    COMPONENT_MENU_DECREASE_MIRROR_CHEST(ItemType.HEAD, "component_menu_mirror_chest_decrease", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ed9d5c2b4807058d987c6e1d6300a1cc4b9eee7b16f1f0acac14ffcd1a9699f");
        }
    }),
    COMPONENT_MENU_DECREASE_SPHERE_SIZE(ItemType.HEAD, "component_menu_decrease_attraction_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "10d4aeb26aa17345f9bdab1f199c27daf6efb1fd71546f23d35229f612f37328");
        }
    }),
    COMPONENT_MENU_INCREASE_SPHERE_SIZE(ItemType.HEAD, "component_menu_increase_attraction_size", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "3213c1a420d11a06a74c3db98b21332b7493a8c5622d558003186882a1b99f56");
        }
    }),
    COMPONENT_MENU_MOD_ATTRACTION(ItemType.HEAD, "component_menu_mod_attraction", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "89375d805291bac7d045c5c0448ab7f5599fa6d9a8caeae4bd251d586e2c8");
        }
    }),
    COMPONENT_MENU_MOD_ATTRACTION_CHECKED(ItemType.HEAD, "component_menu_mod_attraction_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_ATTRACTION_UNCHECKED(ItemType.HEAD, "component_menu_mod_attraction_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_REPULSION(ItemType.HEAD, "component_menu_mod_repulsion", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5bfa6eab81e21bdbfbb987ed695a4596ed33e77510908dadf696912f9423f75");
        }
    }),
    COMPONENT_MENU_MOD_REPULSION_CHECKED(ItemType.HEAD, "component_menu_mod_repulsion_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_REPULSION_UNCHECKED(ItemType.HEAD, "component_menu_mod_repulsion_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_PREV_SONG(ItemType.HEAD, "component_menu_prev_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c4de72d37c2f5ba374bf36fd1d6b4789c9dcb3b8279ba037ff33684282859c89");
        }
    }),
    COMPONENT_MENU_NEXT_SONG(ItemType.HEAD, "component_menu_next_song", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "28f8139707f4fb341d52d2c3e077b2314617d754985784fc13fd8a978a3f74d2");
        }
    }),
    COMPONENT_MENU_SONG_STOP_ON_EXIT(ItemType.HEAD, "component_menu_song_stop_on_exit", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5a4c4ad32980ebd1dd975b9c2ba57ade092f5ae6e29534ebbfd1c9de19e69ebb");
        }
    }),
    COMPONENT_MENU_SONG_LOOP(ItemType.HEAD, "component_menu_song_loop", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "aba40e063fae1ba19599a72426aa0d5ec347c70b8ebe4390dcf7da7fbc5ca5ec");
        }
    }),
    COMPONENT_MENU_SONG_LOOP_CHECKED(ItemType.HEAD, "component_menu_song_loop_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_SONG_LOOP_UNCHECKED(ItemType.HEAD, "component_menu_song_loop_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_SONG_STOP_ON_EXIT_CHECKED(ItemType.HEAD, "component_menu_song_stop_on_exit_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_SONG_STOP_ON_EXIT_UNCHECKED(ItemType.HEAD, "component_menu_song_stop_on_exit_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_RANGE_INCREASE_MIN(ItemType.HEAD, "component_menu_range_increase_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "47456b506ee3dcab215f33cb93a9edd9749f3e2ff8a4893426e1784692de8b5");
        }
    }),
    COMPONENT_MENU_RANGE_INCREASE_MAX(ItemType.HEAD, "component_menu_range_increase_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458");
        }
    }),
    COMPONENT_MENU_RANGE_DECREASE_MIN(ItemType.HEAD, "component_menu_range_decrease_min", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186");
        }
    }),
    COMPONENT_MENU_RANGE_DECREASE_MAX(ItemType.HEAD, "component_menu_range_decrease_max", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dbe6713f20a9c2de43803b90aefa4db9a6bf89915d7c1590f77a7e10df3a3e54");
        }
    }),
    COMPONENT_MENU_UP(ItemType.HEAD, "component_menu_up", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6ccbf9883dd359fdf2385c90a459d737765382ec4117b04895ac4dc4b60fc");
        }
    }),
    COMPONENT_MENU_DOWN(ItemType.HEAD, "component_menu_down", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "72431911f4178b4d2b413aa7f5c78ae4447fe9246943c31df31163c0e043e0d6");
        }
    }),
    COMPONENT_MENU_LEFT(ItemType.HEAD, "component_menu_left", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");
        }
    }),
    COMPONENT_MENU_RIGHT(ItemType.HEAD, "component_menu_right", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
        }
    }),
    COMPONENT_MENU_MOD_PERMANENTLY_ENABLED(ItemType.HEAD, "component_menu_mod_permanently_enabled", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e1284368eb8649e980bed6c66565dea312d262cf5a271935de24dc361337d9de");
        }
    }),
    COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_CHECKED(ItemType.HEAD, "component_menu_mod_permanently_enabled_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_PERMANENTLY_ENABLED_UNCHECKED(ItemType.HEAD, "component_menu_mod_permanently_enabled_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_LASER_RECEIVERS(ItemType.HEAD, "component_menu_mod_laser_receiver", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "76da199506013166eed92be1aabf2b12b6a2be525f7e6b8f4bf0650404774564");
        }
    }),
    COMPONENT_MENU_MOD_LASER_RECEIVERS_CHECKED(ItemType.HEAD, "component_menu_mod_laser_receiver_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_LASER_RECEIVERS_UNCHECKED(ItemType.HEAD, "component_menu_mod_laser_receiver_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE(ItemType.BLOCK, "component_menu_mod_redstone", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.MATERIAL, "REDSTONE");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_CHECKED(ItemType.HEAD, "component_menu_mod_redstone_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_UNCHECKED(ItemType.HEAD, "component_menu_mod_redstone_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_PLAYERS(ItemType.HEAD, "component_menu_mod_players", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "eb7af9e4411217c7de9c60acbd3c3fd6519783332a1b3bc56fbfce90721ef35");
        }
    }),
    COMPONENT_MENU_MOD_PLAYERS_CHECKED(ItemType.HEAD, "component_menu_mod_players_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_PLAYERS_UNCHECKED(ItemType.HEAD, "component_menu_mod_players_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_SENSORS(ItemType.HEAD, "component_menu_mod_redstone_sensors", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dfd12f99a72bbbd19b37e1f328d9e09718066159374b6a1414b8698ce37032db");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_SENSORS_CHECKED(ItemType.HEAD, "component_menu_mod_redstone_sensors_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_REDSTONE_SENSORS_UNCHECKED(ItemType.HEAD, "component_menu_mod_redstone_sensors_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_MOD_LOCKS(ItemType.HEAD, "component_menu_mod_locks", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    COMPONENT_MENU_MOD_LOCKS_CHECKED(ItemType.HEAD, "component_menu_mod_locks_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_MOD_LOCKS_UNCHECKED(ItemType.HEAD, "component_menu_mod_locks_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    COMPONENT_MENU_ADD_KEY_CHEST(ItemType.HEAD, "component_menu_add_key_chest", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_KEY_CHESTS(ItemType.HEAD, "component_menu_delete_all_key_chests", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "36b550aa9a72e911c5d53fa5501c30411897628ff642cc131da12a13cb3f0bfc");
        }
    }),
    COMPONENT_MENU_DELETE_MY_KEYS(ItemType.HEAD, "component_menu_delete_my_keys", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f4f8c46906b56c0d9306673f91bda911b9117924b0c4765346afd1dbd5f3bcc4");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_PLAYERS_KEYS(ItemType.HEAD, "component_menu_delete_all_players_keys", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "11778a7e91052b6a3bd1c50545216c65b8e434dc96c9ae94009e346ac7618305");
        }
    }),
    COMPONENT_MENU_TELEPORT_TO_KEY_CHESTS_MENU(ItemType.HEAD, "component_menu_teleport_to_keys_chests_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_MENU_TELEPORT_TO_LOCK(ItemType.HEAD, "component_menu_teleport_to_lock", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_MENU_SELECT_KEY_NUMBER(ItemType.HEAD, "component_menu_select_key_number_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96b2265d4a091d6e8d9433409e039be7f5191ec2be00b6c015a64166eb8a7403");
        }
    }),
    COMPONENT_MENU_GO_TO_GROUND_FLOOR(ItemType.HEAD, "component_menu_go_to_ground_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "adb0a7a3fe5bc3ab66deedf9eb7ef632c403016f9a7e4473786aa8fe50c157ac");
        }
    }),
    COMPONENT_MENU_GO_TO_PREVIOUS_FLOOR(ItemType.HEAD, "component_menu_go_to_previous_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b6d0fce17905926dad6d7ca9ddf2f03b4b943384f351011a5597ecb2a9dca230");
        }
    }),
    COMPONENT_MENU_GO_TO_NEXT_FLOOR(ItemType.HEAD, "component_menu_go_to_next_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "57190addde4ccc5246980ad4869344c22361183492e0b1c7809cc30e3c0e1cc9");
        }
    }),
    COMPONENT_MENU_GO_TO_TOP_FLOOR(ItemType.HEAD, "component_menu_go_to_top_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1f7e77b19f9e30fd8519ec85eb7ef2b71c28ecbc3864b88c2f7228ca4dc907fe");
        }
    }),
    COMPONENT_MENU_AS_HIGH_AS_POSSIBLE(ItemType.HEAD, "component_menu_go_as_high_as_possible", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cf2ec4e127be4a74a1e255162893c73e3e5df2e7886a2ac4f13ef0ec84ec88ae");
        }
    }),
    COMPONENT_MENU_CREATE_FLOOR(ItemType.HEAD, "component_menu_create_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "7d4acfbd59bdfec5fb7a08adb735206e6db64c1ddb6441a3b5216e2a82d87563");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_FLOORS(ItemType.HEAD, "component_menu_delete_all_floor", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dc95661ca7e5126157603c4c456e68410706e4397e4df6ea70cdf782dd7c4686");
        }
    }),
    COMPONENT_MENU_DELETE_ALL_CALL_BUTTONS(ItemType.HEAD, "component_menu_delete_all_call_button", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8a29357802a9d66f8e7ebf4ce8c2ef6d06fd1f027ddc9bb77f1ffbda3536108b");
        }
    }),
    COMPONENT_MENU_TELEPORT_INSIDE_ELEVATOR(ItemType.HEAD, "component_menu_teleport_inside_elevator", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "b048cc0dee095be5839fdf2f7d382556c00c686c7eb4b3c690be4df02f212aad");
        }
    }),
    COMPONENT_MENU_SCHEDULED_ACTIONS_OPEN_MENU(ItemType.HEAD, "component_menu_scheduled_actions_open_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ba10da526e5111cfb6e3ebd47693e162dd52d41a2182028daa7c2b19aa3143");
        }
    }),
    COMPONENT_MENU_LIGHT_LEVEL_OPEN_MENU(ItemType.HEAD, "component_menu_light_level_open_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ed3d9fed0307399ab61510e9c04103330045d87453afde614af11adc19effdff");
        }
    }),
    COMPONENT_MENU_ANY_COLOR(ItemType.HEAD, "component_menu_any_color", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1e33821edb80516405a8749d94c7ff43dce4359fb0b885263e0ae0786f56e17d");
        }
    }),
    COMPONENT_MENU_ANY_COLOR_CHECKED(ItemType.HEAD, "component_menu_any_color_checked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "fb3cef69237b99295b9db4e77c86afedc50efcee562d220a9e7286c9f55282d");
        }
    }),
    COMPONENT_MENU_ANY_COLOR_UNCHECKED(ItemType.HEAD, "component_menu_any_color_unchecked", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "56bfaca231337ee16828fff4a5e2d128b0a165391532731608e3a058d67e1890");
        }
    }),
    // LIGHT LEVEL
    LIGHT_LEVEL_DECREMENT(ItemType.HEAD, "light_level_decrement", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f1fab0e6aea88748ca3b5512ed502a6d18e76d8afc4770d995233ac0dc5186");
        }
    }),
    LIGHT_LEVEL_INCREMENT(ItemType.HEAD, "light_level_increment", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "16227036b8afed6935d53143d16488d39cf4fb73a671f2b2955e80fc9dfe458");
        }
    }),
    // SCHEDULED ACTIONS
    SCHEDULED_ACTIONS_WAIT(ItemType.HEAD, "action_wait", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8477dafc8c9ea0799633381793866d146c9a39fad4c6684e7117d97e9b6c3");
        }
    }),
    SCHEDULED_ACTIONS_START_SAVING_ACTIONS(ItemType.HEAD, "action_main_menu_save_start", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ff364199440eada585ee475dfdf2e4f74feb3ccfeef7c48a991ab22020f7ff37");
        }
    }),
    SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE(ItemType.HEAD, "action_main_menu_save_end", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c1cadffb521f8041467167ad426f3b21fd3e4be57a2bbeade2ece1987f640ef6");
        }
    }),
    SCHEDULED_ACTIONS_OPEN_EDITION_MENU(ItemType.HEAD, "action_main_menu_open_edition_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "af0af4c16c5ed0d059e5fc27a84779a9cd41ae74c29974ac47a8cc68df12726");
        }
    }),
    SCHEDULED_ACTIONS_CLEAR(ItemType.HEAD, "action_main_menu_clear_actions", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ee37aac354f866d8b41b723518de1556cccc3ae39c4e8efdeef49af188ed9f21");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_NEW_LEFT(ItemType.HEAD, "action_edit_list_add_new_before", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "62e730e1e4f13fd58312fd00603efb126765e47858efc2a6b7dec3bb5dc28dac");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_NEW_RIGHT(ItemType.HEAD, "action_edit_list_add_new_after", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "62e730e1e4f13fd58312fd00603efb126765e47858efc2a6b7dec3bb5dc28dac");
        }
    }),
    BLACK_EMPTY(ItemType.HEAD, "-", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d44c165a14eb13df6b07c85f209c79e2008e0b67ad0b16093b000ed012ccf289");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_DELETE(ItemType.HEAD, "action_edit_list_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ee37aac354f866d8b41b723518de1556cccc3ae39c4e8efdeef49af188ed9f21");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_MOVE_LEFT(ItemType.HEAD, "action_edit_list_move_left", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cdc9e4dcfa4221a1fadc1b5b2b11d8beeb57879af1c42362142bae1edd5");
        }
    }),
    SCHEDULED_ACTIONS_ACTION_MOVE_RIGHT(ItemType.HEAD, "action_edit_list_move_right", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "956a3618459e43b287b22b7e235ec699594546c6fcd6dc84bfca4cf30ab9311");
        }
    }),
    SCHEDULED_ACTION_EDIT_MORE_DELAY(ItemType.HEAD, "action_edit_one_more_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "e02fa3b2dcb11c6639cc9b9146bea54fbc6646d855bdde1dc6435124a11215d");
        }
    }),
    SCHEDULED_ACTION_EDIT_VERY_MORE_DELAY(ItemType.HEAD, "action_edit_one_very_more_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "dd993b8c13588919b9f8b42db065d5adfe78af182815b4e6f0f91ba683deac9");
        }
    }),
    SCHEDULED_ACTION_EDIT_LESS_DELAY(ItemType.HEAD, "action_edit_one_less_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "74133f6ac3be2e2499a784efadcfffeb9ace025c3646ada67f3414e5ef3394");
        }
    }),
    SCHEDULED_ACTION_EDIT_VERY_LESS_DELAY(ItemType.HEAD, "action_edit_one_very_less_delay", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "f4bfef14e8420a256e457a4a7c88112e179485e523457e4385177bad");
        }
    }),
    SCHEDULED_ACTION_EDIT_DELETE(ItemType.HEAD, "action_edit_one_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ee37aac354f866d8b41b723518de1556cccc3ae39c4e8efdeef49af188ed9f21");
        }
    }),
    SCHEDULED_ACTION_EDIT_BACK(ItemType.HEAD, "action_edit_one_back", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    // ARMOR ACTION MENU
    ARMOR_ACTION_MENU_SELF_GIVE(ItemType.HEAD, "armor_action_menu_self_give", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "eaa1d09da8ad1002543533fb5d431f7abf3719c9040290af19acffb871017102");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.GIVE_TO_SELF);
        }
    }),
    ARMOR_ACTION_MENU_GIVE_NEAREST_PLAYER(ItemType.HEAD, "armor_action_menu_give_nearest_player", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "29392be75a43666163e7505ca0756111ec62273c71b20aea61bee789fee4272a");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.GIVE_TO_NEAREST);
        }
    }),
    ARMOR_ACTION_MENU_COMMAND_BLOCK_GIVE_NEAREST_PLAYER(ItemType.HEAD, "armor_action_menu_command_block_give_nearest_player", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1ec68da9014b1733fe27f49b2ad02c289ac7146b53283c43a486891944950f15");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.COMMAND_BLOCK_GIVE_TO_NEAREST);
        }
    }),
    ARMOR_ACTION_MENU_COMMAND_BLOCK_FILLS_CHEST(ItemType.HEAD, "armor_action_menu_command_block_fills_chest", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c7675c84eb4cb7d10bdb733796f8158a150b070154c8bc1d1b9ba909778017c3");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.COMMAND_BLOCK_FILLS_CHEST);
        }
    }),
    ARMOR_ACTION_MENU_COMMAND_BLOCK_PUT_ARMOR_ON_NEAREST_PLAYER(ItemType.HEAD, "armor_action_menu_command_block_armor_on_nearest_player", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "ed13b4879e2adfda42005a8eee2de0b4469752c3d499ecb97f5e800ce8f3081a");
            put(ItemAttribute.ARMOR_ACTION, ArmorAction.COMMAND_BLOCK_EQUIP_ARMOR_ON_NEAREST);
        }
    }),
    // NUMBERS
    COMPONENT_MENU_ZERO(ItemType.HEAD, "0", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "6d68343bd0b129de93cc8d3bba3b97a2faa7ade38d8a6e2b864cd868cfab");
        }
    }),
    COMPONENT_MENU_ONE(ItemType.HEAD, "1", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d2a6f0e84daefc8b21aa99415b16ed5fdaa6d8dc0c3cd591f49ca832b575");
        }
    }),
    COMPONENT_MENU_TWO(ItemType.HEAD, "2", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "96fab991d083993cb83e4bcf44a0b6cefac647d4189ee9cb823e9cc1571e38");
        }
    }),
    COMPONENT_MENU_THREE(ItemType.HEAD, "3", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "cd319b9343f17a35636bcbc26b819625a9333de3736111f2e932827c8e749");
        }
    }),
    COMPONENT_MENU_FOUR(ItemType.HEAD, "4", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "d198d56216156114265973c258f57fc79d246bb65e3c77bbe8312ee35db6");
        }
    }),
    COMPONENT_MENU_FIVE(ItemType.HEAD, "5", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "7fb91bb97749d6a6eed4449d23aea284dc4de6c3818eea5c7e149ddda6f7c9");
        }
    }),
    COMPONENT_MENU_SIX(ItemType.HEAD, "6", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9c613f80a554918c7ab2cd4a278752f151412a44a73d7a286d61d45be4eaae1");
        }
    }),
    COMPONENT_MENU_SEVEN(ItemType.HEAD, "7", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9e198fd831cb61f3927f21cf8a7463af5ea3c7e43bd3e8ec7d2948631cce879");
        }
    }),
    COMPONENT_MENU_EIGHT(ItemType.HEAD, "8", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "84ad12c2f21a1972f3d2f381ed05a6cc088489fcfdf68a713b387482fe91e2");
        }
    }),
    COMPONENT_MENU_NINE(ItemType.HEAD, "9", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9f7aa0d97983cd67dfb67b7d9d9c641bc9aa34d96632f372d26fee19f71f8b7");
        }
    }),
    /**
     * *********************** AREA DELETE CONFIRMATION **********************
     */
    //Area Delete Confirmation
    HELP_AREA_CONFIRM(ItemType.HEAD, "area_delete_confirm_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_DELETE_AREA(ItemType.HEAD, "area_delete_confirm_delete", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_DELETE_AREA(ItemType.HEAD, "area_delete_confirm_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ************************** AREA STATS MENU *****************************
     */
    AREA_STATS_TIME_LIST_MENU(ItemType.HEAD, "area_stats_time_list_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "2ee174f41e594e64ea3141c07daf7acf1fa045c230b2b0b0fb3da163db22f455");
        }
    }),
    AREA_STATS_ACTION_LIST_MENU(ItemType.HEAD, "area_stats_action_list_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "4ae29422db4047efdb9bac2cdae5a0719eb772fccc88a66d912320b343c341");
        }
    }),
    AREA_STATS_STEP_LIST_MENU(ItemType.HEAD, "area_stats_step_list_menu", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "24346ae5e1d13857498bac5e7aaee816c8359021614961d26d2d6ba619e8ca2");
        }
    }),
    AREA_STATS_LINK(ItemType.HEAD, "area_stats_link", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "5c52364520b3a9bb8ed515c01f80ab7b977025cd0b0ff6d86468a5164c6fb78");
        }
    }),
    AREA_STATS_UNLINK(ItemType.HEAD, "area_stats_unlink", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c99bc7dfd3cad70ebe12fc35db6fd3f1d652c6fea9929fa3b22fe6eef5c1");
        }
    }),
    AREA_STATS_CLEAR(ItemType.HEAD, "area_stats_clear", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "c931618dee7d4ecda91b1d9fd2741537c6ed21ea26f72aa3d941e39185391");
        }
    }),
    /**
     * ******************** AREA STATS CLEAR CONFIRMATION ********************
     */
    HELP_AREA_STATS_CLEAR_CONFIRM(ItemType.HEAD, "area_stats_clear_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_AREA_STATS_CLEAR(ItemType.HEAD, "area_stats_clear_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_AREA_STATS_CLEAR(ItemType.HEAD, "area_stats_clear_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * *********************** PAGINABLE INVENTORIES ***********************
     */
    PREVIOUS_COLUMN(ItemType.HEAD, "previous_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "30b8695acbc61d0fd78a8eba0aec351c5faa18a806fe7309f16aafe937da652d");
        }
    }),
    PREVIOUS_PAGE(ItemType.HEAD, "previous_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "9cdb8f43656c06c4e8683e2e6341b4479f157f48082fea4aff09b37ca3c6995b");
        }
    }),
    PAGINABLE_INDICATOR(ItemType.HEAD, "-", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "967a2f218a6e6e38f2b545f6c17733f4ef9bbb288e75402949c052189ee");
        }
    }),
    NEXT_PAGE(ItemType.HEAD, "next_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "61e1e730c77279c8e2e15d8b271a117e5e2ca93d25c8be3a00cc92a00cc0bb85");
        }
    }),
    NEXT_COLUMN(ItemType.HEAD, "next_page", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "84c8ba3eee68d618e783022a81d4804d2f13a5dce298de8932482b5f47b0705f");
        }
    }),
    /**
     * ******************* AREA STATS LINK CONFIRMATION ********************
     */
    HELP_AREA_STATS_LINK_CONFIRM(ItemType.HEAD, "area_stats_link_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_AREA_STATS_LINK(ItemType.HEAD, "area_stats_link_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_AREA_STATS_LINK(ItemType.HEAD, "area_stats_link_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ******************* AREA STATS UNLINK CONFIRMATION *******************
     */
    HELP_AREA_STATS_UNLINK_CONFIRM(ItemType.HEAD, "area_stats_unlink_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_AREA_STATS_UNLINK(ItemType.HEAD, "area_stats_unlink_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_AREA_STATS_UNLINK(ItemType.HEAD, "area_stats_unlink_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ********************* AREA STATS LINK SHORTCUTBAR *********************
     */
    AREA_STATS_LINK_SHORTCUTBAR_EXIT(ItemType.HEAD, "area_stats_shortcutbar_exit", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA(ItemType.HEAD, "area_stats_shortcutbar_select_area", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "8e88ead488ff68d35b61910c927a890ebc2c7605d76a1d13b5edb715e68a2cf3");
        }
    }),
    /**
     * ********************* PLACE KEY CHESTS SHORTCUTBAR *********************
     */
    PLACE_KEY_CHEST_SHORTCUTBAR_EXIT(ItemType.HEAD, "place_key_chest_shortcutbar_exit", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "1d3c33cf5b1c09418a3caa7f3969c11abe73de347cf45f8f285fdcc8d8ffe87");
        }
    }),
    PLACE_KEY_CHEST_SHORTCUTBAR_PLACE(ItemType.HEAD, "place_key_chest_shortcutbar_place", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "407940711108966031f5e86128528d362736cac1514593531008a3c6026192b2");
        }
    }),
    /**
     * ****************** DELETE LOCK'S PLAYERS KEYS CONFIRM ******************
     */
    HELP_LOCK_S_PLAYERS_KEYS_DELETE_CONFIRM(ItemType.HEAD, "lock_players_keys_delete_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS(ItemType.HEAD, "lock_players_keys_delete_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_DELETE_LOCK_S_PLAYERS_KEYS(ItemType.HEAD, "lock_players_keys_delete_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ******************* DELETE LOCK'S KEY CHESTS CONFIRM *******************
     */
    HELP_LOCK_S_KEY_CHEST_DELETE_CONFIRM(ItemType.HEAD, "lock_key_chests_delete_confirmation_help", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "abbe86f0040cb46de53c0cfe1ffecd4fa11f804e26f8c2ce6d864d0020027009");
        }
    }),
    CONFIRM_DELETE_LOCK_S_KEY_CHEST(ItemType.HEAD, "lock_key_chests_delete_confirmation_confirm", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "bf2c182be6ea2a37c40b5be4a21413feb061c53d03ba34ccaaed6d02465f83d6");
        }
    }),
    CANCEL_DELETE_LOCK_S_KEY_CHEST(ItemType.HEAD, "lock_key_chests_delete_confirmation_cancel", false, false, new HashMap<ItemAttribute, Object>() {
        {
            put(ItemAttribute.TEXTURE, "901225aeeb3f82b396cace4db296fbdbbdd54a81ce81c32dae19a758e6e0f442");
        }
    }),
    /**
     * ******************* CONCENTRATORS *******************
     */
    CONCENTRATOR(ItemType.HEAD, "concentrator", false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c94275a11938279f22a539a8a0c9fa8041b1c0ff485b5ae71a6fa002f1396d51");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.CONCENTRATOR);
        }
    }),
    CONCENTRATOR_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "fb7b6200fc4e57517999362dbe45561c6c6a3474e1ce37d2acce03459a3c01c9");
        }
    }),
    CONCENTRATOR_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ed174d104a855d5e793fe01ab189517bfd24dea9e4de5db67f8f3113edca79f8");
        }
    }),
    CONCENTRATOR_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "e11cfa45da48a9a44bb1e9095878a53638c572c0b94516f4417b7282c56588b8");
        }
    }),
    /**
     * ******************* LASER_SENDER *******************
     */
    LASER_SENDER_BLUE_ALWAYS_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7246da0c01caf993c657e7135fa2d631655e75d2787df21b7df6271679c4fb9f");
        }
    }),
    LASER_SENDER_BLUE_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f100ac64f40855c12c3e487f9d1c2bada8fb9a6bb0fa2c1e0ec8671ceaf55a20");
        }
    }),
    LASER_SENDER_BLUE_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "557d544785f7a79e2e924fcc7ea61d2a47cd42d4cf00ea0386a45be829676372");
        }
    }),
    LASER_SENDER_BLUE_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "221a57fba9d1aee0f6b045683830a938eaa9509eff7779d7f189b03f950061ef");
        }
    }),
    LASER_SENDER_BLUE_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "53c18e84e1f660fba2ef0b59304cbdfd270959d24d1e025339c603222a4168ce");
        }
    }),
    LASER_SENDER_BLUE_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "301384f36aeb887b1d88d93f901cabb346caef90c2e227c261b8d79d30397a73");
        }
    }),
    LASER_SENDER_BLUE_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "72f8b8510a7de41328b7accb1d940cce7cca275eb5e9c4de9d7d863cd39aac02");
        }
    }),
    LASER_SENDER_BLUE_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "40b37505d2c7325879b4e44090880d5f7e328e690a87664ee112432f07ef791f");
        }
    }),
    LASER_SENDER_BLUE_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f0f61d5111010e471b6b17c5362574bfc145c97e9acc724fb7914090484962dc");
        }
    }),
    LASER_SENDER_BLUE_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6f097a3df9b7d4ee6c4a0b0bbeffd115222957cc22ceec550a6606e7a3a8da49");
        }
    }),
    LASER_SENDER_BLUE_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "833f7cda5772ce068429ffa5de47368878cebad2230f9f6eb407c4e1c3d9bab6");
        }
    }),
    LASER_SENDER_BLUE_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2d4c0220db66c705fb54ac8276f3998859cd52cd9bc662f95ff3e608afdc0116");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5dce34410bba55a1152b52912d40bf84abd84ed3e60d26ecc9296f53c6b0e06b");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7ae5973b1710697f67bedcb9d4c7a7643402869695ed87c70ce9a9b329051253");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bd5117bc3efd00cb1a2c754e9dc56441769dcdc7b3daa76fb57349bd345a40ae");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b98f59f1b0a6c41bf1de91a4df393931f05acba5dffb11fac5553f7a00acbbdc");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "dc610053cdd01c39d42fcfe2e6e1d04be048f9edeea88c54809ed340f2a6eae9");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ddb9f869e28f5c2ff6c2bea3518a79ea20208cde948d5d58db3ecac0034b98e");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a0871d18ac7308895c427929bf12375b2ba8dad340d9551b271161846a00da1b");
        }
    }),
    LASER_SENDER_BLUE_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "e6a6aa55931b7031df930e1214fb2970a3fee466a988944b50cce459e3a43838");
        }
    }),
    LASER_SENDER_CYAN_ALWAYS_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6bd48111194e4193cdcb163acaafe1faecbfc0c50f5769bba526de458653f1ab");
        }
    }),
    LASER_SENDER_CYAN_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2a2d461b3ee5306f868439dabcd638b936227b238952cbd69f9cfd439f5ca99a");
        }
    }),
    LASER_SENDER_CYAN_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1f44c9cee8c3ceca9e724c7dbb7b1694f2ffbbfc15b1d7043a05eb57b6038be6");
        }
    }),
    LASER_SENDER_CYAN_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "570fd824024a0abda677eb4be166c96b9d84d1d218c5b893fb73ec18cf1ef9cb");
        }
    }),
    LASER_SENDER_CYAN_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a20d13d59fa7976db0247ddbc9b7e489928b2aaefcf175788fc0f999177b931b");
        }
    }),
    LASER_SENDER_CYAN_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f05a6b510ab237b400166605d348a2b2257dd6d65b349b72328033d35d3abd05");
        }
    }),
    LASER_SENDER_CYAN_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "e5854f4a31fb27de0c306be7247362f546595f17248164af6888d910c778c722");
        }
    }),
    LASER_SENDER_CYAN_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "981cf528480c845de5d4daae5ec270d4ac2541e019bacdc3cee0ec3be2cf5b4a");
        }
    }),
    LASER_SENDER_CYAN_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d126159904e4faef32b08654acc3200d6a68c8c09e86b4673e0375def886397c");
        }
    }),
    LASER_SENDER_CYAN_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a44f38cfc4be108ae578abdc1917bae6b24cabd3b06100ecb8e203497f6589c2");
        }
    }),
    LASER_SENDER_CYAN_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "871c2a39e314c8ea79dd2be993521083c6b893c78526f4715d98eada1b951781");
        }
    }),
    LASER_SENDER_CYAN_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "773d7b4f0ef63efe2981482ad3485314b5e5808e7f6ff1445301facee6078022");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "204e5be750bb9bb850fd0c615272f1121fcf86b02913ec79d0a9ab48200c3bee");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1f4d6d3bf6ffa6175313fb49b2d5f6f3fe3e14465ec1a7222e1aaacea46f535f");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c70e16e0bf84aad6b312f70032ba1a579192d6d0306e8a8cafe5d7fcf8f8f515");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "80d77dff6450107425bcbfd62e3a7b6e220099d0be1bf65b172367fc1d6473f0");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6f70c89cb46733fe0e940f247e1d8ba3a8750c44b9c1ab9088df8f74785bd19b");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9691a3485f1459ae3020ec8eb638081404fba6789a108c05a1139f725e55b3a9");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "870f706efd7659abd1ec826f811cab15bfcb35482eacc72159306596e2b58b9a");
        }
    }),
    LASER_SENDER_CYAN_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b83dabfa9e9149997926a6fe59e99c93503db326d934ae27ca85a6f215409904");
        }
    }),
    LASER_SENDER_GREEN_ALWAYS_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5f0490b1aa9d3bcbf3030c8eba9cc0439182b5396429ed5281dc1da485deff01");
        }
    }),
    LASER_SENDER_GREEN_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2f12899fe0e4ee4bd4806295512f5bfe717a3d0b9bd5e0a116934fc9fed3aa37");
        }
    }),
    LASER_SENDER_GREEN_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f52b3254690bfb1c00505939ec56714aa19f6e26fe964b6be87aa7b5f7b36d1f");
        }
    }),
    LASER_SENDER_GREEN_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "aed1d4c74c81548edba02fcec9e1d9e8fec9826780b49a66449bd88841d3422a");
        }
    }),
    LASER_SENDER_GREEN_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "378bb95ddf7c9a7f2253191c8f19dc7bb137dbe4b6352b581c540f1e07106509");
        }
    }),
    LASER_SENDER_GREEN_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "36555d09a95d1a0aa2e45c0fa0f9748e15feb284bc2913ba21519a70e48540af");
        }
    }),
    LASER_SENDER_GREEN_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2190d13aaacacd3f8d70c26850f457aea4141c35eb9440e154d8768880018316");
        }
    }),
    LASER_SENDER_GREEN_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "34516d49ac78176d50151097ee9bd07f803c9abd3cc465ad4a44f00d575333e0");
        }
    }),
    LASER_SENDER_GREEN_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "444956051ad335ab010f5618c0f653c335fa8f60513e0d4b99a954ac07fa741e");
        }
    }),
    LASER_SENDER_GREEN_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7d179625016ed1405b25d26d248b26faf7e220bcb32d8bff209a83f42f285845");
        }
    }),
    LASER_SENDER_GREEN_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "579229bc0b6aa815795bce59db4b59922d3328bbd56ad23dd60efe80c71a7cdd");
        }
    }),
    LASER_SENDER_GREEN_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "35fd19b7b2e52ea4066391449b8c5db6289499467f17204603e21df34957031f");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1a296736bce8406a34b950aa3b13dfff9e59d404094988f0b267af494b15069d");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "8c8d785bd50f5e14fa0ae4113a6c3eecac89f92b47f2e2495f1ddcf253c01bd9");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b8beb004f6502737c99f1323bf3769502fb266e37d4a703f5d15ca3b3d5819b6");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d21a93380052bfbded10922789ebe30ed62cac60cc29d42ad83e6dad2bfc74a7");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5c2567f357d22cba00861801527bdb0341ccda785e41e5af3180776e43c87c90");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9a7a2ba1c09b964d4a1b573f5bcd65a7d916f8792b23422f113a8dea95e908cd");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9faf943ea52563e3e1ce76c0a3f74031792e3ecdee48fcdc9b3498871ff3b251");
        }
    }),
    LASER_SENDER_GREEN_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bb4df06c7297024bc4ed22f53d4b3635876433b41e5167ef7f99171a91f792d9");
        }
    }),
    LASER_SENDER_MAGENTA_ALWAYS_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "95abfae402fc72a1ce0bbbd581717117ee6b4f90e6e52e1488d341a152cbb70b");
        }
    }),
    LASER_SENDER_MAGENTA_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1feeb8b3641bba473b2ac48d69fe43583c68431f923f7993dc3ca128324208c");
        }
    }),
    LASER_SENDER_MAGENTA_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a22cb972100bd94bc7cf734b720532f7a5a8afb85cbde603b25ca9ede92b8656");
        }
    }),
    LASER_SENDER_MAGENTA_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "98979541dd4f66d5da56c2f1e63c79b17ca380d61a44ab30b60eebf8dfdb0539");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "422fd4f4da0b3d8b70f8f96ee3a07495fe50490a431ba390d665d4781f026131");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "411b5a89bd62a1d6c458f5edc07c6332022dba8dacd3977f86ab99c32bf2028c");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7f7814d17830d36780d6e9f7ed967fa1b1873e9b08c7caa78be4b02e1252fbc9");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d6fd7b2535ea7598bf0856f714c253afb6f08b96c39c0fb8ddd1100afdef72d");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1e6b52b8a8b54906a796170e737341425774fdbb83519f1d13d8308570340af7");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f8178c5d869bde0edd14e3072f9b477dab44bda68fa96f5486734a713f79f7e0");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5093a1382573ec08b1aaa0024de7d1dd4247e86991e7f94dd4314c9a815c639e");
        }
    }),
    LASER_SENDER_MAGENTA_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "349fa7a59ada0ec373d75d990f090be930f7edd18672a06664398b11b17215d1");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "83aee19e0ce0e966131e4a1550a67f02ee558facf7e7402375fd1ceaec98ff79");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c4df891b235838ef5c8f9cf6cb993ed2e2150ea5bd6cad8de93e8069de115f72");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7925c563178029ccca7c91b2a51b020a7e06ab25d7f0c0861a5da1727e777814");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a26a6fea0d0d126ddc1da4a3df94ba69109d0476f880a8445f2b3bd4260a822a");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "e388184d3d35a74fbd817c614946f1ab3fced825e860c3227016eae683fae6b2");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bbfc618fa828ac20607e6b1aa3c6120e488f1ce5f915935cd7e6c9de73bf35f9");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1732b86076ed4bc7c51fc286a51b959ecc4212bd66cc1c3568522a1d39721d1c");
        }
    }),
    LASER_SENDER_MAGENTA_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "418c5cd41d338f5993d840a3c07e2509d82ac4be85fbcffff325bf246ec12572");
        }
    }),
    LASER_SENDER_RED_ALWAYS_ON(ItemType.HEAD, "laser_sender", false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d6b1f9ba141dd07701e50731efd98997a1c8f271d2c88e8ad9bcd437e08c2213");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.LASER_SENDER);
        }
    }),
    LASER_SENDER_RED_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f7428c9f3a4f595abf85a8806ec9d9e325b38ee8391eb6eddc6aa6d0fdc1598a");
        }
    }),
    LASER_SENDER_RED_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "3a0c6d5ca87b58d1a0f8b9abd22d818950f46746eb1525e838bea2a25037b54b");
        }
    }),
    LASER_SENDER_RED_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2f48cf2f318f811fc6c73500ec185e2e04f38e5e56b21e73307ea3b8910d2a7c");
        }
    }),
    LASER_SENDER_RED_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "214aaa0f998884d3f704c75f96677967ca08e7432f93c32c49815e3f35f7e96d");
        }
    }),
    LASER_SENDER_RED_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d472818caa9921467ea4c59d2c26f643cc62939bfe14cf2e4ff39d32efb26e52");
        }
    }),
    LASER_SENDER_RED_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "66c8a4c0727667ec2bd438da140f91b4bbe1a30cded2a44cf0b313cd9eef3f93");
        }
    }),
    LASER_SENDER_RED_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b9670a6fb5001fd39d16161047aed1c3ec17db6ad7f104255a2b5b997bb6a067");
        }
    }),
    LASER_SENDER_RED_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "64167d9845512b867cf91973939763912ba2b808b2c609419a9bd9c0d3a2e4dd");
        }
    }),
    LASER_SENDER_RED_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "458d3eb6fd829d27dd17d2cb732afdee2a46aa041fdc5d0e49fef25eb4c6078");
        }
    }),
    LASER_SENDER_RED_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "8fe56f28783c649ce44ea96d0bf1f84777aff13927e2dd90dd7de987b6820e1");
        }
    }),
    LASER_SENDER_RED_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a9737249e621d9b4759f5953b4dd3b066732ee0dc372732eac90d5eaf9db72a4");
        }
    }),
    LASER_SENDER_RED_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c1967c4eaf83a1a73dc2b9ee49263140d3bf1303c39634c225809c1aa98132da");
        }
    }),
    LASER_SENDER_RED_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1c6896ff16cf7d84fa2d28e36902486cd2325a2ca9884fd97bedff68bc0af7e0");
        }
    }),
    LASER_SENDER_RED_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b44e8b27b5b2de972fd02963983371d552fde56170fbb40601ffdb130db87763");
        }
    }),
    LASER_SENDER_RED_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9cc279626661a9a5fedaeee1e50364a4a3b1b72b1686f8ac7312ce0b49b3c6a3");
        }
    }),
    LASER_SENDER_RED_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "25d46e8b52e66cd5aab316544b20eb60c47a607b25f3c746ff09315e328254df");
        }
    }),
    LASER_SENDER_RED_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d8e7c48c75be89b67994355f4ff73cb17a70c2074b5ed364abd1a6fccaffe2f7");
        }
    }),
    LASER_SENDER_RED_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "4f8623b13b3b30e15726189cb6079d539f4fac0c02fb03db360c840e7a813ea8");
        }
    }),
    LASER_SENDER_RED_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6ee71e5f3bed8b9bc98e64ba994d4d33264a631db87b2f31a561118ba40fb6b2");
        }
    }),
    LASER_SENDER_WHITE_ALWAYS_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a29fd4c49ac3b0316beaf2a350b8892799b5cb4e0ef1ae44b3f038dcdd14271d");
        }
    }),
    LASER_SENDER_WHITE_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "8ef36eb76cbc4eda5a1ffa88b8518245daea897cbab671d68bffc0ca38260cc8");
        }
    }),
    LASER_SENDER_WHITE_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "68e2c1f546dc36e2d7f97288c261c34fae13cad004e5ee4fac277ad39f868414");
        }
    }),
    LASER_SENDER_WHITE_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6e15e8d837cb042dfd372e638d99849e97c688dfe2dcfebd3704df4f5e3b931");
        }
    }),
    LASER_SENDER_WHITE_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "dc819494d1296ab476f3cb47f08e6f5fb109d3d5b571a45842ebc941b4cad64");
        }
    }),
    LASER_SENDER_WHITE_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "fe462fbdb2d3c195616c2257a006a847c7c8169e20e5de0701451dd205420994");
        }
    }),
    LASER_SENDER_WHITE_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "dde7ead905132534ee439d7ca2d36a62d6e23ea6d2851827bc60d450b9ae655f");
        }
    }),
    LASER_SENDER_WHITE_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "e034dbec29d4cf759077453a2083914eb2be03b700212a03a36a8e537eeaa845");
        }
    }),
    LASER_SENDER_WHITE_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d7c1431a143d2700870f088903a358da22222791a79caf2dc0525362bc91d139");
        }
    }),
    LASER_SENDER_WHITE_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7ead2068b9edd0c2a078312b166d804458355310e03a9b260609d7ab2fd3329");
        }
    }),
    LASER_SENDER_WHITE_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9c2bd401d17c0ad135de6299eb267114b9837b4880aafff92a33a7839a51840a");
        }
    }),
    LASER_SENDER_WHITE_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bf00190335f10b5e501892ef33058dd8046451fe1b124aac44429704ff91eecf");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2ddc660395fd5a2769762abed6178772d494a0b76bf2448bd124b7edce5c959");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c05d6e3f0b3cc88baf5a8d0b853b03ad308d88486d009e1076813613c29f42ae");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "3647ac9cd682f8636ee42cadc8592ac44f8ef1d0dcb000cbf12d13666c095c15");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "4e742547da1ace358c4a208ab31ae83d8c98501c37dbc98f864a43131a4b3361");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "18a584f73945c5179d9f48e93ced7550036e3c41cd058105fdcd15f65da6b68f");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1b9c7570615b9b3b82260b2b95f79c51fc86bd5a1222a1f8d7e4766bd6eccc9");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "61c026d1562bf34b14ed41cc1098bb4909f0c59c47fb3ccb25b3c62e84989204");
        }
    }),
    LASER_SENDER_WHITE_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f8bf81ac6b3a93b298fea3f0848737b4693daee65e924d49d60918bb776f2dea");
        }
    }),
    LASER_SENDER_YELLOW_ALWAYS_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "893ec5bbd13b5535973f6d363dfa436ece13a49ac0e1d48a3743c1041719ba40");
        }
    }),
    LASER_SENDER_YELLOW_ALWAYS_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "61a995abdff98d9f121661ab123672a0aeafb4b840a2e4dec64a34ffa7d4131c");
        }
    }),
    LASER_SENDER_YELLOW_ALWAYS_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "aee5060852b6c5e4c4f75bba2141a5a0db30da2dca122d6a11c650fc06c7ea9");
        }
    }),
    LASER_SENDER_YELLOW_ALWAYS_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c4db5a8a1ec5a6f92d24876b62a89f6b6794f3fcfb4e4bb3fc547c443400f47f");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "989d06d1fd91d9ed332cd0fd47a0923aa6f4e9511ba451112a32359295c6fadb");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "32b5ad83991eec42bc3f4bc142de3702a1c61c19cff412fdd80f7e2202f0bc17");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f288d55c95235912e911f8deb451f68d1cd61debf75c2581cd7553923577d98c");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bbcbd550f596b4cc5dd823ac45aa464f23b75c5da41bfe5c5341f8f1a8e7860e");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ced0ef0fd56e29c51c7da7f60b3693fc78be399a6f7d050de112eeddaaca6aad");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b7415b3cce3656c33e267399fa5cbf52a0f1dd2d1859711ccba82ca9a1ba3fbe");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bceb286423ce328e5a7f06cef90752da62cfcdbdd2a4ea22feae9eeb92d0dea3");
        }
    }),
    LASER_SENDER_YELLOW_RANGE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2b9dd0ffa574f371f8a1516415d3910abce8abf424aa3c4ee77faffdf476a46b");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ad51dd5663e45df131c7b1dc4af5913220346efa80a4fe57332d03754066326c");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f911a66840433e08750fee733eba24abe609797a0507e09fe718ee072a471875");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "54265ac61371fbb1d80e11de0581cba66e524cc9b85a2028c58e30c0f492cb12");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2dc52dfca0f2b8874d8167a6344e17a7f3ec7dba29f5c8723b100c0e57492505");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6fb1ea5628bea8b58b97434c8359c2ae80141bc34e37e0803db902fe384b15ef");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f87a915a2a9599ff3f746e0d78a039b9fdd800f50faf9e605c23330bfd50993c");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "34f0afb6c0a02f54ff7e190848df6c1bd60751b14cedade18456a55060178fa9");
        }
    }),
    LASER_SENDER_YELLOW_REDSTONE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "73ad8a748f6d405b23f2025529bf719d877fa34a18805f83fd88166186d030cf");
        }
    }),
    /**
     * ******************* LASERS_RECEIVER *******************
     */
    LASERS_RECEIVER_BLUE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5e0b7c6907ade3829dc86d92b6a51a682b7ee34098c32253dc5d1b7c8c022a2");
        }
    }),
    LASERS_RECEIVER_BLUE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "4da05c15a644388bea56bb9787ca80901f0b9bff47d932c987f9a460d91418");
        }
    }),
    LASERS_RECEIVER_BLUE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5a5c1f34b56557ad9db7104ce3a90e4646515afeef75bff5d19bd7bab278668");
        }
    }),
    LASERS_RECEIVER_BLUE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "868f1e087f41510ebf9aef1e92ded8fdff3bcc48d7a11c1cf9938caa5a698583");
        }
    }),
    LASERS_RECEIVER_BLUE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2b854fce9fb0a452b9ee0ddd0089bafa66a38c41db2dbb3f6b3e597d611501d8");
        }
    }),
    LASERS_RECEIVER_BLUE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "8b67388af52ae417ba4fe849f697bd7e6a5b5ae16e5a38ddcbd1b3f65bb1b3a8");
        }
    }),
    LASERS_RECEIVER_BLUE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a66883d66dd7dbb81cf47eb17fc243204a0232461cd9101c241bd43a29a0bf3f");
        }
    }),
    LASERS_RECEIVER_BLUE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1ce7f64ddfbe68795d9d8d2be685b24486e8afa98f9ed1223c8a03bb6afda89e");
        }
    }),
    LASERS_RECEIVER_CYAN_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "49a2f68a525f67a02acd9d94d4860c25a0623cc000139467395ea1ce5520acfa");
        }
    }),
    LASERS_RECEIVER_CYAN_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1d1b68c6840b822fe891669d1f19be7a30adf25d241e2496a86e49b60d0f7149");
        }
    }),
    LASERS_RECEIVER_CYAN_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "46798e56d9b4031128ecc41fd1cf496247d5c3c3a361d31ade3921eaa0981335");
        }
    }),
    LASERS_RECEIVER_CYAN_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b926fdab238953766852c0862ae4bcec4bf23f8eca35dc1441e9df10b1395da2");
        }
    }),
    LASERS_RECEIVER_CYAN_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ec46adc73d8de57851e8b29cfffa9e401b570849c80ee5c3cb2c6320450a8765");
        }
    }),
    LASERS_RECEIVER_CYAN_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5ff7310435e1c5ffa043e56101c4b700e41ad46ce2be61fc840be0aa17462368");
        }
    }),
    LASERS_RECEIVER_CYAN_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "e3e478ac0972f86fba38fff447d9057c5f51c9f57d65341f4abcba0601bd5c26");
        }
    }),
    LASERS_RECEIVER_CYAN_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "89c3c49c5faed241bbce964d96611f4eae0c076bb42cf4403b72f38db4f2a941");
        }
    }),
    LASERS_RECEIVER_GREEN_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d6074f50c3ee9275ac13792c9b56f58ec25aaf07338e4799476d8f6826016db5");
        }
    }),
    LASERS_RECEIVER_GREEN_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bd029dcd1e74971fbef76092331c3abdd3b410bec119ff10065858a5a5647a54");
        }
    }),
    LASERS_RECEIVER_GREEN_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6d2f61272256ac8ae135a80847d20e8cb4d1dc72cbb3ee80dd2d0141e60377db");
        }
    }),
    LASERS_RECEIVER_GREEN_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "98ee46f0a9733aca5de8ea78cecb6e4182804256ea634f58273a94c5c918fb2e");
        }
    }),
    LASERS_RECEIVER_GREEN_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "dc07c8c9f50bd828e7404e3669d9451a7cc8a394de0498023ba601335d0b41f9");
        }
    }),
    LASERS_RECEIVER_GREEN_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b8e0055a56015c747d8fb4973f8b32a9a3e0020f849665f6b056953798bf55a9");
        }
    }),
    LASERS_RECEIVER_GREEN_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "f04eaa2db065260ea45b26b9ca407e19bef06d322c3bfdf82ae295b02c1f4a3");
        }
    }),
    LASERS_RECEIVER_GREEN_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c2cbca342191d62fc34b5f6e5afd1fd5761260e79681f4a034e30e480a95c1f3");
        }
    }),
    LASERS_RECEIVER_MAGENTA_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bf2a8a73ef0e12954deeafe5c0597e2e83a918b7144aa2300bff6038d15da6d7");
        }
    }),
    LASERS_RECEIVER_MAGENTA_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "d274c64c38fab79c82cd0430d2aac0dff8f4b83c770c88301dc8a8cfc7c6475c");
        }
    }),
    LASERS_RECEIVER_MAGENTA_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "7c711e6cef9d0c0b3cad14ef508aaaa7b360fd870e6881d7afce65a3ff4062d7");
        }
    }),
    LASERS_RECEIVER_MAGENTA_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "906bc70c2c25cc54cc1acb174e0ce3db772d7c53d934808b306fdd9226673ab3");
        }
    }),
    LASERS_RECEIVER_MAGENTA_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b1f6f7a88d81877d771b5873eed61f772d429aaaaf7784a09c14ff1383ca25d5");
        }
    }),
    LASERS_RECEIVER_MAGENTA_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6ff93e77f39e3ed326573e3e9b66e446d51cd362f78beec743ba59a27b197681");
        }
    }),
    LASERS_RECEIVER_MAGENTA_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b56bfb51ec695e1ad6c465fd4125193d40f99706426626377558821acb2a6ea4");
        }
    }),
    LASERS_RECEIVER_MAGENTA_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a502e53bfa981281e5b161e089548fa7f9cf0cb428e886c0c88dd4aa1d62da24");
        }
    }),
    LASERS_RECEIVER_RED_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5d3d2098cbee3284c7899ee9e96c0c54d236c1fc1cfff8f771a14b9ff39abf31");
        }
    }),
    LASERS_RECEIVER_RED_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bd5b4e644ec3446ab0734b36e367bea4a9fd730c8644dfdcbf2af6cf1e0da99d");
        }
    }),
    LASERS_RECEIVER_RED_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "406b70681bc30284a95651ac7757da5fbfb1739b98f99eb87cbdd5db76eff082");
        }
    }),
    LASERS_RECEIVER_RED_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "59c9948ded364749ccc025e17ee207acb95b3852c21fda509780bbf1bae6b2ad");
        }
    }),
    LASERS_RECEIVER_RED_ON(ItemType.HEAD, "laser_receiver", false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ae6bf64050cafa4655c4cac626a4f60823ea43c65c9e27c0ff3323f538c947d7");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.LASER_RECEIVER);
        }
    }),
    LASERS_RECEIVER_RED_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9afdce8cd0a1cb2131c7daf9b87b08c68dda97e3e074a8761589543082b38e0f");
        }
    }),
    LASERS_RECEIVER_RED_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "cad3d73f361f779738e85850dc8167b3a3f2be36e60e20febc46897e4dc6f383");
        }
    }),
    LASERS_RECEIVER_RED_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5584f65433ba2ecf3ed37778e1ee147f78175cb88d5416b89f9e2183d36c7ea4");
        }
    }),
    LASERS_RECEIVER_WHITE_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "c1d70acf39b70c5161bfcce11e02fed213a5208e4ec43a830c7452b3370d22dc");
        }
    }),
    LASERS_RECEIVER_WHITE_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2782042ed2a1a65761631624716359e68260aac7c72de7ef85a4cee87718e454");
        }
    }),
    LASERS_RECEIVER_WHITE_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "8b23a4f5677b30061f717a180c545b30a53b5310a8320b48127335a0c64452b0");
        }
    }),
    LASERS_RECEIVER_WHITE_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a15f95936df99657450b3b5cc81195e755c05def11b32ffea62e59ffdfec9399");
        }
    }),
    LASERS_RECEIVER_WHITE_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "cdd5f0afc574015e9d8a0a9fba5cafb4dfb8d47f21c83347aa8d213dfbf63211");
        }
    }),
    LASERS_RECEIVER_WHITE_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "6cbde2e6d214e36da8ce8201e8c642f1b9cb30726fec4e8d9d7189df2f18bf46");
        }
    }),
    LASERS_RECEIVER_WHITE_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a93977bf53e736831531aab3b24e5f6413dc58e83ad3abc4d09c106b66ec13fa");
        }
    }),
    LASERS_RECEIVER_WHITE_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9ef52a4b4c38d187f8345bea0fb4070f994e364da191597c08c21920093f11d3");
        }
    }),
    LASERS_RECEIVER_YELLOW_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "bb338beb00db81215fedb3ca1b556f5e053950beb062a5e307ed81d5438dc7a0");
        }
    }),
    LASERS_RECEIVER_YELLOW_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "a0eb75b4072b051a4094952f452a41ea1b73f04dbae0058daf421f5bc8de2028");
        }
    }),
    LASERS_RECEIVER_YELLOW_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "9ce58e1e8c93d4fd5d276d206ffd09ac853e1ace5f631e2f00c23ffff16746b7");
        }
    }),
    LASERS_RECEIVER_YELLOW_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "56cac87a7f5f9c981ae43e9be3c76c86edd2901a0321e4d511d8f433fc42afa1");
        }
    }),
    LASERS_RECEIVER_YELLOW_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "aa9c0437111337280986cec27966868b425eb7a2e2bb878de2eadc20f6707763");
        }
    }),
    LASERS_RECEIVER_YELLOW_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "52390f4eacf048ffd5dc1eb7fd2a10315c6c15f89df044c82e573646df99cd94");
        }
    }),
    LASERS_RECEIVER_YELLOW_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "48a42effa28e7085d4e4d4bea482b3138691e12d3571a086ce2d74f81af5e05f");
        }
    }),
    LASERS_RECEIVER_YELLOW_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "4979f8bb5c9662af0733aac55de0a7e5f568c8d08974fb4d884ad1576aef9398");
        }
    }),
    LASERS_RECEIVER_ANY_OFF(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5f369c496ccb0c234efdf01edf1e4768e5677358e43fa32cb91ce2516b854d99");
        }
    }),
    LASERS_RECEIVER_ANY_OFF_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "2a0fd8282f0a5d96d261fdbafee12aff309fbb7c1d7d4739d5433514d3faa994");
        }
    }),
    LASERS_RECEIVER_ANY_OFF_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "abb8482e2f7f62fc54ae15b4fb7ac05d46ee5b994383401b3f0829b1e09317ea");
        }
    }),
    LASERS_RECEIVER_ANY_OFF_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "ccec2cea5fddc24066ea38d4eef3a86c79cc5508bf9dc7ef89d13a8ceba82e9f");
        }
    }),
    LASERS_RECEIVER_ANY_ON(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "1e33821edb80516405a8749d94c7ff43dce4359fb0b885263e0ae0786f56e17d");
        }
    }),
    LASERS_RECEIVER_ANY_ON_H(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "dcab0258e117b0b30bb2d82617330909156596faa6b5947f558a98c148e16c12");
        }
    }),
    LASERS_RECEIVER_ANY_ON_HV(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "360cd9e21c12213cb67122c2ba4001e35c8998c4980612dbb4c4060a632844e2");
        }
    }),
    LASERS_RECEIVER_ANY_ON_V(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "3e4171848046cc20d9c9bf15d8a0f607d2d08880a45d6251aff1cd650c0fd748");
        }
    }),
    /**
     * ******************* LOCK *******************
     */
    LOCK_ACTIVATED(ItemType.HEAD, null, false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "5a780c1bfc3fc1606fc3dcfbf7fe49265ef29082dc01133a4dccc431077edf30");
        }
    }),
    LOCK_DEACTIVATED(ItemType.HEAD, "lock", false, false, new HashMap() {
        {
            put(ItemAttribute.TEXTURE, "b90c16b00b15d1e065695867bb5f17fa06157b1750da3aa757fc8d4f390ace83");
            put(ItemAttribute.COMPONENT_TYPE, ComponentType.LOCK);
        }
    });

    /**
     * the last slot from which the specials items will be added in the players
     * inventories
     */
    public static final int SPECIAL_ITEMS_INVENTORY_LAST_SLOT = 17;
    /**
     * the items base path in the configuration
     */
    public static final String BASE_PATH = "items.";
    /**
     * the relative path to items name in the configuration
     */
    public static final String NAME_PATH = ".name";

    /**
     * the relative path to items description in the configuration
     */
    public static final String DESCRIPTION_PATH = ".description";
    /**
     * the item type
     */
    private final ItemType type;
    /**
     * the config path
     */
    private final String configPath;
    /**
     * the item attributes and values
     */
    private final HashMap<ItemAttribute, Object> attributes;
    private final boolean isPlacableOutsideAnArea;
    private final boolean isPlacableAsBlock;

    /**
     * Constructor
     *
     * @param type                    the item type
     * @param configPath              the config path
     * @param attributes              the item attributes and values
     * @param isPlacableAsBlock       can this item be placed as a block
     * @param isPlacableOutsideAnArea can this item be placed outside a puzzle area
     */
    Item(ItemType type, String configPath, boolean isPlacableOutsideAnArea, boolean isPlacableAsBlock, HashMap<ItemAttribute, Object> attributes) {
        this.type = type;
        this.configPath = configPath;
        this.attributes = attributes;
        this.isPlacableAsBlock = isPlacableAsBlock;
        this.isPlacableOutsideAnArea = isPlacableOutsideAnArea;
    }

    public static Item getMirror(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.MIRROR;
            case RED:
                return Item.RED_MIRROR;
            case GREEN:
                return Item.GREEN_MIRROR;
            case BLUE:
                return Item.BLUE_MIRROR;
            case YELLOW:
                return Item.YELLOW_MIRROR;
            case MAGENTA:
                return Item.MAGENTA_MIRROR;
            case CYAN:
                return Item.LIGHT_BLUE_MIRROR;
            case BLACK:
                return Item.BLACK_MIRROR;
            default:
                return null;
        }
    }

    public static ArrayList<Item> getMirrors() {
        return new ArrayList<>(Arrays.asList(Item.MIRROR,
                Item.RED_MIRROR,
                Item.GREEN_MIRROR,
                Item.BLUE_MIRROR,
                Item.YELLOW_MIRROR,
                Item.MAGENTA_MIRROR,
                Item.LIGHT_BLUE_MIRROR,
                Item.BLACK_MIRROR
        ));
    }

    public static Item getReflectingSphereItem(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.REFLECTING_SPHERE_WHITE;
            case RED:
                return Item.REFLECTING_SPHERE_RED;
            case GREEN:
                return Item.REFLECTING_SPHERE_GREEN;
            case BLUE:
                return Item.REFLECTING_SPHERE_BLUE;
            case YELLOW:
                return Item.REFLECTING_SPHERE_YELLOW;
            case MAGENTA:
                return Item.REFLECTING_SPHERE_MAGENTA;
            case CYAN:
                return Item.REFLECTING_SPHERE_LIGHT_BLUE;
            case BLACK:
                return Item.REFLECTING_SPHERE;
            default:
                return null;
        }
    }

    public static Item getFilteringSphereItem(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.FILTERING_SPHERE_WHITE;
            case RED:
                return Item.FILTERING_SPHERE_RED;
            case GREEN:
                return Item.FILTERING_SPHERE_GREEN;
            case BLUE:
                return Item.FILTERING_SPHERE_BLUE;
            case YELLOW:
                return Item.FILTERING_SPHERE_YELLOW;
            case MAGENTA:
                return Item.FILTERING_SPHERE_MAGENTA;
            case CYAN:
                return Item.FILTERING_SPHERE_LIGHT_BLUE;
            case BLACK:
                return Item.FILTERING_SPHERE;
            default:
                return null;
        }
    }

    public static Item getMeltableClay(LasersColor color) {
        switch (color) {
            case WHITE:
                return Item.WHITE_MELTABLE_CLAY;
            case RED:
                return Item.RED_MELTABLE_CLAY;
            case GREEN:
                return Item.GREEN_MELTABLE_CLAY;
            case BLUE:
                return Item.BLUE_MELTABLE_CLAY;
            case YELLOW:
                return Item.YELLOW_MELTABLE_CLAY;
            case MAGENTA:
                return Item.MAGENTA_MELTABLE_CLAY;
            case CYAN:
                return Item.LIGHT_BLUE_MELTABLE_CLAY;
            default:
                return null;
        }
    }

    /**
     * gets the item type
     *
     * @return the item type
     */
    public ItemType getType() {
        return this.type;
    }

    public boolean isUsableWithoutEditionMode() {
        return isPlacableOutsideAnArea;
    }

    /**
     * checks if this item has a name and description
     *
     * @return true if this item has a name and description
     */
    public boolean hasNameAndLore() {
        return (this.configPath != null && this.configPath.length() != 0);
    }

    public boolean isMirror(boolean blackAccepted) {
        switch (this) {
            case BLACK_MIRROR:
                return blackAccepted;
            case MIRROR:
            case RED_MIRROR:
            case BLUE_MIRROR:
            case GREEN_MIRROR:
            case YELLOW_MIRROR:
            case MAGENTA_MIRROR:
            case LIGHT_BLUE_MIRROR:
                return true;
            default:
                return false;
        }
    }

    /**
     * gets the path of this item inside configuration
     *
     * @return the path of this item inside configuration
     */
    public String getConfigPath() {
        if (this.configPath == null) {
            return this.toString();

        }
        return this.configPath;
    }

    public boolean isPlacableOutsideAnArea() {
        return isPlacableOutsideAnArea;
    }

    public boolean isPlacableAsBlock() {
        return isPlacableAsBlock;
    }

    /**
     * gets the attributes of this item
     *
     * @return the attributes of this item
     */
    public HashMap<ItemAttribute, Object> getAttributes() {
        return this.attributes;
    }

}
