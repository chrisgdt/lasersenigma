package eu.lasersenigma.items;

/**
 * the item attributes
 */
public enum ItemAttribute {
    TEXTURE,
    MATERIAL,
    COLOR,
    ENCHANT,
    COMPONENT_TYPE,
    ARMOR_ACTION
}
