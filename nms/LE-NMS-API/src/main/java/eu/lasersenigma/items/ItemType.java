package eu.lasersenigma.items;

/**
 * The item types
 */
public enum ItemType {
    HEAD,
    BLOCK
}
