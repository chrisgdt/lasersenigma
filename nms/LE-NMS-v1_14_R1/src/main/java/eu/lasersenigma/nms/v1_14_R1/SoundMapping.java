package eu.lasersenigma.nms.v1_14_R1;

import org.bukkit.Sound;

import java.util.HashMap;

public enum SoundMapping {
    ENTITY_IRON_GOLEM_ATTACK("ENTITY_IRONGOLEM_ATTACK"),
    ENTITY_FIREWORK_ROCKET_LAUNCH("ENTITY_FIREWORK_LAUNCH"),
    BLOCK_NOTE_BLOCK_BASS("BLOCK_NOTE_BASS"),
    ENTITY_LIGHTNING_BOLT_IMPACT("ENTITY_LIGHTNING_IMPACT");
    private static final HashMap<String, Sound> cachedSearch = new HashMap<>();
    private final String oldName;

    SoundMapping(String oldName) {
        this.oldName = oldName;
    }

    public static Sound requestSound(String name) {
        if (cachedSearch.containsKey(name.toUpperCase())) {
            return cachedSearch.get(name.toUpperCase());
        }

        Sound sound = null;
        try {
            sound = Sound.valueOf(name);
            if (sound != null) {
                cachedSearch.put(name, sound);
                return sound;
            }
        } catch (IllegalArgumentException e) {
        }
        for (SoundMapping currentSound : SoundMapping.values()) {
            if (name.toUpperCase().equals(currentSound.oldName)) {
                sound = Sound.valueOf(currentSound.name());
                if (sound != null) {
                    cachedSearch.put(name, sound);
                    return sound;
                }
            }
        }
        return null;
    }
}
