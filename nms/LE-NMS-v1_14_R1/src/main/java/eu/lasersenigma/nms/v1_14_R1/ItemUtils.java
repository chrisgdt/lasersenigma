package eu.lasersenigma.nms.v1_14_R1;

/*
 *         ----------------------
 *         | PHOENIX REBIRTH FR |
 *         ----------------------
 * Copyright 2016 PhoenixRebirthFr.
 *
 * Ce projet est réservé au serveur PhoenixRebirth
 * Vous ne pouvez pas utiliser ces fichiers sans autorisation préalable
 * d'un administrateur du serveur PhoenixRebirth France.
 *
 *  http://phoenix-rebirth.fr - contact@phoenix-rebirth.fr
 */

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_14_R1.NBTTagCompound;
import net.minecraft.server.v1_14_R1.NBTTagList;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Detobel36
 */
public class ItemUtils {

    /**
     * Permet de savoir si 2 itemStacks sont les mêmes
     *
     * @param item1 le premier a tester
     * @param item2 le second
     * @return vrai si ce sont les mêmes
     */
    public static boolean equals(ItemStack item1, ItemStack item2) {
        return item1.isSimilar(item2);
    }

    /**
     * Permet de savoir si 2 itemStacks sont les mêmes et qu'il ne sont pas vide
     * ou de l'air
     *
     * @param item1 le premier a tester
     * @param item2 le second
     * @return vrai si ce sont les mêmes
     */
    public static boolean equalsNotNul(ItemStack item1, ItemStack item2) {
        return itemNotNull(item1) && equals(item1, item2);
    }

    /**
     * Permet de vérifier qu'un item n'est pas vide ou égal à de l'air
     *
     * @param item l'item à tester
     * @return True si il n'est pas null et que ce n'est pas de l'aire
     */
    public static boolean itemNotNull(ItemStack item) {
        return item != null && !item.getType().equals(Material.AIR);
    }

    /**
     * Permet de vérifier qu'un item n'est pas vide ou égal à de l'air et que
     * l'item meta n'est pas null
     *
     * @param item l'item à tester
     * @return True si les conditions sont respectées
     */
    public static boolean itemAndMetaNotNull(ItemStack item) {
        return itemNotNull(item) && item.getItemMeta() != null;
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param name   Nom de l'item
     * @param lore   Lore à utiliser (\n pour aller à la ligne)
     * @param amount Nombre d'item désiré
     * @return l'itemstack créé
     */
    public static ItemStack getItem(Material m, String name, String lore, int amount) {
        return getItem(m, name, lore, amount, (short) -1);
    }

    /**
     * Permet de créer un item
     *
     * @param m    Material
     * @param name Nom de l'item
     * @param lore Lore à utiliser (\n pour aller à la ligne)
     * @return l'itemstack créé
     */
    public static ItemStack getItem(Material m, String name, String lore) {
        return getItem(m, name, lore, 1);
    }

    /**
     * Permet de créer un item
     *
     * @param m    Material
     * @param name Nom de l'item
     * @return l'itemstack créé
     */
    public static ItemStack getItem(Material m, String name) {
        return getItem(m, name, "", 1);
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param name   Nom de l'item
     * @param lore   Lore à utiliser (\n pour aller à la ligne)
     * @param amount Nombre d'item désiré
     * @param id     id de l'item (pour les laines par exemple)
     * @return l'itemstack créé
     * @deprecated Voir la méthode getItem
     */
    @Deprecated
    public static ItemStack getItemStackByID(Material m, String name, String lore,
                                             int amount, short id) {
        return getItem(m, name, lore, amount, id);
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param name   Nom de l'item
     * @param lore   Lore à utiliser (\n pour aller à la ligne)
     * @param amount Nombre d'item désiré
     * @param id     id de l'item (pour les laines par exemple)
     * @return l'itmestack créé
     */
    public static ItemStack getItem(Material m, String name, String lore, int amount,
                                    short id) {
        return getItem(m, name, lore, amount, id, new HashMap<>());
    }

    /**
     * Permet de créer un item
     *
     * @param m                Material
     * @param name             Nom de l'item
     * @param lore             Lore à utiliser (\n pour aller à la ligne)
     * @param amount           Nombre d'item désiré
     * @param id               id de l'item (pour les laines par exemple)
     * @param listEnchantement liste des enchantements
     * @return l'itmestack créé
     */
    public static ItemStack getItem(Material m, String name, String lore, int amount,
                                    short id, Map<Enchantment, Integer> listEnchantement) {
        ItemStack is;
        if (id >= 0) {
            is = new ItemStack(m, amount, id);
        } else {
            is = new ItemStack(m, amount);
        }

        if (!m.equals(Material.AIR)) {
            ItemMeta im = is.getItemMeta();
            im.setDisplayName(name);
            im.setLore(String2Arrays(lore));
            is.setItemMeta(im);
        }

        if (!listEnchantement.isEmpty()) {
            is.addEnchantments(listEnchantement);
        }

        return is;
    }

    public static ItemStack getItem(Material leatherPiece, String displayName, Color color) {
        ItemStack item = new ItemStack(leatherPiece);
        LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setColor(Color.fromRGB(color.asRGB()));
        item.setItemMeta(meta);
        return item;
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param name   Nom de l'item
     * @param amount Nombre d'item désiré
     * @param id     Id de l'item (pour les laines par exemple)
     * @return l'itmestack créé
     * @deprecated Voir la méthode getItem
     */
    @Deprecated
    public static ItemStack getItemStackWithNoLore(Material m, String name, int amount, short id) {
        return getItem(m, name, amount, id);
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param name   Nom de l'item
     * @param amount Nombre d'item désiré
     * @param id     Id de l'item (pour les laines par exemple)
     * @return l'itmestack créé
     */
    public static ItemStack getItem(Material m, String name, int amount, short id) {
        return getItem(m, name, "", amount, id);
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param amount Nombre d'item désiré
     * @return l'itemstack créé
     * @deprecated Voir la méthode getItem
     */
    @Deprecated
    public static ItemStack getNormalItem(Material m, int amount) {
        return getItem(m, amount);
    }

    /**
     * Permet de créer un item
     *
     * @param m      Material
     * @param amount Nombre d'item désiré
     * @return l'itemstack créé
     */
    public static ItemStack getItem(Material m, int amount) {
        return new ItemStack(m, amount);
    }

    /**
     * Permet d'avoir un itemstack d'air
     *
     * @return un itemstack contenant un cube d'air
     */
    public static ItemStack getNothing() {
        return getItem(Material.AIR, 1);
    }

    public static ItemStack setLetherArmorMeta(ItemStack is, Color c) {
        LeatherArmorMeta la = (LeatherArmorMeta) is.getItemMeta();
        la.setColor(c);
        is.setItemMeta(la);
        return is;
    }

    /**
     * Permet de supprimer l'inventaire complètement
     *
     * @param p Le joueur dont l'inventaire doit être remis à 0
     */
    public static void ClearFullyPlayer(Player p) {
        p.getInventory().clear();
        p.getInventory().setHelmet(getNothing());
        p.getInventory().setBoots(getNothing());
        p.getInventory().setLeggings(getNothing());
        p.getInventory().setChestplate(getNothing());
        p.updateInventory();
    }

    public static ItemStack addDurability(ItemStack i, int durability) {
        i.setDurability((short) +(i.getDurability() + durability));
        if (i.getDurability() >= i.getType().getMaxDurability()) {
            i = new ItemStack(Material.AIR);
        }
        return i;
    }

    public static boolean inventoryEquals(Inventory in, Inventory in2) {
        if (in.getContents().length != in2.getContents().length) {
            return false;
        }
        for (int i = 0; i < in.getSize(); i++) {
            ItemStack it = in.getItem(i);
            if (it != null) {
                if (!it.equals(in2.getItem(i))) {
                    return false;
                }
            } else {
                if (in2.getItem(i) != null) {
                    if (in2.getItem(i).getType() != Material.AIR) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Permet de récupérer une potion en fonction de son id
     *
     * @param type de potion
     * @return l'item stack
     * @deprecated
     */
    public static ItemStack getPotionBy(PotionType type) {
        Potion p = new Potion(type);
        return p.toItemStack(1);
    }

    /**
     * Permet d'avoir une splash potion
     *
     * @param type  de potion
     * @param temps en seconde
     * @return l'item stack
     */
    public static ItemStack getSplashPotion(PotionEffectType type, int temps) {
        return getSplashPotion(type, temps, 0);
    }

    /**
     * Permet d'avoir une splash potion
     *
     * @param type      de potion
     * @param temps     en seconde
     * @param amplifier amplificateur (1 pour avoir le niveau 2)
     * @return l'item stack
     */
    public static ItemStack getSplashPotion(PotionEffectType type, int temps, int amplifier) {
        ItemStack is = new ItemStack(Material.SPLASH_POTION, 1);
        PotionMeta im = (PotionMeta) is.getItemMeta();
        im.addCustomEffect(new PotionEffect(type, temps * 20, amplifier), true);
        is.setItemMeta(im);
        return is;
    }

    /**
     * Permet d'avoir une splash potion de zone
     *
     * @param type  de potion
     * @param temps en seconde
     * @return l'item stack
     */
    public static ItemStack getLingeringPotion(PotionEffectType type, int temps) {
        return getLingeringPotion(type, temps, 0);
    }

    /**
     * Permet d'avoir une splash potion de zone
     *
     * @param type      de potion
     * @param temps     en seconde
     * @param amplifier amplificateur (1 pour avoir le niveau 2)
     * @return l'item stack
     */
    public static ItemStack getLingeringPotion(PotionEffectType type, int temps, int amplifier) {
        ItemStack is = new ItemStack(Material.LINGERING_POTION, 1);
        PotionMeta im = (PotionMeta) is.getItemMeta();
        im.addCustomEffect(new PotionEffect(type, temps * 20, amplifier), true);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack getPotion(PotionEffectType type, int temps) {
        return getPotion(type, temps, 0);
    }

    public static ItemStack getPotion(PotionEffectType type, int temps, int amplifier) {
        ItemStack is = new ItemStack(Material.POTION, 1);
        PotionMeta im = (PotionMeta) is.getItemMeta();
        im.addCustomEffect(new PotionEffect(type, temps * 20, amplifier), true);
        is.setItemMeta(im);
        return is;
    }

    /**
     * Permet d'avoir la tête de quelqu'un
     *
     * @param name le nom de la personne
     * @return l'ItemStack demandé
     */
    public static ItemStack getPlayerHead(String name) {
        ItemStack player = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta sk = (SkullMeta) player.getItemMeta();
        sk.setOwner(name);
        player.setItemMeta(sk);

        return player;
    }

    /**
     * Permet d'avoir la tête de quelqu'un tout en précisant le nom de l'item et
     * le lore
     *
     * @param pseudo le pseudo de la personne
     * @param nom    le nom de l'item
     * @param lore   le lore (où \n permet d'aller à la ligne)
     * @return l'ItemStack demandé
     */
    public static ItemStack getPlayerHead(String pseudo, String nom, String lore) {
        ItemStack player = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta sk = (SkullMeta) player.getItemMeta();
        sk.setDisplayName(nom);
        sk.setLore(String2Arrays(lore));
        sk.setOwner(pseudo);
        player.setItemMeta(sk);

        return player;
    }

    /**
     * Permet d'avoir la tête en fonction d'une texture<br>
     * Pour obtenir l'URL:
     * <ul>
     * <li>Copier la valeur présente dans "Value" (commande de give)</li>
     * <li>Aller sur: http://base64decode.net/</li>
     * <li>Il n'y a plus qu'a copier le résultat en paramètre</li>
     * </ul>
     *
     * @param playerSkullTexture la texture à charger
     * @return l'ItemStack
     */
    public static ItemStack getHead(String playerSkullTexture) {
        return getHead(playerSkullTexture, "", "");
    }

    /**
     * Permet d'avoir la tête en fonction d'une texture<br>
     * Pour obtenir l'URL:
     * <ul>
     * <li>Copier la valeur présente dans "Value" (commande de give)</li>
     * <li>Aller sur: http://base64decode.net/</li>
     * <li>Il n'y a plus qu'a copier le résultat en paramètre</li>
     * </ul>
     *
     * @param playerSkullTexture la texture à charger
     * @param nom                nom de l'item
     * @param lore               le lore (où \n permet d'aller à la ligne)
     * @return l'ItemStack
     */
    public static ItemStack getHead(String playerSkullTexture, String nom, String lore) {
        playerSkullTexture = "http://textures.minecraft.net/texture/" + playerSkullTexture;
        ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta sk = (SkullMeta) skull.getItemMeta();
        if (!nom.equalsIgnoreCase("")) {
            sk.setDisplayName(nom);
        }

        if (!lore.equalsIgnoreCase("")) {
            sk.setLore(String2Arrays(lore));
        }

        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", playerSkullTexture).getBytes());
        profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
        Field profileField;
        try {
            profileField = sk.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(sk, profile);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
            Logger.getLogger(ItemUtils.class.getName()).log(Level.SEVERE, "Error during textured head item stack creation", e1);
        }
        skull.setItemMeta(sk);

        return skull;
    }

    /**
     * Permet d'avoir un livre enchanté
     *
     * @param enchant l'enchantement
     * @param level   le niveau
     * @return le livre
     */
    public static ItemStack getEnchantedBook(Enchantment enchant, int level) {
        return getEnchantedBook("", "", enchant, level);
    }

    /**
     * Permet d'avoir un livre enchanté
     *
     * @param displayName le nom du livre
     * @param enchant     l'enchantement
     * @param level       le niveau
     * @return le livre
     */
    public static ItemStack getEnchantedBook(String displayName, Enchantment enchant,
                                             int level) {
        return getEnchantedBook(displayName, "", enchant, level);
    }

    /**
     * Permet d'avoir un livre enchanté
     *
     * @param displayName le nom du livre
     * @param lore        le lore
     * @param enchant     l'enchantement
     * @return le livre
     */
    public static ItemStack getEnchantedBook(String displayName, String lore, Enchantment enchant) {
        return getEnchantedBook(displayName, lore, enchant, 1);
    }

    /**
     * Permet d'avoir un livre enchanté
     *
     * @param displayName le nom du livre
     * @param lore        le lore
     * @param enchant     l'enchantement
     * @param level       le niveau
     * @return le livre
     */
    public static ItemStack getEnchantedBook(String displayName, String lore, Enchantment enchant,
                                             int level) {
        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK, 1);
        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();
        if (!displayName.equals("")) {
            meta.setDisplayName(displayName);
        }
        if (!lore.equals("")) {
            meta.setLore(String2Arrays(lore));
        }
        meta.addStoredEnchant(enchant, level, true);
        book.setItemMeta(meta);

        return book;
    }

    /**
     * Permet d'ajouter un enchantement sur un item (sans nom spécifique)
     *
     * @param item l'item à enchanté
     * @return l'item enchanté
     */
    public static ItemStack addEnchantement(ItemStack item) {
        net.minecraft.server.v1_14_R1.ItemStack nmsitem = CraftItemStack.asNMSCopy(item);
        NBTTagCompound nbt = nmsitem.getTag() == null ? new NBTTagCompound() : nmsitem.getTag();
        NBTTagList ench = new NBTTagList();
        nbt.set("ench", ench);
        nmsitem.setTag(nbt);
        return CraftItemStack.asBukkitCopy(nmsitem);
    }

    /**
     * Permet de transformer une chaine de caractère en liste<br>
     * La séparation se fait à chaque fois qu'un '\n' est trouvé
     *
     * @param str le texte à convertir
     * @return la liste de texte
     */
    public static List<String> String2Arrays(String str) {
        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(str.split("\n")));
        return list;
    }

    /**
     * Converts the player inventory to a String array of Base64 strings. First
     * string is the content and second string is the armor.
     *
     * @param playerInventory to turn into an array of strings.
     * @return Array of strings: [ main content, armor content ]
     * @throws IllegalStateException In case of players during inventory conversion
     */
    public static String[] playerInventoryToBase64(PlayerInventory playerInventory) throws IllegalStateException {
        //get the main content part, this doesn't return the armor
        String content = toBase64(playerInventory);
        String armor = itemStackArrayToBase64(playerInventory.getArmorContents());

        return new String[]{content, armor};
    }

    /**
     * A method to serialize an {@link ItemStack} array to Base64 String.
     * <p>
     * Based off of {@link #toBase64(Inventory)}.
     *
     * @param items to turn into a Base64 String.
     * @return Base64 string of the items.
     * @throws IllegalStateException In case of players during itemstack conversion
     */
    public static String itemStackArrayToBase64(ItemStack[] items) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            // Write the size of the inventory
            dataOutput.writeInt(items.length);

            // Save every element in the list
            for (ItemStack item : items) {
                dataOutput.writeObject(item);
            }

            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    /**
     * A method to serialize an inventory to Base64 string.
     * <p>
     * Special thanks to Comphenix in the Bukkit forums or also known as aadnk
     * on GitHub.
     *
     * <a href="https://gist.github.com/aadnk/8138186">Original Source</a>
     *
     * @param inventory to serialize
     * @return Base64 string of the provided inventory
     * @throws IllegalStateException in case conversion to base64 did not work
     */
    public static String toBase64(Inventory inventory) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            // Write the type of the inventory
            dataOutput.writeObject(inventory.getType());

            // Save every element in the list
            for (int i = 0; i < inventory.getSize(); i++) {
                dataOutput.writeObject(inventory.getItem(i));
            }
            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    /**
     * A method to get an {@link Inventory} from an encoded, Base64, string.
     * <p>
     * Special thanks to Comphenix in the Bukkit forums or also known as aadnk
     * on GitHub.
     *
     * <a href="https://gist.github.com/aadnk/8138186">Original Source</a>
     *
     * @param data Base64 string of data containing an inventory.
     * @return Inventory created from the Base64 string.
     * @throws IOException in case of problem when gettinng back Inventory from base64 string
     */
    public static Inventory fromBase64(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            InventoryType type = (InventoryType) dataInput.readObject();
            Inventory inventory = Bukkit.getServer().createInventory(null, type);
            // Read the serialized inventory
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }

            dataInput.close();
            return inventory;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }

    /**
     * Gets an array of ItemStacks from Base64 string.
     * <p>
     * Base off of {@link #fromBase64(String)}.
     *
     * @param data Base64 string to convert to ItemStack array.
     * @return ItemStack array created from the Base64 string.
     * @throws IOException
     */
    public static ItemStack[] itemStackArrayFromBase64(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack[] items = new ItemStack[dataInput.readInt()];

            // Read the serialized inventory
            for (int i = 0; i < items.length; i++) {
                items[i] = (ItemStack) dataInput.readObject();
            }

            dataInput.close();
            return items;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }

}
