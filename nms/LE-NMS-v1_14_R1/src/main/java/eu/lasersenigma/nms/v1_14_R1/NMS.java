package eu.lasersenigma.nms.v1_14_R1;

import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicSaveException;
import eu.lasersenigma.exceptions.SelectionNotCuboidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.items.AArmorActionProcessor;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import eu.lasersenigma.nms.AItemStackFlagsProcessor;
import eu.lasersenigma.nms.ICoreNMS;
import eu.lasersenigma.nms.IWECopy;
import eu.lasersenigma.nms.IWEPaste;
import net.minecraft.server.v1_14_R1.AxisAlignedBB;
import net.minecraft.server.v1_14_R1.Vec3D;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.RedstoneWire;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NMS implements ICoreNMS {

    private final Random rand = new Random();

    public NMS() {
    }

    @Override
    public void playSound(Location loc, String soundName, String soundCategory, float volume, float pitch) {
        loc.getWorld().playSound(loc, SoundMapping.requestSound(soundName), SoundCategory.valueOf(soundCategory), volume, pitch);
    }

    @Override
    public void playEffect(Location location, String effect, int count) {
        if (effect.equals("COLOURED_DUST")) {
            Particle.DustOptions dustOptions = new Particle.DustOptions(Color.fromRGB(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)), 2);
            location.getWorld().spawnParticle(ParticleMapping.requestParticle(effect), location, count, dustOptions);
        } else {
            location.getWorld().spawnParticle(ParticleMapping.requestParticle(effect), location, count);
        }
    }

    @Override
    public void playEffect(HashSet<Player> players, Location location, String effect, Color color) {
        Particle.DustOptions dustOptions = new Particle.DustOptions(color, 0.9f);
        location.getWorld().spawnParticle(Particle.REDSTONE, location, 1, dustOptions);
    }

    @Override
    public void playEffect(HashSet<Player> players, Location location, String effect, double xOffset, double yOffSet, double zOffSet, double speed, int count) {
        if (effect.equals("COLOURED_DUST")) {
            Particle.DustOptions dustOptions = new Particle.DustOptions(Color.fromRGB(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)), 2);
            location.getWorld().spawnParticle(ParticleMapping.requestParticle(effect), location, count, dustOptions);
        } else {
            location.getWorld().spawnParticle(ParticleMapping.requestParticle(effect), location, count, xOffset, yOffSet, zOffSet, speed);
        }
    }

    @Override
    public ItemStack getItemStack(Item item, FileConfiguration translationConfigurationFile) {
        return NMSItems.getItemStack(item, translationConfigurationFile);
    }

    @Override
    public void powerBlock(Block block, byte power) {
        BlockData blockData = block.getBlockData();
        if (blockData instanceof RedstoneWire) {
            RedstoneWire redstoneWire = (RedstoneWire) blockData;
            redstoneWire.setPower(power);
            block.setBlockData(redstoneWire);
        }
    }

    @Override
    public void setBlock(Block block, Item item) {
        String materialName = (String) item.getAttributes().get(ItemAttribute.MATERIAL);
        Material material = Material.getMaterial(materialName);
        if (material == null) {
            byte data = 0;
            if (item.getAttributes().containsKey(ItemAttribute.COLOR)) {
                data = (byte) ((LasersColor) item.getAttributes().get(ItemAttribute.COLOR)).getDatavalue();
            }
            material = Material.getMaterial(MaterialMapping.requestMaterials(materialName, data).name());
        }
        if (material == null) {
            throw new IllegalArgumentException("ERROR setting block at " + block.getLocation().toString() + ": Item " + item.toString() + " material type couldn't be find.");
        } else {
            block.setType(material);
            block.getState().update();
        }
    }

    @Override
    public String getSkullMetaStr(ItemMeta itemMeta) {
        return NMSItems.getSkullMetaStr(itemMeta);
    }

    @Override
    public ItemStack getPlayerSkull(UUID key, String loreStr) {
        return NMSItems.getPlayerSkull(key, loreStr);
    }

    @Override
    public LasersColor getColorFromBlock(Block block) {
        return NMSColors.getColorFromBlock(block);
    }

    @Override
    public DyeColor getDyeColor(LasersColor color) {
        return NMSColors.getDyeColor(color);
    }

    @Override
    public boolean isConcretePowder(Material type) {
        switch (type) {
            case WHITE_CONCRETE_POWDER:
            case RED_CONCRETE_POWDER:
            case GREEN_CONCRETE_POWDER:
            case YELLOW_CONCRETE_POWDER:
            case MAGENTA_CONCRETE_POWDER:
            case LIGHT_BLUE_CONCRETE_POWDER:
            case BLACK_CONCRETE_POWDER:
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean isGlassOrStainedGlass(Material type) {
        switch (type) {
            case BLACK_STAINED_GLASS_PANE:
            case BLUE_STAINED_GLASS_PANE:
            case BROWN_STAINED_GLASS_PANE:
            case CYAN_STAINED_GLASS_PANE:
            case GRAY_STAINED_GLASS_PANE:
            case GREEN_STAINED_GLASS_PANE:
            case LIME_STAINED_GLASS_PANE:
            case MAGENTA_STAINED_GLASS_PANE:
            case ORANGE_STAINED_GLASS_PANE:
            case PINK_STAINED_GLASS_PANE:
            case PURPLE_STAINED_GLASS_PANE:
            case RED_STAINED_GLASS_PANE:
            case WHITE_STAINED_GLASS_PANE:
            case YELLOW_STAINED_GLASS_PANE:
            case LIGHT_GRAY_STAINED_GLASS_PANE:
            case LIGHT_BLUE_STAINED_GLASS_PANE:
            case BLACK_STAINED_GLASS:
            case BLUE_STAINED_GLASS:
            case BROWN_STAINED_GLASS:
            case CYAN_STAINED_GLASS:
            case GRAY_STAINED_GLASS:
            case GREEN_STAINED_GLASS:
            case LIME_STAINED_GLASS:
            case MAGENTA_STAINED_GLASS:
            case ORANGE_STAINED_GLASS:
            case PINK_STAINED_GLASS:
            case PURPLE_STAINED_GLASS:
            case RED_STAINED_GLASS:
            case WHITE_STAINED_GLASS:
            case YELLOW_STAINED_GLASS:
            case LIGHT_GRAY_STAINED_GLASS:
            case LIGHT_BLUE_STAINED_GLASS:
                return true;
            default:
                return false;
        }
    }

    @Override
    public HashSet<Material> getCrossableMaterials(String[] crossableMaterialsStr) {
        if (NMSItems.CROSSABLE_MATERIALS.isEmpty()) {
            Arrays.asList(crossableMaterialsStr).forEach(materialStr -> {
                Material material;
                try {
                    material = Material.valueOf(materialStr);
                    NMSItems.CROSSABLE_MATERIALS.add(material);
                } catch (IllegalArgumentException e) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, "The following material, defined in crossable_materials list within the configuration file, does not exist:" + materialStr + ". See https://www.google.com/search?q=Material+spigot+javadoc to find the complete list of possible materials according to your minecraft version.", e);
                }
            });
        }
        return NMSItems.CROSSABLE_MATERIALS;
    }

    @Override
    public boolean isInsideEntityHitbox(LivingEntity lentity, Location location) {
        AxisAlignedBB bb = ((CraftEntity) lentity).getHandle().getBoundingBox();
        return bb.c(new Vec3D(location.getX(), location.getY(), location.getZ())); //AxisAlignedBB.b(Vec3D locationVector) is the equivalent of AArea.containsLocation method.
    }

    @Override
    public IWECopy worldEditCopy(Player player) throws SelectionNotCuboidException, InvalidSelectionException, WorldEditNotAvailableException, SchematicSaveException {
        return new NMSWECopy(player);
    }

    @Override
    public IWEPaste worldEditPaste(Player player, byte[] worldeditSchematic) {
        return new NMSWEPaste(player, worldeditSchematic);
    }

    @Override
    public AItemStackFlagsProcessor getItemStackFlagsProcessor() {
        return new NMSItemStackFlagsProcessor();
    }

    @Override
    public Inventory inventoryFromBase64(String data) throws IOException {
        return ItemUtils.fromBase64(data);
    }

    @Override
    public String base64FromInventory(Inventory inventory) {
        return ItemUtils.toBase64(inventory);
    }

    @Override
    public Material getItemMaterial(Item item) {
        return NMSItems.getItemMaterial(item);
    }

    @Override
    public void setBlock(Block block, Material material, LasersColor color) {
        NMSItems.setBlock(block, material, color);
    }

    @Override
    public AArmorActionProcessor getArmorActionProcessor(Player bukkitPlayer, ArmorAction selectedArmorAction, Location newMaterialLocation, boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber, String armor_no_knockback_tag, String armor_no_burn_tag, String armor_no_damage_tag, String armor_reflect_tag, String armor_prism_tag, String armor_focus_tag, String armor_lasers_durability_regex) {
        return new NMSArmorActionProcessor(bukkitPlayer, selectedArmorAction, newMaterialLocation, noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber, armor_no_knockback_tag, armor_no_burn_tag, armor_no_damage_tag, armor_reflect_tag, armor_prism_tag, armor_focus_tag, armor_lasers_durability_regex);
    }

    @Override
    public void appendToItemLore(ItemStack itemStack, String txtToAdd) {
        NMSItems.appendToItemLore(itemStack, txtToAdd);
    }
}
