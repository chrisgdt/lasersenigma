package eu.lasersenigma.nms.v1_13_R2;

import eu.lasersenigma.nms.AItemStackFlagsProcessor;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import org.apache.commons.lang.StringUtils;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NMSItemStackFlagsProcessor extends AItemStackFlagsProcessor {

    public NMSItemStackFlagsProcessor() {
    }

    @Override
    public void process(ItemStack itemStack) {
        net.minecraft.server.v1_13_R2.ItemStack chestPlateNMS = CraftItemStack.asNMSCopy(itemStack);
        String tagsStr = "";
        if (chestPlateNMS.hasTag()) {
            NBTTagCompound rootNBTtagCompound = chestPlateNMS.getTag();
            if (rootNBTtagCompound != null) {
                tagsStr = rootNBTtagCompound.asString();
            }
        }

        if (itemStack.getItemMeta() == null) {
            return;
        }

        List<String> loreLst = itemStack.getItemMeta().getLore();
        if (loreLst != null) {
            String lore = StringUtils.join(loreLst.toArray());
            tagsStr += lore;
        }
        if (tagsStr == null || tagsStr.isEmpty()) {
            return;
        }
        if (tagsStr.contains(armor_no_knockback_tag)) {
            no_knockback = true;
        }
        if (tagsStr.contains(armor_no_burn_tag)) {
            no_burn = true;
        }
        if (tagsStr.contains(armor_no_damage_tag)) {
            no_damage = true;
        }
        if (tagsStr.contains(armor_reflect_tag)) {
            reflect = true;
        }
        if (tagsStr.contains(armor_prism_tag)) {
            prism = true;
        }
        if (tagsStr.contains(armor_focus_tag)) {
            focus = true;
        }
        Matcher durabilityRegExResult = Pattern.compile(armor_lasers_durability_regex).matcher(tagsStr);
        if (durabilityRegExResult.find()) {
            durabilityModifier = Integer.parseInt(durabilityRegExResult.group(1));
        }

    }

}
