package eu.lasersenigma.nms.v1_12_R1;

import eu.lasersenigma.nms.AItemStackFlagsProcessor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NMSItemStackFlagsProcessor extends AItemStackFlagsProcessor {

    public NMSItemStackFlagsProcessor() {
    }

    @Override
    public void process(ItemStack itemStack) {
        List<String> loreLst = itemStack.getItemMeta().getLore();
        if (loreLst == null || loreLst.isEmpty()) {
            return;
        }
        String lore = StringUtils.join(loreLst, "\n");
        if (lore.contains(armor_no_knockback_tag)) {
            no_knockback = true;
        }
        if (lore.contains(armor_no_burn_tag)) {
            no_burn = true;
        }
        if (lore.contains(armor_no_damage_tag)) {
            no_damage = true;
        }
        if (lore.contains(armor_reflect_tag)) {
            reflect = true;
        }
        if (lore.contains(armor_prism_tag)) {
            prism = true;
        }
        if (lore.contains(armor_focus_tag)) {
            focus = true;
        }
        Matcher durabilityRegExResult = Pattern.compile(armor_lasers_durability_regex).matcher(lore);
        if (durabilityRegExResult.find()) {
            durabilityModifier = Integer.parseInt(durabilityRegExResult.group(1));
        }
    }

}
