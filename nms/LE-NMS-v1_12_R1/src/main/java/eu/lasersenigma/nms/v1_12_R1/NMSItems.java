/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.nms.v1_12_R1;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author Benjamin
 */
public class NMSItems {

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final HashSet<Material> CROSSABLE_MATERIALS = new HashSet<>();

    public static ItemStack getItemStack(Item item, FileConfiguration translationConfigurationFile) {
        String name = null;
        String lore = null;
        ItemStack itemStack = null;
        if (item == null) {
            throw new UnsupportedOperationException("Error during item initialization");
        }
        boolean hasNameAndLore = item.hasNameAndLore();
        if (hasNameAndLore) {
            name = translationConfigurationFile.getString(Item.BASE_PATH + item.getConfigPath() + Item.NAME_PATH, item.getConfigPath());
            lore = translationConfigurationFile.getString(Item.BASE_PATH + item.getConfigPath() + Item.DESCRIPTION_PATH, "");
        }
        Boolean enchant = (Boolean) item.getAttributes().get(ItemAttribute.ENCHANT);
        switch (item.getType()) {
            case HEAD:
                String texture = (String) item.getAttributes().get(ItemAttribute.TEXTURE);
                if (item.hasNameAndLore()) {
                    return ItemUtils.getHead(texture, name, lore);
                } else {
                    return ItemUtils.getHead(texture, "", "");
                }
            case BLOCK:
                Material material = getItemMaterial(item);
                LasersColor color = (LasersColor) item.getAttributes().get(ItemAttribute.COLOR);
                if (!hasNameAndLore) {
                    if (color == null) {
                        itemStack = new ItemStack(material, 1);
                    } else {
                        itemStack = new ItemStack(material, 1, color.getDatavalue());
                    }
                } else {
                    if (color != null) {
                        itemStack = ItemUtils.getItem(material, name, lore, 1, color.getDatavalue());
                    } else {
                        itemStack = ItemUtils.getItem(material, name, lore);
                    }
                }
                break;
        }
        if (enchant != null && enchant) {
            itemStack = ItemUtils.addEnchantement(itemStack);
        }
        return itemStack;

    }

    public static String getSkullMetaStr(ItemMeta itemMeta) {
        if (!(itemMeta instanceof SkullMeta)) {
            return "";
        }

        String skullMetaStr = "";
        try {
            Field profileField;
            profileField = itemMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            Object profileObject = profileField.get(itemMeta);
            if (profileObject instanceof GameProfile) {
                GameProfile profile = (GameProfile) profileObject;
                Collection<Property> properties = profile.getProperties().get("textures");
                skullMetaStr = properties.stream().map(Property::getValue).reduce(skullMetaStr, String::concat);
            }
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e2) {
        }
        return skullMetaStr;
    }

    public static ItemStack getPlayerSkull(UUID key, String loreStr) {
        OfflinePlayer player = Bukkit.getOfflinePlayer(key);
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta skull = (SkullMeta) item.getItemMeta();
        skull.setDisplayName(player.getName());
        ArrayList<String> lore = new ArrayList<>(Arrays.asList(loreStr.split("\n")));
        skull.setLore(lore);
        skull.setOwner(player.getName());
        item.setItemMeta(skull);
        return item;
    }

    public static Material getItemMaterial(Item item) {
        return Material.getMaterial((String) item.getAttributes().get(ItemAttribute.MATERIAL));
    }

    public static void setBlock(Block block, Material material, LasersColor color) {
        block.setType(material);
        BlockState blockState = block.getState();
        blockState.setData(new MaterialData(material, (byte) color.getDatavalue()));
        blockState.update();
    }

    public static ItemStack getArmor(String tags) {
        return ItemUtils.getItem(Material.DIAMOND_CHESTPLATE, "", tags);
    }

    static void appendToItemLore(ItemStack itemStack, String txtToAdd) {
        ItemMeta itemMeta = itemStack.getItemMeta();
        List<String> loreLines = itemMeta.getLore();
        loreLines.add(txtToAdd);
        itemMeta.setLore(loreLines);
        itemStack.setItemMeta(itemMeta);
    }
}
