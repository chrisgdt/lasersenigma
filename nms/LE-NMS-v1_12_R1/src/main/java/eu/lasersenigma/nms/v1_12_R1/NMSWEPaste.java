package eu.lasersenigma.nms.v1_12_R1;

import com.sk89q.jnbt.NBTInputStream;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.extent.clipboard.io.SchematicReader;
import com.sk89q.worldedit.function.operation.ForwardExtentCopy;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.internal.LocalWorldAdapter;
import com.sk89q.worldedit.regions.Region;
import com.sk89q.worldedit.util.io.Closer;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldedit.world.registry.WorldData;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import eu.lasersenigma.exceptions.SchematicInvalidException;
import eu.lasersenigma.exceptions.WorldEditNotAvailableException;
import eu.lasersenigma.nms.IWEPaste;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public class NMSWEPaste extends ANMSWE implements IWEPaste {

    public static final int NB_MAXIMUM_BLOCK = 1000000000;

    private final Player player;
    private final Location playerLocation;
    private final byte[] worldEditSchematic;

    private Extent sourceExtent;
    private Region region;
    private Vector from;
    private Extent targetExtent;
    private Vector to;
    private EditSession editSession;

    public NMSWEPaste(Player player, byte[] worldEditSchematic) {
        this.player = player;
        this.worldEditSchematic = worldEditSchematic;
        this.playerLocation = player.getLocation();
    }

    @Override
    public Location getPlayerLocation() {
        return this.playerLocation;
    }

    @Override
    public boolean initializePaste() throws WorldEditNotAvailableException, InvalidSelectionException, SchematicInvalidException {
        WorldEditPlugin worldEditPlugin = getWorldEditPlugin();
        World world = LocalWorldAdapter.adapt(new BukkitWorld(player.getWorld()));
        editSession = worldEditPlugin.getWorldEdit().getEditSessionFactory().getEditSession(world, NB_MAXIMUM_BLOCK);
        editSession.enableQueue();
        try (Closer closer = Closer.create()) {
            WorldData worldData = world.getWorldData();
            ByteArrayInputStream bas = closer.register(new ByteArrayInputStream(worldEditSchematic));
            BufferedInputStream bis = closer.register(new BufferedInputStream(bas));
            NBTInputStream nbtStream = new NBTInputStream(new GZIPInputStream(bis));
            ClipboardReader reader = new SchematicReader(nbtStream);
            Clipboard clipboard = reader.read(worldData);
            sourceExtent = clipboard;
            region = clipboard.getRegion();
            from = clipboard.getOrigin();

            targetExtent = world;
            to = new Vector(playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ());
            closer.close();
            return true;
        } catch (IOException e) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.SEVERE, "An error occured while restoring schematic.", e);
            throw new SchematicInvalidException();
        }
    }

    @Override
    public void finalizePaste() throws SchematicInvalidException {
        try {
            ForwardExtentCopy copy = new ForwardExtentCopy(sourceExtent, region, from, targetExtent, to);
            Operations.completeLegacy(copy);
            editSession.flushQueue();
        } catch (MaxChangedBlocksException ex) {
            Logger.getLogger(NMSWECopy.class.getName()).log(Level.WARNING, "Exceeded the block limit while restoring schematic. limit in exception: {0}, limit passed by plugin: {1}", new Object[]{ex.getBlockLimit(), NB_MAXIMUM_BLOCK});
            throw new SchematicInvalidException();
        }
    }
}
