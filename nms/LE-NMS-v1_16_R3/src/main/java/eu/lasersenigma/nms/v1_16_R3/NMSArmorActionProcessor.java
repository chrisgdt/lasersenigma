package eu.lasersenigma.nms.v1_16_R3;

import eu.lasersenigma.items.AArmorActionProcessor;
import eu.lasersenigma.items.ArmorAction;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CommandBlock;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class NMSArmorActionProcessor extends AArmorActionProcessor {

    public NMSArmorActionProcessor(Player player, ArmorAction selectedArmorAction, Location newMaterialLocation, boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber, String armor_no_knockback_tag, String armor_no_burn_tag, String armor_no_damage_tag, String armor_reflect_tag, String armor_prism_tag, String armor_focus_tag, String armor_lasers_durability_regex) {
        super(player, selectedArmorAction, newMaterialLocation, noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber, armor_no_knockback_tag, armor_no_burn_tag, armor_no_damage_tag, armor_reflect_tag, armor_prism_tag, armor_focus_tag, armor_lasers_durability_regex);
    }

    @Override
    public void process() {
        String tags = getTagsString();
        switch (selectedArmorAction) {
            case GIVE_TO_NEAREST:
            case GIVE_TO_SELF:
                Player targetedPlayer = getTargetedPlayer();
                if (targetedPlayer != null) {
                    targetedPlayer.getInventory().addItem(NMSItems.getArmor(tags));
                }
                return;
            default:
                break;
        }
        Block b = newMaterialLocation.getBlock();
        b.setType(Material.COMMAND_BLOCK);
        CommandBlock cmdBlock = (CommandBlock) b.getState();
        String command = null;
        switch (selectedArmorAction) {
            case COMMAND_BLOCK_GIVE_TO_NEAREST:
                command = "/give @p minecraft:diamond_chestplate{Tags:[\"" + tags + "\"]} 1";
                break;
            case COMMAND_BLOCK_EQUIP_ARMOR_ON_NEAREST:
                command = "/replaceitem entity @p armor.chest minecraft:diamond_chestplate{Tags:[\"" + tags + "\"]} 1";
                break;
            case COMMAND_BLOCK_FILLS_CHEST:
                command = "/setblock ~ ~3 ~ air replace";
                Block b2 = newMaterialLocation.clone().add(0, 1, 0).getBlock();
                b2.setType(Material.COMMAND_BLOCK);
                CommandBlock cmdBlock2 = (CommandBlock) b2.getState();
                cmdBlock2.setCommand("/setblock ~ ~2 ~ chest{Items:[{Slot:0,id:diamond_chestplate,Count:1,tag:{Tags:[\"" + tags + "\"]}}]} replace");
                cmdBlock2.update();
                break;
            default:
                break;
        }
        cmdBlock.setCommand(command);
        cmdBlock.update();
    }

    @Override
    public ItemStack getArmorItemStack() {
        return NMSItems.getArmor(getTagsString());
    }
}
