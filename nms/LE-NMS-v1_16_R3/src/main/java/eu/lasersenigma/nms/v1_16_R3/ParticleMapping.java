package eu.lasersenigma.nms.v1_16_R3;

import org.bukkit.Particle;

import java.util.HashMap;

public enum ParticleMapping {
    REDSTONE("COLOURED_DUST");
    private static final HashMap<String, Particle> cachedSearch = new HashMap<>();
    private final String oldName;

    ParticleMapping(String oldName) {
        this.oldName = oldName;
    }

    public static Particle requestParticle(String name) {
        if (cachedSearch.containsKey(name.toUpperCase())) {
            return cachedSearch.get(name.toUpperCase());
        }

        Particle particle;

        try {
            particle = Particle.valueOf(name);
            if (particle != null) {
                cachedSearch.put(name, particle);
                return particle;
            }
        } catch (IllegalArgumentException e) {
        }
        for (ParticleMapping currentParticle : ParticleMapping.values()) {
            if (name.toUpperCase().equals(currentParticle.oldName)) {
                particle = Particle.valueOf(currentParticle.name());
                if (particle != null) {
                    cachedSearch.put(name, particle);
                    return particle;
                }
            }
        }
        return null;
    }
}
