/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.lasersenigma.nms.v1_16_R3;

import eu.lasersenigma.components.attributes.LasersColor;
import org.bukkit.DyeColor;
import org.bukkit.block.Block;

import static eu.lasersenigma.components.attributes.LasersColor.*;

/**
 * @author Benjamin
 */
public class NMSColors {


    @SuppressWarnings("null")
    public static LasersColor getColorFromBlock(Block block) {
        short colorShort = block.getData();
        DyeColor dyeColor = null;
        switch (colorShort) {
            case 0:
                dyeColor = DyeColor.WHITE;
                break;
            case 1:
                dyeColor = DyeColor.ORANGE;
                break;
            case 2:
                dyeColor = DyeColor.MAGENTA;
                break;
            case 3:
                dyeColor = DyeColor.LIGHT_BLUE;
                break;
            case 4:
                dyeColor = DyeColor.YELLOW;
                break;
            case 5:
                dyeColor = DyeColor.LIME;
                break;
            case 6:
                dyeColor = DyeColor.PINK;
                break;
            case 7:
                dyeColor = DyeColor.GRAY;
                break;
            case 8:
                dyeColor = DyeColor.LIGHT_GRAY;
                break;
            case 9:
                dyeColor = DyeColor.CYAN;
                break;
            case 10:
                dyeColor = DyeColor.PURPLE;
                break;
            case 11:
                dyeColor = DyeColor.BLUE;
                break;
            case 12:
                dyeColor = DyeColor.BROWN;
                break;
            case 13:
                dyeColor = DyeColor.GREEN;
                break;
            case 14:
                dyeColor = DyeColor.RED;
                break;
            case 15:
                dyeColor = DyeColor.BLACK;
                break;
            default:
                throw new IllegalArgumentException("unexpected dyecolor datavalue");
        }

        switch (dyeColor) {
            case BLACK:
                return BLACK;
            case BLUE:
                return BLUE;
            case BROWN:
                return BLACK;
            case CYAN:
                return BLUE;
            case GRAY:
                return WHITE;
            case GREEN:
                return GREEN;
            case LIGHT_BLUE:
                return CYAN;
            case LIME:
                return GREEN;
            case MAGENTA:
                return MAGENTA;
            case ORANGE:
                return YELLOW;
            case PINK:
                return MAGENTA;
            case PURPLE:
                return MAGENTA;
            case RED:
                return RED;
            case LIGHT_GRAY:
                return BLACK;
            case WHITE:
                return WHITE;
            case YELLOW:
                return YELLOW;
            default:
                return null;
        }
    }

    public static DyeColor getDyeColor(LasersColor color) {
        switch (color) {
            case BLACK:
                return DyeColor.BLACK;
            case RED:
                return DyeColor.RED;
            case BLUE:
                return DyeColor.BLUE;
            case GREEN:
                return DyeColor.LIME;
            case YELLOW:
                return DyeColor.YELLOW;
            case MAGENTA:
                return DyeColor.MAGENTA;
            case CYAN:
                return DyeColor.LIGHT_BLUE;
            case WHITE:
                return DyeColor.WHITE;
            default:
                return DyeColor.getByColor(color.getBukkitColor());
        }
    }
}
