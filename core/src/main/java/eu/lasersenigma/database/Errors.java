package eu.lasersenigma.database;

/**
 * database errors message class
 */
public class Errors {

    /**
     * gets the message corresponding to an error while executing an sql
     * statement
     *
     * @return the message to show
     */
    public static String sqlConnectionExecute() {
        return "Couldn't execute MySQL statement: ";
    }

    /**
     * gets the message corresponding to an error while closing the sql
     * connection
     *
     * @return the message to show
     */
    public static String sqlConnectionClose() {
        return "Failed to close MySQL connection: ";
    }

    /**
     * gets the message corresponding to an error while trying to retrieve an
     * sql connection
     *
     * @return the message to show
     */
    public static String noSQLConnection() {
        return "Unable to retreive MYSQL connection: ";
    }

    /**
     * gets the message corresponding to an error if a table is not found
     *
     * @return the message to show
     */
    public static String noTableFound() {
        return "Database Error: No Table Found";
    }
}
