package eu.lasersenigma.database;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.areas.ComponentFactory;
import eu.lasersenigma.areas.VictoryArea;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.exceptions.*;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.stats.AreaStats;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

import java.io.IOException;
import java.sql.*;
import java.time.Duration;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * the abstract database
 */
public abstract class Database {

    /**
     * the database table containing areas
     */
    public static final String AREA_TABLE = "area";
    /**
     * the database table containing components
     */
    public static final String COMPONENT_TABLE = "component";
    /**
     * the database table containing stats
     */
    public static final String STATS_TABLE = "playerstats";
    /**
     * the database table containing inventories
     */
    public static final String INVENTORIES_TABLE = "playersinventories";
    /**
     * the database table containing players checkpoints
     */
    public static final String CHECKPOINTS_TABLE = "playerscheckpoints";
    /**
     * the database table containing players checkpoints by world
     */
    public static final String CHECKPOINTS_WORLD_TABLE = "playersworldcheckpoints";
    /**
     * the database table containing victory areas
     */
    public static final String VICTORY_AREA_TABLE = "victoryareas";
    /**
     * the database table containing each players keys
     */
    public static final String PLAYERS_KEYS_TABLE = "playerskeys";
    /**
     * the database table containing each component's scheduled actions
     */
    public static final String COMPONENTS_SCHEDULED_ACTIONS_TABLE = "scheduledactions";

    /**
     * the plugin instance
     */
    LasersEnigmaPlugin plugin;
    /**
     * the connection to the database
     */
    Connection connection;

    /**
     * Constructor
     *
     * @param instance the plugin instance
     */
    public Database(LasersEnigmaPlugin instance) {
        plugin = instance;
    }

    /**
     * Retrieves the SQL connection
     *
     * @return SQL connection
     */
    public abstract Connection getSQLConnection();

    /**
     * load the database
     */
    public abstract void load();

    /**
     * initialize the connection to the database
     */
    public void initialize() {
        connection = getSQLConnection();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM " + AREA_TABLE);
            ResultSet rs = ps.executeQuery();
            close(ps, rs);
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, "Unable to retreive connection", ex);
        }
    }

    /**
     * Loads datas from the database
     *
     * @throws AreaOverlapException                   if an area overlaps another area
     * @throws AreaCrossWorldsException               if an area is in two worlds at the same
     *                                                time
     * @throws AreaNoDepthException                   if an area as no depth/width/height
     * @throws VictoryAreaMustHaveCommonWallException if a victory area does not
     *                                                have a common wall with the area that contains it
     */
    public void loadData() throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, VictoryAreaMustHaveCommonWallException {
        loadData(null);
    }

    public void loadData(String worldName) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, VictoryAreaMustHaveCommonWallException {
        Connection conn = null;
        PreparedStatement psArea = null, psComponent = null, psKeyChests = null, psStats = null, psInventories = null, psPlayersKeys = null, psVictoryArea = null, psWorldCheckpoints = null, psCheckpoints = null, psScheduledActions = null;
        ResultSet rsArea, rsComponent, rsKeyChests, rsStats, rsInventories, rsPlayersKeys, rsVictoryArea, rsWorldCheckpoints, rsCheckpoints, rsScheduledActions;
        try {
            conn = getSQLConnection();
            psArea = conn.prepareStatement("SELECT * FROM " + AREA_TABLE);

            rsArea = psArea.executeQuery();
            LinkedHashMap<Integer, Area> areasById = new LinkedHashMap<>();
            HashMap<Integer, Integer> linkedAreaIdByAreaId = new HashMap<>();
            HashSet<Integer> unloadedWorldsAreasIds = new HashSet<>();
            while (rsArea.next()) {
                String worldStr = rsArea.getString("world");
                int areaId = rsArea.getInt("_id");
                if (worldName != null && !worldName.equals(worldStr)) {
                    unloadedWorldsAreasIds.add(areaId);
                    continue;
                }
                World world = Bukkit.getServer().getWorld(worldStr);
                if (world == null) {
                    unloadedWorldsAreasIds.add(areaId);
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.WARNING, "The world \"{1}\" of the area {0} does not exist.", new Object[]{areaId, worldStr});
                    continue;
                }
                Location min = new Location(world, rsArea.getDouble("minX"), rsArea.getDouble("minY"), rsArea.getDouble("minZ"));
                Location max = new Location(world, rsArea.getDouble("maxX"), rsArea.getDouble("maxY"), rsArea.getDouble("maxZ"));
                boolean vrotation = rsArea.getBoolean("vrotation");
                boolean hrotation = rsArea.getBoolean("hrotation");
                boolean cvrotation = rsArea.getBoolean("cvrotation");
                boolean chrotation = rsArea.getBoolean("chrotation");
                boolean lsvrotation = rsArea.getBoolean("lsvrotation");
                boolean lshrotation = rsArea.getBoolean("lshrotation");
                boolean lrvrotation = rsArea.getBoolean("lrvrotation");
                boolean lrhrotation = rsArea.getBoolean("lrhrotation");
                boolean arspheresizing = rsArea.getBoolean("arspheresizing");
                linkedAreaIdByAreaId.put(areaId, rsArea.getInt("linked_area_id"));
                double checkpointX = rsArea.getDouble("checkpointX");
                double checkpointY = rsArea.getDouble("checkpointY");
                double checkpointZ = rsArea.getDouble("checkpointZ");
                float checkpointPitch = rsArea.getFloat("checkpointPitch");
                float checkpointYaw = rsArea.getFloat("checkpointYaw");
                DetectionMode mode = DetectionMode.valueOf(rsArea.getString("mode"));
                int minRange = rsArea.getInt("minRange");
                int maxRange = rsArea.getInt("maxRange");
                Location checkpoint;
                if (checkpointX == 0
                        && checkpointY == 0
                        && checkpointZ == 0
                        && checkpointPitch == 0
                        && checkpointYaw == 0) {
                    checkpoint = null;
                } else {
                    checkpoint = new Location(world, checkpointX, checkpointY, checkpointZ, checkpointYaw, checkpointPitch);
                }
                areasById.put(areaId, Areas.getInstance().createArea(min, max, areaId, vrotation, hrotation, cvrotation, chrotation, lsvrotation, lshrotation, lrvrotation, lrhrotation, arspheresizing, checkpoint, mode, minRange, maxRange));
            }
            psArea.close();

            linkedAreaIdByAreaId.forEach((key, value) -> {
                Area area = areasById.get(key);
                area.getStats().setLinkedArea(areasById.get(value));
            });

            psComponent = conn.prepareStatement("SELECT * FROM " + COMPONENT_TABLE + " WHERE type <> 'KEY_CHEST' AND type <> 'CALL_BUTTON'");
            rsComponent = psComponent.executeQuery();
            while (rsComponent.next()) {
                int areaId = rsComponent.getInt("area_id");
                Area area = areasById.get(areaId);
                ComponentType componentType = ComponentType.valueOf(rsComponent.getString("type"));
                double componentLocX = rsComponent.getDouble("x");
                double componentLocY = rsComponent.getDouble("y");
                double componentLocZ = rsComponent.getDouble("z");
                boolean updateAreaId = false;
                if (area == null || !area.containsLocation(new Location(area.getMinLocation().getWorld(), componentLocX, componentLocY, componentLocZ))) {
                    area = areasById.values().stream()
                            .filter(a -> a.containsLocation(new Location(a.getMinLocation().getWorld(), componentLocX, componentLocY, componentLocZ)))
                            .findAny().orElse(null);
                    if (area == null) {
                        if (!unloadedWorldsAreasIds.contains(areaId)) {
                            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, componentType.toString() + "at coordinates " + componentLocX + "," + componentLocY + "," + componentLocZ + " is not placed within an area (id: " + rsComponent.getInt("_id"));
                        }
                        continue;
                    } else {
                        updateAreaId = true;
                    }
                }
                Location componentLocation = new Location(
                        area.getMinLocation().getWorld(),
                        rsComponent.getDouble("x"),
                        rsComponent.getDouble("y"),
                        rsComponent.getDouble("z"));
                Rotation rot = new Rotation(rsComponent.getDouble("rotationX"), rsComponent.getDouble("rotationY"));
                Area linkedComponentArea = areasById.get(rsComponent.getInt("corresponding_lock_area_id"));
                IComponent linkedComponent = null;
                if (linkedComponentArea != null) {
                    linkedComponent = linkedComponentArea.getComponent(rsComponent.getInt("corresponding_lock_component_id"));
                }
                String locationsStr = rsComponent.getString("locations");
                LinkedList<Location> locationsLst = null;
                World w = area.getMinLocation().getWorld();
                if (locationsStr != null && !locationsStr.equals("")) {
                    locationsLst = new LinkedList<>();
                    String[] locationAsStrArray = locationsStr.split(";");
                    for (String locationAsStr : locationAsStrArray) {
                        String[] coordinates = locationAsStr.split("\\|");
                        if (coordinates.length != 3) {
                            throw new SQLException("An elevator floor should have a location x|y|z. Location field is badly formated.");
                        }
                        double x = Double.parseDouble(coordinates[0]);
                        double y = Double.parseDouble(coordinates[1]);
                        double z = Double.parseDouble(coordinates[2]);
                        locationsLst.add(new Location(w, x, y, z));
                    }
                    if (locationsLst.isEmpty()) {
                        throw new SQLException("An elevator floors locations should never be empty.");
                    }
                }

                LasersColor color = null;
                int colorInt = rsComponent.getInt("color");
                if (colorInt < 16 && colorInt >= 0) { //For elements that can but doesn't always contain mirrors
                    color = LasersColor.fromInt(colorInt);
                }

                IComponent component = ComponentFactory.createComponentFromDatabase(
                        rsComponent.getInt("_id"),
                        area,
                        componentType,
                        componentLocation,
                        rsComponent.getString("face"),
                        rot,
                        rsComponent.getInt("min"),
                        rsComponent.getInt("max"),
                        color,
                        rsComponent.getString("songFileName"),
                        rsComponent.getBoolean("loopmode"),
                        rsComponent.getBoolean("stopOnExit"),
                        rsComponent.getString("mode"),
                        linkedComponent,
                        locationsLst,
                        new Vector(
                                rsComponent.getDouble("vectorX"),
                                rsComponent.getDouble("vectorY"),
                                rsComponent.getDouble("vectorZ")),
                        rsComponent.getInt("lightLevel")
                );
                area.addComponent(component);
                if (updateAreaId) {
                    PreparedStatement psUpdateComponentsAreaId = null;
                    try {
                        psUpdateComponentsAreaId = conn.prepareStatement("UPDATE " + COMPONENT_TABLE + " SET area_id = ? WHERE _id = ?");
                        psUpdateComponentsAreaId.setInt(1, component.getComponentId());
                        psUpdateComponentsAreaId.setInt(2, area.getAreaId());
                        psUpdateComponentsAreaId.executeUpdate();
                        psUpdateComponentsAreaId.close();
                    } catch (SQLException ex) {
                        if (psUpdateComponentsAreaId != null) {
                            plugin.getLogger().log(Level.SEVERE, "Query:\n{0}", psUpdateComponentsAreaId.toString());
                        }
                        plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
                    } finally {
                        try {
                            if (psUpdateComponentsAreaId != null) {
                                psUpdateComponentsAreaId.close();
                            }
                        } catch (SQLException ex) {
                            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
                        }
                    }
                }
            }
            psComponent.close();

            psKeyChests = conn.prepareStatement("SELECT * FROM " + COMPONENT_TABLE + " WHERE type = 'KEY_CHEST' OR type = 'CALL_BUTTON'");
            rsKeyChests = psKeyChests.executeQuery();
            while (rsKeyChests.next()) {
                int areaId = rsKeyChests.getInt("area_id");
                Area area = areasById.get(areaId);
                ComponentType componentType = ComponentType.valueOf(rsKeyChests.getString("type"));
                double componentLocX = rsKeyChests.getDouble("x");
                double componentLocY = rsKeyChests.getDouble("y");
                double componentLocZ = rsKeyChests.getDouble("z");
                boolean updateAreaId = false;
                if (area == null || !area.containsLocation(new Location(area.getMinLocation().getWorld(), componentLocX, componentLocY, componentLocZ))) {
                    area = areasById.values().stream()
                            .filter(a -> a.containsLocation(new Location(a.getMinLocation().getWorld(), componentLocX, componentLocY, componentLocZ)))
                            .findAny().orElse(null);
                    if (area == null) {
                        if (!unloadedWorldsAreasIds.contains(areaId)) {
                            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, componentType.toString() + "at coordinates " + componentLocX + "," + componentLocY + "," + componentLocZ + " is not placed within an area");
                        }
                        continue;
                    } else {
                        updateAreaId = true;
                    }
                }
                Location componentLocation = new Location(
                        area.getMinLocation().getWorld(),
                        componentLocX,
                        componentLocY,
                        componentLocZ);
                Rotation rot = new Rotation(rsKeyChests.getDouble("rotationX"), rsKeyChests.getDouble("rotationY"));
                Area linkedComponentArea = areasById.get(rsKeyChests.getInt("corresponding_lock_area_id"));
                IComponent linkedComponent = null;
                if (linkedComponentArea != null) {
                    linkedComponent = linkedComponentArea.getComponent(rsKeyChests.getInt("corresponding_lock_component_id"));
                }

                LasersColor color = null;
                int colorInt = rsKeyChests.getInt("color");
                if (colorInt < 16 && colorInt >= 0) { //For elements that can but doesn't always contain mirrors
                    color = LasersColor.fromInt(colorInt);
                }

                IComponent component = ComponentFactory.createComponentFromDatabase(
                        rsKeyChests.getInt("_id"),
                        area,
                        componentType,
                        componentLocation,
                        rsKeyChests.getString("face"),
                        rot,
                        rsKeyChests.getInt("min"),
                        rsKeyChests.getInt("max"),
                        color,
                        rsKeyChests.getString("songFileName"),
                        rsKeyChests.getBoolean("loopmode"),
                        rsKeyChests.getBoolean("stopOnExit"),
                        rsKeyChests.getString("mode"),
                        linkedComponent,
                        null,
                        new Vector(
                                rsKeyChests.getDouble("vectorX"),
                                rsKeyChests.getDouble("vectorY"),
                                rsKeyChests.getDouble("vectorZ")),
                        rsKeyChests.getInt("lightLevel")
                );
                area.addComponent(component);
                if (updateAreaId) {
                    PreparedStatement psUpdateComponentsAreaId = null;
                    try {
                        psUpdateComponentsAreaId = conn.prepareStatement("UPDATE " + COMPONENT_TABLE + " SET area_id = ? WHERE _id = ?");
                        psUpdateComponentsAreaId.setInt(1, component.getComponentId());
                        psUpdateComponentsAreaId.setInt(2, area.getAreaId());
                        psUpdateComponentsAreaId.executeUpdate();
                        psUpdateComponentsAreaId.close();
                    } catch (SQLException ex) {
                        if (psUpdateComponentsAreaId != null) {
                            plugin.getLogger().log(Level.SEVERE, "Query:\n{0}", psUpdateComponentsAreaId.toString());
                        }
                        plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
                    } finally {
                        try {
                            if (psUpdateComponentsAreaId != null) {
                                psUpdateComponentsAreaId.close();
                            }
                        } catch (SQLException ex) {
                            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
                        }
                    }
                }
            }
            psKeyChests.close();

            psScheduledActions = conn.prepareStatement("SELECT * FROM " + COMPONENTS_SCHEDULED_ACTIONS_TABLE);
            rsScheduledActions = psScheduledActions.executeQuery();
            Map<IComponent, TreeMap<Integer, SavedAction>> scheduledActionsByIndexByComponent = new HashMap<>();
            while (rsScheduledActions.next()) {
                //Find component
                IComponent component = null;
                int componentId = rsScheduledActions.getInt("component_id");
                for (Area area : Areas.getInstance().getAreaSet()) {
                    component = area.getComponent(componentId);
                    if (component != null) {
                        break;
                    }
                }
                if (component == null) {
                    continue;
                }
                //Create saved action
                SavedAction scheduledAction = new SavedAction(component, ActionType.valueOf(rsScheduledActions.getString("type")), rsScheduledActions.getInt("delay"));
                Integer index = rsScheduledActions.getInt("position");
                TreeMap<Integer, SavedAction> componentScheduledActions = scheduledActionsByIndexByComponent.computeIfAbsent(component, k -> new TreeMap<>());
                componentScheduledActions.put(index, scheduledAction);
            }
            psScheduledActions.close();
            //Set the scheduled actions to this component.
            scheduledActionsByIndexByComponent.forEach((key, value) -> key.getScheduledActions().addAll(
                    value.values()
            ));

            for (Entry<Integer, Area> e : areasById.entrySet()) {
                psStats = conn.prepareStatement("SELECT * FROM " + STATS_TABLE + " WHERE area_id = ?");
                psStats.setInt(1, e.getKey());
                rsStats = psStats.executeQuery();
                HashMap<UUID, PlayerStats> areaPlayerStats = new HashMap<>();
                AreaStats stats = e.getValue().getStats();
                while (rsStats.next()) {
                    areaPlayerStats.put(
                            UUID.fromString(rsStats.getString("playeruuid")),
                            new PlayerStats(
                                    Duration.parse(rsStats.getString("duration")),
                                    rsStats.getInt("nbAction"),
                                    rsStats.getInt("nbStep")
                            )
                    );
                }
                stats.addStatsFromDb(areaPlayerStats);
                psStats.close();

                psVictoryArea = conn.prepareStatement("SELECT * FROM " + VICTORY_AREA_TABLE + " WHERE area_id = ?");
                psVictoryArea.setInt(1, e.getKey());
                rsVictoryArea = psVictoryArea.executeQuery();
                while (rsVictoryArea.next()) {
                    Area area = e.getValue();
                    Location minVArea = new Location(area.getMinLocation().getWorld(), rsVictoryArea.getDouble("minX"), rsVictoryArea.getDouble("minY"), rsVictoryArea.getDouble("minZ"));
                    Location maxVArea = new Location(area.getMinLocation().getWorld(), rsVictoryArea.getDouble("maxX"), rsVictoryArea.getDouble("maxY"), rsVictoryArea.getDouble("maxZ"));
                    e.getValue().addVictoryArea(new VictoryArea(e.getValue(), minVArea, maxVArea));
                }
                psVictoryArea.close();
            }

            psWorldCheckpoints = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_WORLD_TABLE);
            rsWorldCheckpoints = psWorldCheckpoints.executeQuery();
            while (rsWorldCheckpoints.next()) {
                LEPlayer player = LEPlayers.getInstance().findLEPlayer(UUID.fromString(rsWorldCheckpoints.getString("playeruuid")));
                String worldStr = rsWorldCheckpoints.getString("world");
                if (worldName != null && !worldName.equals(worldStr)) {
                    continue;
                }
                World world = Bukkit.getServer().getWorld(worldStr);
                if (world == null) {
                    continue;
                }
                double checkpointX = rsWorldCheckpoints.getDouble("checkpointX");
                double checkpointY = rsWorldCheckpoints.getDouble("checkpointY");
                double checkpointZ = rsWorldCheckpoints.getDouble("checkpointZ");
                float checkpointPitch = rsWorldCheckpoints.getFloat("checkpointPitch");
                float checkpointYaw = rsWorldCheckpoints.getFloat("checkpointYaw");
                Location checkpoint;
                if (checkpointX == 0
                        && checkpointY == 0
                        && checkpointZ == 0
                        && checkpointPitch == 0
                        && checkpointYaw == 0) {
                    checkpoint = null;
                } else {
                    checkpoint = new Location(world, checkpointX, checkpointY, checkpointZ, checkpointYaw, checkpointPitch);
                }
                double vcheckpointX = rsWorldCheckpoints.getDouble("vcheckpointX");
                double vcheckpointY = rsWorldCheckpoints.getDouble("vcheckpointY");
                double vcheckpointZ = rsWorldCheckpoints.getDouble("vcheckpointZ");
                float vcheckpointPitch = rsWorldCheckpoints.getFloat("vcheckpointPitch");
                float vcheckpointYaw = rsWorldCheckpoints.getFloat("vcheckpointYaw");
                Location vcheckpoint;
                if (vcheckpointX == 0
                        && vcheckpointY == 0
                        && vcheckpointZ == 0
                        && vcheckpointPitch == 0
                        && vcheckpointYaw == 0) {
                    vcheckpoint = null;
                } else {
                    vcheckpoint = new Location(world, vcheckpointX, vcheckpointY, vcheckpointZ, vcheckpointYaw, vcheckpointPitch);
                }
                player.setWorldCheckpointsFromDB(checkpoint, vcheckpoint, rsWorldCheckpoints.getBoolean("islastcheckpointreachedavictorycheckpoint"));
            }
            psWorldCheckpoints.close();

            psCheckpoints = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_TABLE);
            rsCheckpoints = psCheckpoints.executeQuery();
            while (rsCheckpoints.next()) {
                LEPlayer player = LEPlayers.getInstance().findLEPlayer(UUID.fromString(rsCheckpoints.getString("playeruuid")));
                String worldStr = rsCheckpoints.getString("world");
                if (!worldStr.equals("") && (Bukkit.getServer().getWorld(worldStr) == null || (worldName != null && !worldName.equals(worldStr)))) {
                    continue;
                }
                World world = !"".equals(worldStr) ? Bukkit.getServer().getWorld(worldStr) : null;
                String vworldStr = rsCheckpoints.getString("vworld");
                World vworld = !"".equals(worldStr) ? Bukkit.getServer().getWorld(vworldStr) : null;
                boolean isLastCheckpointReachedAVictoryCheckpoint = rsCheckpoints.getBoolean("islastcheckpointreachedavictorycheckpoint");
                player.setCheckpointsFromDB(world, vworld, isLastCheckpointReachedAVictoryCheckpoint);
            }
            psCheckpoints.close();

            psInventories = conn.prepareStatement("SELECT * FROM " + INVENTORIES_TABLE);
            rsInventories = psInventories.executeQuery();
            while (rsInventories.next()) {
                LEPlayer player = LEPlayers.getInstance().findLEPlayer(UUID.fromString(rsInventories.getString("playeruuid")));

                player.getInventoryManager().setSavedInventory(
                        NMSManager.getNMS().inventoryFromBase64(rsInventories.getString("savedinventory")),
                        rsInventories.getBoolean("isineditionmode"),
                        rsInventories.getBoolean("isrotationshortcutbaropened")
                );
            }
            psInventories.close();

            psPlayersKeys = conn.prepareStatement("SELECT * FROM " + PLAYERS_KEYS_TABLE);
            rsPlayersKeys = psPlayersKeys.executeQuery();
            while (rsPlayersKeys.next()) {
                LEPlayer player = LEPlayers.getInstance().findLEPlayer(UUID.fromString(rsPlayersKeys.getString("playeruuid")));
                Area area = areasById.get(rsPlayersKeys.getInt("keychest_area_id"));
                if (area == null) {
                    continue;
                }
                player.addKeyFromDB((KeyChest) area.getComponent(rsPlayersKeys.getInt("keychest_component_id")));
            }
            psPlayersKeys.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } catch (IOException | NoAreaFoundException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "No area found", ex);
        } finally {
            try {
                if (psArea != null) {
                    psArea.close();
                }
                if (psComponent != null) {
                    psComponent.close();
                }
                if (psKeyChests != null) {
                    psKeyChests.close();
                }
                if (psStats != null) {
                    psStats.close();
                }
                if (psInventories != null) {
                    psInventories.close();
                }
                if (psPlayersKeys != null) {
                    psPlayersKeys.close();
                }
                if (psVictoryArea != null) {
                    psVictoryArea.close();
                }
                if (psWorldCheckpoints != null) {
                    psWorldCheckpoints.close();
                }
                if (psCheckpoints != null) {
                    psCheckpoints.close();
                }
                if (psScheduledActions != null) {
                    psScheduledActions.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    /**
     * Create the given area in the database
     *
     * @param area a area to create in the database
     * @return the id of the area created in the database
     */
    public int createArea(Area area) {
        Connection conn = null;
        PreparedStatement psArea = null;
        int areaId = -1;
        try {
            conn = getSQLConnection();
            //save areas
            psArea = conn.prepareStatement("INSERT INTO " + AREA_TABLE + " ("
                    + "minX,minY,minZ,maxX,maxY,maxZ,"
                    + "world,"
                    + "vrotation,hrotation,cvrotation,chrotation,"
                    + "lsvrotation,lshrotation,lrvrotation,lrhrotation,"
                    + "arspheresizing,"
                    + "linked_area_id,"
                    + "checkpointX,checkpointY,checkpointZ,checkpointPitch,checkpointYaw,"
                    + "mode, minRange, maxRange"
                    + ") VALUES("
                    + "?,?,?,?,?,?,"
                    + "?,"
                    + "?,?,?,?,"
                    + "?,?,?,?,"
                    + "?,"
                    + "?,"
                    + "?,?,?,?,?,"
                    + "?,?,?)", Statement.RETURN_GENERATED_KEYS);
            psArea.setDouble(1, area.getMinLocation().getX());
            psArea.setDouble(2, area.getMinLocation().getY());
            psArea.setDouble(3, area.getMinLocation().getZ());
            psArea.setDouble(4, area.getMaxLocation().getX());
            psArea.setDouble(5, area.getMaxLocation().getY());
            psArea.setDouble(6, area.getMaxLocation().getZ());
            psArea.setString(7, Objects.requireNonNull(area.getMinLocation().getWorld()).getName());
            psArea.setBoolean(8, area.isVMirrorsRotationAllowed());
            psArea.setBoolean(9, area.isHMirrorsRotationAllowed());
            psArea.setBoolean(10, area.isVConcentratorsRotationAllowed());
            psArea.setBoolean(11, area.isHConcentratorsRotationAllowed());
            psArea.setBoolean(12, area.isVLaserSendersRotationAllowed());
            psArea.setBoolean(13, area.isHLaserSendersRotationAllowed());
            psArea.setBoolean(14, area.isVLaserReceiversRotationAllowed());
            psArea.setBoolean(15, area.isHLaserReceiversRotationAllowed());
            psArea.setBoolean(16, area.isAttractionRepulsionSpheresResizingAllowed());
            Area linkedArea = area.getStats().getLinkedArea();
            int linkedAreaId = -1;
            if (linkedArea != null) {
                linkedAreaId = linkedArea.getAreaId();
            }
            psArea.setInt(17, linkedAreaId);
            psArea.setDouble(18, 0);
            psArea.setDouble(19, 0);
            psArea.setDouble(20, 0);
            psArea.setFloat(21, 0);
            psArea.setFloat(22, 0);
            psArea.setString(23, area.getVictoryCriteria().toString());
            psArea.setInt(24, area.getMinimumRange());
            psArea.setInt(25, area.getMaximumRange());
            psArea.executeUpdate();
            //retrieve generated id
            ResultSet rs = psArea.getGeneratedKeys();
            rs.next();
            areaId = rs.getInt(1);
            //save components
            psArea.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psArea != null) {
                    psArea.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
        return areaId;
    }

    public void updateArea(Area area) {
        Connection conn = null;
        PreparedStatement psArea = null, psRemoveVictoryArea = null, psVictoryArea = null;
        try {
            conn = getSQLConnection();
            psArea = conn.prepareStatement("UPDATE " + AREA_TABLE + " SET vrotation = ?, hrotation = ?, cvrotation = ?, chrotation = ?, lsvrotation = ?, lshrotation = ?, lrvrotation = ?, lrhrotation = ?, arspheresizing = ?, linked_area_id = ?, "
                    + "checkpointX = ?,"
                    + "checkpointY = ?,"
                    + "checkpointZ = ?,"
                    + "checkpointPitch = ?,"
                    + "checkpointYaw = ?,"
                    + "mode = ?,"
                    + "minRange = ?,"
                    + "maxRange = ?,"
                    + "minX = ?,"
                    + "minY = ?,"
                    + "minZ = ?,"
                    + "maxX = ?,"
                    + "maxY = ?,"
                    + "maxZ = ? "
                    + "WHERE _id = ?");
            psArea.setBoolean(1, area.isVMirrorsRotationAllowed());
            psArea.setBoolean(2, area.isHMirrorsRotationAllowed());
            psArea.setBoolean(3, area.isVConcentratorsRotationAllowed());
            psArea.setBoolean(4, area.isHConcentratorsRotationAllowed());
            psArea.setBoolean(5, area.isVLaserSendersRotationAllowed());
            psArea.setBoolean(6, area.isHLaserSendersRotationAllowed());
            psArea.setBoolean(7, area.isVLaserReceiversRotationAllowed());
            psArea.setBoolean(8, area.isHLaserReceiversRotationAllowed());
            psArea.setBoolean(9, area.isAttractionRepulsionSpheresResizingAllowed());
            Area linkedArea = area.getStats().getLinkedArea();
            int linkedAreaId = -1;
            if (linkedArea != null) {
                linkedAreaId = linkedArea.getAreaId();
            }
            psArea.setInt(10, linkedAreaId);
            if (area.getCheckpoint() != null) {
                psArea.setDouble(11, area.getCheckpoint().getX());
                psArea.setDouble(12, area.getCheckpoint().getY());
                psArea.setDouble(13, area.getCheckpoint().getZ());
                psArea.setFloat(14, area.getCheckpoint().getPitch());
                psArea.setFloat(15, area.getCheckpoint().getYaw());
            } else {
                psArea.setDouble(11, 0);
                psArea.setDouble(12, 0);
                psArea.setDouble(13, 0);
                psArea.setFloat(14, 0);
                psArea.setFloat(15, 0);
            }
            psArea.setString(16, area.getVictoryCriteria().toString());
            psArea.setInt(17, area.getMinimumRange());
            psArea.setInt(18, area.getMaximumRange());
            psArea.setDouble(19, area.getMinLocation().getX());
            psArea.setDouble(20, area.getMinLocation().getY());
            psArea.setDouble(21, area.getMinLocation().getZ());
            psArea.setDouble(22, area.getMaxLocation().getX());
            psArea.setDouble(23, area.getMaxLocation().getY());
            psArea.setDouble(24, area.getMaxLocation().getZ());
            psArea.setInt(25, area.getAreaId());
            psArea.executeUpdate();
            psArea.close();

            psRemoveVictoryArea = conn.prepareStatement("DELETE FROM " + VICTORY_AREA_TABLE + " WHERE area_id = ?");
            psRemoveVictoryArea.setInt(1, area.getAreaId());
            psRemoveVictoryArea.executeUpdate();
            psRemoveVictoryArea.close();

            for (VictoryArea va : area.getVictoryAreas()) {
                psVictoryArea = conn.prepareStatement("INSERT INTO " + VICTORY_AREA_TABLE + " "
                        + "(area_id,minX,minY,minZ,maxX,maxY,maxZ)"
                        + " VALUES(?,?,?,?,?,?,?);");
                psVictoryArea.setInt(1, area.getAreaId());
                psVictoryArea.setDouble(2, va.getMin().getX());
                psVictoryArea.setDouble(3, va.getMin().getY());
                psVictoryArea.setDouble(4, va.getMin().getZ());
                psVictoryArea.setDouble(5, va.getMax().getX());
                psVictoryArea.setDouble(6, va.getMax().getY());
                psVictoryArea.setDouble(7, va.getMax().getZ());
                psVictoryArea.executeUpdate();
                psVictoryArea.close();
            }
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psArea != null) {
                    psArea.close();
                }
                if (psRemoveVictoryArea != null) {
                    psRemoveVictoryArea.close();
                }
                if (psVictoryArea != null) {
                    psVictoryArea.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    /**
     * Removes the area from the database
     *
     * @param area the area to remove
     */
    public void removeArea(Area area) {
        Connection conn = null;
        PreparedStatement psRemoveArea = null, psRemoveAreaStats = null, psRemoveVictoryArea = null;
        try {
            conn = getSQLConnection();
            //clear database
            psRemoveArea = conn.prepareStatement("DELETE FROM " + AREA_TABLE + " WHERE _id = ?");
            psRemoveArea.setInt(1, area.getAreaId());
            psRemoveArea.executeUpdate();
            psRemoveArea.close();
            psRemoveAreaStats = conn.prepareStatement("DELETE FROM " + STATS_TABLE + " WHERE area_id = ?");
            psRemoveAreaStats.setInt(1, area.getAreaId());
            psRemoveAreaStats.executeUpdate();
            psRemoveAreaStats.close();
            psRemoveVictoryArea = conn.prepareStatement("DELETE FROM " + VICTORY_AREA_TABLE + " WHERE area_id = ?");
            psRemoveVictoryArea.setInt(1, area.getAreaId());
            psRemoveVictoryArea.executeUpdate();
            psRemoveVictoryArea.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psRemoveArea != null) {
                    psRemoveArea.close();
                }
                if (psRemoveAreaStats != null) {
                    psRemoveAreaStats.close();
                }
                if (psRemoveVictoryArea != null) {
                    psRemoveVictoryArea.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    /**
     * Removes the component from the database
     *
     * @param component the component to remove
     */
    public void removeComponent(AComponent component) {

        Connection conn = null;
        PreparedStatement psRemoveComponent = null, psRemoveComponentScheduledActions = null;
        try {
            conn = getSQLConnection();
            psRemoveComponentScheduledActions = conn.prepareStatement("DELETE FROM " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + " WHERE component_id = ?");
            psRemoveComponentScheduledActions.setInt(1, component.getComponentId());
            psRemoveComponentScheduledActions.executeUpdate();
            psRemoveComponentScheduledActions.close();
            psRemoveComponent = conn.prepareStatement("DELETE FROM " + COMPONENT_TABLE + " WHERE _id = ?");
            psRemoveComponent.setInt(1, component.getComponentId());
            psRemoveComponent.executeUpdate();
            psRemoveComponent.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psRemoveComponent != null) {
                    psRemoveComponent.close();
                }
                if (psRemoveComponentScheduledActions != null) {
                    psRemoveComponentScheduledActions.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public int createComponent(AComponent component) {
        Connection conn = null;
        PreparedStatement psComponent = null;
        int componentId = -1;
        try {
            conn = getSQLConnection();
            psComponent = conn.prepareStatement("INSERT INTO " + COMPONENT_TABLE + " (area_id,type,x,y,z,face,rotationX,rotationY,rotationZ,min,max,color,songFileName,loopmode,stopOnExit,mode,corresponding_lock_area_id,corresponding_lock_component_id,vectorX,vectorY,vectorZ,locations,lightLevel) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            psComponent.setInt(1, component.getArea().getAreaId());

            ComponentType type = component.getType();
            psComponent.setString(2, type.toString());

            Location componentLocation;
            if (component instanceof Elevator) {
                componentLocation = ((Elevator) component).getCageMin();
            } else {
                componentLocation = component.getLocation();
            }

            psComponent.setDouble(3, componentLocation.getX());
            psComponent.setDouble(4, componentLocation.getY());
            psComponent.setDouble(5, componentLocation.getZ());

            double rotationX = 0;
            double rotationY = 0;
            double rotationZ = 0;
            String face = "";
            if (component instanceof AArmorStandComponent) {
                AArmorStandComponent rotatableComponent = ((AArmorStandComponent) component);
                rotationX = rotatableComponent.getSavedRotation().getX();
                rotationY = rotatableComponent.getSavedRotation().getY();
                rotationZ = rotatableComponent.getSavedRotation().getZ();
                face = rotatableComponent.getFace().toString();
            } else if (component instanceof MirrorChest) {
                face = ((MirrorChest) component).getFace().toString();
            }
            psComponent.setString(6, face);

            psComponent.setDouble(7, rotationX);
            psComponent.setDouble(8, rotationY);
            psComponent.setDouble(9, rotationZ);

            int min, max = 0;
            switch (type) {
                case MIRROR_CHEST:
                    min = ((MirrorChest) component).getNbMirrors();
                    break;
                case REDSTONE_WINNER_BLOCK:
                case DISAPPEARING_WINNER_BLOCK:
                case APPEARING_WINNER_BLOCK:
                case MUSIC_BLOCK:
                case ELEVATOR:
                case LASER_SENDER:
                    min = ((IDetectionComponent) component).getMin();
                    max = ((IDetectionComponent) component).getMax();
                    break;
                case ATTRACTION_REPULSION_SPHERE:
                    min = ((AttractionRepulsionSphere) component).getSize();
                    break;
                case KEY_CHEST:
                    min = ((KeyChest) component).getKeyNumber();
                    break;
                default:
                    min = 0;
                    break;
            }
            psComponent.setInt(10, min);
            psComponent.setInt(11, max);

            int color = 16;
            if (component instanceof IColorableComponent) {
                if (!(component instanceof LaserReceiver) || !((LaserReceiver)component).isAnyColorAccepted()) {
                    LasersColor savedColor = ((IColorableComponent) component).getSavedColor();
                    if (savedColor != null) {
                        color = savedColor.getInt();
                    }
                }
            }
            psComponent.setInt(12, color);

            String songFileName = "";
            boolean loopmode = false;
            boolean stopOnExit = false;
            if (component instanceof MusicBlock) {
                MusicBlock musicBlock = (MusicBlock) component;
                songFileName = musicBlock.getSongFileName();
                loopmode = musicBlock.isLoop();
                stopOnExit = musicBlock.isStopOnExit();
            }
            psComponent.setString(13, songFileName);
            psComponent.setBoolean(14, loopmode);
            psComponent.setBoolean(15, stopOnExit);
            String mode = "";
            if (component instanceof IDetectionComponent) {
                mode = ((IDetectionComponent) component).getMode().toString();
            } else if (component instanceof AttractionRepulsionSphere) {
                mode = ((AttractionRepulsionSphere) component).getMode().toString();
            } else if (component instanceof MirrorSupport) {
                mode = ((MirrorSupport) component).getMode().toString();
            }

            psComponent.setString(16, mode);
            int correspondingComponentAreaId = -1;
            int correspondingComponentComponentId = -1;
            if (component instanceof KeyChest) {
                Lock lock = ((KeyChest) component).getLock();
                correspondingComponentAreaId = lock.getArea().getAreaId();
                correspondingComponentComponentId = lock.getComponentId();
            }
            if (component instanceof CallButton) {
                Elevator elevator = ((CallButton) component).getElevator();
                correspondingComponentAreaId = elevator.getArea().getAreaId();
                correspondingComponentComponentId = elevator.getComponentId();
            }
            psComponent.setInt(17, correspondingComponentAreaId);
            psComponent.setInt(18, correspondingComponentComponentId);

            double vectorX = 0;
            double vectorY = 0;
            double vectorZ = 0;
            String locationsStr = "";
            if (component instanceof Elevator) {
                Elevator elevator = (Elevator) component;

                Vector cageVectorFromMinToMax = elevator.getCageVectorFromMinToMax();
                vectorX = cageVectorFromMinToMax.getX();
                vectorY = cageVectorFromMinToMax.getY();
                vectorZ = cageVectorFromMinToMax.getZ();

                ArrayList<String> locationsLstAsString = elevator.getFloorsCageMin().stream().map((floorLoc) -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(floorLoc.getBlockX());
                    sb.append("|");
                    sb.append(floorLoc.getBlockY());
                    sb.append("|");
                    sb.append(floorLoc.getBlockZ());
                    return sb;
                }).map(StringBuilder::toString).collect(Collectors.toCollection(ArrayList::new));

                locationsStr = String.join(";", locationsLstAsString);
            }
            psComponent.setDouble(19, vectorX);
            psComponent.setDouble(20, vectorY);
            psComponent.setDouble(21, vectorZ);
            psComponent.setString(22, locationsStr);
            int lightLevel = 0;
            if (component instanceof ILightComponent) {
                lightLevel = ((ILightComponent) component).getLightLevel();
            }
            psComponent.setInt(23, lightLevel);

            psComponent.executeUpdate();
            //retrieve generated id
            ResultSet rs = psComponent.getGeneratedKeys();
            rs.next();
            componentId = rs.getInt(1);
            psComponent.close();
        } catch (SQLException ex) {
            if (psComponent != null) {
                plugin.getLogger().log(Level.SEVERE, "Query:\n{0}", psComponent.toString());
            }
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psComponent != null) {
                    psComponent.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
        return componentId;
    }

    public void updateComponent(AComponent component) {

        Connection conn = null;
        PreparedStatement psComponent = null;
        try {
            conn = getSQLConnection();
            psComponent = conn.prepareStatement("UPDATE " + COMPONENT_TABLE + " SET type = ?, x = ?, y = ?, z = ?, rotationX = ?, rotationY = ?, rotationZ = ?, face = ?, min = ?, max = ?, color = ?, songFileName = ?, loopmode = ?, stopOnExit = ?, mode = ?, corresponding_lock_area_id = ?,corresponding_lock_component_id = ?, vectorX = ?, vectorY = ?, vectorZ = ?, locations = ?, lightLevel = ? WHERE _id = ?");

            ComponentType type = component.getType();
            psComponent.setString(1, type.toString());

            Location componentLocation;
            if (component instanceof Elevator) {
                componentLocation = ((Elevator) component).getCageMin();
            } else {
                componentLocation = component.getLocation();
            }

            psComponent.setDouble(2, componentLocation.getX());
            psComponent.setDouble(3, componentLocation.getY());
            psComponent.setDouble(4, componentLocation.getZ());

            double rotationX = 0, rotationY = 0, rotationZ = 0;
            String face = "";
            if (component instanceof AArmorStandComponent) {
                AArmorStandComponent rotatableComponent = ((AArmorStandComponent) component);
                rotationX = rotatableComponent.getSavedRotation().getX();
                rotationY = rotatableComponent.getSavedRotation().getY();
                rotationZ = rotatableComponent.getSavedRotation().getZ();
                face = rotatableComponent.getFace().toString();
            } else if (component instanceof MirrorChest) {
                face = ((MirrorChest) component).getFace().toString();
            }
            psComponent.setDouble(5, rotationX);
            psComponent.setDouble(6, rotationY);
            psComponent.setDouble(7, rotationZ);
            psComponent.setString(8, face);

            int min, max = 0;
            switch (type) {
                case MIRROR_CHEST:
                    min = ((MirrorChest) component).getNbMirrors();
                    break;
                case REDSTONE_WINNER_BLOCK:
                case DISAPPEARING_WINNER_BLOCK:
                case APPEARING_WINNER_BLOCK:
                case MUSIC_BLOCK:
                case ELEVATOR:
                case LASER_SENDER:
                    min = ((IDetectionComponent) component).getMin();
                    max = ((IDetectionComponent) component).getMax();
                    break;
                case ATTRACTION_REPULSION_SPHERE:
                    min = ((AttractionRepulsionSphere) component).getSize();
                    break;
                case KEY_CHEST:
                    min = ((KeyChest) component).getKeyNumber();
                    break;
                default:
                    min = 0;
                    break;
            }
            psComponent.setInt(9, min);
            psComponent.setInt(10, max);

            int color = 16;
            if (component instanceof IColorableComponent) {
                if (!(component instanceof LaserReceiver) || !((LaserReceiver)component).isAnyColorAccepted()) {
                    LasersColor savedColor = ((IColorableComponent) component).getSavedColor();
                    if (savedColor != null) {
                        color = savedColor.getInt();
                    }
                }
            }

            psComponent.setInt(11, color);

            String songFileName = "";
            boolean loopmode = false;
            boolean stopOnExit = false;
            if (component instanceof MusicBlock) {
                MusicBlock musicBlock = (MusicBlock) component;
                songFileName = musicBlock.getSongFileName();
                loopmode = musicBlock.isLoop();
                stopOnExit = musicBlock.isStopOnExit();
            }
            psComponent.setString(12, songFileName);
            psComponent.setBoolean(13, loopmode);
            psComponent.setBoolean(14, stopOnExit);

            String mode = "";
            if (component instanceof IDetectionComponent) {
                mode = ((IDetectionComponent) component).getMode().toString();
            } else if (component instanceof AttractionRepulsionSphere) {
                mode = ((AttractionRepulsionSphere) component).getMode().toString();
            } else if (component instanceof MirrorSupport) {
                mode = ((MirrorSupport) component).getMode().toString();
            }
            psComponent.setString(15, mode);
            int correspondingComponentAreaId = -1;
            int correspondingComponentComponentId = -1;
            if (component instanceof KeyChest) {
                Lock lock = ((KeyChest) component).getLock();
                correspondingComponentAreaId = lock.getArea().getAreaId();
                correspondingComponentComponentId = lock.getComponentId();
            }
            if (component instanceof CallButton) {
                Elevator elevator = ((CallButton) component).getElevator();
                correspondingComponentAreaId = elevator.getArea().getAreaId();
                correspondingComponentComponentId = elevator.getComponentId();
            }
            psComponent.setInt(16, correspondingComponentAreaId);
            psComponent.setInt(17, correspondingComponentComponentId);

            double vectorX = 0;
            double vectorY = 0;
            double vectorZ = 0;
            String locationsStr = "";
            if (component instanceof Elevator) {
                Elevator elevator = (Elevator) component;

                Vector cageVectorFromMinToMax = elevator.getCageVectorFromMinToMax();
                vectorX = cageVectorFromMinToMax.getX();
                vectorY = cageVectorFromMinToMax.getY();
                vectorZ = cageVectorFromMinToMax.getZ();

                ArrayList<String> locationsLstAsString = new ArrayList<>();

                elevator.getFloorsCageMin().stream().map((floorLoc) -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append(floorLoc.getBlockX());
                    sb.append("|");
                    sb.append(floorLoc.getBlockY());
                    sb.append("|");
                    sb.append(floorLoc.getBlockZ());
                    return sb;
                }).forEachOrdered((sb) -> locationsLstAsString.add(sb.toString()));
                locationsStr = String.join(";", locationsLstAsString);
            }
            psComponent.setDouble(18, vectorX);
            psComponent.setDouble(19, vectorY);
            psComponent.setDouble(20, vectorZ);
            psComponent.setString(21, locationsStr);
            int lightLevel = 0;
            if (component instanceof ILightComponent) {
                lightLevel = ((ILightComponent) component).getLightLevel();
            }
            psComponent.setInt(22, lightLevel);

            psComponent.setInt(23, component.getComponentId());
            psComponent.executeUpdate();
            psComponent.close();
        } catch (SQLException ex) {
            if (psComponent != null) {
                plugin.getLogger().log(Level.SEVERE, "Query:\n{0}", psComponent.toString());
            }
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psComponent != null) {
                    psComponent.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void createStats(int areaId, UUID playerUUID, PlayerStats playerStats) {

        Connection conn = null;
        PreparedStatement psStats = null;
        try {
            conn = getSQLConnection();
            //save areas
            psStats = conn.prepareStatement("INSERT INTO " + STATS_TABLE + " ("
                    + "area_id,playeruuid,"
                    + "duration,nbAction,nbStep"
                    + ") VALUES("
                    + "?,?,"
                    + "?,?,?)");
            psStats.setInt(1, areaId);
            psStats.setString(2, playerUUID.toString());
            psStats.setString(3, playerStats.getDuration().toString());
            psStats.setInt(4, playerStats.getNbAction());
            psStats.setInt(5, playerStats.getNbStep());
            psStats.executeUpdate();
            psStats.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psStats != null) {
                    psStats.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void updateStats(int areaId, UUID playerUUID, PlayerStats playerStats) {
        Connection conn = null;
        PreparedStatement psStats = null;
        try {
            conn = getSQLConnection();
            psStats = conn.prepareStatement("UPDATE " + STATS_TABLE + " SET duration = ?, nbAction = ?, nbStep = ? WHERE area_id = ? AND playeruuid = ?");
            psStats.setInt(4, areaId);
            psStats.setString(5, playerUUID.toString());
            psStats.setString(1, playerStats.getDuration().toString());
            psStats.setInt(2, playerStats.getNbAction());
            psStats.setInt(3, playerStats.getNbStep());
            psStats.executeUpdate();
            psStats.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psStats != null) {
                    psStats.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void clearStats(Area area) {
        Connection conn = null;
        PreparedStatement psRemoveArea = null;
        try {
            conn = getSQLConnection();
            psRemoveArea = conn.prepareStatement("DELETE FROM " + STATS_TABLE + " WHERE area_id = ?");
            psRemoveArea.setInt(1, area.getAreaId());
            psRemoveArea.executeUpdate();
            psRemoveArea.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psRemoveArea != null) {
                    psRemoveArea.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void clearSavedInventory(UUID playerUUID) {
        Connection conn = null;
        PreparedStatement psDeleteInventory = null;
        try {
            conn = getSQLConnection();
            psDeleteInventory = conn.prepareStatement("DELETE FROM " + INVENTORIES_TABLE + " WHERE playeruuid = ?");
            psDeleteInventory.setString(1, playerUUID.toString());
            psDeleteInventory.executeUpdate();
            psDeleteInventory.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psDeleteInventory != null) {
                    psDeleteInventory.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void saveInventory(UUID playerUUID, boolean isInEditionMode, boolean isRotationShortcutBarOpened, PlayerInventory inventory) {
        Connection conn = null;
        PreparedStatement psInventory = null;
        try {
            conn = getSQLConnection();
            //save areas
            psInventory = conn.prepareStatement("INSERT INTO " + INVENTORIES_TABLE + " (playeruuid, savedinventory, isineditionmode, isrotationshortcutbaropened) VALUES(?,?,?,?)");
            psInventory.setString(1, playerUUID.toString());
            psInventory.setString(2, NMSManager.getNMS().base64FromInventory(inventory));
            psInventory.setBoolean(3, isInEditionMode);
            psInventory.setBoolean(4, isRotationShortcutBarOpened);
            psInventory.executeUpdate();
            psInventory.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psInventory != null) {
                    psInventory.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void updatePlayerCheckpoint(UUID playerUUID, Location lastCheckpointReached, boolean isLastCheckpointReachedAVictoryCheckpoint) {
        Connection conn = null;
        PreparedStatement psCheckpointsExists = null, psWorldCheckpointsExists = null, psUpdateCheckpoints = null, psWorldUpdateCheckpoints = null;
        ResultSet rsCheckpointsExists, rsWorldCheckpointsExists;
        boolean update = true, updateWorld = true;
        if (lastCheckpointReached == null) {
            throw new IllegalArgumentException("lastCheckpointReached is null.");
        }
        try {
            conn = getSQLConnection();
            psCheckpointsExists = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_TABLE + " WHERE playeruuid = ?");
            psCheckpointsExists.setString(1, playerUUID.toString());
            rsCheckpointsExists = psCheckpointsExists.executeQuery();
            if (!rsCheckpointsExists.next()) {
                update = false;
            }
            rsCheckpointsExists.close();
            psCheckpointsExists.close();

            psWorldCheckpointsExists = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_WORLD_TABLE + " WHERE playeruuid = ? AND world = ?");
            psWorldCheckpointsExists.setString(1, playerUUID.toString());
            psWorldCheckpointsExists.setString(2, Objects.requireNonNull(lastCheckpointReached.getWorld()).getName());
            rsWorldCheckpointsExists = psWorldCheckpointsExists.executeQuery();
            if (!rsWorldCheckpointsExists.next()) {
                updateWorld = false;
            }
            rsWorldCheckpointsExists.close();
            psWorldCheckpointsExists.close();
            if (update) {
                if (isLastCheckpointReachedAVictoryCheckpoint) {
                    psUpdateCheckpoints = conn.prepareStatement("UPDATE " + CHECKPOINTS_TABLE + " SET "
                            + "vworld = ?,"
                            + "islastcheckpointreachedavictorycheckpoint = ?"
                            + " WHERE playeruuid = ?"
                    );
                } else {
                    psUpdateCheckpoints = conn.prepareStatement("UPDATE " + CHECKPOINTS_TABLE + " SET "
                            + "world = ?,"
                            + "islastcheckpointreachedavictorycheckpoint = ?"
                            + " WHERE playeruuid = ?"
                    );
                }
                psUpdateCheckpoints.setString(1, lastCheckpointReached.getWorld().getName());
                psUpdateCheckpoints.setBoolean(2, isLastCheckpointReachedAVictoryCheckpoint);
                psUpdateCheckpoints.setString(3, playerUUID.toString());
            } else {
                psUpdateCheckpoints = conn.prepareStatement("INSERT INTO " + CHECKPOINTS_TABLE + " ("
                        + "world,"
                        + "vworld,"
                        + "islastcheckpointreachedavictorycheckpoint,"
                        + "playeruuid"
                        + ") VALUES("
                        + "?,?,?,?"
                        + ")"
                );
                if (isLastCheckpointReachedAVictoryCheckpoint) {
                    psUpdateCheckpoints.setString(1, "");
                    psUpdateCheckpoints.setString(2, lastCheckpointReached.getWorld().getName());
                } else {
                    psUpdateCheckpoints.setString(1, lastCheckpointReached.getWorld().getName());
                    psUpdateCheckpoints.setString(2, "");
                }
                psUpdateCheckpoints.setBoolean(3, isLastCheckpointReachedAVictoryCheckpoint);
                psUpdateCheckpoints.setString(4, playerUUID.toString());
            }
            psUpdateCheckpoints.executeUpdate();
            psUpdateCheckpoints.close();

            if (updateWorld) {
                if (isLastCheckpointReachedAVictoryCheckpoint) {
                    psWorldUpdateCheckpoints = conn.prepareStatement("UPDATE " + CHECKPOINTS_WORLD_TABLE + " SET "
                            + "vcheckpointX = ?, vcheckpointY = ?, vcheckpointZ = ?, vcheckpointPitch = ?, vcheckpointYaw = ?,"
                            + "islastcheckpointreachedavictorycheckpoint = ?"
                            + " WHERE playeruuid = ? AND world = ?");
                } else {
                    psWorldUpdateCheckpoints = conn.prepareStatement("UPDATE " + CHECKPOINTS_WORLD_TABLE + " SET "
                            + "checkpointX = ?, checkpointY = ?, checkpointZ = ?, checkpointPitch = ?, checkpointYaw = ?,"
                            + "islastcheckpointreachedavictorycheckpoint = ?"
                            + " WHERE playeruuid = ? AND world = ?");
                }
                psWorldUpdateCheckpoints.setDouble(1, lastCheckpointReached.getX());
                psWorldUpdateCheckpoints.setDouble(2, lastCheckpointReached.getY());
                psWorldUpdateCheckpoints.setDouble(3, lastCheckpointReached.getZ());
                psWorldUpdateCheckpoints.setFloat(4, lastCheckpointReached.getPitch());
                psWorldUpdateCheckpoints.setFloat(5, lastCheckpointReached.getYaw());
                psWorldUpdateCheckpoints.setBoolean(6, isLastCheckpointReachedAVictoryCheckpoint);
                psWorldUpdateCheckpoints.setString(7, playerUUID.toString());
                psWorldUpdateCheckpoints.setString(8, lastCheckpointReached.getWorld().getName());
            } else {
                psWorldUpdateCheckpoints = conn.prepareStatement("INSERT INTO " + CHECKPOINTS_WORLD_TABLE + " ("
                        + "playeruuid,"
                        + "world, checkpointX, checkpointY, checkpointZ, checkpointPitch, checkpointYaw,"
                        + "vcheckpointX, vcheckpointY, vcheckpointZ, vcheckpointPitch, vcheckpointYaw,"
                        + "islastcheckpointreachedavictorycheckpoint"
                        + ") VALUES("
                        + "?,"
                        + "?,?,?,?,?,?,"
                        + "?,?,?,?,?,"
                        + "?"
                        + ")");
                psWorldUpdateCheckpoints.setString(1, playerUUID.toString());
                psWorldUpdateCheckpoints.setString(2, lastCheckpointReached.getWorld().getName());
                if (isLastCheckpointReachedAVictoryCheckpoint) {
                    psWorldUpdateCheckpoints.setDouble(3, 0);
                    psWorldUpdateCheckpoints.setDouble(4, 0);
                    psWorldUpdateCheckpoints.setDouble(5, 0);
                    psWorldUpdateCheckpoints.setFloat(6, 0);
                    psWorldUpdateCheckpoints.setFloat(7, 0);
                    psWorldUpdateCheckpoints.setDouble(8, lastCheckpointReached.getX());
                    psWorldUpdateCheckpoints.setDouble(9, lastCheckpointReached.getY());
                    psWorldUpdateCheckpoints.setDouble(10, lastCheckpointReached.getZ());
                    psWorldUpdateCheckpoints.setFloat(11, lastCheckpointReached.getPitch());
                    psWorldUpdateCheckpoints.setFloat(12, lastCheckpointReached.getYaw());
                } else {
                    psWorldUpdateCheckpoints.setDouble(3, lastCheckpointReached.getX());
                    psWorldUpdateCheckpoints.setDouble(4, lastCheckpointReached.getY());
                    psWorldUpdateCheckpoints.setDouble(5, lastCheckpointReached.getZ());
                    psWorldUpdateCheckpoints.setFloat(6, lastCheckpointReached.getPitch());
                    psWorldUpdateCheckpoints.setFloat(7, lastCheckpointReached.getYaw());
                    psWorldUpdateCheckpoints.setDouble(8, 0);
                    psWorldUpdateCheckpoints.setDouble(9, 0);
                    psWorldUpdateCheckpoints.setDouble(10, 0);
                    psWorldUpdateCheckpoints.setFloat(11, 0);
                    psWorldUpdateCheckpoints.setFloat(12, 0);
                }
                psWorldUpdateCheckpoints.setBoolean(13, isLastCheckpointReachedAVictoryCheckpoint);
            }
            psWorldUpdateCheckpoints.executeUpdate();
            psWorldUpdateCheckpoints.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psCheckpointsExists != null) {
                    psCheckpointsExists.close();
                }
                if (psWorldCheckpointsExists != null) {
                    psWorldCheckpointsExists.close();
                }
                if (psUpdateCheckpoints != null) {
                    psUpdateCheckpoints.close();
                }
                if (psWorldUpdateCheckpoints != null) {
                    psWorldUpdateCheckpoints.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void clearAllCheckpoints(UUID playerUUID) {
        Connection conn = null;
        PreparedStatement psClearWorldCheckpoints = null, psClearLastCheckpoints = null;
        try {
            conn = getSQLConnection();
            psClearWorldCheckpoints = conn.prepareStatement("DELETE FROM " + CHECKPOINTS_WORLD_TABLE + " WHERE playeruuid = ?");
            psClearWorldCheckpoints.setString(1, playerUUID.toString());
            psClearWorldCheckpoints.executeUpdate();
            psClearWorldCheckpoints.close();
            conn = getSQLConnection();
            psClearLastCheckpoints = conn.prepareStatement("DELETE FROM " + CHECKPOINTS_TABLE + " WHERE playeruuid = ?");
            psClearLastCheckpoints.setString(1, playerUUID.toString());
            psClearLastCheckpoints.executeUpdate();
            psClearLastCheckpoints.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psClearWorldCheckpoints != null) {
                    psClearWorldCheckpoints.close();
                }
                if (psClearLastCheckpoints != null) {
                    psClearLastCheckpoints.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void deleteWorldCheckpointData(UUID playerUUID, String worldName) {
        deleteWorldCheckpointData(playerUUID, worldName, null);
    }

    public void deleteWorldCheckpointData(UUID playerUUID, String worldName, Connection conn) {
        PreparedStatement psClearWorldCheckpoints = null;
        boolean existingConnection = conn != null;
        try {
            if (!existingConnection) conn = getSQLConnection();
            psClearWorldCheckpoints = conn.prepareStatement("DELETE FROM " + CHECKPOINTS_WORLD_TABLE + " WHERE world = ?" + (playerUUID == null ? "" : "AND playeruuid = ?"));
            psClearWorldCheckpoints.setString(1, worldName);
            if (playerUUID != null) {
                psClearWorldCheckpoints.setString(2, playerUUID.toString());
            }
            psClearWorldCheckpoints.executeUpdate();
            psClearWorldCheckpoints.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psClearWorldCheckpoints != null) {
                    psClearWorldCheckpoints.close();
                }
                if (!existingConnection && conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void deleteLastCheckpointReached(UUID playerUUID) {
        Connection conn = null;
        PreparedStatement psCheckpointsExists = null, psUpdateCheckpoints = null;
        ResultSet rsCheckpointsExists;
        boolean exists = true;
        try {
            conn = getSQLConnection();
            psCheckpointsExists = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_TABLE + " WHERE playeruuid = ?");
            psCheckpointsExists.setString(1, playerUUID.toString());
            rsCheckpointsExists = psCheckpointsExists.executeQuery();
            if (!rsCheckpointsExists.next()) {
                exists = false;
            }
            rsCheckpointsExists.close();
            psCheckpointsExists.close();
            if (exists) {
                conn = getSQLConnection();
                psUpdateCheckpoints = conn.prepareStatement("UPDATE " + CHECKPOINTS_TABLE + " SET "
                        + "world = ?,"
                        + "islastcheckpointreachedavictorycheckpoint = ?"
                        + " WHERE playeruuid = ?"
                );
                psUpdateCheckpoints.setString(1, "");
                psUpdateCheckpoints.setBoolean(2, true);
                psUpdateCheckpoints.setString(3, playerUUID.toString());
            }
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psCheckpointsExists != null) {
                    psCheckpointsExists.close();
                }
                if (psUpdateCheckpoints != null) {
                    psUpdateCheckpoints.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void deleteLastVictoryCheckpointReached(UUID playerUUID) {
        Connection conn = null;
        PreparedStatement psCheckpointsExists = null, psUpdateCheckpoints = null;
        ResultSet rsCheckpointsExists;
        boolean exists = true;
        try {
            conn = getSQLConnection();
            psCheckpointsExists = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_TABLE + " WHERE playeruuid = ?");
            psCheckpointsExists.setString(1, playerUUID.toString());
            rsCheckpointsExists = psCheckpointsExists.executeQuery();
            if (!rsCheckpointsExists.next()) {
                exists = false;
            }
            rsCheckpointsExists.close();
            psCheckpointsExists.close();
            if (exists) {
                conn = getSQLConnection();
                psUpdateCheckpoints = conn.prepareStatement("UPDATE " + CHECKPOINTS_TABLE + " SET "
                        + "vworld = ?,"
                        + "islastcheckpointreachedavictorycheckpoint = ?"
                        + " WHERE playeruuid = ?"
                );
                psUpdateCheckpoints.setString(1, "");
                psUpdateCheckpoints.setBoolean(2, false);
                psUpdateCheckpoints.setString(3, playerUUID.toString());
                psUpdateCheckpoints.executeUpdate();
                psUpdateCheckpoints.close();
            }
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psCheckpointsExists != null) {
                    psCheckpointsExists.close();
                }
                if (psUpdateCheckpoints != null) {
                    psUpdateCheckpoints.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void addPlayerKey(UUID playerUUID, KeyChest keyChest) {
        Connection conn = null;
        PreparedStatement psInsertKey = null;
        try {
            conn = getSQLConnection();
            //save areas
            psInsertKey = conn.prepareStatement("INSERT INTO " + INVENTORIES_TABLE + " (playeruuid, keychest_area_id, keychest_component_id) VALUES(?,?,?)");
            psInsertKey.setString(1, playerUUID.toString());
            psInsertKey.setInt(2, keyChest.getArea().getAreaId());
            psInsertKey.setInt(3, keyChest.getComponentId());
            psInsertKey.executeUpdate();
            psInsertKey.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psInsertKey != null) {
                    psInsertKey.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }

    }

    public void removePlayerKeys(UUID playerUUID, HashSet<KeyChest> keyChests) {
        keyChests.forEach((keyChest) -> {
            Connection conn = null;
            PreparedStatement psRemovePlayerKey = null;
            try {
                conn = getSQLConnection();
                psRemovePlayerKey = conn.prepareStatement("DELETE FROM " + PLAYERS_KEYS_TABLE + " WHERE playeruuid = ? AND keychest_component_id = ?");
                psRemovePlayerKey.setString(1, playerUUID.toString());
                psRemovePlayerKey.setInt(2, keyChest.getComponentId());
                psRemovePlayerKey.executeUpdate();
                psRemovePlayerKey.close();
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
            } finally {
                try {
                    if (psRemovePlayerKey != null) {
                        psRemovePlayerKey.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                } catch (SQLException ex) {
                    plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
                }
            }
        });
    }

    public void removePlayerKeys(UUID playerUUID) {
        Connection conn = null;
        PreparedStatement psRemovePlayerKey = null;
        try {
            conn = getSQLConnection();
            psRemovePlayerKey = conn.prepareStatement("DELETE FROM " + PLAYERS_KEYS_TABLE + " WHERE playeruuid = ?");
            psRemovePlayerKey.setString(1, playerUUID.toString());
            psRemovePlayerKey.executeUpdate();
            psRemovePlayerKey.close();
        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psRemovePlayerKey != null) {
                    psRemovePlayerKey.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    /**
     * closes the connection, prepared statement and result set
     *
     * @param ps the prepared statement
     * @param rs the result set
     */
    public void close(PreparedStatement ps, ResultSet rs) {
        try {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException ex) {
            Error.close(plugin, ex);
        }
    }

    public void updateComponentScheduledActions(IComponent component) {
        Connection conn = null;
        PreparedStatement psRemoveComponentScheduledActions = null, psInsertComponentScheduledActions = null;
        try {
            conn = getSQLConnection();
            int componentId = component.getComponentId();
            psRemoveComponentScheduledActions = conn.prepareStatement("DELETE FROM " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + " WHERE component_id = ?");
            psRemoveComponentScheduledActions.setInt(1, componentId);
            psRemoveComponentScheduledActions.executeUpdate();
            psRemoveComponentScheduledActions.close();
            final ArrayList<SavedAction> scheduledActions = component.getScheduledActions();
            for (SavedAction scheduledAction : scheduledActions) {
                psInsertComponentScheduledActions = conn.prepareStatement("INSERT INTO " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + " (component_id, position, type, delay) VALUES(?, ?, ?, ?)");
                psInsertComponentScheduledActions.setInt(1, componentId);
                psInsertComponentScheduledActions.setInt(2, scheduledAction.getIndex());
                psInsertComponentScheduledActions.setString(3, scheduledAction.getType().toString());
                psInsertComponentScheduledActions.setInt(4, scheduledAction.getDelay());
                psInsertComponentScheduledActions.executeUpdate();
                psInsertComponentScheduledActions.close();
            }

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psRemoveComponentScheduledActions != null) {
                    psRemoveComponentScheduledActions.close();
                }
                if (psInsertComponentScheduledActions != null) {
                    psInsertComponentScheduledActions.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void clearWorldData(String worldName) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "Database.clearWorldData(worldName: {0})", worldName);
        if (Bukkit.getWorld(worldName) != null) {
            plugin.getLogger().log(Level.SEVERE, "A world must be unloaded in order to delete all its data.");
            return;
        }
        Connection conn = null;

        PreparedStatement psArea = null, psWorldCheckpointsIndex = null, psDeleteFromCheckpointIndex = null, psUpdateCheckpointIndex = null, psRemoveAreaStats = null, psGetComponentsPerArea = null, psRemoveComponentScheduledActions = null, psRemoveLinkedComponents = null, psRemoveComponents = null, psRemoveKeys = null, psRemoveVictoryArea = null, psRemoveArea = null;
        ResultSet rsArea, rsWorldCheckpointsIndex, rsGetComponentsPerArea;
        try {
            conn = getSQLConnection();


            //Retrieve every area's id inside the world worldName
            psArea = conn.prepareStatement("SELECT _id FROM " + AREA_TABLE + " WHERE world = ?");
            psArea.setString(1, worldName);
            rsArea = psArea.executeQuery();
            HashSet<Integer> areasId = new HashSet<>();
            while (rsArea.next()) {
                areasId.add(rsArea.getInt("_id"));
            }
            psArea.close();

            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing playersworldcheckpoint data for this world.");
            //Remove playersworldcheckpoint
            deleteWorldCheckpointData(null, worldName, conn);


            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing checkpoints from checkpoints index (playerCheckpoint).");
            //Remove checkpoints from checkpoints index (playerCheckpoint)
            psWorldCheckpointsIndex = conn.prepareStatement("SELECT * FROM " + CHECKPOINTS_TABLE + " WHERE world = ? OR vworld = ?");
            psWorldCheckpointsIndex.setString(1, worldName);
            psWorldCheckpointsIndex.setString(2, worldName);
            rsWorldCheckpointsIndex = psWorldCheckpointsIndex.executeQuery();
            while (rsWorldCheckpointsIndex.next()) {
                String oldWorldName = rsWorldCheckpointsIndex.getString("world");
                String oldVWorldName = rsWorldCheckpointsIndex.getString("vworld");
                boolean shouldDeleteWorld = worldName.equals(oldWorldName);
                boolean shouldDeleteVWorld = worldName.equals(oldVWorldName);

                if ((shouldDeleteWorld || oldWorldName.equals("")) && (shouldDeleteVWorld || oldVWorldName.equals(""))) {
                    //Delete record
                    psDeleteFromCheckpointIndex = conn.prepareStatement("DELETE FROM " + CHECKPOINTS_TABLE + " WHERE playeruuid = ?");
                    psDeleteFromCheckpointIndex.setString(1, rsWorldCheckpointsIndex.getString("playeruuid"));
                    psDeleteFromCheckpointIndex.executeUpdate();
                    psDeleteFromCheckpointIndex.close();
                } else if (shouldDeleteWorld || shouldDeleteVWorld) {
                    //Update record
                    String newWorldName = shouldDeleteWorld ? "" : oldWorldName;
                    String newVWorldName = shouldDeleteVWorld ? "" : oldVWorldName;
                    boolean newIsLastCheckpointReachedAVictoryCheckpoint = shouldDeleteWorld;

                    psUpdateCheckpointIndex = conn.prepareStatement("UPDATE " + CHECKPOINTS_TABLE + " SET "
                            + "world = ?,"
                            + "vworld = ?,"
                            + "islastcheckpointreachedavictorycheckpoint = ?"
                            + " WHERE playeruuid = ?"
                    );
                    psUpdateCheckpointIndex.setString(1, newWorldName);
                    psUpdateCheckpointIndex.setString(2, newVWorldName);

                    psUpdateCheckpointIndex.setBoolean(3, newIsLastCheckpointReachedAVictoryCheckpoint);
                    psUpdateCheckpointIndex.setString(4, rsWorldCheckpointsIndex.getString("playeruuid"));
                    psUpdateCheckpointIndex.executeUpdate();
                    psUpdateCheckpointIndex.close();
                }
            }


            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing playerStats.");
            //remove playerStats
            for (Integer areaId : areasId) {
                psRemoveAreaStats = conn.prepareStatement("DELETE FROM " + STATS_TABLE + " WHERE area_id = ?");
                psRemoveAreaStats.setInt(1, areaId);
                psRemoveAreaStats.executeUpdate();
                psRemoveAreaStats.close();
            }

            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing playerKeys.");
            //Remove player keys
            for (Integer areaId : areasId) {
                psRemoveKeys = conn.prepareStatement("DELETE FROM " + PLAYERS_KEYS_TABLE + " WHERE keychest_area_id = ?");
                psRemoveKeys.setInt(1, areaId);
                psRemoveKeys.executeUpdate();
                psRemoveKeys.close();
            }

            //get components id
            HashMap<Integer, Boolean> componentsIdsAndLinkState = new HashMap<>(); //isLinked per component id map
            for (Integer areaId : areasId) {
                psGetComponentsPerArea = conn.prepareStatement("SELECT * FROM " + COMPONENT_TABLE + " WHERE area_id = ?");
                psGetComponentsPerArea.setInt(1, areaId);
                rsGetComponentsPerArea = psGetComponentsPerArea.executeQuery();
                while (rsGetComponentsPerArea.next()) {
                    boolean isLinked = rsGetComponentsPerArea.getInt("corresponding_lock_component_id") != -1;
                    componentsIdsAndLinkState.put(rsGetComponentsPerArea.getInt("_id"), isLinked);
                }
                psGetComponentsPerArea.close();
            }

            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing scheduled actions.");
            //Remove scheduled actions
            for (Entry<Integer, Boolean> entry : componentsIdsAndLinkState.entrySet()) {
                psRemoveComponentScheduledActions = conn.prepareStatement("DELETE FROM " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + " WHERE component_id = ?");
                psRemoveComponentScheduledActions.setInt(1, entry.getKey());
                psRemoveComponentScheduledActions.executeUpdate();
                psRemoveComponentScheduledActions.close();
            }


            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing linked components.");
            //Remove linked components
            for (Entry<Integer, Boolean> entry : componentsIdsAndLinkState.entrySet()) {
                Integer componentId = entry.getKey();
                Boolean isLinked = entry.getValue();
                if (isLinked) {
                    psRemoveLinkedComponents = conn.prepareStatement("DELETE FROM " + COMPONENT_TABLE + " WHERE _id = ?");
                    psRemoveLinkedComponents.setInt(1, componentId);
                    psRemoveLinkedComponents.executeUpdate();
                    psRemoveLinkedComponents.close();
                }
            }


            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing other components.");
            //Remove other components
            for (Entry<Integer, Boolean> entry : componentsIdsAndLinkState.entrySet()) {
                Integer componentId = entry.getKey();
                Boolean isLinked = entry.getValue();
                if (!isLinked) {
                    psRemoveComponents = conn.prepareStatement("DELETE FROM " + COMPONENT_TABLE + " WHERE _id = ?");
                    psRemoveComponents.setInt(1, componentId);
                    psRemoveComponents.executeUpdate();
                    psRemoveComponents.close();
                }
            }

            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing victory areas.");
            //remove victory areas
            for (Integer areaId : areasId) {
                psRemoveVictoryArea = conn.prepareStatement("DELETE FROM " + VICTORY_AREA_TABLE + " WHERE area_id = ?");
                psRemoveVictoryArea.setInt(1, areaId);
                psRemoveVictoryArea.executeUpdate();
                psRemoveVictoryArea.close();
            }

            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Removing areas.");
            //remove areas
            psRemoveArea = conn.prepareStatement("DELETE FROM " + AREA_TABLE + " WHERE world = ?");
            psRemoveArea.setString(1, worldName);
            psRemoveArea.executeUpdate();
            psRemoveArea.close();

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psArea != null) {
                    psArea.close();
                }
                if (psWorldCheckpointsIndex != null) {
                    psWorldCheckpointsIndex.close();
                }
                if (psDeleteFromCheckpointIndex != null) {
                    psDeleteFromCheckpointIndex.close();
                }
                if (psUpdateCheckpointIndex != null) {
                    psUpdateCheckpointIndex.close();
                }
                if (psRemoveAreaStats != null) {
                    psRemoveAreaStats.close();
                }
                if (psRemoveKeys != null) {
                    psRemoveKeys.close();
                }
                if (psGetComponentsPerArea != null) {
                    psGetComponentsPerArea.close();
                }
                if (psRemoveComponentScheduledActions != null) {
                    psRemoveComponentScheduledActions.close();
                }
                if (psRemoveLinkedComponents != null) {
                    psRemoveLinkedComponents.close();
                }
                if (psRemoveComponents != null) {
                    psRemoveComponents.close();
                }
                if (psRemoveVictoryArea != null) {
                    psRemoveVictoryArea.close();
                }
                if (psRemoveArea != null) {
                    psRemoveArea.close();
                }

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }
    }

    public void copyWorldData(String originWorldName, String destinationWorldName) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "Database.copyWorldData(originWorldName: {0}, destinationWorldName: {1})", originWorldName, destinationWorldName);
        if (Bukkit.getWorld(destinationWorldName) != null) {
            plugin.getLogger().log(Level.SEVERE, "A world must be unloaded in order to paste data inside it.");
            return;
        }
        Connection conn = null;

        PreparedStatement psGetArea = null, psGetVictoryArea = null, psGetComponent = null, psGetLinkedComponent = null, psGetScheduledAction = null;
        ResultSet rsGetArea, rsGetVictoryArea, rsGetComponent, rsGetLinkedComponent, rsGetScheduledAction;
        Map<Integer, PreparedStatement> psInsertAreas = new HashMap<>();
        Set<PreparedStatement> psInsertVictoryAreas = new HashSet<>();
        Map<Integer, PreparedStatement> psInsertLinkedComponents = new HashMap<>();
        Map<Integer, PreparedStatement> psInsertComponents = new HashMap<>();
        Set<PreparedStatement> psInsertScheduledActions = new HashSet<>();

        try {
            conn = getSQLConnection();

            //Retrieve every area's id inside the world worldName
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Copying areas.");
            psGetArea = conn.prepareStatement("SELECT * FROM " + AREA_TABLE + " WHERE world = ?");
            psGetArea.setString(1, originWorldName);
            rsGetArea = psGetArea.executeQuery();
            HashMap<Integer, Integer> areaSourceDestinationIds = new HashMap<>();
            while (rsGetArea.next()) {
                int oldAreaId = rsGetArea.getInt("_id");
                PreparedStatement psInsertArea = conn.prepareStatement("INSERT INTO " + AREA_TABLE + " ("
                        + "minX,minY,minZ,maxX,maxY,maxZ,"
                        + "world,"
                        + "vrotation,hrotation,cvrotation,chrotation,"
                        + "lsvrotation,lshrotation,lrvrotation,lrhrotation,"
                        + "arspheresizing,"
                        + "linked_area_id,"
                        + "checkpointX,checkpointY,checkpointZ,checkpointPitch,checkpointYaw,"
                        + "mode, minRange, maxRange"
                        + ") VALUES("
                        + "?,?,?,?,?,?,"
                        + "?,"
                        + "?,?,?,?,"
                        + "?,?,?,?,"
                        + "?,"
                        + "?,"
                        + "?,?,?,?,?,"
                        + "?,?,?)", Statement.RETURN_GENERATED_KEYS);
                psInsertArea.setDouble(1, rsGetArea.getDouble("minX"));
                psInsertArea.setDouble(2, rsGetArea.getDouble("minY"));
                psInsertArea.setDouble(3, rsGetArea.getDouble("minZ"));
                psInsertArea.setDouble(4, rsGetArea.getDouble("maxX"));
                psInsertArea.setDouble(5, rsGetArea.getDouble("maxY"));
                psInsertArea.setDouble(6, rsGetArea.getDouble("maxZ"));
                psInsertArea.setString(7, destinationWorldName);
                psInsertArea.setBoolean(8, rsGetArea.getBoolean("vrotation"));
                psInsertArea.setBoolean(9, rsGetArea.getBoolean("hrotation"));
                psInsertArea.setBoolean(10, rsGetArea.getBoolean("cvrotation"));
                psInsertArea.setBoolean(11, rsGetArea.getBoolean("chrotation"));
                psInsertArea.setBoolean(12, rsGetArea.getBoolean("lsvrotation"));
                psInsertArea.setBoolean(13, rsGetArea.getBoolean("lshrotation"));
                psInsertArea.setBoolean(14, rsGetArea.getBoolean("lrvrotation"));
                psInsertArea.setBoolean(15, rsGetArea.getBoolean("lrhrotation"));
                psInsertArea.setBoolean(16, rsGetArea.getBoolean("arspheresizing"));
                psInsertArea.setInt(17, -1);
                psInsertArea.setDouble(18, rsGetArea.getDouble("checkpointX"));
                psInsertArea.setDouble(19, rsGetArea.getDouble("checkpointY"));
                psInsertArea.setDouble(20, rsGetArea.getDouble("checkpointZ"));
                psInsertArea.setFloat(21, rsGetArea.getFloat("checkpointPitch"));
                psInsertArea.setFloat(22, rsGetArea.getFloat("checkpointYaw"));
                psInsertArea.setString(23, rsGetArea.getString("mode"));
                psInsertArea.setInt(24, rsGetArea.getInt("minRange"));
                psInsertArea.setInt(25, rsGetArea.getInt("maxRange"));
                psInsertAreas.put(oldAreaId, psInsertArea);
            }
            psGetArea.close();

            for (Entry<Integer, PreparedStatement> e : psInsertAreas.entrySet()) {
                e.getValue().executeUpdate();
                //retrieve generated id
                ResultSet rs = e.getValue().getGeneratedKeys();
                rs.next();
                int newAreaId = rs.getInt(1);
                areaSourceDestinationIds.put(e.getKey(), newAreaId);
                e.getValue().close();
            }

            //Copy victory areas
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Copying victory areas.");
            for (Entry<Integer, Integer> entry : areaSourceDestinationIds.entrySet()) {
                int oldAreaId = entry.getKey();
                int newAreaId = entry.getValue();
                psGetVictoryArea = conn.prepareStatement("SELECT * FROM " + VICTORY_AREA_TABLE + " WHERE area_id = ?");
                psGetVictoryArea.setInt(1, oldAreaId);
                rsGetVictoryArea = psGetVictoryArea.executeQuery();
                while (rsGetVictoryArea.next()) {
                    PreparedStatement psInsertVictoryArea = conn.prepareStatement("INSERT INTO " + VICTORY_AREA_TABLE + " "
                            + "(area_id,minX,minY,minZ,maxX,maxY,maxZ)"
                            + " VALUES(?,?,?,?,?,?,?);");
                    psInsertVictoryArea.setInt(1, newAreaId);
                    psInsertVictoryArea.setDouble(2, rsGetVictoryArea.getDouble("minX"));
                    psInsertVictoryArea.setDouble(3, rsGetVictoryArea.getDouble("minY"));
                    psInsertVictoryArea.setDouble(4, rsGetVictoryArea.getDouble("minZ"));
                    psInsertVictoryArea.setDouble(5, rsGetVictoryArea.getDouble("maxX"));
                    psInsertVictoryArea.setDouble(6, rsGetVictoryArea.getDouble("maxY"));
                    psInsertVictoryArea.setDouble(7, rsGetVictoryArea.getDouble("maxZ"));
                    psInsertVictoryAreas.add(psInsertVictoryArea);
                }
                psGetVictoryArea.close();
            }

            for (PreparedStatement psInsertVictoryArea : psInsertVictoryAreas) {
                psInsertVictoryArea.executeUpdate();
                psInsertVictoryArea.close();
            }


            //Copy unlinked components
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Copying unlinked components.");
            HashMap<Integer, Integer> componentSourceDestinationIds = new HashMap<>();
            for (Entry<Integer, Integer> entry : areaSourceDestinationIds.entrySet()) {
                psGetComponent = conn.prepareStatement("SELECT * FROM " + COMPONENT_TABLE + " WHERE type <> 'KEY_CHEST' AND type <> 'CALL_BUTTON' AND area_id = ?");
                psGetComponent.setInt(1, entry.getKey());
                rsGetComponent = psGetComponent.executeQuery();
                while (rsGetComponent.next()) {

                    PreparedStatement psInsertComponent = conn.prepareStatement("INSERT INTO " + COMPONENT_TABLE + " (area_id,type,x,y,z,face,rotationX,rotationY,rotationZ,min,max,color,songFileName,loopmode,stopOnExit,mode,corresponding_lock_area_id,corresponding_lock_component_id,vectorX,vectorY,vectorZ,locations,lightLevel) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                    int oldComponentId = rsGetComponent.getInt("_id");
                    psInsertComponent.setInt(1, entry.getValue());
                    psInsertComponent.setString(2, rsGetComponent.getString("type"));
                    psInsertComponent.setDouble(3, rsGetComponent.getDouble("x"));
                    psInsertComponent.setDouble(4, rsGetComponent.getDouble("y"));
                    psInsertComponent.setDouble(5, rsGetComponent.getDouble("z"));
                    psInsertComponent.setString(6, rsGetComponent.getString("face"));
                    psInsertComponent.setDouble(7, rsGetComponent.getFloat("rotationX"));
                    psInsertComponent.setDouble(8, rsGetComponent.getFloat("rotationY"));
                    psInsertComponent.setDouble(9, rsGetComponent.getFloat("rotationZ"));
                    psInsertComponent.setInt(10, rsGetComponent.getInt("min"));
                    psInsertComponent.setInt(11, rsGetComponent.getInt("max"));
                    psInsertComponent.setInt(12, rsGetComponent.getInt("color"));
                    psInsertComponent.setString(13, rsGetComponent.getString("songFileName"));
                    psInsertComponent.setBoolean(14, rsGetComponent.getBoolean("loopmode"));
                    psInsertComponent.setBoolean(15, rsGetComponent.getBoolean("stopOnExit"));
                    psInsertComponent.setString(16, rsGetComponent.getString("mode"));
                    psInsertComponent.setInt(17, rsGetComponent.getInt("corresponding_lock_area_id"));
                    psInsertComponent.setInt(18, rsGetComponent.getInt("corresponding_lock_component_id"));
                    psInsertComponent.setDouble(19, rsGetComponent.getDouble("vectorX"));
                    psInsertComponent.setDouble(20, rsGetComponent.getDouble("vectorY"));
                    psInsertComponent.setDouble(21, rsGetComponent.getDouble("vectorZ"));
                    psInsertComponent.setString(22, rsGetComponent.getString("locations"));
                    psInsertComponent.setInt(23, rsGetComponent.getInt("lightLevel"));
                    psInsertComponents.put(oldComponentId, psInsertComponent);
                }
                psGetComponent.close();
            }


            for (Entry<Integer, PreparedStatement> e : psInsertComponents.entrySet()) {
                e.getValue().executeUpdate();
                //retrieve generated id
                ResultSet rs = e.getValue().getGeneratedKeys();
                rs.next();
                int newComponentId = rs.getInt(1);
                componentSourceDestinationIds.put(e.getKey(), newComponentId);
                e.getValue().close();
            }

            //Copy linked components
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Copying linked components.");
            for (Entry<Integer, Integer> entry : areaSourceDestinationIds.entrySet()) {
                psGetLinkedComponent = conn.prepareStatement("SELECT * FROM " + COMPONENT_TABLE + " WHERE (type = 'KEY_CHEST' OR type = 'CALL_BUTTON') AND area_id = ?");
                psGetLinkedComponent.setInt(1, entry.getKey());
                rsGetLinkedComponent = psGetLinkedComponent.executeQuery();
                while (rsGetLinkedComponent.next()) {

                    PreparedStatement psInsertLinkedComponent = conn.prepareStatement("INSERT INTO " + COMPONENT_TABLE + " (area_id,type,x,y,z,face,rotationX,rotationY,rotationZ,min,max,color,songFileName,loopmode,stopOnExit,mode,corresponding_lock_area_id,corresponding_lock_component_id,vectorX,vectorY,vectorZ,locations,lightLevel) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                    psInsertLinkedComponent.setInt(1, entry.getValue());
                    psInsertLinkedComponent.setString(2, rsGetLinkedComponent.getString("type"));
                    psInsertLinkedComponent.setDouble(3, rsGetLinkedComponent.getDouble("x"));
                    psInsertLinkedComponent.setDouble(4, rsGetLinkedComponent.getDouble("y"));
                    psInsertLinkedComponent.setDouble(5, rsGetLinkedComponent.getDouble("z"));
                    psInsertLinkedComponent.setString(6, rsGetLinkedComponent.getString("face"));
                    psInsertLinkedComponent.setDouble(7, rsGetLinkedComponent.getFloat("rotationX"));
                    psInsertLinkedComponent.setDouble(8, rsGetLinkedComponent.getFloat("rotationY"));
                    psInsertLinkedComponent.setDouble(9, rsGetLinkedComponent.getFloat("rotationZ"));
                    psInsertLinkedComponent.setInt(10, rsGetLinkedComponent.getInt("min"));
                    psInsertLinkedComponent.setInt(11, rsGetLinkedComponent.getInt("max"));
                    psInsertLinkedComponent.setInt(12, rsGetLinkedComponent.getInt("color"));
                    psInsertLinkedComponent.setString(13, rsGetLinkedComponent.getString("songFileName"));
                    psInsertLinkedComponent.setBoolean(14, rsGetLinkedComponent.getBoolean("loopmode"));
                    psInsertLinkedComponent.setBoolean(15, rsGetLinkedComponent.getBoolean("stopOnExit"));
                    psInsertLinkedComponent.setString(16, rsGetLinkedComponent.getString("mode"));
                    psInsertLinkedComponent.setInt(17, rsGetLinkedComponent.getInt("corresponding_lock_area_id"));
                    psInsertLinkedComponent.setInt(18, rsGetLinkedComponent.getInt("corresponding_lock_component_id"));
                    psInsertLinkedComponent.setDouble(19, rsGetLinkedComponent.getDouble("vectorX"));
                    psInsertLinkedComponent.setDouble(20, rsGetLinkedComponent.getDouble("vectorY"));
                    psInsertLinkedComponent.setDouble(21, rsGetLinkedComponent.getDouble("vectorZ"));
                    psInsertLinkedComponent.setString(22, rsGetLinkedComponent.getString("locations"));
                    psInsertLinkedComponent.setInt(23, rsGetLinkedComponent.getInt("lightLevel"));
                    psInsertLinkedComponents.put(rsGetLinkedComponent.getInt("_id"), psInsertLinkedComponent);
                }
                psGetLinkedComponent.close();
            }


            for (Entry<Integer, PreparedStatement> e : psInsertLinkedComponents.entrySet()) {
                e.getValue().executeUpdate();
                //retrieve generated id
                ResultSet rs = e.getValue().getGeneratedKeys();
                rs.next();
                componentSourceDestinationIds.put(e.getKey(), rs.getInt(1));
                e.getValue().close();
            }

            //Copy scheduled actions
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "|- Copying scheduled actions.");
            for (Entry<Integer, Integer> entry : componentSourceDestinationIds.entrySet()) {
                int oldComponentId = entry.getKey();
                int newComponentId = entry.getValue();
                psGetScheduledAction = conn.prepareStatement("SELECT * FROM " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + " WHERE component_id = ?");
                psGetScheduledAction.setInt(1, oldComponentId);
                rsGetScheduledAction = psGetScheduledAction.executeQuery();
                while (rsGetScheduledAction.next()) {
                    PreparedStatement psInsertScheduledAction = conn.prepareStatement("INSERT INTO " + COMPONENTS_SCHEDULED_ACTIONS_TABLE + " (component_id, position, type, delay) VALUES(?, ?, ?, ?)");
                    psInsertScheduledAction.setInt(1, newComponentId);
                    psInsertScheduledAction.setInt(2, rsGetScheduledAction.getInt("position"));
                    psInsertScheduledAction.setString(3, rsGetScheduledAction.getString("type"));
                    psInsertScheduledAction.setInt(4, rsGetScheduledAction.getInt("delay"));
                    psInsertScheduledActions.add(psInsertScheduledAction);
                }
                psGetScheduledAction.close();
            }

            for (PreparedStatement psInsertScheduledAction : psInsertScheduledActions) {
                psInsertScheduledAction.executeUpdate();
                psInsertScheduledAction.close();
            }

        } catch (SQLException ex) {
            plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionExecute(), ex);
        } finally {
            try {
                if (psGetArea != null) {
                    psGetArea.close();
                }
                for (PreparedStatement psInsertArea : psInsertAreas.values()) {
                    if (psInsertArea != null) {
                        psInsertArea.close();
                    }
                }
                if (psGetVictoryArea != null) {
                    psGetVictoryArea.close();
                }
                for (PreparedStatement psInsertVictoryArea : psInsertVictoryAreas) {
                    if (psInsertVictoryArea != null) {
                        psInsertVictoryArea.close();
                    }
                }
                if (psGetComponent != null) {
                    psGetComponent.close();
                }
                if (psGetLinkedComponent != null) {
                    psGetLinkedComponent.close();
                }
                for (PreparedStatement psInsertComponent : psInsertComponents.values()) {
                    if (psInsertComponent != null) {
                        psInsertComponent.close();
                    }
                }
                for (PreparedStatement psInsertLinkedComponent : psInsertLinkedComponents.values()) {
                    if (psInsertLinkedComponent != null) {
                        psInsertLinkedComponent.close();
                    }
                }
                if (psGetScheduledAction != null) {
                    psGetScheduledAction.close();
                }
                for (PreparedStatement psInsertScheduledAction : psInsertScheduledActions) {
                    if (psInsertScheduledAction != null) {
                        psInsertScheduledAction.close();
                    }
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                plugin.getLogger().log(Level.SEVERE, Errors.sqlConnectionClose(), ex);
            }
        }

    }
}