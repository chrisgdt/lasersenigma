package eu.lasersenigma.items;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.nms.NMSManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This class handles the itemStack creation
 */
public class ItemsFactory {

    /**
     * the static instance of this class
     */
    private static ItemsFactory instance = null;
    /**
     * Map containing the items
     */
    private final Map<Item, ItemStack> items;

    /**
     * constructor
     */
    private ItemsFactory() {
        FileConfiguration translationConfigurationFile = LasersEnigmaPlugin.getInstance().getTranslationConfig().getCustomConfig();
        this.items = Arrays.stream(Item.values())
                .collect(
                        Collectors.toMap(item -> item, item -> NMSManager.getNMS().getItemStack(item, translationConfigurationFile))
                );
    }

    /**
     * Retrieves the instance of this class
     *
     * @return the instance of this class
     */
    public static ItemsFactory getInstance() {
        if (instance == null) {
            instance = new ItemsFactory();
        }
        return instance;
    }

    /**
     * Reload all items
     */
    public static void reload() {
        instance = new ItemsFactory();
    }

    public static boolean isSimilar(ItemStack item1, ItemStack item2) {
        if (item1 == item2) {
            return true;
        }
        if (item1 == null || item2 == null) {
            return false;
        }
        if (item2.getType() == item1.getType() && item2.getDurability() == item1.getDurability()) {
            ItemMeta item1Meta = item1.getItemMeta();
            ItemMeta item2Meta = item2.getItemMeta();
            if (item1Meta == item2Meta) {
                return true;
            }
            if (item1Meta == null || item2Meta == null) {
                return false;
            }
            if (item1Meta.hasDisplayName() != item2Meta.hasDisplayName()) {
                return false;
            }
            if (item1Meta.hasDisplayName()) {
                if (!item1Meta.getDisplayName().equals(item2Meta.getDisplayName())) {
                    return false;
                }
            }
            if (item1Meta.hasLore() != item2Meta.hasLore()) {
                return false;
            }
            if (item1Meta.hasLore()) {
                if (item1Meta.getLore().size() != item2Meta.getLore().size()) {
                    return false;
                }
                for (int index = 0; index < item1Meta.getLore().size(); index++) {
                    if (!item1Meta.getLore().get(index).equals(item2Meta.getLore().get(index))) {
                        return false;
                    }
                }
            }
            if (item1Meta.hasEnchants() != item2Meta.hasEnchants()) {
                return false;
            }
            if (item1Meta.hasEnchants()) {
                if (item1Meta.getEnchants().size() != item2Meta.getEnchants().size()) {
                    return false;
                }
                if (item1Meta.getEnchants().entrySet().stream().anyMatch((enchantInfo) -> (item1Meta.getEnchantLevel(enchantInfo.getKey()) != item2Meta.getEnchantLevel(enchantInfo.getKey())))) {
                    return false;
                }
            }
            if (item1Meta.getItemFlags().size() != item2Meta.getItemFlags().size()) {
                return false;
            }
            if (item1Meta.getItemFlags().stream().anyMatch((flag) -> (!item2Meta.hasItemFlag(flag)))) {
                return false;
            }
            if (item1Meta instanceof SkullMeta) {
                return getSkullMetaStr(item1Meta).equals(getSkullMetaStr(item2Meta));
            }
            return true;
        }
        return false;

    }

    private static String getSkullMetaStr(ItemMeta itemMeta) {
        return NMSManager.getNMS().getSkullMetaStr(itemMeta);
    }

    /**
     * retrieves an itemStack in the cache of this class
     *
     * @param item the item we want to retrieve
     * @return the item
     */
    public ItemStack getItemStack(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Item should not be null");
        }
        ItemStack itemStack = items.get(item);
        if (itemStack == null) {
            throw new IllegalStateException("No ItemStack for item " + item.toString());
        }
        return itemStack.clone();
    }

    public Item getSimilarItem(ItemStack itemStack) {
        if (itemStack == null) {
            return null;
        }
        for (Map.Entry<Item, ItemStack> entry : this.items.entrySet()) {
            if (isSimilar(entry.getValue(), itemStack)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public ItemStack getPlayerStatsItem(UUID key, String loreStr) {
        return NMSManager.getNMS().getPlayerSkull(key, loreStr);
    }

    public ItemStack getArmorItemStack(boolean noBurn, boolean noDamage, boolean noKnockback, boolean reflection, boolean focus, boolean prism, Integer durability) {
        String armor_no_knockback_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_knockback_tag");
        String armor_no_burn_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_burn_tag");
        String armor_no_damage_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_damage_tag");
        String armor_reflect_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_reflect_tag");
        String armor_prism_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_prism_tag");
        String armor_focus_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_focus_tag");
        String armor_lasers_durability_regex = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_lasers_reduces_durability_tag");

        return NMSManager.getNMS().getArmorActionProcessor(
                null, ArmorAction.GIVE_TO_NEAREST, null,
                noBurn, noDamage, noKnockback, reflection, focus, prism, durability,
                armor_no_knockback_tag, armor_no_burn_tag, armor_no_damage_tag, armor_reflect_tag, armor_prism_tag, armor_focus_tag, armor_lasers_durability_regex)
                .getArmorItemStack();
    }
}
