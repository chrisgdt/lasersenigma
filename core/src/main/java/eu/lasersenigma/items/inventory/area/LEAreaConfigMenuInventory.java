package eu.lasersenigma.items.inventory.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public final class LEAreaConfigMenuInventory extends ALEOpenableInventory {

    private final Area area;

    public LEAreaConfigMenuInventory(LEPlayer player, Area area) {
        super(player, Message.AREA_CONF_MENU_TITLE);
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> content = new ArrayList<>();
        //Line 1
        content.add(new ArrayList<>(Arrays.asList(
                Item.EMPTY, Item.AREA_CONFIG_MIRROR_SUPPORT, Item.AREA_CONFIG_CONCENTRATOR, Item.AREA_CONFIG_LASER_SENDER, Item.AREA_CONFIG_LASER_RECEIVER, Item.AREA_CONFIG_GRAVITY_SPHERE, Item.EMPTY, Item.AREA_CONFIG_MOD_LASER_RECEIVERS, getModLaserReceiverCheckbox()
        )));
        //Line 2
        content.add(new ArrayList<>(Arrays.asList(
                Item.AREA_CONFIG_HORIZONTAL, getMirrorSupportHCheckbox(), getConcentratorHCheckbox(), getLaserSenderHCheckbox(), getLaserReceiverHCheckbox(), getGravitySphereCheckbox(), Item.EMPTY, Item.AREA_CONFIG_MOD_PLAYERS, getModPlayersCheckbox()
        )));
        //Line 3
        content.add(new ArrayList<>(Arrays.asList(
                Item.AREA_CONFIG_VERTICAL, getMirrorSupportVCheckbox(), getConcentratorVCheckbox(), getLaserSenderVCheckbox(), getLaserReceiverVCheckbox(), Item.EMPTY, Item.EMPTY, Item.AREA_CONFIG_MOD_REDSTONE_SENSORS, getModRedstoneSensorsCheckbox()
        )));

        //Add ranges 
        ArrayList<Item> line4 = new ArrayList<>();
        ArrayList<Item> line5 = new ArrayList<>();
        ArrayList<Item> line6 = new ArrayList<>();
        if (area.getVictoryCriteria().isRangeModificationCompatible()) {
            int max = area.getMaximumRange();
            int min = area.getMinimumRange();
            if (min > 98) {
                line4.add(Item.EMPTY);
            } else {
                line4.add(Item.COMPONENT_MENU_RANGE_INCREASE_MIN);
            }
            line4 = addEmpty(line4, 3);
            if (max > 98) {
                line4.add(Item.EMPTY);
            } else {
                line4.add(Item.COMPONENT_MENU_RANGE_INCREASE_MAX);
            }

            line5.addAll(getNumberAsItems(area.getMinimumRange(), true));
            line5.add(Item.EMPTY);
            line5.addAll(getNumberAsItems(area.getMaximumRange(), true));
            if (min < 1) {
                line6.add(Item.EMPTY);
            } else {
                line6.add(Item.COMPONENT_MENU_RANGE_DECREASE_MIN);
            }
            line6 = addEmpty(line6, 3);
            if (max < 1) {
                line6.add(Item.EMPTY);
            } else {
                line6.add(Item.COMPONENT_MENU_RANGE_DECREASE_MAX);
            }
        } //Add victory areas
        else if (area.getVictoryCriteria() == DetectionMode.DETECTION_VICTORY_AREA) {
            line5.addAll(Arrays.asList(Item.EMPTY, Item.AREA_CONFIG_VICTORY_AREA_ADD, getVictoryAreaDelete()));
        }
        //Add the last detection mode
        line4 = addEmptyUntil(line4, 7);
        line4.add(Item.AREA_CONFIG_MOD_LOCKS);
        line4.add(getModLockCheckbox());
        line5 = addEmptyUntil(line5, 7);
        line5.add(Item.AREA_CONFIG_MOD_VICTORY_AREA);
        line5.add(getModVictoryAreaCheckbox());
        //Add checkpoints buttons
        line6 = addEmptyUntil(line6, 7);
        if (LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            line6.addAll(Arrays.asList(
                    Item.AREA_CONFIG_CHECKPOINT_DEFINE, getCheckpointDelete()
            ));
        }
        content.add(line4);
        content.add(line5);
        content.add(line6);
        return content;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEAreaConfigMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        switch (item) {
            case AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_CHECKED:
            case AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_UNCHECKED:
                AreaController.toggleHMirrorsRotation(player, area);
                break;
            case AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_CHECKED:
            case AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_UNCHECKED:
                AreaController.toggleVMirrorsRotation(player, area);
                break;
            case AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_CHECKED:
            case AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_UNCHECKED:
                AreaController.toggleHConcentratorsRotation(player, area);
                break;
            case AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_CHECKED:
            case AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_UNCHECKED:
                AreaController.toggleVConcentratorsRotation(player, area);
                break;
            case AREA_CONFIG_LASER_SENDER_H_CHECKBOX_CHECKED:
            case AREA_CONFIG_LASER_SENDER_H_CHECKBOX_UNCHECKED:
                AreaController.toggleHLaserSendersRotation(player, area);
                break;
            case AREA_CONFIG_LASER_SENDER_V_CHECKBOX_CHECKED:
            case AREA_CONFIG_LASER_SENDER_V_CHECKBOX_UNCHECKED:
                AreaController.toggleVLaserSendersRotation(player, area);
                break;
            case AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_CHECKED:
            case AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_UNCHECKED:
                AreaController.toggleHLaserReceiversRotation(player, area);
                break;
            case AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_CHECKED:
            case AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_UNCHECKED:
                AreaController.toggleVLaserReceiversRotation(player, area);
                break;
            case AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_CHECKED:
            case AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_UNCHECKED:
                AreaController.toggleARSpheresResizing(player, area);
                break;
            case AREA_CONFIG_CHECKPOINT_DEFINE:
                AreaController.defineCheckpoint(player, area);
                break;
            case AREA_CONFIG_CHECKPOINT_DELETE:
                AreaController.deleteCheckpoint(player, area);
                break;
            case AREA_CONFIG_VICTORY_AREA_ADD:
                player.getInventoryManager().openLEInventory(new LEAreaVictoryAreaShortcutBarInventory(player, area));
                break;
            case AREA_CONFIG_VICTORY_AREA_DELETE:
                AreaController.deleteAllVictoryAreas(player, area);
                break;
            case AREA_CONFIG_MOD_LASER_RECEIVERS:
                break;
            case AREA_CONFIG_MOD_LASER_RECEIVERS_CHECKED:
                break;
            case AREA_CONFIG_MOD_LASER_RECEIVERS_UNCHECKED:
                AreaController.setVictoryCriteria(player, area, DetectionMode.DETECTION_LASER_RECEIVERS);
                break;
            case AREA_CONFIG_MOD_PLAYERS:
                break;
            case AREA_CONFIG_MOD_PLAYERS_CHECKED:
                break;
            case AREA_CONFIG_MOD_PLAYERS_UNCHECKED:
                AreaController.setVictoryCriteria(player, area, DetectionMode.DETECTION_PLAYERS);
                break;
            case AREA_CONFIG_MOD_REDSTONE_SENSORS:
                break;
            case AREA_CONFIG_MOD_REDSTONE_SENSORS_CHECKED:
                break;
            case AREA_CONFIG_MOD_REDSTONE_SENSORS_UNCHECKED:
                AreaController.setVictoryCriteria(player, area, DetectionMode.DETECTION_REDSTONE_SENSORS);
                break;
            case AREA_CONFIG_MOD_LOCKS:
                break;
            case AREA_CONFIG_MOD_LOCKS_CHECKED:
                break;
            case AREA_CONFIG_MOD_LOCKS_UNCHECKED:
                AreaController.setVictoryCriteria(player, area, DetectionMode.DETECTION_LOCKS);
                break;
            case AREA_CONFIG_MOD_VICTORY_AREA:
                break;
            case AREA_CONFIG_MOD_VICTORY_AREA_CHECKED:
                break;
            case AREA_CONFIG_MOD_VICTORY_AREA_UNCHECKED:
                AreaController.setVictoryCriteria(player, area, DetectionMode.DETECTION_VICTORY_AREA);
                break;
            case COMPONENT_MENU_RANGE_INCREASE_MIN:
                AreaController.increaseMin(player, area);
                break;
            case COMPONENT_MENU_RANGE_INCREASE_MAX:
                AreaController.increaseMax(player, area);
                break;
            case COMPONENT_MENU_RANGE_DECREASE_MIN:
                AreaController.decreaseMin(player, area);
                break;
            case COMPONENT_MENU_RANGE_DECREASE_MAX:
                AreaController.decreaseMax(player, area);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.AREA_CONFIG_MIRROR_SUPPORT,
                Item.AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_CONCENTRATOR,
                Item.AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_LASER_SENDER,
                Item.AREA_CONFIG_LASER_SENDER_H_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_LASER_SENDER_H_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_LASER_SENDER_V_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_LASER_SENDER_V_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_LASER_RECEIVER,
                Item.AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_GRAVITY_SPHERE,
                Item.AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_CHECKED,
                Item.AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_UNCHECKED,
                Item.AREA_CONFIG_HORIZONTAL,
                Item.AREA_CONFIG_VERTICAL,
                Item.AREA_CONFIG_CHECKPOINT_DEFINE,
                Item.AREA_CONFIG_CHECKPOINT_DELETE,
                Item.AREA_CONFIG_VICTORY_AREA_ADD,
                Item.AREA_CONFIG_VICTORY_AREA_DELETE,
                Item.AREA_CONFIG_MOD_LASER_RECEIVERS,
                Item.AREA_CONFIG_MOD_LASER_RECEIVERS_CHECKED,
                Item.AREA_CONFIG_MOD_LASER_RECEIVERS_UNCHECKED,
                Item.AREA_CONFIG_MOD_PLAYERS,
                Item.AREA_CONFIG_MOD_PLAYERS_CHECKED,
                Item.AREA_CONFIG_MOD_PLAYERS_UNCHECKED,
                Item.AREA_CONFIG_MOD_REDSTONE_SENSORS,
                Item.AREA_CONFIG_MOD_REDSTONE_SENSORS_CHECKED,
                Item.AREA_CONFIG_MOD_REDSTONE_SENSORS_UNCHECKED,
                Item.AREA_CONFIG_MOD_LOCKS,
                Item.AREA_CONFIG_MOD_LOCKS_CHECKED,
                Item.AREA_CONFIG_MOD_LOCKS_UNCHECKED,
                Item.AREA_CONFIG_MOD_VICTORY_AREA,
                Item.AREA_CONFIG_MOD_VICTORY_AREA_CHECKED,
                Item.AREA_CONFIG_MOD_VICTORY_AREA_UNCHECKED,
                Item.COMPONENT_MENU_RANGE_INCREASE_MIN,
                Item.COMPONENT_MENU_RANGE_INCREASE_MAX,
                Item.COMPONENT_MENU_RANGE_DECREASE_MIN,
                Item.COMPONENT_MENU_RANGE_DECREASE_MAX
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.AREA_CONFIGURATION_MENU;
    }

    private Item getMirrorSupportHCheckbox() {
        if (area.isHMirrorsRotationAllowed()) {
            return Item.AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_MIRROR_SUPPORT_H_CHECKBOX_UNCHECKED;

    }

    private Item getMirrorSupportVCheckbox() {
        if (area.isVMirrorsRotationAllowed()) {
            return Item.AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_MIRROR_SUPPORT_V_CHECKBOX_UNCHECKED;
    }

    private Item getConcentratorHCheckbox() {
        if (area.isHConcentratorsRotationAllowed()) {
            return Item.AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_CONCENTRATOR_H_CHECKBOX_UNCHECKED;
    }

    private Item getConcentratorVCheckbox() {
        if (area.isVConcentratorsRotationAllowed()) {
            return Item.AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_CONCENTRATOR_V_CHECKBOX_UNCHECKED;
    }

    private Item getLaserSenderHCheckbox() {
        if (area.isHLaserSendersRotationAllowed()) {
            return Item.AREA_CONFIG_LASER_SENDER_H_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_LASER_SENDER_H_CHECKBOX_UNCHECKED;
    }

    private Item getLaserSenderVCheckbox() {
        if (area.isVLaserSendersRotationAllowed()) {
            return Item.AREA_CONFIG_LASER_SENDER_V_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_LASER_SENDER_V_CHECKBOX_UNCHECKED;
    }

    private Item getLaserReceiverHCheckbox() {
        if (area.isHLaserReceiversRotationAllowed()) {
            return Item.AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_LASER_RECEIVER_H_CHECKBOX_UNCHECKED;
    }

    private Item getLaserReceiverVCheckbox() {
        if (area.isVLaserReceiversRotationAllowed()) {
            return Item.AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_LASER_RECEIVER_V_CHECKBOX_UNCHECKED;
    }

    private Item getGravitySphereCheckbox() {
        if (area.isAttractionRepulsionSpheresResizingAllowed()) {
            return Item.AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_CHECKED;
        }
        return Item.AREA_CONFIG_GRAVITY_SPHERE_CHECKBOX_UNCHECKED;
    }

    private Item getModLaserReceiverCheckbox() {
        if (area.getVictoryCriteria() == DetectionMode.DETECTION_LASER_RECEIVERS) {
            return Item.AREA_CONFIG_MOD_LASER_RECEIVERS_CHECKED;
        }
        return Item.AREA_CONFIG_MOD_LASER_RECEIVERS_UNCHECKED;
    }

    private Item getModPlayersCheckbox() {
        if (area.getVictoryCriteria() == DetectionMode.DETECTION_PLAYERS) {
            return Item.AREA_CONFIG_MOD_PLAYERS_CHECKED;
        }
        return Item.AREA_CONFIG_MOD_PLAYERS_UNCHECKED;
    }

    private Item getModRedstoneSensorsCheckbox() {
        if (area.getVictoryCriteria() == DetectionMode.DETECTION_REDSTONE_SENSORS) {
            return Item.AREA_CONFIG_MOD_REDSTONE_SENSORS_CHECKED;
        }
        return Item.AREA_CONFIG_MOD_REDSTONE_SENSORS_UNCHECKED;
    }

    private Item getModVictoryAreaCheckbox() {
        if (area.getVictoryCriteria() == DetectionMode.DETECTION_VICTORY_AREA) {
            return Item.AREA_CONFIG_MOD_VICTORY_AREA_CHECKED;
        }
        return Item.AREA_CONFIG_MOD_VICTORY_AREA_UNCHECKED;
    }

    private Item getModLockCheckbox() {
        if (area.getVictoryCriteria() == DetectionMode.DETECTION_LOCKS) {
            return Item.AREA_CONFIG_MOD_LOCKS_CHECKED;
        }
        return Item.AREA_CONFIG_MOD_LOCKS_UNCHECKED;
    }

    private Item getCheckpointDelete() {
        return area.getCheckpoint() == null ? Item.EMPTY : Item.AREA_CONFIG_CHECKPOINT_DELETE;
    }

    private Item getVictoryAreaDelete() {
        return area.getVictoryAreas().isEmpty() ? Item.EMPTY : Item.AREA_CONFIG_VICTORY_AREA_DELETE;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return this.area;
    }
}
