package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class LEKeyChestCreationShortcutBarInventory extends ALEShortcutBarInventory {

    private final Lock lock;

    public LEKeyChestCreationShortcutBarInventory(LEPlayer player, Lock lock) {
        super(player);
        this.lock = lock;
    }

    @Override
    protected ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.PLACE_KEY_CHEST_SHORTCUTBAR_PLACE);
        shortcutBar = addEmpty(shortcutBar, 7);
        shortcutBar.add(Item.PLACE_KEY_CHEST_SHORTCUTBAR_EXIT);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case PLACE_KEY_CHEST_SHORTCUTBAR_EXIT:
                player.getInventoryManager().openLEInventory(new LEMainShortcutBarInventory(player));
                break;
            case PLACE_KEY_CHEST_SHORTCUTBAR_PLACE:
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
                    return;
                }
                Area area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area == null || !area.isActivated()) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(LEPlayer.TRANSPARENT_MATERIALS, 20);
                if (blocks.size() < 2) {
                    return;
                }
                ComponentController.addComponentFromBlockFace(ComponentType.KEY_CHEST, blocks.get(1), blocks.get(1).getFace(blocks.get(0)), player, lock);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new HashSet<>(Arrays.asList(
                Item.PLACE_KEY_CHEST_SHORTCUTBAR_PLACE,
                Item.PLACE_KEY_CHEST_SHORTCUTBAR_EXIT
        )).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.KEY_CHEST_CREATION_SHORTCUTBAR;
    }

}
