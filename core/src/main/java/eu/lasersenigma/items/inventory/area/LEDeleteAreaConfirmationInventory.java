package eu.lasersenigma.items.inventory.area;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LEDeleteAreaConfirmationInventory extends ALEOpenableInventory {

    private final Area area;

    public LEDeleteAreaConfirmationInventory(LEPlayer player, Area area) {
        super(player, Message.AREA_DELETE_CONFIRM);
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_AREA_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_DELETE_AREA, Item.EMPTY, Item.CANCEL_DELETE_AREA)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEDeleteAreaConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != area) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            return;
        }
        switch (item) {
            case CONFIRM_DELETE_AREA:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                AreaController.deleteArea(player, player.getBukkitPlayer().getLocation());
                break;
            case CANCEL_DELETE_AREA:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_AREA_CONFIRM,
                Item.CONFIRM_DELETE_AREA,
                Item.CANCEL_DELETE_AREA
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.DELETE_AREA_CONFIRM_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
