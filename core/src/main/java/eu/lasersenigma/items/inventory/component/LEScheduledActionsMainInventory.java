package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LEScheduledActionsMainInventory extends ALEOpenableInventory {

    private final IComponent component;

    public LEScheduledActionsMainInventory(LEPlayer player, IComponent component) {
        super(player, Message.SCHEDULED_ACTIONS_MAIN_MENU_TITLE);
        this.component = component;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        ArrayList<Item> firstLine = new ArrayList<>();
        if (player.isSavingActions()) {
            firstLine.add(Item.SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE);
        } else {
            firstLine.add(Item.SCHEDULED_ACTIONS_START_SAVING_ACTIONS);
        }
        firstLine.addAll(Arrays.asList(Item.EMPTY, Item.SCHEDULED_ACTIONS_OPEN_EDITION_MENU));
        if (component.getScheduledActions().size() > 0) {
            firstLine.addAll(Arrays.asList(Item.EMPTY, Item.SCHEDULED_ACTIONS_CLEAR));
        }
        firstLine = addEmptyUntil(firstLine, 8);
        firstLine.add(Item.SCHEDULED_ACTION_EDIT_BACK);
        inv.add(firstLine);
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEScheduledActionsMainInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != component.getArea()) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> player.getInventoryManager().closeOpenedInventory(), 1);
            return;
        }
        switch (item) {
            case SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE:
                if (player.isSavingActions()) {
                    ComponentController.saveActionsDone(player, component);
                }
                break;
            case SCHEDULED_ACTIONS_START_SAVING_ACTIONS:
                player.startSavingActions(component.getArea(), component);
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> player.getInventoryManager().closeOpenedInventory(), 1);
                break;
            case SCHEDULED_ACTIONS_OPEN_EDITION_MENU:
                player.stopSavingActions();
                player.getInventoryManager().openLEInventory(new LEScheduledActionsEditionInventory(player, component));
                break;
            case SCHEDULED_ACTIONS_CLEAR:
                ComponentController.clearScheduledActions(player, component);
                break;
            case SCHEDULED_ACTION_EDIT_BACK:
                player.getInventoryManager().openLEInventory(new LEComponentMenuInventory(player, component));
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.SCHEDULED_ACTIONS_SAVE_ACTIONS_DONE,
                Item.SCHEDULED_ACTIONS_START_SAVING_ACTIONS,
                Item.SCHEDULED_ACTIONS_OPEN_EDITION_MENU,
                Item.SCHEDULED_ACTIONS_CLEAR,
                Item.SCHEDULED_ACTION_EDIT_BACK
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.SCHEDULED_ACTIONS_MAIN;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

}
