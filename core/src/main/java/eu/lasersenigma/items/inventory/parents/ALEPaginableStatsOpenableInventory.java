package eu.lasersenigma.items.inventory.parents;

import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.inventory.ItemStack;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Abstract LEInventory for openable inventories
 */
public abstract class ALEPaginableStatsOpenableInventory extends ALEOpenableInventory {

    protected static final String NEW_LINE_STR = "\n";
    private static final float PAGE_SIZE = 42.0f;

    private final double nbPage;
    private final ArrayList<ItemStack> items;
    private final int nbItems;

    private int page;

    protected ALEPaginableStatsOpenableInventory(LEPlayer player, Message titleMessage, ArrayList<ItemStack> items) {
        super(player, titleMessage);
        this.page = 1;
        this.items = items;
        this.nbItems = items.size();
        this.nbPage = Math.ceil(items.size() / PAGE_SIZE);
    }

    protected static String getRankAndStats(int rank, PlayerStats playerStats, LEInventoryType context) {
        StringBuilder sb = new StringBuilder();
        sb
                .append(getRank(rank))
                .append(NEW_LINE_STR);
        switch (context) {
            case AREA_STATS_ACTION_MENU:
                sb.append(getActionsStats(playerStats));
                sb.append(NEW_LINE_STR);
                sb.append(getDurationStats(playerStats));
                sb.append(NEW_LINE_STR);
                sb.append(getStepsStats(playerStats));
                break;
            case AREA_STATS_STEP_MENU:
                sb.append(getStepsStats(playerStats));
                sb.append(NEW_LINE_STR);
                sb.append(getActionsStats(playerStats));
                sb.append(NEW_LINE_STR);
                sb.append(getDurationStats(playerStats));
                sb.append(NEW_LINE_STR);
                break;
            case AREA_STATS_TIME_MENU:
                sb.append(getDurationStats(playerStats));
                sb.append(NEW_LINE_STR);
                sb.append(getActionsStats(playerStats));
                sb.append(NEW_LINE_STR);
                sb.append(getStepsStats(playerStats));
                break;
            default:
                throw new UnsupportedOperationException("This inventory type is not supported here");
        }
        return sb.toString();
    }

    protected static String getRank(int rank) {
        return new StringBuilder("#").append(rank).toString();
    }

    protected static String getDurationStats(PlayerStats playerStats) {
        StringBuilder sb = new StringBuilder();
        Duration d = playerStats.getDuration();
        int hours = (int) Math.ceil(d.toHours());
        int minutes = (int) Math.ceil(d.toMinutes() % 60);
        int seconds = (int) Math.ceil(d.getSeconds() % 60);

        if (hours > 0) {
            sb.append(hours).append(":");
        }
        sb.append(minutes).append(":").append(seconds);
        return sb.toString();
    }

    protected static String getStepsStats(PlayerStats playerStats) {
        return String.format("%s %s", playerStats.getNbStep(), Message.STEPS.getMessage());
    }

    protected static String getActionsStats(PlayerStats playerStats) {
        return String.format("%s %s", playerStats.getNbAction(), Message.ACTIONS.getMessage());
    }

    @Override
    public ItemStack[] getContents() {
        ArrayList<ArrayList<ItemStack>> content = new ArrayList<>();
        int startItemIndex = (int) ((page - 1) * PAGE_SIZE); //First item index
        int itemIndex = startItemIndex;
        for (int line = 0; line < 6; line++) {
            ArrayList<ItemStack> lineContent = new ArrayList<>();
            for (int column = 0; column < 7; column++) {
                if (itemIndex < nbItems) {
                    lineContent.add(items.get(itemIndex));
                    itemIndex++;
                } else {
                    break;
                }
            }
            content.add(lineContent);
        }
        addPagination(content);

        int lineIndex = 0;
        ItemStack[] itemStacks = new ItemStack[content.size() * 9];
        if (content.size() > 6) {
            throw new IllegalStateException("Too much lines in the inventory.");
        }

        for (ArrayList<ItemStack> line : content) {
            if (line.size() > 9) {
                throw new IllegalStateException("An inventory line has more than 9 items.");
            }
            int columnIndex = 0;
            for (ItemStack item : line) {
                itemStacks[columnIndex + lineIndex * 9] = item;
                columnIndex++;
            }
            if (columnIndex < 9) {
                while (columnIndex < 9) {
                    itemStacks[columnIndex + lineIndex * 9] = ItemsFactory.getInstance().getItemStack(Item.EMPTY);
                    columnIndex++;
                }
            }
            lineIndex++;
        }
        return itemStacks;
    }

    @Override
    public final List<List<Item>> getOpenableInventory() {
        return null;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case PREVIOUS_PAGE:
                if (page > 1) {
                    page--;
                }
                updateInventory();
                break;
            case NEXT_PAGE:
                if (page < nbPage) {
                    page++;
                }
                updateInventory();
                break;
            default:
                break;
        }
    }

    @Override
    public final boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.PREVIOUS_PAGE,
                Item.NEXT_PAGE
        )).contains(item);
    }

    private void addPagination(ArrayList<ArrayList<ItemStack>> content) {
        ItemsFactory itemFactory = ItemsFactory.getInstance();
        content.stream().forEach(line -> line.add(itemFactory.getItemStack(Item.EMPTY)));
        if (page > 1) {
            content.get(0).add(itemFactory.getItemStack(Item.PREVIOUS_PAGE));
        }
        if (nbPage > 1) {
            int slotWhereToPlaceScrollIndicator = (int) Math.round(page / nbPage * 3) + 1;
            content.get(slotWhereToPlaceScrollIndicator).add(itemFactory.getItemStack(Item.PAGINABLE_INDICATOR));
        }
        if (page < nbPage) {
            content.get(5).add(itemFactory.getItemStack(Item.NEXT_PAGE));
        }
    }
}
