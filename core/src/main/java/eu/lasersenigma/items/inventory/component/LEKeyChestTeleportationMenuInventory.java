package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.KeyChest;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class LEKeyChestTeleportationMenuInventory extends ALEOpenableInventory {

    private final Lock lock;

    private final TreeMap<Integer, Item> keyChestsItemByKeyNumber;

    public LEKeyChestTeleportationMenuInventory(LEPlayer player, Lock lock) {
        super(player, Message.TELEPORTATION_MENU_TITLE);
        this.lock = lock;
        keyChestsItemByKeyNumber = new TreeMap<>();
        lock.getKeyChests().forEach(keyChest -> keyChestsItemByKeyNumber.put(keyChest.getKeyNumber(), keyChest.getHelmetItem()));
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> res = new ArrayList<>();
        List<Item> line = new ArrayList<>();
        for (Item item : keyChestsItemByKeyNumber.values()) {
            line.add(item);
            if (line.size() > 8) {
                res.add(line);
                line = new ArrayList<>();
            }
        }
        if (!line.isEmpty()) {
            res.add(line);
        }
        return res;
    }

    @Override
    public void onClick(Item item) {
        ArrayList<KeyChest> keyChestsClicked = lock.getKeyChests().stream()
                .filter(keyChest -> keyChest.getHelmetItem() == item)
                .collect(Collectors.toCollection(ArrayList::new));
        if (keyChestsClicked.size() == 1) {
            player.teleportToNearestSafeLocation(keyChestsClicked.get(0).getLocation());
            player.getInventoryManager().closeOpenedInventory();
        } else {
            player.getInventoryManager().openLEInventory(new LEKeyChestWithSameKeyNumberTeleportationMenuInventory(player, lock, keyChestsClicked));
        }
    }

    @Override
    public boolean contains(Item item) {
        return keyChestsItemByKeyNumber.containsValue(item);
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.KEY_CHEST_TELEPORTATION_MENU;
    }

}
