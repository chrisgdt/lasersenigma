package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.KeyChest;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LEKeyChestSelectorMenuInventory extends ALEOpenableInventory {

    private final KeyChest keyChest;

    public LEKeyChestSelectorMenuInventory(LEPlayer player, KeyChest keyChest) {
        super(player, Message.KEY_CHEST_SELECTOR_MENU_TITLE);
        this.keyChest = keyChest;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> res = new ArrayList<>();
        res.add(new ArrayList<>(Arrays.asList(Item.KEY_CHEST_1, Item.KEY_CHEST_2, Item.KEY_CHEST_3, Item.KEY_CHEST_4, Item.KEY_CHEST_5, Item.KEY_CHEST_6, Item.KEY_CHEST_7, Item.KEY_CHEST_8, Item.KEY_CHEST_9)));
        res.add(new ArrayList<>(Arrays.asList(Item.KEY_CHEST_10, Item.KEY_CHEST_11, Item.KEY_CHEST_12, Item.KEY_CHEST_13, Item.KEY_CHEST_14, Item.KEY_CHEST_15, Item.KEY_CHEST_16, Item.KEY_CHEST_17, Item.KEY_CHEST_18)));
        res.add(new ArrayList<>(Arrays.asList(Item.KEY_CHEST_19, Item.KEY_CHEST_20, Item.KEY_CHEST_21, Item.KEY_CHEST_22, Item.KEY_CHEST_23, Item.KEY_CHEST_24, Item.KEY_CHEST_25)));
        return res;
    }

    @Override
    public void onClick(Item item) {
        int keyNb;
        switch (item) {
            case KEY_CHEST_1:
                keyNb = 1;
                break;
            case KEY_CHEST_2:
                keyNb = 2;
                break;
            case KEY_CHEST_3:
                keyNb = 3;
                break;
            case KEY_CHEST_4:
                keyNb = 4;
                break;
            case KEY_CHEST_5:
                keyNb = 5;
                break;
            case KEY_CHEST_6:
                keyNb = 6;
                break;
            case KEY_CHEST_7:
                keyNb = 7;
                break;
            case KEY_CHEST_8:
                keyNb = 8;
                break;
            case KEY_CHEST_9:
                keyNb = 9;
                break;
            case KEY_CHEST_10:
                keyNb = 10;
                break;
            case KEY_CHEST_11:
                keyNb = 11;
                break;
            case KEY_CHEST_12:
                keyNb = 12;
                break;
            case KEY_CHEST_13:
                keyNb = 13;
                break;
            case KEY_CHEST_14:
                keyNb = 14;
                break;
            case KEY_CHEST_15:
                keyNb = 15;
                break;
            case KEY_CHEST_16:
                keyNb = 16;
                break;
            case KEY_CHEST_17:
                keyNb = 17;
                break;
            case KEY_CHEST_18:
                keyNb = 18;
                break;
            case KEY_CHEST_19:
                keyNb = 19;
                break;
            case KEY_CHEST_20:
                keyNb = 20;
                break;
            case KEY_CHEST_22:
                keyNb = 22;
                break;
            case KEY_CHEST_23:
                keyNb = 23;
                break;
            case KEY_CHEST_24:
                keyNb = 24;
                break;
            default:
                keyNb = 25;
                break;
        }
        ComponentController.setKeyNumber(player, keyChest, keyNb);
        player.getInventoryManager().closeOpenedInventory();
    }

    @Override
    public boolean contains(Item item) {
        return Arrays.asList(
                Item.KEY_CHEST_1, Item.KEY_CHEST_2, Item.KEY_CHEST_3, Item.KEY_CHEST_4, Item.KEY_CHEST_5, Item.KEY_CHEST_6, Item.KEY_CHEST_7, Item.KEY_CHEST_8, Item.KEY_CHEST_9,
                Item.KEY_CHEST_10, Item.KEY_CHEST_11, Item.KEY_CHEST_12, Item.KEY_CHEST_13, Item.KEY_CHEST_14, Item.KEY_CHEST_15, Item.KEY_CHEST_16, Item.KEY_CHEST_17, Item.KEY_CHEST_18,
                Item.KEY_CHEST_19, Item.KEY_CHEST_20, Item.KEY_CHEST_21, Item.KEY_CHEST_22, Item.KEY_CHEST_23, Item.KEY_CHEST_24, Item.KEY_CHEST_25).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return keyChest;
    }

    @Override
    public Area getArea() {
        return keyChest.getArea();
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.KEY_CHEST_SELECTOR;
    }

}
