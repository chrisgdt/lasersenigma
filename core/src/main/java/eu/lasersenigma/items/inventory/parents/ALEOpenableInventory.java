package eu.lasersenigma.items.inventory.parents;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Abstract LEInventory for openable inventories
 */
public abstract class ALEOpenableInventory extends ALEInventory {

    private final Message titleMessage;

    private Inventory bukkitInventory;

    protected ALEOpenableInventory(LEPlayer player, Message titleMessage) {
        super(player);
        this.titleMessage = titleMessage;
        this.bukkitInventory = null;
    }

    @Override
    public ItemStack[] getContents() {
        int lineIndex = 0;
        List<List<Item>> openableInventoryList = getOpenableInventory();
        ItemStack[] itemStacks = new ItemStack[openableInventoryList.size() * 9];
        if (openableInventoryList.size() > 9) {
            logInventory(openableInventoryList);
            throw new IllegalStateException("Too much lines in the inventory.");
        }

        for (List<Item> line : openableInventoryList) {
            if (line.size() > 9) {
                logInventory(openableInventoryList);
                throw new IllegalStateException("An inventory line has more than 9 items.");
            }
            int columnIndex = 0;
            for (Item item : line) {
                itemStacks[columnIndex + lineIndex * 9] = ItemsFactory.getInstance().getItemStack(item);
                columnIndex++;
            }
            if (columnIndex < 9) {
                while (columnIndex < 9) {
                    itemStacks[columnIndex + lineIndex * 9] = ItemsFactory.getInstance().getItemStack(Item.EMPTY);
                    columnIndex++;
                }
            }
            lineIndex++;
        }
        return itemStacks;
    }

    /**
     * Check if the inventory contains an itemStack that does not correpsond to an existing item.
     *
     * @param itemStack the item stack
     * @return true if the inventory contains this itemStack
     * @deprecated To only use if the itemStack is not an item (only available for specific inventories)
     */
    public boolean contains(ItemStack itemStack) {
        return false;
    }

    ;

    /**
     * @param itemStack the itemStack
     * @deprecated To only use if the itemStack is not an item (only available for specific inventories)
     */
    public void onClick(ItemStack itemStack) {
    }

    ;

    public final Inventory getBukkitInventory() {
        ItemStack[] items = getContents();
        if (bukkitInventory == null) {
            bukkitInventory = Bukkit.createInventory(null, items.length, getTitle());
        }
        bukkitInventory.setContents(items);
        return bukkitInventory;
    }

    @Override
    public final void updateInventory() {
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            this.bukkitInventory.setContents(getContents());
            player.getBukkitPlayer().updateInventory();
        }, 2);
    }

    public final String getTitle() {
        return titleMessage.getMessage();
    }

    protected final void addToInnerLists(List<List<Item>> currentContent, List<? extends List<Item>> toAdd) {
        int currentContentSize = currentContent.size();

        //cloning inner lists to mutable list
        for (int i = 0; i < currentContentSize; ++i) {
            List<Item> immutableLine = currentContent.get(i);
            ArrayList<Item> line = new ArrayList<>(immutableLine);
            currentContent.set(i, line);
        }

        int toAddSize = toAdd.size();
        int nbLines = Integer.max(currentContentSize, toAddSize);

        // Equalizing the number of lines
        if (nbLines > currentContentSize) {
            for (int i = currentContentSize; i < nbLines; ++i) {
                currentContent.add(new ArrayList<>());
            }
        }

        // Getting the lines were Items must be added
        HashSet<Integer> toAddNotEmptyLinesIndex = new HashSet<>();
        for (Integer toAddLineIndex = 0; toAddLineIndex < toAddSize; toAddLineIndex++) {
            List<Item> toAddLine = toAdd.get(toAddLineIndex);
            if (toAddLine == null || toAddLine.isEmpty()) {
                continue;
            }
            toAddNotEmptyLinesIndex.add(toAddLineIndex);
        }

        // Getting the size of the biggest currentContent line amongst
        int maxOriginLineSize = 0;
        for (Integer toAddNotEmptyLineIndex : toAddNotEmptyLinesIndex) {
            maxOriginLineSize = Integer.max(maxOriginLineSize, currentContent.get(toAddNotEmptyLineIndex).size());
        }

        // Adding empty item(s) to each line where equalizing is needed
        for (Integer toAddNotEmptyLineIndex : toAddNotEmptyLinesIndex) {
            List<Item> currentContentLine = currentContent.get(toAddNotEmptyLineIndex);
            currentContentLine = addEmpty(currentContentLine, maxOriginLineSize - currentContentLine.size());
            currentContent.set(toAddNotEmptyLineIndex, currentContentLine);
        }

        // Adding toAdd to currentContent
        toAddNotEmptyLinesIndex.forEach((toAddNotEmptyLineIndex) -> {
            List<Item> toAddLine = toAdd.get(toAddNotEmptyLineIndex);
            currentContent.get(toAddNotEmptyLineIndex).addAll(toAddLine);
        });
    }

    protected final ArrayList<ArrayList<Item>> addBorders(ArrayList<ArrayList<Item>> source) {
        ArrayList<ArrayList<Item>> result = new ArrayList<>();
        int max = 0;
        for (ArrayList<Item> line : source) {
            if (line != null) {
                max = Integer.max(max, line.size());
            }
        }
        //adding right border
        for (ArrayList<Item> line : source) {
            if (line != null) {
                int nbEmptyToAdd = max - line.size() + 1;
                for (int i = 0; i < nbEmptyToAdd; i++) {
                    line.add(Item.EMPTY);
                }
                result.add(line);
            }
        }
        //adding bottom border
        ArrayList<Item> bottomBorder = addEmpty(new ArrayList<>(), max + 1);
        result.add(bottomBorder);
        return result;
    }

    protected abstract List<List<Item>> getOpenableInventory();

}
