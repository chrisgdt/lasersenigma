package eu.lasersenigma.items.inventory.area.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Benjamin
 */
public class LEAreaStatsLinkShortcutBarInventory extends ALEShortcutBarInventory {

    private final Area area;

    public LEAreaStatsLinkShortcutBarInventory(LEPlayer player, Area area) {
        super(player);
        this.area = area;
    }

    @Override
    protected ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        shortcutBar.add(Item.AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA);
        shortcutBar = addEmpty(shortcutBar, 7);
        shortcutBar.add(Item.AREA_STATS_LINK_SHORTCUTBAR_EXIT);
        return shortcutBar;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case AREA_STATS_LINK_SHORTCUTBAR_EXIT:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new LEMainShortcutBarInventory(player));
                }, 1);
                break;
            case AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA:
                if (!checkPermissionClearInvAndSendMessageOnFail(Permission.ADMIN)) {
                    return;
                }
                Area area2 = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
                if (area2 == null) {
                    ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
                    return;
                }
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().openLEInventory(new LEAreaStatsLinkConfirmationInventory(player, area, area2));
                }, 1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.AREA_STATS_LINK_SHORTCUTBAR_SELECT_AREA, Item.AREA_STATS_LINK_SHORTCUTBAR_EXIT)).contains(item);
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.AREA_STATS_LINK_SHORTCUTBAR;
    }

}
