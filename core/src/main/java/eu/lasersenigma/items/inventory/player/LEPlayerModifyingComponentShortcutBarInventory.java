package eu.lasersenigma.items.inventory.player;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.AttractionRepulsionSphere;
import eu.lasersenigma.components.MirrorChest;
import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.components.attributes.MirrorSupportMode;
import eu.lasersenigma.components.attributes.RotationType;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.components.parents.IPlayerModifiableComponent;
import eu.lasersenigma.components.parents.IRotatableComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import static eu.lasersenigma.player.LEPlayer.TRANSPARENT_MATERIALS;

/**
 * The ShortcutBar opened when players rotates/resizes components
 */
public class LEPlayerModifyingComponentShortcutBarInventory extends ALEShortcutBarInventory {

    private final IPlayerModifiableComponent component;

    private final int taskId;

    public LEPlayerModifyingComponentShortcutBarInventory(LEPlayer player, IPlayerModifiableComponent component) {
        super(player);
        this.component = component;
        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (() -> {
            if (player.getBukkitPlayer() == null) {
                stopTask();
                return;
            }
            Block block = player.getBukkitPlayer().getTargetBlock(TRANSPARENT_MATERIALS, 5);

            if (!player.isInArea() || !component.getLocation().equals(block.getLocation())) {
                player.getInventoryManager().closeRotationShortcutBarInventory();
            }
        }), 20, 20);
        taskId = task.getTaskId();
    }

    @Override
    public IPlayerModifiableComponent getComponent() {
        return component;
    }

    @Override
    public ArrayList<Item> getShortcutBar() {
        ArrayList<Item> shortcutBar = new ArrayList<>();
        boolean canRotateHorizontally = true;
        boolean canRotateVertically = true;
        boolean canResizeGravitationalSphere = false;
        switch (component.getType()) {
            case LASER_SENDER:
                if (!getArea().isHLaserSendersRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVLaserSendersRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case LASER_RECEIVER:
                if (!getArea().isHLaserReceiversRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVLaserReceiversRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case CONCENTRATOR:
                if (!getArea().isHConcentratorsRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVConcentratorsRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case MIRROR_SUPPORT:
                MirrorSupportMode mode = ((MirrorSupport) component).getMode();
                if (!getArea().isHMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateHorizontally = false;
                }
                if (!getArea().isVMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateVertically = false;
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                canResizeGravitationalSphere = getArea().isAttractionRepulsionSpheresResizingAllowed();
                canRotateVertically = false;
                canRotateHorizontally = false;
                break;
            default:
                canRotateHorizontally = false;
                canRotateVertically = false;
                canResizeGravitationalSphere = false;
                break;
        }
        if (canRotateHorizontally) {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT);
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT);
        }
        if (canRotateVertically) {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_UP);
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN);
        }
        if (canResizeGravitationalSphere) {
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE);
            shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);
        }
        while (shortcutBar.size() < 8) {
            shortcutBar.add(Item.EMPTY);
        }
        shortcutBar.add(Item.COMPONENT_SHORTCUTBAR_CLOSE);
        return shortcutBar;
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_UP,
                Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN,
                Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE,
                Item.COMPONENT_SHORTCUTBAR_CLOSE
        )).contains(item);
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEComponentShortcutBarInventory.onClick");
        Area area = component.getArea();

        if (!area.containsLocation(player.getBukkitPlayer().getLocation())) {
            item = Item.COMPONENT_SHORTCUTBAR_CLOSE;
        }
        MirrorChest mc;
        switch (item) {
            case COMPONENT_SHORTCUTBAR_ROTATION_LEFT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.LEFT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_RIGHT:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.RIGHT);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_UP:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.UP);
                break;
            case COMPONENT_SHORTCUTBAR_ROTATION_DOWN:
                ComponentController.rotate(player, (IRotatableComponent) component, RotationType.DOWN);
                break;
            case COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, ((AttractionRepulsionSphere) component), false);
                break;
            case COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE:
                ComponentController.modifySize(player, ((AttractionRepulsionSphere) component), true);
                break;
            case COMPONENT_SHORTCUTBAR_CLOSE:
                if (!(component instanceof IMirrorContainer && ((IMirrorContainer) component).getOnGoingAnimation())) {
                    player.getInventoryManager().closeRotationShortcutBarInventory(false);
                }
                break;
        }
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.ROTATION_SHORTCUTBAR;
    }

    @Override
    public Area getArea() {
        return component.getArea();
    }

    @Override
    public void onClose() {
        stopTask();
    }

    private void stopTask() {
        Bukkit.getScheduler().cancelTask(taskId);
    }
}
