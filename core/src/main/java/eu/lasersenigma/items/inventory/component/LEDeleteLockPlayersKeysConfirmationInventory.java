package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.PlayerController;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LEDeleteLockPlayersKeysConfirmationInventory extends ALEOpenableInventory {

    private final Lock lock;

    public LEDeleteLockPlayersKeysConfirmationInventory(LEPlayer player, Lock lock) {
        super(player, Message.LOCK_S_PLAYERS_KEYS_DELETION);
        this.lock = lock;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.EMPTY, Item.HELP_LOCK_S_PLAYERS_KEYS_DELETE_CONFIRM)));
        inv.add(new ArrayList<>(Arrays.asList(Item.CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS, Item.EMPTY, Item.CANCEL_DELETE_LOCK_S_PLAYERS_KEYS)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEDeleteLockPlayersKeysConfirmationInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }

        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != lock.getArea()) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            return;
        }
        switch (item) {
            case CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                PlayerController.removeLockKeysFromEveryPlayers(player, lock);
                break;
            case CANCEL_DELETE_LOCK_S_PLAYERS_KEYS:
                Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                    player.getInventoryManager().closeOpenedInventory();
                }, 1);
                break;
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.HELP_LOCK_S_PLAYERS_KEYS_DELETE_CONFIRM,
                Item.CONFIRM_DELETE_LOCK_S_PLAYERS_KEYS,
                Item.CANCEL_DELETE_LOCK_S_PLAYERS_KEYS
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.DELETE_LOCK_S_PLAYERS_KEYS_CONFIRM_MENU;
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

}
