package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class LEComponentSelectorInventory extends ALEOpenableInventory {

    private final Area area;

    public LEComponentSelectorInventory(LEPlayer player, Area area) {
        super(player, Message.COMPONENT_SELECTOR_TITLE);
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> inv = new ArrayList<>();
        inv.add(new ArrayList<>(Arrays.asList(Item.LASER_SENDER_RED_ALWAYS_ON, Item.EMPTY, Item.LASERS_RECEIVER_RED_ON, Item.EMPTY, Item.EMPTY, Item.EMPTY, Item.EMPTY, Item.LOCK_DEACTIVATED)));
        inv.add(new ArrayList<>(Arrays.asList(Item.MIRROR_SUPPORT, Item.EMPTY, Item.MIRROR_CHEST, Item.EMPTY, Item.WHITE_MELTABLE_CLAY, Item.WHITE_CONCRETE_POWDER, Item.WHITE_GLASS_PANE, Item.WHITE_GLASS)));
        inv.add(new ArrayList<>(Arrays.asList(Item.PRISM, Item.EMPTY, Item.CONCENTRATOR, Item.EMPTY, Item.FILTERING_SPHERE, Item.REFLECTING_SPHERE, Item.EMPTY, Item.ATTRACTION_SPHERE)));
        inv.add(new ArrayList<>(Arrays.asList(Item.REDSTONE_WINNER_BLOCK, Item.DISAPPEARING_WINNER_BLOCK, Item.APPEARING_WINNER_BLOCK, Item.EMPTY, Item.MUSIC_BLOCK, Item.ELEVATOR, Item.EMPTY, Item.REDSTONE_SENSOR_ACTIVATED)));
        return inv;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEComponentSelectorInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        ComponentType componentType = (ComponentType) item.getAttributes().get(ItemAttribute.COMPONENT_TYPE);
        if (componentType != null) {
            player.getInventoryManager().onComponentSelected(componentType);
        } else {
            player.getInventoryManager().onComponentSelected(item);
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(Item.LASER_SENDER_RED_ALWAYS_ON,
                Item.LASERS_RECEIVER_RED_ON,
                Item.MIRROR_SUPPORT,
                Item.MIRROR_CHEST,
                Item.PRISM,
                Item.CONCENTRATOR,
                Item.FILTERING_SPHERE,
                Item.REFLECTING_SPHERE,
                Item.ATTRACTION_SPHERE,
                Item.WHITE_MELTABLE_CLAY,
                Item.WHITE_CONCRETE_POWDER,
                Item.WHITE_GLASS_PANE,
                Item.WHITE_GLASS,
                Item.REDSTONE_SENSOR_ACTIVATED,
                Item.MUSIC_BLOCK,
                Item.REDSTONE_WINNER_BLOCK,
                Item.DISAPPEARING_WINNER_BLOCK,
                Item.APPEARING_WINNER_BLOCK,
                Item.LOCK_DEACTIVATED,
                Item.ELEVATOR
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.NEW_COMPONENT_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
