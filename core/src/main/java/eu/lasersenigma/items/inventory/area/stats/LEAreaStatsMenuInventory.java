package eu.lasersenigma.items.inventory.area.stats;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public final class LEAreaStatsMenuInventory extends ALEOpenableInventory {

    Area area;

    public LEAreaStatsMenuInventory(LEPlayer player, Area area) {
        super(player, Message.STATS_MENU_TITLE);
        this.area = area;
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> content = new ArrayList<>();
        ArrayList<Item> firstLine = new ArrayList<>(Arrays.asList(
                Item.AREA_STATS_TIME_LIST_MENU,
                Item.AREA_STATS_ACTION_LIST_MENU,
                Item.AREA_STATS_STEP_LIST_MENU
        ));
        if (Permission.ADMIN.hasPermission(player.getBukkitPlayer())) {
            firstLine = addEmpty(firstLine, 4);
            if (area.getStats().isLinked()) {
                firstLine.add(Item.AREA_STATS_UNLINK);
            } else {
                firstLine.add(Item.AREA_STATS_LINK);
            }
            firstLine.add(Item.AREA_STATS_CLEAR);
        }
        content.add(firstLine);
        return content;
    }

    @Override
    public void onClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEAreaStatsMenuInventory.onClick");
        if (!checkPermissionClearInvAndSendMessageOnFail(Permission.EDIT)) {
            return;
        }
        if (Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation()) != area) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                player.getInventoryManager().closeOpenedInventory();
            }, 1);
            return;
        }
        switch (item) {
            case AREA_STATS_TIME_LIST_MENU:
                player.getInventoryManager().openLEInventory(new LEAreaStatsTimeListInventory(player, area));
                break;
            case AREA_STATS_ACTION_LIST_MENU:
                player.getInventoryManager().openLEInventory(new LEAreaStatsActionListInventory(player, area));
                break;
            case AREA_STATS_STEP_LIST_MENU:
                player.getInventoryManager().openLEInventory(new LEAreaStatsStepListInventory(player, area));
                break;
            case AREA_STATS_LINK:
                player.getInventoryManager().openLEInventory(new LEAreaStatsLinkShortcutBarInventory(player, area));
                break;
            case AREA_STATS_UNLINK:
                player.getInventoryManager().openLEInventory(new LEAreaStatsUnlinkConfirmationInventory(player, area));
                break;
            case AREA_STATS_CLEAR:
                player.getInventoryManager().openLEInventory(new LEAreaStatsClearConfirmationInventory(player, area));
                break;
            default:
                throw new UnsupportedOperationException("Unknown item type");
        }
    }

    @Override
    public boolean contains(Item item) {
        return new ArrayList<>(Arrays.asList(
                Item.AREA_STATS_TIME_LIST_MENU,
                Item.AREA_STATS_ACTION_LIST_MENU,
                Item.AREA_STATS_STEP_LIST_MENU,
                Item.AREA_STATS_LINK,
                Item.AREA_STATS_UNLINK,
                Item.AREA_STATS_CLEAR
        )).contains(item);
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.AREA_STATS_MENU;
    }

    @Override
    public IComponent getComponent() {
        return null;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
