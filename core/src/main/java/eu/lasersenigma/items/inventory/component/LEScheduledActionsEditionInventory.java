package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.attributes.ActionType;
import eu.lasersenigma.components.attributes.SavedAction;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.items.inventory.parents.ALEHorizontallyPaginableOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.player.LEPlayer;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;

public final class LEScheduledActionsEditionInventory extends ALEHorizontallyPaginableOpenableInventory {

    private final Area area;

    private final IComponent component;

    public LEScheduledActionsEditionInventory(LEPlayer player, IComponent component) {
        super(player, Message.SCHEDULED_ACTIONS_EDITION_MENU_TITLE);
        this.area = component.getArea();
        this.component = component;
    }

    private static TreeMap<SavedAction, ArrayList<ItemStack>> getSavedActionColumns(IComponent component) {
        TreeMap<SavedAction, ArrayList<ItemStack>> savedActionsColumns = new TreeMap<>();
        ItemsFactory itemsFactory = ItemsFactory.getInstance();
        ArrayList<SavedAction> savedActions = component.getScheduledActions();
        int lastColumnIndex = savedActions.size() - 1;

        for (int i = 0; i <= lastColumnIndex; i++) {
            SavedAction savedAction = savedActions.get(i);
            ArrayList<ItemStack> column = new ArrayList<>();

            ItemStack actionEdit = itemsFactory.getItemStack(savedAction.getType().getItem());
            ItemStack actionDelete = itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_DELETE);
            boolean shouldShowActionMoveLeft = i != 0;
            boolean shouldShowActionMoveRight = i != lastColumnIndex;
            ItemStack actionMoveLeft = shouldShowActionMoveLeft ? itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_MOVE_LEFT) : itemsFactory.getItemStack(Item.EMPTY);
            ItemStack actionMoveRight = shouldShowActionMoveRight ? itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_MOVE_RIGHT) : itemsFactory.getItemStack(Item.EMPTY);

            // Modify itemStacks name to make them unique (necessary for letting contains and onClick identify the specific column users clics)
            String txtToAdd = "#" + i;
            NMSManager.getNMS().appendToItemLore(actionEdit, savedAction.getType() == ActionType.WAIT ? "(" + savedAction.getDelay() + " ticks) " + txtToAdd : txtToAdd);
            NMSManager.getNMS().appendToItemLore(actionDelete, txtToAdd);
            if (shouldShowActionMoveLeft) {
                NMSManager.getNMS().appendToItemLore(actionMoveLeft, txtToAdd);
            }
            if (shouldShowActionMoveRight) {
                NMSManager.getNMS().appendToItemLore(actionMoveRight, txtToAdd);
            }

            column.add(actionEdit);
            column.add(actionDelete);
            column.add(actionMoveLeft);
            column.add(actionMoveRight);

            savedActionsColumns.put(savedAction, column);
        }
        return savedActionsColumns;
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.SCHEDULED_ACTIONS_EDITION_MENU;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    @Override
    public Area getArea() {
        return area;
    }

    @Override
    public void onClick(Item item) {
        switch (item) {
            case SCHEDULED_ACTIONS_ACTION_NEW_LEFT:
                ComponentController.addNewScheduledActionBefore(player, component);
                updateInventory();
                break;
            case SCHEDULED_ACTIONS_ACTION_NEW_RIGHT:
                ComponentController.addNewScheduledActionAfter(player, component);
                updateInventory();
                break;
            case BLACK_EMPTY:
                break;
            case SCHEDULED_ACTION_EDIT_BACK:
                player.getInventoryManager().openLEInventory(new LEScheduledActionsMainInventory(player, component));
                break;
            default:
                super.onClick(item);
                break;
        }
    }

    @Override
    public boolean contains(ItemStack itemStack) {
        return getColumnsItemStacks().stream().anyMatch((column) -> (column.stream().anyMatch((curItemStack) -> (ItemsFactory.isSimilar(itemStack, curItemStack)))));
    }

    @Override
    public void onClick(ItemStack itemStack) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack)");
        TreeMap<SavedAction, ArrayList<ItemStack>> savedActionsColumns = getSavedActionColumns(component);
        for (Entry<SavedAction, ArrayList<ItemStack>> savedActionColumn : savedActionsColumns.entrySet()) {
            ArrayList<ItemStack> column = savedActionColumn.getValue();
            for (int i = 0; i < column.size(); i++) {
                ItemStack curItemStack = column.get(i);

                if (ItemsFactory.isSimilar(itemStack, curItemStack)) {
                    switch (i) {
                        case 0:
                            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => edit");
                            player.getInventoryManager().openLEInventory(new LEScheduledActionsEditActionInventory(player, component, savedActionColumn.getKey()));
                            return;
                        case 1:
                            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => delete");
                            ComponentController.deleteScheduledAction(player, component, savedActionColumn.getKey());
                            return;
                        case 2:
                            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => moveBefore");
                            ComponentController.moveScheduledActionBefore(player, component, savedActionColumn.getKey());
                            return;
                        case 3:
                            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "LEScheduledActionsEditionInventory.onClick(ItemStack) => moveAfter");
                            ComponentController.moveScheduledActionAfter(player, component, savedActionColumn.getKey());
                            return;
                    }
                }
            }
        }
    }

    @Override
    public final boolean contains(Item item) {
        if (Arrays.asList(
                Item.SCHEDULED_ACTIONS_ACTION_NEW_LEFT,
                Item.SCHEDULED_ACTIONS_ACTION_NEW_RIGHT,
                Item.BLACK_EMPTY,
                Item.SCHEDULED_ACTION_EDIT_BACK
        ).contains(item)) {
            return true;
        }
        return super.contains(item);
    }

    @Override
    protected List<List<ItemStack>> getColumnsItemStacks() {
        ItemsFactory itemsFactory = ItemsFactory.getInstance();
        List<List<ItemStack>> columns = new ArrayList<>();
        TreeMap<SavedAction, ArrayList<ItemStack>> savedActionsColumns = getSavedActionColumns(component);
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_NEW_LEFT),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.SCHEDULED_ACTION_EDIT_BACK)
        )));
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY)
        )));
        savedActionsColumns.forEach((action, column) -> columns.add(column));
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY),
                itemsFactory.getItemStack(Item.BLACK_EMPTY)
        )));
        columns.add(new ArrayList<>(Arrays.asList(
                itemsFactory.getItemStack(Item.SCHEDULED_ACTIONS_ACTION_NEW_RIGHT),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.EMPTY),
                itemsFactory.getItemStack(Item.SCHEDULED_ACTION_EDIT_BACK)
        )));
        return columns;
    }
}
