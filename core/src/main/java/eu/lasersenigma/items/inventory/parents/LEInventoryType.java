package eu.lasersenigma.items.inventory.parents;

import eu.lasersenigma.config.Permission;

/**
 * States of the edition process
 */
public enum LEInventoryType {
    NORMAL(null),
    ROTATION_SHORTCUTBAR(null),
    MAIN_SHORTCUTBAR(Permission.EDIT),
    DELETE_AREA_CONFIRM_MENU(Permission.EDIT),
    NEW_COMPONENT_MENU(Permission.EDIT),
    COLOR_MENU(Permission.EDIT),
    AREA_CONFIGURATION_MENU(Permission.EDIT),
    AREA_STATS_MENU(null),
    AREA_STATS_TIME_MENU(Permission.EDIT),
    AREA_STATS_ACTION_MENU(Permission.EDIT),
    AREA_STATS_STEP_MENU(Permission.EDIT),
    ARMOR_ACTION_MENU(Permission.EDIT),
    COMPONENT_SHORTCUTBAR(Permission.EDIT),
    COMPONENT_MENU(Permission.EDIT),
    AREA_VICTORY_AREAS(Permission.EDIT),
    AREA_STATS_LINK_SHORTCUTBAR(Permission.ADMIN),
    AREA_STATS_CLEAR_CONFIRM(Permission.ADMIN),
    AREA_STATS_LINK_CONFIRM(Permission.ADMIN),
    AREA_STATS_UNLINK_CONFIRM(Permission.ADMIN),
    KEY_CHEST_TELEPORTATION_MENU(Permission.EDIT),
    KEY_CHEST_WITH_SAME_KEY_NUMBER_TELEPORTATION_MENU(Permission.EDIT),
    KEY_CHEST_SELECTOR(Permission.EDIT),
    KEY_CHEST_CREATION_SHORTCUTBAR(Permission.EDIT),
    DELETE_LOCK_S_KEY_CHEST_CONFIRM_MENU(Permission.EDIT),
    DELETE_LOCK_S_PLAYERS_KEYS_CONFIRM_MENU(Permission.EDIT),
    ELEVATOR_SECOND_LOCATION_SHORTCUTBAR(Permission.EDIT),
    SCHEDULED_ACTIONS_MAIN(Permission.EDIT),
    SCHEDULED_ACTIONS_EDITION_MENU(Permission.EDIT),
    SCHEDULED_ACTIONS_ACTION_EDIT_MENU(Permission.EDIT),
    LIGHT_LEVEL_EDIT_MENU(Permission.EDIT);

    private final Permission requiredPermissions;

    LEInventoryType(Permission requiredPermissions) {
        this.requiredPermissions = requiredPermissions;
    }

    public Permission getRequiredPermissions() {
        return this.requiredPermissions;
    }
}
