package eu.lasersenigma.items.inventory.saving;

public enum PlayerInventorySaveLocation {
    OUTSIDE_AREA,
    INSIDE_AREA;

    public static PlayerInventorySaveLocation fromIsInArea(boolean isInArea) {
        return isInArea ? INSIDE_AREA : OUTSIDE_AREA;
    }

    public boolean wasSavedInArea() {
        return this == INSIDE_AREA;
    }
}
