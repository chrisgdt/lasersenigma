package eu.lasersenigma.items.inventory.component;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.KeyChest;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.player.LEPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * @author Benjamin
 */
public class LEKeyChestWithSameKeyNumberTeleportationMenuInventory extends ALEOpenableInventory {

    private final Lock lock;

    private final TreeMap<Item, KeyChest> keyChestsItems;

    public LEKeyChestWithSameKeyNumberTeleportationMenuInventory(LEPlayer player, Lock lock, ArrayList<KeyChest> keyChestsClicked) {
        super(player, Message.TELEPORTATION_MENU_TITLE);
        this.lock = lock;
        keyChestsItems = new TreeMap<>();
        for (int i = 0; i < keyChestsClicked.size(); i++) {
            keyChestsItems.put(LEComponentMenuInventory.getNumberAsItems(i, false).get(0), keyChestsClicked.get(i));
            if (i > 9) {
                break;
            }
        }
    }

    @Override
    protected List<List<Item>> getOpenableInventory() {
        List<List<Item>> res = new ArrayList<>();
        if (keyChestsItems.size() < 9) {
            res.add(new ArrayList<>(keyChestsItems.keySet()));
        } else {
            res.add(new ArrayList<>(keyChestsItems.subMap(Item.COMPONENT_MENU_ZERO, Item.COMPONENT_MENU_FIVE).keySet()));
            res.add(new ArrayList<>(keyChestsItems.subMap(Item.COMPONENT_MENU_SIX, Item.COMPONENT_MENU_NINE).keySet()));
        }
        return res;
    }

    @Override
    public void onClick(Item item) {
        Entry<Item, KeyChest> res = keyChestsItems.entrySet().stream()
                .filter(e -> e.getKey() == item)
                .findAny().orElse(null);
        if (res == null) {
            return;
        }
        player.teleportToNearestSafeLocation(res.getValue().getLocation());
        player.getInventoryManager().closeOpenedInventory();
    }

    @Override
    public boolean contains(Item item) {
        return keyChestsItems.containsKey(item);
    }

    @Override
    public IComponent getComponent() {
        return lock;
    }

    @Override
    public Area getArea() {
        return lock.getArea();
    }

    @Override
    public LEInventoryType getType() {
        return LEInventoryType.KEY_CHEST_WITH_SAME_KEY_NUMBER_TELEPORTATION_MENU;
    }

}
