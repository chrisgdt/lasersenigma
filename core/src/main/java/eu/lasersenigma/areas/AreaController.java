package eu.lasersenigma.areas;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.events.areas.*;
import eu.lasersenigma.exceptions.AbstractLasersException;
import eu.lasersenigma.exceptions.AreaOverlapException;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.exceptions.NoAreaFoundNearbyException;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public class AreaController {

    public static final int AREA_SEARCH_RANGE = 100;
    /**
     * saves the first location selected by players during area creation
     */
    private static final HashMap<UUID, Location> PLAYERS_CREATE_AREA_FIRST_LOCATION = new HashMap<>();
    private static final HashMap<UUID, Location> PLAYERS_VICTORY_AREA_FIRST_LOCATION = new HashMap<>();

    private AreaController() {

    }

    public static void createArea(LEPlayer player, Location location) {
        UUID playerUUID = player.getUniqueId();
        Location firstAreaLocation = PLAYERS_CREATE_AREA_FIRST_LOCATION.get(playerUUID);
        if (firstAreaLocation == null) {
            PlayerTryToSelectFirstLocCreateAreaLEEvent firstLocEvent = new PlayerTryToSelectFirstLocCreateAreaLEEvent(player.getBukkitPlayer(), location);
            Bukkit.getServer().getPluginManager().callEvent(firstLocEvent);
            if (firstLocEvent.isCancelled()) {
                return;
            }
            if (!firstLocEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
                return;
            }
            PLAYERS_CREATE_AREA_FIRST_LOCATION.put(playerUUID, location);
            Message.CREATE_AREA_FIRST_LOCATION.sendMessage(player.getBukkitPlayer(), Message.toArray(location));
        } else {
            PlayerTryToCreateAreaLEEvent createAreaEvent = new PlayerTryToCreateAreaLEEvent(player.getBukkitPlayer(), firstAreaLocation, location);
            Bukkit.getServer().getPluginManager().callEvent(createAreaEvent);
            if (createAreaEvent.isCancelled()) {
                return;
            }
            if (!createAreaEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
                return;
            }
            Message.CREATE_AREA_SECOND_LOCATION.sendMessage(player.getBukkitPlayer(), Message.toArray(location));
            try {
                Area createdArea = Areas.getInstance().createArea(firstAreaLocation, location, -1, true, true, true, true, false, false, false, false, false, null, DetectionMode.DETECTION_LASER_RECEIVERS, 1, 10);
                Bukkit.getServer().getPluginManager().callEvent(new PlayerCreatedAreaLEEvent(player.getBukkitPlayer(), createdArea));
                Message.CREATE_AREA_SUCCESS.sendMessage(player.getBukkitPlayer());
                createdArea.show(player.getBukkitPlayer());
            } catch (AbstractLasersException ex) {
                ErrorsConfig.showError(player.getBukkitPlayer(), ex);
                if (ex instanceof AreaOverlapException) {
                    ((AreaOverlapException) ex).getOverlappedArea().show(player.getBukkitPlayer());
                }
            }
            PLAYERS_CREATE_AREA_FIRST_LOCATION.remove(playerUUID);
        }
    }

    public static void deleteArea(LEPlayer player, Location location) {
        Area area = Areas.getInstance().getAreaFromLocation(location);
        PlayerTryToDeleteAreaLEEvent deleteAreaEvent = new PlayerTryToDeleteAreaLEEvent(player.getBukkitPlayer(), area);
        Bukkit.getServer().getPluginManager().callEvent(deleteAreaEvent);
        if (deleteAreaEvent.isCancelled()) {
            return;
        }
        if (!deleteAreaEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getStats().isLinked()) {
            statsUnlink(player, area);
        }
        Areas.getInstance().deleteArea(area);
        LEPlayers.getInstance().onAreaDeleted(area);
        Bukkit.getServer().getPluginManager().callEvent(new PlayerDeletedAreaLEEvent(player.getBukkitPlayer(), area));
        Message.DELETE_AREA.sendMessage(player.getBukkitPlayer());
    }

    public static void toggleVMirrorsRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVMirrorsRotationAllowed();
        PlayerTryToToogleVMirrorsRotationAreaLEEvent mirrorVrotationEvent = new PlayerTryToToogleVMirrorsRotationAreaLEEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(mirrorVrotationEvent);
        if (mirrorVrotationEvent.isCancelled()) {
            return;
        }
        if (!mirrorVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVMirrorsRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_AREA_VROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newVRotationAllowed));
    }

    public static void toggleHMirrorsRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHMirrorsRotationAllowed();
        PlayerTryToToogleHMirrorsRotationAreaLEEvent mirrorHrotationEvent = new PlayerTryToToogleHMirrorsRotationAreaLEEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(mirrorHrotationEvent);
        if (mirrorHrotationEvent.isCancelled()) {
            return;
        }
        if (!mirrorHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHMirrorsRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_AREA_HROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newHRotationAllowed));
    }

    public static void toggleVConcentratorsRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVConcentratorsRotationAllowed();
        PlayerTryToToogleVConcentratorsRotationAreaLEEvent concentratorsVrotationEvent = new PlayerTryToToogleVConcentratorsRotationAreaLEEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(concentratorsVrotationEvent);
        if (concentratorsVrotationEvent.isCancelled()) {
            return;
        }
        if (!concentratorsVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVConcentratorsRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_CONCENTRATORS_VROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newVRotationAllowed));
        area.components.stream().filter(component -> component.getType() == ComponentType.CONCENTRATOR).forEach(IComponent::updateDisplay);
    }

    public static void toggleHConcentratorsRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHConcentratorsRotationAllowed();
        PlayerTryToToogleHConcentratorsRotationAreaLEEvent concentratorsHrotationEvent = new PlayerTryToToogleHConcentratorsRotationAreaLEEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(concentratorsHrotationEvent);
        if (concentratorsHrotationEvent.isCancelled()) {
            return;
        }
        if (!concentratorsHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHConcentratorsRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_CONCENTRATORS_HROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newHRotationAllowed));
        area.components.stream().filter(component -> component.getType() == ComponentType.CONCENTRATOR).forEach(IComponent::updateDisplay);
    }

    public static void toggleVLaserSendersRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVLaserSendersRotationAllowed();
        PlayerTryToToogleVLaserSendersRotationAreaLEEvent lsVrotationEvent = new PlayerTryToToogleVLaserSendersRotationAreaLEEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lsVrotationEvent);
        if (lsVrotationEvent.isCancelled()) {
            return;
        }
        if (!lsVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVLaserSendersRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_LASER_SENDERS_VROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newVRotationAllowed));
        area.components.stream().filter(component -> component.getType() == ComponentType.LASER_SENDER).forEach(IComponent::updateDisplay);
    }

    public static void toggleHLaserSendersRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHLaserSendersRotationAllowed();
        PlayerTryToToogleHLaserSendersRotationAreaLEEvent lsHrotationEvent = new PlayerTryToToogleHLaserSendersRotationAreaLEEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lsHrotationEvent);
        if (lsHrotationEvent.isCancelled()) {
            return;
        }
        if (!lsHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHLaserSendersRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_LASER_SENDERS_HROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newHRotationAllowed));
        area.components.stream().filter(component -> component.getType() == ComponentType.LASER_SENDER).forEach(IComponent::updateDisplay);
    }

    public static void toggleVLaserReceiversRotation(LEPlayer player, Area area) {
        boolean newVRotationAllowed = !area.isVLaserReceiversRotationAllowed();
        PlayerTryToToogleVLaserReceiversRotationAreaLEEvent lrVrotationEvent = new PlayerTryToToogleVLaserReceiversRotationAreaLEEvent(player.getBukkitPlayer(), area, newVRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lrVrotationEvent);
        if (lrVrotationEvent.isCancelled()) {
            return;
        }
        if (!lrVrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setVLaserReceiversRotationAllowed(newVRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_LASER_RECEIVERS_VROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newVRotationAllowed));
        area.components.stream().filter(component -> component.getType() == ComponentType.LASER_RECEIVER).forEach(IComponent::updateDisplay);
    }

    public static void toggleHLaserReceiversRotation(LEPlayer player, Area area) {
        boolean newHRotationAllowed = !area.isHLaserReceiversRotationAllowed();
        PlayerTryToToogleHLaserReceiversRotationAreaLEEvent lrHrotationEvent = new PlayerTryToToogleHLaserReceiversRotationAreaLEEvent(player.getBukkitPlayer(), area, newHRotationAllowed);
        Bukkit.getServer().getPluginManager().callEvent(lrHrotationEvent);
        if (lrHrotationEvent.isCancelled()) {
            return;
        }
        if (!lrHrotationEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setHLaserReceiversRotationAllowed(newHRotationAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_LASER_RECEIVERS_HROTATION.sendMessage(player.getBukkitPlayer(), Message.toArray(newHRotationAllowed));
        area.components.stream().filter(component -> component.getType() == ComponentType.LASER_RECEIVER).forEach(IComponent::updateDisplay);
    }

    public static void toggleARSpheresResizing(LEPlayer player, Area area) {
        boolean newARSpheresResizingAllowed = !area.isAttractionRepulsionSpheresResizingAllowed();
        PlayerTryToToogleARSpheresResizingAreaLEEvent arspheresResizingEvent = new PlayerTryToToogleARSpheresResizingAreaLEEvent(player.getBukkitPlayer(), area, newARSpheresResizingAllowed);
        Bukkit.getServer().getPluginManager().callEvent(arspheresResizingEvent);
        if (arspheresResizingEvent.isCancelled()) {
            return;
        }
        if (!arspheresResizingEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.setAttractionRepulsionSpheresResizingAllowed(newARSpheresResizingAllowed);
        LEPlayers.getInstance().onAreaUpdated(area);
        area.dbUpdate();
        Message.TOGGLE_SPHERES_SIZING.sendMessage(player.getBukkitPlayer(), Message.toArray(newARSpheresResizingAllowed));
    }

    public static void statsLink(LEPlayer player, Area area, Area area2) {
        if (!Permission.ADMIN.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        try {
            area.getStats().linkStats(area2);
            Message.STATS_MERGE_AND_LINKED.sendMessage(player.getBukkitPlayer());
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (AbstractLasersException ex) {
            ErrorsConfig.showError(player.getBukkitPlayer(), ex);
        }

    }

    public static void statsUnlink(LEPlayer player, Area area) {
        if (!Permission.ADMIN.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        try {
            area.getStats().unlinkStats();
            Message.STATS_UNLINKED.sendMessage(player.getBukkitPlayer());
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (AbstractLasersException ex) {
            ErrorsConfig.showError(player.getBukkitPlayer(), ex);
        }
    }

    public static void statsClear(LEPlayer player, Area area) {
        if (!Permission.ADMIN.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        area.getStats().clear();
    }

    public static void defineCheckpoint(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        try {
            area.setCheckpoint(player.getBukkitPlayer().getLocation().clone());
            area.dbUpdate();
            Message.CHECKPOINT_SELECTED.sendMessage(player.getBukkitPlayer(), Message.toArray(player.getBukkitPlayer().getLocation()));
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (NoAreaFoundException ex) {
            ErrorsConfig.showError(player.getBukkitPlayer(), ex);
        }
    }

    public static void deleteCheckpoint(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        try {
            area.setCheckpoint(null);
            area.dbUpdate();
            Message.CHECKPOINT_DELETED.sendMessage(player.getBukkitPlayer());
            LEPlayers.getInstance().onAreaUpdated(area);
        } catch (NoAreaFoundException ex) {
            ErrorsConfig.showError(player.getBukkitPlayer(), ex);
        }
    }

    public static void deleteAllVictoryAreas(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        area.deleteVictoryAreas();
        area.dbUpdate();
        Message.VICTORY_AREAS_DELETED.sendMessage(player.getBukkitPlayer());
        LEPlayers.getInstance().onAreaUpdated(area);
    }

    public static void createVictoryArea(LEPlayer player, Area area, Location location) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer()) || !player.getInventoryManager().isInEditionMode() || !LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        UUID playerUUID = player.getUniqueId();
        Location firstVAreaLocation = PLAYERS_VICTORY_AREA_FIRST_LOCATION.get(playerUUID);
        if (firstVAreaLocation == null) {
            PLAYERS_VICTORY_AREA_FIRST_LOCATION.put(playerUUID, location);
            Message.VICTORY_AREA_FIRST_LOCATION.sendMessage(player.getBukkitPlayer(), Message.toArray(location));
        } else {
            Message.CREATE_AREA_SECOND_LOCATION.sendMessage(player.getBukkitPlayer(), Message.toArray(location));
            PLAYERS_VICTORY_AREA_FIRST_LOCATION.remove(playerUUID);
            try {
                area.addVictoryArea(new VictoryArea(area, firstVAreaLocation, location));
                area.dbUpdate();
                Message.VICTORY_AREA_CREATED.sendMessage(player.getBukkitPlayer());
            } catch (AbstractLasersException ex) {
                ErrorsConfig.showError(player.getBukkitPlayer(), ex);
            }
        }
    }

    public static void setVictoryCriteria(LEPlayer player, Area area, DetectionMode victoryCriteria) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (victoryCriteria.isSpecificToLaserSender()) {
            throw new IllegalStateException();
        }
        area.setVictoryCriteria(victoryCriteria);
        area.dbUpdate();
        victoryCriteria.getCorrespondingMessage().sendMessage(player.getBukkitPlayer());
        LEPlayers.getInstance().onAreaUpdated(area);
    }

    public static void decreaseMin(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMinimumRange() < 1) {
            return;
        }
        if (!area.getVictoryCriteria().isRangeModificationCompatible()) {
            return;
        }
        area.setMinimumRange(area.getMinimumRange() - 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    public static void decreaseMax(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMaximumRange() < 1) {
            return;
        }
        if (!area.getVictoryCriteria().isRangeModificationCompatible()) {
            return;
        }
        if (area.getMaximumRange() == area.getMinimumRange()) {
            area.setMinimumRange(area.getMinimumRange() - 1);
        }
        area.setMaximumRange(area.getMaximumRange() - 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    public static void increaseMin(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMinimumRange() > 98) {
            return;
        }
        if (!area.getVictoryCriteria().isRangeModificationCompatible()) {
            return;
        }
        if (area.getMinimumRange() == area.getMaximumRange()) {
            area.setMaximumRange(area.getMaximumRange() + 1);
        }
        area.setMinimumRange(area.getMinimumRange() + 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    public static void increaseMax(LEPlayer player, Area area) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (area.getMaximumRange() > 98) {
            return;
        }
        if (!area.getVictoryCriteria().isRangeModificationCompatible()) {
            return;
        }
        area.setMaximumRange(area.getMaximumRange() + 1);
        area.dbUpdate();
        LEPlayers.getInstance().onAreaUpdated(area);
        rangeChanged(player, area.getMinimumRange(), area.getMaximumRange());
    }

    private static void rangeChanged(LEPlayer player, int min, int max) {
        Message.RANGE_CHANGED.sendMessage(player.getBukkitPlayer(), new String[]{Integer.toString(min), Integer.toString(max)});
    }

    public static void clearTemporaryLocations(LEPlayer player) {
        PLAYERS_CREATE_AREA_FIRST_LOCATION.remove(player.getBukkitPlayer().getUniqueId());
        PLAYERS_VICTORY_AREA_FIRST_LOCATION.remove(player.getBukkitPlayer().getUniqueId());
    }

    public static void modifyArea(LEPlayer player, AreaSizeModificationAction action, Integer amount, CardinalDirection direction) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Area area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
        if (area == null) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            return;
        }
        int diff = 0;
        switch (action) {
            case CONTRACT:
                diff -= amount;
                break;
            case EXPAND:
                diff += amount;
                break;
        }

        double minX = area.getMinLocation().getBlockX();
        double minY = area.getMinLocation().getBlockY();
        double minZ = area.getMinLocation().getBlockZ();
        double maxX = area.getMaxLocation().getBlockX();
        double maxY = area.getMaxLocation().getBlockY();
        double maxZ = area.getMaxLocation().getBlockZ();

        if (CardinalDirection.ALL == direction) {
            minY -= diff;
            maxY += diff;
            minX -= diff;
            maxX += diff;
            minZ -= diff;
            maxZ += diff;
        }

        if (CardinalDirection.DOWN == direction) {
            minY -= diff;
        } else if (CardinalDirection.UP == direction) {
            maxY += diff;
        } else if (CardinalDirection.WEST == direction) {
            minX -= diff;
        } else if (CardinalDirection.EAST == direction) {
            maxX += diff;
        } else if (CardinalDirection.NORTH == direction) {
            minZ -= diff;
        } else if (CardinalDirection.SOUTH == direction) {
            maxZ += diff;
        }

        try {
            area.modify(minX, minY, minZ, maxX, maxY, maxZ);
            area.show(player.getBukkitPlayer());
            Message.AREA_SIZE_UPDATE.sendMessage(player.getBukkitPlayer());
        } catch (AbstractLasersException ex) {
            ErrorsConfig.showError(player.getBukkitPlayer(), ex);
            if (ex instanceof AreaOverlapException) {
                ((AreaOverlapException) ex).getOverlappedArea().show(player.getBukkitPlayer());
            }
        }
    }

    public static void showNearestArea(LEPlayer player) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        double closestRange = AREA_SEARCH_RANGE;
        Area foundArea = null;
        for (Area area : Areas.getInstance().getAreaSet()) {
            if (!Objects.requireNonNull(area.getMinLocation().getWorld()).getUID().equals(Objects.requireNonNull(player.getBukkitPlayer().getLocation().getWorld()).getUID())) {
                continue;
            }
            Location areaCenter = area.getCenter();
            double range = areaCenter.distance(player.getBukkitPlayer().getLocation());
            if (range < closestRange) {
                closestRange = range;
                foundArea = area;
            }
        }
        if (foundArea == null) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundNearbyException());
            return;
        }
        foundArea.show(player.getBukkitPlayer());
        Message.SHOW_AREA.sendMessage(player.getBukkitPlayer(), new String[]{
                String.valueOf(foundArea.min.getBlockX()),
                String.valueOf(foundArea.min.getBlockY()),
                String.valueOf(foundArea.min.getBlockZ()),
                String.valueOf(foundArea.max.getBlockX()),
                String.valueOf(foundArea.max.getBlockY()),
                String.valueOf(foundArea.max.getBlockZ())
        });
    }
}
