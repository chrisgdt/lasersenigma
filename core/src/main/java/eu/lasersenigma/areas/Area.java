package eu.lasersenigma.areas;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.components.parents.ITaskComponent;
import eu.lasersenigma.events.areas.*;
import eu.lasersenigma.events.components.ConditionalComponentActivatedLEEvent;
import eu.lasersenigma.events.components.ConditionalComponentDeactivatedLEEvent;
import eu.lasersenigma.exceptions.*;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.stats.AreaStats;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import ru.beykerykt.lightapi.LightAPI;
import ru.beykerykt.lightapi.LightType;
import ru.beykerykt.lightapi.chunks.ChunkInfo;

import java.util.*;
import java.util.stream.Collectors;

public class Area extends AArea {

    public static final HashSet<BlockFace> FACES_TO_POWER = new HashSet<>(Arrays.asList(
            BlockFace.DOWN,
            BlockFace.UP,
            BlockFace.EAST,
            BlockFace.NORTH,
            BlockFace.SOUTH,
            BlockFace.WEST
    ));

    public static final boolean isLightApiEnabled = LasersEnigmaPlugin.getInstance().isLightAPIAvailable();
    /**
     * AttractionRepulsionSphere Cache
     */
    private final HashMap<Location, ArrayList<AttractionRepulsionSphere>> attractionRepulsionSphereCache;
    /**
     * Block reflection Cache
     */
    private final HashMap<ReflectionData, ReflectionData> blocksReflectionCache;
    /**
     * PoweredLocations
     */
    private final HashSet<Location> poweredLocation;
    /**
     * The area stats
     */
    private final AreaStats stats;
    /**
     * The area's victory areas. Whenever a player enters one of those area, it
     * means he won the enigma.
     */
    private final HashSet<VictoryArea> victoryAreas;
    /**
     * The number of players inside the area
     */
    protected int nbPlayersInside;
    /**
     * The number of players inside the area during the last check
     */
    protected int lastNbPlayersInside;
    /**
     * The number of activated locks inside the area
     */
    protected int nbActivatedLocks;
    /**
     * The number of activated locks inside the area during the last check
     */
    protected int lastNbActivatedLocks;
    /**
     * The players inside the area (except players in gamemode spectator)
     */
    protected HashSet<Player> playersInside;
    /**
     * The players inside the area
     */
    protected HashSet<Player> allPlayersInside;
    /**
     * Last player known location
     */
    protected HashMap<UUID, Location> lastPlayersKnownLocation;
    /**
     * Used to know how often should we check entity impact within particles update process
     */
    private int laserDamageLooper = 1;
    /**
     * Used to know how often show particles within particles update process
     */
    private int laserShowLooper = 1;
    /**
     * Used to know how often update light within particles update process
     */
    private int laserLightUpdateLooper = 1;
    /**
     * Was the light updated during the last execution of the particle update process
     */
    private boolean wasLightUpdatedLastRun = false;
    /**
     * The number of laser receivers activated
     */
    private int nbActivatedLaserReceivers;
    /**
     * The number of laser receivers activated during the last check
     */
    private int lastNbActivatedLaserReceivers;
    /**
     * The number of redstone sensors activated
     */
    private int nbActivatedRedstoneSensor;
    /**
     * The number of redstone sensors activated during the last check
     */
    private int lastNbActivatedRedstoneSensor;
    /**
     * The place where players will respawn
     */
    private Location checkpoint;

    /**
     * Constructor
     *
     * @param a first location (not minimal location yet, simply a corner
     *          location)
     * @param b second location (not maximal location yet, simply the opposite
     *          corner location)
     * @throws AreaCrossWorldsException if the 2 locations are in different
     *                                  worlds
     * @throws AreaNoDepthException     if the area has no depth/width/height
     */
    public Area(Location a, Location b) throws AreaCrossWorldsException, AreaNoDepthException {
        super(a, b);
        this.nbActivatedLaserReceivers = 0;
        this.lastNbActivatedLaserReceivers = 0;
        this.nbActivatedRedstoneSensor = 0;
        this.lastNbActivatedRedstoneSensor = 0;
        this.nbPlayersInside = 0;
        this.lastNbPlayersInside = 0;
        this.nbActivatedLocks = 0;
        this.lastNbActivatedLocks = 0;
        this.attractionRepulsionSphereCache = new HashMap<>();
        this.blocksReflectionCache = new HashMap<>();
        this.playersInside = new HashSet();
        this.lastPlayersKnownLocation = new HashMap<>();
        this.allPlayersInside = new HashSet();
        this.stats = new AreaStats(this);
        this.poweredLocation = new HashSet<>();
        this.victoryAreas = new HashSet<>();
        this.checkpoint = null;
    }

    private static HashSet<Location> getRedstoneSignalUpdateLocations(Location location) {
        HashSet<Location> result = new HashSet<>();
        HashSet<Location> tmpResult = FACES_TO_POWER.stream()
                .map(faceToPower -> location.getBlock().getRelative(faceToPower).getLocation())
                .collect(Collectors.toCollection(HashSet::new));
        tmpResult.forEach(l -> {
            result.add(l);
            result.addAll(FACES_TO_POWER.stream()
                    .map(faceToPower -> l.getBlock().getRelative(faceToPower).getLocation())
                    .collect(Collectors.toCollection(HashSet::new)));
        });
        return result;
    }

    private static void callLater(final BlockRedstoneEvent event) {
        if (LasersEnigmaPlugin.getInstance().isDisabling()) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            Bukkit.getServer().getPluginManager().callEvent(event);
        }, 1);
    }

    public void dbCreate() {
        this.areaId = LasersEnigmaPlugin.getInstance().getPluginDatabase().createArea(this);
    }

    public void dbRemove() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removeArea(this);
    }

    public void dbUpdate() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().updateArea(this);
    }

    public AreaStats getStats() {
        return this.stats;
    }

    /**
     * gets the players inside the area except Spectators
     *
     * @return the players inside the area
     */
    public final HashSet<Player> getPlayersExceptSpectators() {
        return playersInside;
    }

    /**
     * gets the players inside the area
     *
     * @return the players inside the area
     */
    public final HashSet<Player> getPlayers() {
        return allPlayersInside;
    }

    /**
     * Retrieves the reflection cache for blocks within this area
     *
     * @return the block's reflection cache
     */
    public HashMap<ReflectionData, ReflectionData> getBlocksReflectionCache() {
        return blocksReflectionCache;
    }

    /**
     * gets the number of activated laser receivers during the last check
     *
     * @return the number of activated laser receivers during the last check
     */
    public int getNbActivatedLaserReceivers() {
        return this.nbActivatedLaserReceivers;
    }

    /**
     * gets the number of activated laser sensor activated
     *
     * @return the number of activated laser sensors
     */
    public int getNbActivatedRedstoneSensor() {
        return this.nbActivatedRedstoneSensor;
    }

    /**
     * Retrieves the number of players (except spectators) within the area
     *
     * @return the number of players (except spectators) within the area
     */
    public int getNbPlayersInside() {
        return nbPlayersInside;
    }

    /**
     * Retrieves the number of activated locks within the area
     *
     * @return the number of activated locks within the area
     */
    public int getNbActivatedLocks() {
        return nbActivatedLocks;
    }

    public void hideComponents() {
        components.stream().forEach(c -> c.hide());
    }

    public void clearAttractionRepulsionCache() {
        attractionRepulsionSphereCache.clear();
    }

    public void clearBlockReflectionCache() {
        blocksReflectionCache.clear();
    }

    public ArrayList<AttractionRepulsionSphere> getAffectingAttractionRepulsionSphere(Location l) {
        ArrayList<AttractionRepulsionSphere> affectingAttractionRepulsionSpheres = attractionRepulsionSphereCache.get(l);
        if (affectingAttractionRepulsionSpheres == null) {
            affectingAttractionRepulsionSpheres = components.stream()
                    .filter(c -> c instanceof AttractionRepulsionSphere)
                    .map(c -> (AttractionRepulsionSphere) c)
                    .filter(arsphere -> arsphere.getASHeadCenterLocation().distance(l) <= arsphere.getCurrentSize())
                    .collect(Collectors.toCollection(ArrayList::new));
            attractionRepulsionSphereCache.put(l.clone(), affectingAttractionRepulsionSpheres);
        }
        return affectingAttractionRepulsionSpheres;
    }

    /**
     * updates the area
     */
    public void updateArea() {
        //*************************************************
        //*************************************************
        //*************** updates NBPlayers ***************
        //*************************************************
        //*************************************************
        HashSet<Player> actualPlayersInside = getPlayersNoCache();

        //*************** On PlayerLeaveArea ***************
        this.allPlayersInside
                .stream()
                .filter((p) -> (!actualPlayersInside.contains(p) && !p.getGameMode().equals(GameMode.SPECTATOR)))
                //send PlayerLeavedAreaLEEvent
                .forEach(p -> {
                        LEPlayers.getInstance().findLEPlayer(p.getUniqueId()).onLeaveArea();
                        Bukkit.getServer().getPluginManager().callEvent(new PlayerLeavedAreaLEEvent(p, this, lastPlayersKnownLocation.get(p.getUniqueId())));
                });

        //*************** On PlayerEnterArea ***************
        actualPlayersInside.stream()
                .peek((p) -> lastPlayersKnownLocation.put(p.getUniqueId(), p.getLocation()))
                .filter((p) -> (!this.playersInside.contains(p)))
                .forEachOrdered((p) -> {
                    //Starts musicblocks
                    components
                            .stream()
                            .filter(c -> c instanceof MusicBlock)
                            .forEach(mb -> ((MusicBlock) mb).onPlayerEnteredArea(p));
                    if (!p.getGameMode().equals(GameMode.SPECTATOR)) {
                        LEPlayers.getInstance().findLEPlayer(p.getUniqueId()).onEnterArea();
                        //Send PlayerEnteredAreaLEEvent
                        Bukkit.getServer().getPluginManager().callEvent(new PlayerEnteredAreaLEEvent(p, this));
                    }
                });
        allPlayersInside = actualPlayersInside;

        playersInside = filterSpectators(allPlayersInside);

        int nbPlayers = playersInside.size();

        //*************************************************
        //*************************************************
        //*************** (De)Activate Area ***************
        //*************************************************
        //*************************************************
        if (activated && nbPlayers == 0) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().finest("Area deactivated");
            activated = false;
            //Stop tasks task for components
            components.forEach(component -> {
                if (component instanceof ITaskComponent) {
                    ((ITaskComponent) component).cancelTask();
                }
                component.stopScheduledActions();
            });

            reset();

            //Update components (show their new position)
            components.stream().filter(c -> c.getLocation().getChunk().isLoaded()).forEach(IComponent::updateDisplay);
        } else if (!activated && nbPlayers > 0) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().finest("Area activated");
            activated = true;
            //Start task for components
            components.forEach(component -> {
                if (component instanceof ITaskComponent) {
                    ((ITaskComponent) component).startTask();
                }
                component.startScheduledActions();
            });

            //Delete potential phantom entities
            removeAllASEntitiesInside();

            reset();

            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                //Update components (show their new position)
                components.stream().filter(c -> c.getLocation().getChunk().isLoaded()).forEach(IComponent::updateDisplay);
            }, 1);
        }
    }

    /**
     * resets the area
     */
    public void reset() {
        components.forEach(IComponent::reset);
        laserParticles.clear();
        laserParticlesToAdd.clear();
        if (isLightApiEnabled) {
            HashSet<ChunkInfo> chunksToUpdateLight = new HashSet<>();
            previousLightLevelPerLocations.forEach((location, lightLevel) -> {
                LightAPI.deleteLight(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), LightType.BLOCK, false);
                chunksToUpdateLight.addAll(LightAPI.collectChunks(location, LightType.BLOCK, lightLevel));
            });
            previousLightLevelPerLocations.clear();
            lightLevelPerLocations.forEach((location, lightLevel) -> {
                LightAPI.deleteLight(location.getWorld(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), LightType.BLOCK, false);
                chunksToUpdateLight.addAll(LightAPI.collectChunks(location, LightType.BLOCK, lightLevel));
            });
            lightLevelPerLocations.clear();
            //Send chunks update to players
            for (ChunkInfo chunkToUpdate : chunksToUpdateLight) {
                LightAPI.updateChunk(chunkToUpdate, LightType.BLOCK);
            }
            chunksToUpdateLight.clear();
            wasLightUpdatedLastRun = false;
        }
        clearAttractionRepulsionCache();
        clearBlockReflectionCache();
        nbActivatedLaserReceivers = 0;
        lastNbActivatedLaserReceivers = 0;
        nbActivatedRedstoneSensor = 0;
        lastNbActivatedRedstoneSensor = 0;
        componentByLocationCache.clear();
        locationsWithNoComponent.clear();
        //Kill previous entities (hide components)
    }

    /**
     * updates laser particles
     */
    @SuppressWarnings({"ResultOfObjectAllocationIgnored"})
    public void updateLasers() {
        updateNbPlayer();
        updateNbLaserReceivers();
        updateNbRedstoneSensor();
        updateNbLocks();
        //Updating IDetection components
        updateDetectionComponents();
        //update existing laser particle
        updateParticles();
        //update Redstone Receivers (RedstoneSensor & Laser Sender)
        updateRedstoneReceivers();
    }

    private void updateNbLaserReceivers() {
        nbActivatedLaserReceivers = (int) components.stream().filter(c -> (c instanceof LaserReceiver && ((LaserReceiver) c).isActivated())).count();
        if (nbActivatedLaserReceivers != lastNbActivatedLaserReceivers) {
            Bukkit.getPluginManager().callEvent(new NBActivatedLaserReceiversChangeLEEvent(this, lastNbActivatedLaserReceivers, nbActivatedLaserReceivers));
        }
        lastNbActivatedLaserReceivers = nbActivatedLaserReceivers;
    }

    private void updateNbPlayer() {
        nbPlayersInside = getPlayersExceptSpectators().size();
        if (nbPlayersInside != lastNbPlayersInside) {
            Bukkit.getPluginManager().callEvent(new NBPlayersChangeLEEvent(this, lastNbPlayersInside, nbPlayersInside));
        }
        lastNbPlayersInside = nbPlayersInside;
    }

    private void updateNbRedstoneSensor() {
        nbActivatedRedstoneSensor = (int) components.stream().filter(c -> (c instanceof RedstoneSensor && ((RedstoneSensor) c).isPowered())).count();
        if (nbActivatedRedstoneSensor != lastNbActivatedRedstoneSensor) {
            Bukkit.getPluginManager().callEvent(new NBRedstoneSensorChangeLEEvent(this, lastNbActivatedRedstoneSensor, nbActivatedRedstoneSensor));
        }

        lastNbActivatedRedstoneSensor = nbActivatedRedstoneSensor;
    }

    private void updateNbLocks() {
        nbActivatedLocks = (int) components.stream().filter(c -> (c instanceof Lock && ((Lock) c).isOpened())).count();
        if (nbActivatedLocks != lastNbActivatedLocks) {
            Bukkit.getPluginManager().callEvent(new NBActivatedLocksChangeLEEvent(this, lastNbActivatedLocks, nbActivatedLocks));
        }

        lastNbActivatedLocks = nbActivatedLocks;
    }

    private void updateDetectionComponents() {
        components.stream()
                .filter(c -> c instanceof IDetectionComponent)
                .forEach(c -> {
                    IDetectionComponent detectionComponent = ((IDetectionComponent) c);
                    boolean previousState = detectionComponent.isActivated();
                    switch (detectionComponent.getMode()) {
                        case DETECTION_PLAYERS:
                            detectionComponent.setNbActivated(nbPlayersInside);
                            break;
                        case DETECTION_REDSTONE_SENSORS:
                            detectionComponent.setNbActivated(nbActivatedRedstoneSensor);
                            break;
                        case DETECTION_LASER_RECEIVERS:
                            detectionComponent.setNbActivated(nbActivatedLaserReceivers);
                            break;
                        case DETECTION_LOCKS:
                            detectionComponent.setNbActivated(nbActivatedLocks);
                            break;
                        default:
                            break;
                    }
                    boolean newState = detectionComponent.isActivated();
                    if (previousState != newState) {
                        if (newState) {
                            Bukkit.getPluginManager().callEvent(new ConditionalComponentActivatedLEEvent(detectionComponent));
                        } else {
                            Bukkit.getPluginManager().callEvent(new ConditionalComponentDeactivatedLEEvent(detectionComponent));
                        }
                    }
                });
    }

    private void updateParticles() {
        laserDamageLooper = (laserDamageLooper + 1) % LASER_DAMAGE_FREQUENCY;
        laserShowLooper = (laserShowLooper + 1) % LASERS_SHOW_FREQUENCY;
        laserLightUpdateLooper = (laserLightUpdateLooper + 1) % LASERS_LIGHT_UPDATE_FREQUENCY;

        //update each particle (move, show, interact). Removes them if they return false
        laserParticles.removeIf(l -> !l.update(laserDamageLooper == 0, laserShowLooper == 0));

        //update light
        updateLight();
        //add just created laser particles (by pass_through)
        laserParticles.addAll(laserParticlesToAdd);
        laserParticlesToAdd.clear();

        //create laser particle from laser_senders
        createLaserSendersParticles();

        //create laser particle from concentrators
        createConcentratorsParticles();

        //show LaserReceivers corners colors
        showLaserReceiversCornersParticles();
    }

    private void createLaserSendersParticles() {
        components.stream()
                .filter(c -> c.getType().equals(ComponentType.LASER_SENDER))
                .map(LaserSender.class::cast)
                .filter(LaserSender::isActivated)
                .forEach(c -> this.addLaserParticle(new LaserParticle(c, this)));
    }

    private void createConcentratorsParticles() {
        components.stream()
                .filter(c -> c.getType().equals(ComponentType.CONCENTRATOR))
                .forEach(c -> {
                    Concentrator concentrator = ((Concentrator) c);
                    LasersColor color = concentrator.getResultingColor();
                    if (color != null) {
                        this.addLaserParticle(new LaserParticle(concentrator, color, this));
                    }
                });
    }

    private void showLaserReceiversCornersParticles() {
        components.stream()
                .filter(c -> c.getType().equals(ComponentType.LASER_RECEIVER))
                .map(c -> ((LaserReceiver) c))
                .filter(lr -> !lr.isActivated())
                .forEach(lr -> {
                    if (isLightApiEnabled && lr.getLightLevel() != 0)
                        lightLevelPerLocations.put(lr.getLocation(), lr.getLightLevel());
                    Color color = lr.getColor().getBukkitColor();
                    lr.getFrontCorners().forEach(l -> {
                        NMSManager.getNMS().playEffect(getPlayers(), l, "REDSTONE", color);
                    });
                });
    }

    private void updateLight() {
        if (isLightApiEnabled && laserLightUpdateLooper == 0) {

            Set<Location> lightLocations = lightLevelPerLocations.keySet();


            Set<Location> previousLightLocations = previousLightLevelPerLocations.keySet();

            //The created light locations correspond to lightLevelPerLocations (current) - previousLightLevelPerLocations (last run).
            Set<Location> createdLightLocations = new HashSet<>(lightLocations);
            createdLightLocations.removeAll(previousLightLocations);

            //The removed light locations correspond to previousLightLevelPerLocations (last run) - lightLevelPerLocations (current).
            Set<Location> removedLightLocations = new HashSet<>(previousLightLocations);
            removedLightLocations.removeAll(lightLocations);

            //were there any changes in the particles locations that should impact the lighting of this area.
            boolean lightShouldChange = (!createdLightLocations.isEmpty() || !removedLightLocations.isEmpty());

            //Even if there were no changes, if the lighting was updated last run, we will do another (last) update, just to fix that last situation (without doing it, we have lighting issues).
            if (lightShouldChange || wasLightUpdatedLastRun) {

                HashSet<ChunkInfo> chunksToUpdateLight = new HashSet<>();

                //delete light were there is no particle anymore
                removedLightLocations.forEach(location -> {
                    LightAPI.deleteLight(location, LightType.BLOCK, false);
                    chunksToUpdateLight.addAll(LightAPI.collectChunks(location, LightType.BLOCK, previousLightLevelPerLocations.get(location)));
                });

                if (lightShouldChange) {
                    //create light were new particles appeared
                    createdLightLocations.forEach(location -> {
                        Integer lightLevel = lightLevelPerLocations.get(location);
                        LightAPI.createLight(location, LightType.BLOCK, lightLevel, false);
                        chunksToUpdateLight.addAll(LightAPI.collectChunks(location, LightType.BLOCK, lightLevel));
                    });
                } else {
                    //update the whole lighting a last time
                    lightLevelPerLocations.forEach((location, lightLevel) -> {
                        LightAPI.createLight(location, LightType.BLOCK, lightLevel, false);
                        chunksToUpdateLight.addAll(LightAPI.collectChunks(location, LightType.BLOCK, lightLevel));
                    });
                }

                //Send chunks update to players
                for (ChunkInfo updatedChunk : chunksToUpdateLight) {
                    LightAPI.updateChunk(updatedChunk, LightType.BLOCK);
                }
                chunksToUpdateLight.clear();

                //This boolean is used to update the light one last time even if there were no changes
                wasLightUpdatedLastRun = lightShouldChange;
            }

            //prepare next execution
            previousLightLevelPerLocations.clear();
            previousLightLevelPerLocations.putAll(lightLevelPerLocations);
            lightLevelPerLocations.clear();
        }
    }

    private void updateRedstoneReceivers() {
        this.getComponents()
                .stream()
                .filter(c -> c.getType().equals(ComponentType.LASER_SENDER))
                .map(LaserSender.class::cast)
                .filter(ls -> ls.getMode() == DetectionMode.DETECTION_REDSTONE)
                .forEach(ls -> {
                    final Block b = ls.getLocation().getBlock();
                    ls.setPowered(b.isBlockIndirectlyPowered() || b.isBlockPowered());
                });

        this.getComponents()
                .stream()
                .filter(c -> c.getType().equals(ComponentType.REDSTONE_SENSOR))
                .map(RedstoneSensor.class::cast)
                .forEach(rs -> {
                    final Block b = rs.getLocation().getBlock();
                    rs.setPowered(b.isBlockIndirectlyPowered() || b.isBlockPowered());
                });
    }

    public void setPower(Location source, boolean powered) {
        getRedstoneSignalUpdateLocations(source).forEach(l -> {
            Block block = l.getBlock();
            int newCurrent = 0, oldCurrent = 0;
            if (powered) {
                poweredLocation.add(l);
                if (block.getType() == Material.REDSTONE_WIRE) {
                    NMSManager.getNMS().powerBlock(block, (byte) 15);
                    newCurrent = 15;
                }
            } else {
                poweredLocation.remove(l);
                if (block.getType() == Material.REDSTONE_WIRE) {
                    NMSManager.getNMS().powerBlock(block, (byte) 0);
                    oldCurrent = 0;
                }
            }
            callLater(new BlockRedstoneEvent(block, oldCurrent, newCurrent));
        });
    }

    public void onBlockRedstoneEvent(BlockRedstoneEvent e, Location loc) {
        if (!poweredLocation.contains(loc)) {
            return;
        }
        e.setNewCurrent(15);
    }

    public void onBlockBreakEvent(BlockPlaceEvent e) {
        if (!poweredLocation.contains(e.getBlock().getLocation())) {
            return;
        }
        updateRedstone();
    }

    public void onBlockBreakEvent(BlockBreakEvent e) {
        //updateRedstone();
    }

    public void updateRedstone() {
        poweredLocation.stream().forEach(l -> {
            Block block = l.getBlock();
            int newCurrent = 0, oldCurrent = 0;
            switch (block.getType()) {
                case REDSTONE_WIRE:
                    NMSManager.getNMS().powerBlock(block, (byte) 15);
                    newCurrent = 15;
                    break;
                default:
                    break;
            }
        });
    }

    public void deleteAreaCheckpoint() {
        this.checkpoint = null;
    }

    public Location getCheckpoint() {
        return this.checkpoint;
    }

    public void setCheckpoint(Location checkpointLocation) throws NoAreaFoundException {
        if (checkpointLocation != null && !this.containsLocation(checkpointLocation)) {
            throw new NoAreaFoundException();
        }
        this.checkpoint = checkpointLocation;
    }

    public void deleteVictoryAreas() {
        this.victoryAreas.clear();
    }

    public void addVictoryArea(VictoryArea victoryArea) {
        this.victoryAreas.add(victoryArea);
    }

    public HashSet<VictoryArea> getVictoryAreas() {
        return this.victoryAreas;
    }

    public void modify(double minX, double minY, double minZ, double maxX, double maxY, double maxZ) throws AreaNoDepthException, AreaOverlapException, ComponentOutsideAreaException {
        Location newMin = new Location(min.getWorld(), minX, minY, minZ);
        Location newMax = new Location(max.getWorld(), maxX, maxY, maxZ);

        if (minX >= maxX || minY >= maxY || minZ >= maxZ) {
            throw new AreaNoDepthException();
        }
        Area overlappingArea = Areas.getInstance().getOverlappingOtherAreas(min.getWorld(), minX, minY, minZ, maxX, maxY, maxZ, Collections.singleton(this));
        if (overlappingArea != null) {
            throw new AreaOverlapException(overlappingArea);
        }

        IComponent componentOutsideArea = components.stream().filter(c -> !Area.containsLocation(c.getLocation(), newMin, newMax)).findAny().orElse(null);
        if (componentOutsideArea != null) {
            throw new ComponentOutsideAreaException(componentOutsideArea);
        }

        this.min = newMin;
        this.max = newMax;
        Areas.getInstance().clearAreaByLocationCache();
        clearBlockReflectionCache();
        clearAttractionRepulsionCache();
        dbUpdate();
    }
}
