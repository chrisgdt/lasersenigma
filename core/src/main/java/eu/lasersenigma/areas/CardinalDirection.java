package eu.lasersenigma.areas;

import java.util.*;
import java.util.stream.Collectors;

public enum CardinalDirection {
    DOWN(Collections.singleton("bottom")),
    UP(Collections.singleton("top")),
    EAST(new HashSet<String>()),
    WEST(new HashSet<String>()),
    SOUTH(new HashSet<String>()),
    NORTH(new HashSet<String>()),
    ALL(new HashSet<String>());

    public static CardinalDirection fromString(String name) {
        String lowerCaseName = name.toLowerCase();
        return Arrays.stream(CardinalDirection.values())
                .filter(direction -> direction.toString().toLowerCase().equals(name) || direction.names.contains(lowerCaseName))
                .findFirst().orElse(null);
    }

    public static List<String> getAllNames() {
        return Arrays.stream(CardinalDirection.values())
                .flatMap(c -> {
                    List<String> names = new ArrayList<>(Collections.singletonList(c.toString().toLowerCase()));
                    names.addAll(c.getNames());
                    return names.stream();
                }).collect(Collectors.toList());
    }

    private final Set<String> names;

    CardinalDirection(Set<String> names) {
        this.names = names;
    }

    public Set<String> getNames() {
        return names;
    }
}
