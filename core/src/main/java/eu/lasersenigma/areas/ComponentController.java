package eu.lasersenigma.areas;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.events.components.*;
import eu.lasersenigma.exceptions.ComponentAlreadyInsideAreaException;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.exceptions.NotSameAreaException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.player.PlayerInventoryManager;
import eu.lasersenigma.tasks.ColorChangeTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class ComponentController {

    private ComponentController() {
    }

    /**
     * Create component from a player action
     *
     * @param componentType    the type of the component to create
     * @param clickedBlock     the clicked block
     * @param clickedBlockFace the face clicked
     * @param player           the player that started this action
     * @param linkedComponent  the component linked to this one (for example the
     *                         lock if this component is meant to be a chest)
     * @return the created component
     */
    public static IComponent addComponentFromBlockFace(ComponentType componentType, Block clickedBlock, BlockFace clickedBlockFace, LEPlayer player, IComponent linkedComponent) {
        Location newComponentLocation = clickedBlock.getLocation().add(clickedBlockFace.getModX(), clickedBlockFace.getModY(), clickedBlockFace.getModZ());
        Area area = Areas.getInstance().getAreaFromLocation(newComponentLocation);
        if (area == null) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            return null;
        }
        if (area.getComponentFromLocation(newComponentLocation) != null) {
            return null;
        }
        ComponentFace face = ComponentFace.from(clickedBlockFace);
        if (face == null) {
            return null;
        }
        PlayerTryToCreateComponentLEEvent compEvent = new PlayerTryToCreateComponentLEEvent(player.getBukkitPlayer(), area, componentType, newComponentLocation, face);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return null;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return null;
        }
        if (componentType == ComponentType.MIRROR_CHEST) {
            float dir = (float) Math.toDegrees(Math.atan2(player.getBukkitPlayer().getLocation().getBlockX() - newComponentLocation.getX(), newComponentLocation.getZ() - player.getBukkitPlayer().getLocation().getBlockZ()));
            dir = dir % 360;

            if (dir < 0) {
                dir += 360;
            }

            dir = Math.round(dir / 90);
            switch ((int) dir) {

                case 0:
                    face = ComponentFace.NORTH;
                    break;
                case 1:
                    face = ComponentFace.EAST;
                    break;
                case 2:
                    face = ComponentFace.SOUTH;
                    break;
                default:
                    face = ComponentFace.WEST;
                    break;
            }
        }
        IComponent component = ComponentFactory.createComponentFromBlockFace(area, componentType, newComponentLocation, face, linkedComponent, null);
        if (component == null) {
            return null;
        }

        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "addComponentFromBlockFace => addComponent(component: {0} location: {1},{2},{3})", component.getType().name(), component.getLocation().getBlockX(), component.getLocation().getBlockY(), component.getLocation().getBlockZ());
        area.addComponent(component);
        return component;
    }

    public static IAreaComponent addIAreaComponent(LEPlayer player, Location firstLocation, Location secondLocation, ComponentType componentType) {
        Area area = Areas.getInstance().getAreaFromLocation(firstLocation);
        Area area2 = Areas.getInstance().getAreaFromLocation(secondLocation);
        if (area != area2) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NotSameAreaException());
        }

        World w = firstLocation.getWorld();

        //Defining min and max location:
        double[] xCoordinates = new double[2];
        xCoordinates[0] = firstLocation.getBlockX();
        xCoordinates[1] = secondLocation.getBlockX();
        Arrays.sort(xCoordinates);

        double[] yCoordinates = new double[2];
        yCoordinates[0] = firstLocation.getBlockY();
        yCoordinates[1] = secondLocation.getBlockY();
        Arrays.sort(yCoordinates);

        double[] zCoordinates = new double[2];
        zCoordinates[0] = firstLocation.getBlockZ();
        zCoordinates[1] = secondLocation.getBlockZ();
        Arrays.sort(zCoordinates);
        Location minLoc = new Location(w, xCoordinates[0], yCoordinates[0], zCoordinates[0]);
        Location maxLoc = new Location(w, xCoordinates[1], yCoordinates[1], zCoordinates[1]);

        for (double x = xCoordinates[0]; x <= xCoordinates[1]; x++) {
            for (double y = yCoordinates[0]; y <= yCoordinates[1]; y++) {
                for (double z = zCoordinates[0]; z <= zCoordinates[1]; z++) {
                    if (area.getComponentFromLocation(new Location(w, x, y, z)) != null) {
                        ErrorsConfig.showError(player.getBukkitPlayer(), new ComponentAlreadyInsideAreaException());
                        return null;
                    }
                }
            }
        }

        IComponent component = ComponentFactory.createComponentFromBlockFace(area, componentType, minLoc, null, null, maxLoc);
        if (component == null) {
            return null;
        }

        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "addIAreaComponent => addIAreaComponent(component: {0} minLoc: {1},{2},{3} minLoc: {4},{5},{6})", component.getType().name(), xCoordinates[0], yCoordinates[0], zCoordinates[0], xCoordinates[1], yCoordinates[1], zCoordinates[1]);
        area.addComponent(component);
        return (IAreaComponent) component;

    }

    /**
     * Create component from a player action
     *
     * @param componentType    the type of the component to create
     * @param clickedBlock     the clicked block
     * @param clickedBlockFace the face clicked
     * @param player           the player that started this action
     * @return the created component
     */
    public static IComponent addComponentFromBlockFace(ComponentType componentType, Block clickedBlock, BlockFace clickedBlockFace, LEPlayer player) {
        return addComponentFromBlockFace(componentType, clickedBlock, clickedBlockFace, player, null);
    }

    public static void addColoredMaterialFromBlockFace(Material material, Block clickedBlock, BlockFace clickedBlockFace, LEPlayer player, LasersColor color) {
        Location newMaterialLocation = clickedBlock.getLocation().add(clickedBlockFace.getModX(), clickedBlockFace.getModY(), clickedBlockFace.getModZ());
        Area area = Areas.getInstance().getAreaFromLocation(newMaterialLocation);
        if (area == null) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            return;
        }
        if (area.getComponentFromLocation(newMaterialLocation) != null) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Block block = newMaterialLocation.getBlock();
        NMSManager.getNMS().setBlock(block, material, color);
    }

    /**
     * Removes a component
     *
     * @param area      the area containing the component
     * @param player    the player that started this action
     * @param component the component to hide
     * @return true if the component was deleted
     */
    public static boolean removeComponent(Area area, LEPlayer player, IComponent component) {
        if (!player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return true;
        }
        if (!component.canBeDeleted()) {
            return true;
        }
        PlayerTryToDeleteComponentLEEvent compEvent = new PlayerTryToDeleteComponentLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return true;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return true;
        }
        area.removeComponent(component);
        LEPlayers.getInstance().onComponentDeleted(component);
        return false;
    }

    /**
     * change the color of a component
     *
     * @param area      the area containing the component
     * @param component the component
     * @param player    the player that started this action
     * @param color     the color to set
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void setColor(Area area, IColorableComponent component, LEPlayer player, LasersColor color) {
        PlayerTryToChangeColorLEEvent compEvent = new PlayerTryToChangeColorLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setColor(color, player.getInventoryManager().isInEditionMode());
        new ColorChangeTask(component.getLocation(), component.getColor(), area.getPlayers());
        player.saveActions(component, ActionType.getFromColor(color));
        LEPlayers.getInstance().onComponentUpdated(component);
    }

    /**
     * change the color of a component
     *
     * @param area      the area containing the component
     * @param component the component
     * @param player    the player that started this action
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void changeColor(Area area, IColorableComponent component, LEPlayer player) {
        PlayerTryToChangeColorLEEvent compEvent = new PlayerTryToChangeColorLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.changeColor(player.getInventoryManager().isInEditionMode());
        new ColorChangeTask(component.getLocation(), component.getColor(), area.getPlayers());
        player.saveActions(component, ActionType.COLOR_LOOP);
        LEPlayers.getInstance().onComponentUpdated(component);
    }

    /**
     * Defines if this laser sender accepts any colors or only specific one
     *
     * @param area      the area containing the component
     * @param receiver the component
     * @param player    the player that started this action
     */
    public static void setAnyColorAccepted(Area area, LaserReceiver receiver, LEPlayer player, boolean anyColorAccepted) {
        PlayerTryToChangeAnyColorAcceptedLEEvent receiverEvent = new PlayerTryToChangeAnyColorAcceptedLEEvent(player.getBukkitPlayer(), receiver);
        Bukkit.getServer().getPluginManager().callEvent(receiverEvent);
        if (receiverEvent.isCancelled()) {
            return;
        }
        if (!receiverEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        receiver.setAnyColorAccepted(anyColorAccepted);
        LEPlayers.getInstance().onComponentUpdated(receiver);
    }

    /**
     * place a mirror on a mirror support
     *
     * @param component the mirror support the mirror will be placed on
     * @param player    the player that is placing the mirror
     * @param color     the color of the mirror
     */
    public static void placeMirror(IMirrorContainer component, LEPlayer player, LasersColor color) {
        Area area = component.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        PlayerTryToPlaceMirrorLEEvent compEvent = new PlayerTryToPlaceMirrorLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        boolean inEditionMode = player.getInventoryManager().isInEditionMode();
        boolean authorized = true;
        if (component instanceof MirrorSupport) {
            boolean isPlacingAllowedWithoutPermissions = ((MirrorSupport) component).getMode().isMirrorRetrievable();
            authorized = isPlacingAllowedWithoutPermissions || inEditionMode;
        }
        if (!compEvent.getBypassPermissions() && !authorized) {
            return;
        }
        boolean mirrorPlaced = component.placeMirror(color, inEditionMode);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> LEPlayers.getInstance().onComponentUpdated(component), 31);
        if (mirrorPlaced) {
            player.getBukkitPlayer().getInventory().removeItem(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
            component.getArea().getStats().onActionDone(player.getBukkitPlayer());
        }
    }

    /**
     * gets a mirror off its mirror support
     *
     * @param component the mirror support the mirror will be retrieved from
     * @param player    the player that is retrieving the mirror
     */
    public static void getMirror(IMirrorContainer component, LEPlayer player) {
        Area area = component.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        PlayerTryToRetrieveMirrorLEEvent compEvent = new PlayerTryToRetrieveMirrorLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        boolean inEditionMode = player.getInventoryManager().isInEditionMode();
        boolean authorized = true;
        if (component instanceof MirrorSupport) {
            boolean isPlacingAllowedWithoutPermissions = ((MirrorSupport) component).getMode().isMirrorRetrievable();
            authorized = isPlacingAllowedWithoutPermissions || inEditionMode;
        }
        if (!compEvent.getBypassPermissions() && !authorized) {
            return;
        }
        boolean mirrorRetrieved = component.getMirror(inEditionMode);
        if (mirrorRetrieved) {
            final LasersColor color = component.getColor();
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> LEPlayers.getInstance().onComponentUpdated(component), 31);
            LEPlayers.getInstance().onMirrorRetrieved(component);
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                PlayerInventoryManager inventoryManager = player.getInventoryManager();
                if (inventoryManager.isRotationShortcutBarOpened()) {
                    inventoryManager.getInventorySaveManager().addToSavedInventory(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
                } else {
                    player.getBukkitPlayer().getInventory().addItem(
                            ItemsFactory.getInstance().getItemStack(Item.getMirror(color))
                    );
                }
            }, 38);
            component.getArea().getStats().onActionDone(player.getBukkitPlayer());
        }
    }

    public static void changeMirrorSupportMode(LEPlayer player, MirrorSupport mirrorSupport) {
        changeMirrorSupportMode(player, mirrorSupport, mirrorSupport.getMode().next());
    }

    public static void changeMirrorSupportMode(LEPlayer player, MirrorSupport mirrorSupport, MirrorSupportMode newMode) {
        PlayerTryToChangeMirrorSupportModeLEEvent compEvent = new PlayerTryToChangeMirrorSupportModeLEEvent(player.getBukkitPlayer(), mirrorSupport);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        mirrorSupport.changeMode(newMode);
        LEPlayers.getInstance().onComponentUpdated(mirrorSupport);
        newMode.getCorrespondingMessage().sendMessage(player.getBukkitPlayer());
    }

    public static void decreaseMin(LEPlayer player, IDetectionComponent component) {
        if (component.getMin() < 1) {
            return;
        }
        PlayerTryToChangeRangeLEEvent compEvent = new PlayerTryToChangeRangeLEEvent(player.getBukkitPlayer(), component, -1, 0);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setMin(component.getMin() - 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    public static void decreaseMax(LEPlayer player, IDetectionComponent component) {
        if (component.getMax() < 1) {
            return;
        }
        PlayerTryToChangeRangeLEEvent compEvent = new PlayerTryToChangeRangeLEEvent(player.getBukkitPlayer(), component, 0, -1);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (component.getMax() == component.getMin()) {
            component.setMin(component.getMin() - 1);
        }
        component.setMax(component.getMax() - 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    public static void increaseMin(LEPlayer player, IDetectionComponent component) {
        if (component.getMin() > 98) {
            return;
        }
        PlayerTryToChangeRangeLEEvent compEvent = new PlayerTryToChangeRangeLEEvent(player.getBukkitPlayer(), component, 1, 0);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (component.getMin() == component.getMax()) {
            component.setMax(component.getMax() + 1);
        }
        component.setMin(component.getMin() + 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    public static void increaseMax(LEPlayer player, IDetectionComponent component) {
        if (component.getMax() > 98) {
            return;
        }
        PlayerTryToChangeRangeLEEvent compEvent = new PlayerTryToChangeRangeLEEvent(player.getBukkitPlayer(), component, 0, 1);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setMax(component.getMax() + 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        rangeChanged(player, component.getMin(), component.getMax());
    }

    private static void rangeChanged(LEPlayer player, int min, int max) {
        Message.RANGE_CHANGED.sendMessage(player.getBukkitPlayer(), new String[]{Integer.toString(min), Integer.toString(max)});
    }

    /**
     * increase the number of mirrors inside the chest
     *
     * @param component the component
     * @param player    the player that started this action
     */
    public static void increaseMirrorChest(MirrorChest component, LEPlayer player) {
        if (component.getNbMirrors() > 98) {
            return;
        }
        PlayerTryToIncreaseMirrorChestLEEvent compEvent = new PlayerTryToIncreaseMirrorChestLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setNbMirrors(component.getNbMirrors() + 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        Message.MIRROR_CHEST_INCREASE.sendMessage(player.getBukkitPlayer(), new String[]{Integer.toString(component.getNbMirrors())});
    }

    /**
     * decrease the number of mirrors inside the chest
     *
     * @param component the component
     * @param player    the player that started this action
     */
    public static void decreaseMirrorChest(MirrorChest component, LEPlayer player) {
        if (component.getNbMirrors() < 2) {
            return;
        }
        PlayerTryToDecreaseMirrorChestLEEvent compEvent = new PlayerTryToDecreaseMirrorChestLEEvent(player.getBukkitPlayer(), component);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setNbMirrors(component.getNbMirrors() - 1);
        LEPlayers.getInstance().onComponentUpdated(component);
        Message.MIRROR_CHEST_DECREASE.sendMessage(player.getBukkitPlayer(), new String[]{Integer.toString(component.getNbMirrors())});
    }

    public static void changeMode(LEPlayer player, IDetectionComponent detectionComponent) {
        boolean isLaserSender = detectionComponent instanceof LaserSender;
        changeMode(player, detectionComponent, detectionComponent.getMode().next(isLaserSender, false));
    }

    public static void changeMode(LEPlayer player, IDetectionComponent detectionComponent, DetectionMode newMode) {
        PlayerTryToChangeComponentDetectionModeLEEvent compEvent = new PlayerTryToChangeComponentDetectionModeLEEvent(player.getBukkitPlayer(), detectionComponent);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        detectionComponent.changeMode(newMode);
        LEPlayers.getInstance().onComponentUpdated(detectionComponent);
        newMode.getCorrespondingMessage().sendMessage(player.getBukkitPlayer());
    }

    public static void toogleLoop(LEPlayer player, MusicBlock musicBlock) {
        setLoop(player, musicBlock, !musicBlock.isLoop());
    }

    public static void setLoop(LEPlayer player, MusicBlock musicBlock, boolean newLoopState) {
        PlayerTryToToogleMusicBlockLoopLEEvent compEvent = new PlayerTryToToogleMusicBlockLoopLEEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (newLoopState) {
            Message.MUSIC_LOOP_TRUE.sendMessage(player.getBukkitPlayer());
        } else {
            Message.MUSIC_LOOP_FALSE.sendMessage(player.getBukkitPlayer());
        }
        musicBlock.setLoop(newLoopState);
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static void toogleStopOnExit(LEPlayer player, MusicBlock musicBlock) {
        setStopOnExit(player, musicBlock, !musicBlock.isStopOnExit());
    }

    public static void setStopOnExit(LEPlayer player, MusicBlock musicBlock, boolean newStopOnExitState) {
        PlayerTryToToogleMusicBlockStopOnExitLEEvent compEvent = new PlayerTryToToogleMusicBlockStopOnExitLEEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (newStopOnExitState) {
            Message.MUSIC_STOP_ON_EXIT_TRUE.sendMessage(player.getBukkitPlayer());
        } else {
            Message.MUSIC_STOP_ON_EXIT_FALSE.sendMessage(player.getBukkitPlayer());
        }
        musicBlock.setStopOnExit(newStopOnExitState);
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static void prevSong(LEPlayer player, MusicBlock musicBlock) {
        PlayerTryToChangeMusicBlockSongLEEvent compEvent = new PlayerTryToChangeMusicBlockSongLEEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        musicBlock.prevSong(player.getBukkitPlayer());
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static void nextSong(LEPlayer player, MusicBlock musicBlock) {
        PlayerTryToChangeMusicBlockSongLEEvent compEvent = new PlayerTryToChangeMusicBlockSongLEEvent(player.getBukkitPlayer(), musicBlock);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        musicBlock.nextSong(player.getBukkitPlayer());
        LEPlayers.getInstance().onComponentUpdated(musicBlock);
    }

    public static boolean canModify(LEPlayer player, IPlayerModifiableComponent playerModifiableComponent) {
        if (playerModifiableComponent == null) {
            return false;
        }
        if (playerModifiableComponent instanceof MirrorSupport && !((MirrorSupport) playerModifiableComponent).hasMirror()) {
            return false;
        }
        Area area = playerModifiableComponent.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return false;
        }
        if (playerModifiableComponent instanceof IRotatableComponent) {
            return area.isRotationAllowedWithoutPermissions(playerModifiableComponent.getType(), RotationType.UP) || area.isRotationAllowedWithoutPermissions(playerModifiableComponent.getType(), RotationType.RIGHT);
        }
        if (playerModifiableComponent instanceof AttractionRepulsionSphere) {
            return area.isAttractionRepulsionSpheresResizingAllowed();
        }
        return false;
    }

    public static void rotate(LEPlayer player, IRotatableComponent component, RotationType rotationType) {
        if (component == null) {
            return;
        }
        MirrorSupport mirrorSupport = null;
        if (component instanceof MirrorSupport) {
            mirrorSupport = (MirrorSupport) component;
            if (!mirrorSupport.hasMirror()) {
                return;
            }
        }
        Area area = component.getArea();
        if (!area.containsLocation(player.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        boolean rotationAllowed = true;
        boolean save = true;
        boolean hasEditPermission = Permission.EDIT.hasPermission(player.getBukkitPlayer()) && player.getInventoryManager().isInEditionMode();
        boolean isRotationAllowedWithoutPermissions = area.isRotationAllowedWithoutPermissions(component.getType(), rotationType);
        if (mirrorSupport != null) {
            isRotationAllowedWithoutPermissions = isRotationAllowedWithoutPermissions && mirrorSupport.getMode().isMirrorRotatable();
        }

        if (!hasEditPermission) {
            save = false;
            if (!isRotationAllowedWithoutPermissions) {
                rotationAllowed = false;
            }
        }
        PlayerTryToRotateComponentLEEvent compEvent = new PlayerTryToRotateComponentLEEvent(player.getBukkitPlayer(), component, rotationType);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !rotationAllowed) {
            return;
        }
        component.rotate(rotationType, save);
        player.saveActions(component, ActionType.getFromRotation(rotationType));
        LEPlayers.getInstance().onComponentUpdated(component);
        area.getStats().onActionDone(player.getBukkitPlayer());
    }

    public static void changeMode(LEPlayer player, AttractionRepulsionSphere attractionRepulsionSphere) {
        changeMode(player, attractionRepulsionSphere, attractionRepulsionSphere.getMode().next());
    }

    public static void changeMode(LEPlayer player, AttractionRepulsionSphere attractionRepulsionSphere, AttractionRepulsionSphereMode newMode) {
        PlayerTryToChangeAttractionRepulsionSphereModeLEEvent compEvent = new PlayerTryToChangeAttractionRepulsionSphereModeLEEvent(player.getBukkitPlayer(), attractionRepulsionSphere);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        attractionRepulsionSphere.changeMode(newMode);
        LEPlayers.getInstance().onComponentUpdated(attractionRepulsionSphere);
        newMode.getCorrespondingMessage().sendMessage(player.getBukkitPlayer());
    }

    public static void modifySize(LEPlayer p, AttractionRepulsionSphere attractionRepulsionSphere, boolean increase) {
        Area area = attractionRepulsionSphere.getArea();
        if (!area.containsLocation(p.getBukkitPlayer().getLocation()) || !area.isActivated()) {
            return;
        }
        boolean resizingAllowed = true;
        boolean save = true;
        boolean hasEditPermission = Permission.EDIT.hasPermission(p.getBukkitPlayer()) && p.getInventoryManager().isInEditionMode();
        boolean isResizingAllowedWithoutPermissions = attractionRepulsionSphere.getArea().isAttractionRepulsionSpheresResizingAllowed();
        if (!hasEditPermission) {
            save = false;
            if (!isResizingAllowedWithoutPermissions) {
                resizingAllowed = false;
            }
        }
        int oldSize = attractionRepulsionSphere.getCurrentSize();
        int newSize = oldSize;
        if (increase) {
            newSize++;
            if (newSize > 9) {
                attractionRepulsionSphere.showSphere();
                return;
            }
        } else {
            newSize--;
            if (newSize < 1) {
                attractionRepulsionSphere.showSphere();
                return;
            }
        }
        PlayerTryToChangeAttractionRepulsionSphereSizeLEEvent compEvent = new PlayerTryToChangeAttractionRepulsionSphereSizeLEEvent(p.getBukkitPlayer(), attractionRepulsionSphere, oldSize, newSize);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !resizingAllowed) {
            return;
        }
        attractionRepulsionSphere.setSize(newSize, save);
        p.saveActions(attractionRepulsionSphere, increase ? ActionType.SPHERE_INCREASE : ActionType.SPHERE_DECREASE);
        LEPlayers.getInstance().onComponentUpdated(attractionRepulsionSphere);
        Message.ATTRACTION_REPULSION_SPHERE_RESIZED.sendMessage(p.getBukkitPlayer());
    }

    public static void removeKeyChests(LEPlayer player, Lock lock) {
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        lock.getKeyChests().forEach(keyChest -> {
            keyChest.getArea().removeComponent(keyChest);
            LEPlayers.getInstance().onComponentDeleted(keyChest);
        });
        LEPlayers.getInstance().onComponentUpdated(lock);
        Message.ALL_LOCK_S_KEY_CHESTS_DELETED.sendMessage(player.getBukkitPlayer());
    }

    public static void setKeyNumber(LEPlayer player, KeyChest keyChest, int keyNb) {
        if (keyChest.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        keyChest.setKeyNumber(keyNb);
        LEPlayers.getInstance().onComponentUpdated(keyChest);
        int nbChestsForNumber = keyChest.getLock().getNbChestsForNumber(keyChest.getKeyNumber());
        if (nbChestsForNumber == 1) {
            Message.KEY_REQUIRED.sendMessage(player.getBukkitPlayer());
        } else {
            Message.KEY_OPTIONNAL.sendMessage(player.getBukkitPlayer(), new String[]{String.valueOf(nbChestsForNumber)});
        }
    }

    public static boolean elevatorGoUp(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return false;
        }
        Location cageMin = elevator.getCageMin();
        Location cageMax = elevator.getCageMax();
        Location pLoc = player.getBukkitPlayer().getLocation();
        double pX = pLoc.getBlockX();
        double pY = pLoc.getBlockY();
        double pZ = pLoc.getBlockZ();
        if (cageMin.getBlockX() <= pX && cageMin.getBlockY() <= pY && cageMin.getBlockZ() <= pZ && pX <= cageMax.getBlockX() && pY <= cageMax.getBlockY() && pZ <= cageMax.getBlockZ()) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "ComponentController.elevatorGoUp => true");
            PlayerTryToMoveElevatorLEEvent compEvent = new PlayerTryToMoveElevatorLEEvent(player.getBukkitPlayer(), elevator, ElevatorDirection.UP);
            Bukkit.getServer().getPluginManager().callEvent(compEvent);
            if (compEvent.isCancelled()) {
                return false;
            }
            boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
            return elevator.onPlayerJump(isInEditionMode);
        }
        return false;
    }

    public static boolean elevatorGoDown(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return false;
        }
        Location cageMin = elevator.getCageMin();
        Location cageMax = elevator.getCageMax();
        Location pLoc = player.getBukkitPlayer().getLocation();
        double pX = pLoc.getBlockX();
        double pY = pLoc.getBlockY();
        double pZ = pLoc.getBlockZ();
        if (cageMin.getBlockX() <= pX && cageMin.getBlockY() <= pY && cageMin.getBlockZ() <= pZ && pX <= cageMax.getBlockX() && pY <= cageMax.getBlockY() && pZ <= cageMax.getBlockZ()) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "ComponentController.elevatorGoDown => true");
            PlayerTryToMoveElevatorLEEvent compEvent = new PlayerTryToMoveElevatorLEEvent(player.getBukkitPlayer(), elevator, ElevatorDirection.DOWN);
            Bukkit.getServer().getPluginManager().callEvent(compEvent);
            if (compEvent.isCancelled()) {
                return false;
            }
            boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
            return elevator.onPlayerSneek(isInEditionMode);
        }
        return false;
    }

    public static boolean elevatorCallButton(LEPlayer player, CallButton callButton) {
        if (callButton.isRemoved()) {
            return false;
        }
        PlayerTryToCallElevatorLEEvent compEvent = new PlayerTryToCallElevatorLEEvent(player.getBukkitPlayer(), callButton);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return false;
        }
        boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
        return callButton.onCallButton(isInEditionMode);
    }

    public static void elevatorCreateFloor(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        elevator.addFloorAtCurrentPosition();
        Message.ELEVATOR_FLOOR_CREATED.sendMessage(player.getBukkitPlayer());
    }

    public static void goToFloor(LEPlayer player, Elevator elevator, int index) {
        boolean isInEditionMode = player.getInventoryManager().isInEditionMode();
        elevator.moveToFloor(index, isInEditionMode);
    }

    public static void elevatorGoAsHighAsPossible(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        elevator.goAsHighAsPossible();
    }

    public static void elevatorDeleteFloors(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        elevator.clearFloors();
        Message.ELEVATOR_FLOORS_DELETED.sendMessage(player.getBukkitPlayer());
    }

    public static void elevatorDeleteCallButtons(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        Area area = elevator.getArea();
        ArrayList<CallButton> callButtons = area.getComponents().stream()
                .filter(component -> component instanceof CallButton)
                .map(component -> (CallButton) component)
                .filter(callButton -> callButton.getElevator() == elevator)
                .collect(Collectors.toCollection(ArrayList::new));
        callButtons.forEach(area::removeComponent);
        Message.ELEVATOR_CALL_BUTTONS_DELETED.sendMessage(player.getBukkitPlayer());

    }

    public static void elevatorTeleportInside(LEPlayer player, Elevator elevator) {
        if (elevator.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        player.getBukkitPlayer().teleport(elevator.getCageCenter());
    }

    public static void saveActionsDone(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (!player.isSavingActions()) {
            return;
        }
        ArrayList<SavedAction> savedActions = player.stopSavingActions();
        if (savedActions != null) {
            component.getScheduledActions().addAll(savedActions);
            component.updateActions();
            LEPlayers.getInstance().onComponentUpdated(component);
            int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
            if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
                Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
            }
        }
    }

    public static void clearScheduledActions(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.getScheduledActions().clear();
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
    }

    public static void addNewScheduledActionBefore(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.getScheduledActions().add(0, new SavedAction(component));
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void addNewScheduledActionAfter(LEPlayer player, IComponent component) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<SavedAction> scheduledActions = component.getScheduledActions();
        scheduledActions.add(scheduledActions.size(), new SavedAction(component));
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void deleteScheduledAction(LEPlayer player, IComponent component, SavedAction savedAction) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<SavedAction> scheduledActions = component.getScheduledActions();
        scheduledActions.remove(savedAction);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void moveScheduledActionBefore(LEPlayer player, IComponent component, SavedAction savedAction) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<SavedAction> scheduledActions = component.getScheduledActions();
        int index = scheduledActions.indexOf(savedAction);
        if (index == 0) {
            return;
        }
        Collections.swap(scheduledActions, index, index - 1);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void moveScheduledActionAfter(LEPlayer player, IComponent component, SavedAction savedAction) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        final ArrayList<SavedAction> scheduledActions = component.getScheduledActions();
        int index = scheduledActions.indexOf(savedAction);
        if (index == scheduledActions.size() - 1) {
            return;
        }
        Collections.swap(scheduledActions, index, index + 1);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void changeScheduledActionType(LEPlayer player, IComponent component, SavedAction action, ActionType actionType) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (!component.getScheduledActions().contains(action)) {
            return;
        }
        action.setType(actionType);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void modifyScheduledActionDelay(LEPlayer player, IComponent component, SavedAction action, int delayDiff) {
        if (component.isRemoved()) {
            return;
        }
        if (!player.getInventoryManager().isInEditionMode() || !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        if (!component.getScheduledActions().contains(action)) {
            return;
        }
        int newDelay = action.getDelay() + delayDiff;
        if (newDelay < 1) {
            newDelay = 1;
        } else if (newDelay > 99) {
            newDelay = 99;
        }
        action.setDelay(newDelay);
        component.updateActions();
        LEPlayers.getInstance().onComponentUpdated(component);
        int delay = component.getScheduledActions().stream().filter(curAction -> curAction.getType() == ActionType.WAIT).map(SavedAction::getDelay).reduce(Integer::sum).orElse(0);
        if ((!component.getScheduledActions().isEmpty()) && delay < 10) {
            Message.SCHEDULED_ACTIONS_DELAY_REQUIRED.sendMessage(player.getBukkitPlayer());
        }
    }

    public static void changeLightLevel(LEPlayer player, ILightComponent component, int diff) {
        int newValue = component.getLightLevel() + diff;

        if (newValue > 15) newValue = 15;
        else if (newValue < 0) newValue = 0;

        PlayerTryToChangeLightLevelLEEvent compEvent = new PlayerTryToChangeLightLevelLEEvent(player.getBukkitPlayer(), component, newValue);
        Bukkit.getServer().getPluginManager().callEvent(compEvent);
        if (compEvent.isCancelled()) {
            return;
        }
        if (!compEvent.getBypassPermissions() && !player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        component.setLightLevel(newValue);
        LEPlayers.getInstance().onComponentUpdated(component);
        Message.LIGHT_LEVEL_CHANGED.sendMessage(player.getBukkitPlayer(), new String[]{Integer.toString(newValue)});
    }
}
