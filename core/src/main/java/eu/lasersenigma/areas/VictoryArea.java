package eu.lasersenigma.areas;

import eu.lasersenigma.exceptions.AreaCrossWorldsException;
import eu.lasersenigma.exceptions.AreaNoDepthException;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.exceptions.VictoryAreaMustHaveCommonWallException;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Arrays;

public class VictoryArea {

    private final Area area;

    private final Location min;
    private final Location max;

    public VictoryArea(Area area, Location a, Location b) throws AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException, VictoryAreaMustHaveCommonWallException {
        if (!area.containsLocation(a) || !area.containsLocation(b)) {
            throw new NoAreaFoundException();
        }
        World world = a.getWorld();
        //Defining min and max location:
        double[] xCoordinates = new double[2];
        xCoordinates[0] = a.getBlockX();
        xCoordinates[1] = b.getBlockX();
        Arrays.sort(xCoordinates);

        double[] yCoordinates = new double[2];
        yCoordinates[0] = a.getBlockY();
        yCoordinates[1] = b.getBlockY();
        Arrays.sort(yCoordinates);

        double[] zCoordinates = new double[2];
        zCoordinates[0] = a.getBlockZ();
        zCoordinates[1] = b.getBlockZ();
        Arrays.sort(zCoordinates);

        if (xCoordinates[0] == xCoordinates[1] || yCoordinates[0] == yCoordinates[1] || zCoordinates[0] == zCoordinates[1]) {
            throw new AreaNoDepthException();
        }
        if (!(xCoordinates[0] == area.getMinLocation().getX()
                || yCoordinates[0] == area.getMinLocation().getY()
                || zCoordinates[0] == area.getMinLocation().getZ()
                || xCoordinates[1] == area.getMaxLocation().getX()
                || yCoordinates[1] == area.getMaxLocation().getY()
                || zCoordinates[1] == area.getMaxLocation().getZ())) {
            throw new VictoryAreaMustHaveCommonWallException();
        }
        this.min = new Location(world, xCoordinates[0], yCoordinates[0], zCoordinates[0]);
        this.max = new Location(world, xCoordinates[1], yCoordinates[1], zCoordinates[1]);
        this.area = area;
    }

    public boolean containsLocation(Location loc) {
        return AArea.containsLocation(loc, min, max);
    }

    public Area getArea() {
        return area;
    }

    public Location getMin() {
        return min;
    }

    public Location getMax() {
        return max;
    }

}
