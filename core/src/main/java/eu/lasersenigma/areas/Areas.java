package eu.lasersenigma.areas;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.database.Database;
import eu.lasersenigma.exceptions.*;
import eu.lasersenigma.tasks.DefeatSoundTask;
import eu.lasersenigma.tasks.VictorySoundTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Contains all the areas
 */
public class Areas {

    /**
     * the static instance of this class (one only class will be created)
     */
    private static Areas instance;
    /**
     * a set containing all the areas
     */
    private final LinkedHashSet<Area> areaSet;
    /**
     * a map linking entities uuid to components used to have a quicker access
     * to components
     */
    private final LinkedHashMap<UUID, IComponent> componentByEntityUUID;
    private final HashMap<Location, Area> areaByLocationCache;

    /**
     * Constructor
     */
    private Areas() {
        areaSet = new LinkedHashSet<>();
        componentByEntityUUID = new LinkedHashMap<>();
        areaByLocationCache = new HashMap<>();
    }

    /**
     * gets the instance of this class
     *
     * @return an instance of this class
     */
    public static Areas getInstance() {
        if (instance == null) {
            instance = new Areas();
        }
        return instance;
    }

    /**
     * Creates an area
     *
     * @param a                             first location (not minimal location yet, simply a corner
     *                                      location)
     * @param b                             second location (not maximal location yet, simply the opposite
     *                                      corner location)
     * @param areaId                        the id of the area in the database or -1 if created from
     *                                      ingame.
     * @param vRotationAllowed              Defines if the vertical rotation of mirrors is
     *                                      allowed in this area.
     * @param hRotationAllowed              Defines if the horizontal rotation of mirrors is
     *                                      allowed in this area
     * @param concentratorsvRotationAllowed Defines if the vertical rotation of
     *                                      concentrators is allowed in this area
     * @param concentratorshRotationAllowed Defines if the horizontal rotation
     *                                      of concentrators is allowed in this area
     * @param lsVRotationAllowed            Defines if the vertical rotation of laser
     *                                      senders is allowed in this area
     * @param lsHRotationAllowed            Defines if the horizontal rotation of laser
     *                                      senders is allowed in this area
     * @param lrVRotationAllowed            Defines if the vertical rotation of laser
     *                                      receivers is allowed in this area
     * @param lrHRotationAllowed            Defines if the horizontal rotation of laser
     *                                      receivers is allowed in this area
     * @param arspheresizing                Defines if the resizing of attraction/repulsion
     *                                      spheres area of effect is allowed
     * @param checkpoint                    the respawn point
     * @param victoryCriteria               The victory criteria for the area
     * @param minRange                      the minimum number of elements required for the victory
     * @param maxRange                      the maximum number of elements required for the victory
     * @return the created area
     * @throws AreaOverlapException     if the new area overlapps another area
     * @throws AreaCrossWorldsException if the 2 locations are in different
     *                                  worlds
     * @throws AreaNoDepthException     if the area has no depth/width/height
     * @throws NoAreaFoundException     if the checkpoint is not within the area
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public Area createArea(Location a, Location b, int areaId, boolean vRotationAllowed, boolean hRotationAllowed, boolean concentratorsvRotationAllowed, boolean concentratorshRotationAllowed, boolean lsVRotationAllowed, boolean lsHRotationAllowed, boolean lrVRotationAllowed, boolean lrHRotationAllowed, boolean arspheresizing, Location checkpoint, DetectionMode victoryCriteria, int minRange, int maxRange) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException {
        Area newArea = new Area(a, b);
        Area overlappingArea = getOverlappingOtherAreas(newArea);
        if (overlappingArea != null) {
            throw new AreaOverlapException(overlappingArea);
        }
        if (areaId == -1) {
            newArea.dbCreate();
            new VictorySoundTask(newArea);
        } else {
            newArea.setAreaId(areaId);
        }
        newArea.setVMirrorsRotationAllowed(vRotationAllowed);
        newArea.setHMirrorsRotationAllowed(hRotationAllowed);
        newArea.setHConcentratorsRotationAllowed(concentratorshRotationAllowed);
        newArea.setVConcentratorsRotationAllowed(concentratorsvRotationAllowed);
        newArea.setHLaserSendersRotationAllowed(lsHRotationAllowed);
        newArea.setVLaserSendersRotationAllowed(lsVRotationAllowed);
        newArea.setHLaserReceiversRotationAllowed(lrHRotationAllowed);
        newArea.setVLaserReceiversRotationAllowed(lrVRotationAllowed);
        newArea.setAttractionRepulsionSpheresResizingAllowed(arspheresizing);
        newArea.setCheckpoint(checkpoint);
        newArea.setVictoryCriteria(victoryCriteria);
        newArea.setMinimumRange(minRange);
        newArea.setMaximumRange(maxRange);
        areaSet.add(newArea);
        return newArea;
    }

    /**
     * deletes an area from a location
     *
     * @param area the area to delete
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void deleteArea(Area area) {
        area.remove();
        area.dbRemove();
        areaByLocationCache.entrySet().removeIf(e -> e.getValue().getAreaId() == area.getAreaId());
        new DefeatSoundTask(area);
        areaSet.remove(area);
    }

    public LinkedHashSet<Area> getAreaSet() {
        return areaSet;
    }

    /**
     * gets a component from an entity
     *
     * @param entityUUID the entity UUID used to find a component
     * @return the component corresponding to the given entity UUID
     */
    public IComponent getComponentFromEntity(UUID entityUUID) {
        return componentByEntityUUID.get(entityUUID);
    }

    /**
     * adds a pair entityUUID / component to the set used to have a quicker
     * access to components
     *
     * @param entityUUID the entity UUID
     * @param component  the component corresponding to this entity
     */
    public void addEntity(UUID entityUUID, IComponent component) {
        componentByEntityUUID.put(entityUUID, component);
    }

    /**
     * removes an entity from the entityUUID / component set
     *
     * @param entityUUID the entity UUID
     */
    public void removeEntity(UUID entityUUID) {
        componentByEntityUUID.remove(entityUUID);
    }

    /**
     * Gets an area from a location
     *
     * @param location a location potentially inside an area
     * @return the area containing the location, or null if the location is not
     * inside an area
     */
    public Area getAreaFromLocation(Location location) {
        Area cachedArea = areaByLocationCache.get(location);
        if (cachedArea == null) {
            for (Area area : areaSet) {
                if (area.containsLocation(location)) {
                    areaByLocationCache.put(location, area);
                    return area;
                }
            }
            return null;
        } else {
            return cachedArea;
        }
    }

    public void clearAreaByLocationCache() {
        areaByLocationCache.clear();
    }

    public Area getArea(int correspondingLockAreaId) {
        return this.areaSet.stream()
                .filter(area -> area.getAreaId() != correspondingLockAreaId)
                .findAny()
                .orElse(null);
    }

    /**
     * checks if the area overlapps another area
     *
     * @param area the area to test
     * @return true if the area is overlapping another existing area
     */
    public Area getOverlappingOtherAreas(Area area) {
        for (Area curArea : areaSet) {
            if (curArea.isOverlapping(area)) {
                return curArea;
            }
        }
        return null;
    }

    public Area getOverlappingOtherAreas(World world, double minX, double minY, double minZ, double maxX, double maxY, double maxZ, Set<Area> exceptions) {
        for (Area curArea : areaSet) {
            if (exceptions.contains(curArea)) {
                continue;
            }
            if (curArea.isOverlapping(world, minX, minY, minZ, maxX, maxY, maxZ)) {
                return curArea;
            }
        }
        return null;
    }

    /**
     * loads areas from the database
     */
    public void loadAreas() {
        removeAllASEntities();
        try {
            LasersEnigmaPlugin.getInstance().getPluginDatabase().loadData();
        } catch (AbstractLasersException ex) {
            ErrorsConfig.logError(ex);
        }

    }

    /**
     * Clears areas (todo on plugin disable)
     */
    public void clearAreas() {
        areaSet.forEach(a -> {
            a.reset();
            a.hideComponents();
        });
        for (Area area : areaSet) {
            area.reset();
        }
        removeAllASEntities();
        areaSet.clear();
    }

    /**
     * updates areas
     */
    public void updateAreas() {
        for (Area area : areaSet) {
            area.updateArea();
        }
    }

    /**
     * updates lasers particles
     */
    public void updateLasers() {
        areaSet.stream()
                .filter(AArea::isActivated)
                .forEach(Area::updateLasers);
    }

    protected void removeAllASEntities() {
        List<World> worldList = areaSet.stream()
                .map((area) -> area.getMinLocation().getWorld())
                .distinct()
                .collect(Collectors.toList());

        worldList
                .forEach((w) -> w.getEntities().stream()
                        .filter((entity) -> (entity instanceof ArmorStand))
                        .map((entity) -> (ArmorStand) entity)
                        .filter((as) -> (as.getCustomName() != null && as.getCustomName().equals(AArmorStandComponent.ARMORSTAND_CUSTOM_NAME)))
                        .forEach(Entity::remove)
                );
    }

    public void onWorldLoad(WorldLoadEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "Areas.onWorldLoad({0})", e.getWorld().getName());
        try {
            LasersEnigmaPlugin.getInstance().getPluginDatabase().loadData(e.getWorld().getName());
        } catch (AbstractLasersException ex) {
            ErrorsConfig.logError(ex);
        }
    }

    public void onWorldUnload(WorldUnloadEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "Areas.onWorldUnload({0})", e.getWorld().getName());
        areaSet.removeIf(a -> Objects.requireNonNull(a.getMinLocation().getWorld()).getUID().equals(e.getWorld().getUID()));
    }

    /**
     * This method copy the plugin's data from originWorldName to destinationWorldName
     * In order for this to work. the world destinationWorldName must no be loaded yet OR we need to call Database#loadData(destinationWorldName) after.
     *
     * @param originWorldName      the name of the world we want to copy
     * @param destinationWorldName the name of the world where we want to paste all data
     * @param clearAndReplace      should we delete this world data before
     * @throws IllegalStateException
     */
    public void copyWorldData(String originWorldName, String destinationWorldName, boolean clearAndReplace) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "copyWorldData(originWorldName: {0}, destinationWorldName: {1}, clearAndReplace: {2})", originWorldName, destinationWorldName, String.valueOf(clearAndReplace));
        if (Bukkit.getWorld(destinationWorldName) != null) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "A world must be unloaded in order to paste data in it.");
            return;
        }
        Database db = LasersEnigmaPlugin.getInstance().getPluginDatabase();
        if (clearAndReplace) {
            db.clearWorldData(destinationWorldName);
        }
        db.copyWorldData(originWorldName, destinationWorldName);
    }

    public void removeWorldData(String worldName) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "removeWorldData(worldName: {0})", worldName);
        if (Bukkit.getWorld(worldName) != null) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "A world must be unloaded in order to delete all its data.");
            return;
        }
        LasersEnigmaPlugin.getInstance().getPluginDatabase().clearWorldData(worldName);
    }
}
