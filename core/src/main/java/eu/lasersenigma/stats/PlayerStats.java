package eu.lasersenigma.stats;

import java.time.Duration;

public class PlayerStats {

    private final Duration duration;

    private final int nbAction;

    private final int nbStep;

    public PlayerStats(Duration duration, int nbAction, int nbStep) {
        this.duration = duration;
        this.nbAction = nbAction;
        this.nbStep = nbStep;
    }

    public Duration getDuration() {
        return duration;
    }

    public int getNbAction() {
        return nbAction;
    }

    public int getNbStep() {
        return nbStep;
    }

}
