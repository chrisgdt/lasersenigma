package eu.lasersenigma.events.permissions;

import eu.lasersenigma.events.parents.ALEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerPermissionsCheckLEEvent extends ALEEvent implements IPlayerLEEvent {

    private final Player player;

    private boolean checkResult;

    public PlayerPermissionsCheckLEEvent(Player player, boolean checkResult) {
        this.player = player;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public boolean getPermissionsCheckResult() {
        return checkResult;
    }

    public void setPermissionsCheckResult(boolean checkResult) {
        this.checkResult = checkResult;
    }

}
