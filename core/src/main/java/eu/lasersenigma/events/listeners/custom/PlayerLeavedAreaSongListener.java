package eu.lasersenigma.events.listeners.custom;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.events.areas.PlayerLeavedAreaLEEvent;
import eu.lasersenigma.songs.PlayersListSongManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class PlayerLeavedAreaSongListener implements Listener {

    private final PlayersListSongManager playersListSongManager;

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerLeavedAreaSongListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        playersListSongManager = PlayersListSongManager.getInstance();
    }

    /**
     * executed on a SongEndEvent
     *
     * @param e a SongEndEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerLeavedAreaLEEvent(PlayerLeavedAreaLEEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerLeavedAreaLEEvent");
        playersListSongManager.onPlayerLeavedArea(e.getPlayer());
    }
}
