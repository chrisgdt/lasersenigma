package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffectType;

import java.util.logging.Level;

/**
 * EventListener for PlayerMoveEvent
 */
public class PlayerJumpEventListener implements Listener {

    private static final double JUMP_VELOCITY = 0.4;

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerJumpEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a PlayerMoveEvent
     *
     * @param e a PlayerMoveEvent
     */
    @EventHandler
    public void onPlayerJump(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (player.getVelocity().getY() > 0 && !player.isFlying()) {
            double jumpVelocity = JUMP_VELOCITY;
            if (player.hasPotionEffect(PotionEffectType.JUMP)) {
                jumpVelocity += (double) ((float) (player.getPotionEffect(PotionEffectType.JUMP).getAmplifier() + 1) * 0.1F);
            }
            if (e.getPlayer().getLocation().getBlock().getType() != Material.LADDER) {
                if (!player.isOnGround() && player.getVelocity().getY() > jumpVelocity) {
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerJump");
                    LEPlayers.getInstance().findLEPlayer(player).onPlayerJump();
                }
            }
        }
    }

}
