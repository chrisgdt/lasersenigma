package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.logging.Level;

/**
 * EventListener for PlayerInteractAtEntityEvent
 */
public class EntityDamageByEntityEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public EntityDamageByEntityEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each PlayerInteractAtEntityEvent
     *
     * @param e a PlayerInteractAtEntityEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onEntityDamageByEntityEvent");
        if (e.getEntityType() != EntityType.ARMOR_STAND) {
            return;
        }

        Entity damager = e.getDamager();
        IComponent component = Areas.getInstance().getComponentFromEntity(e.getEntity().getUniqueId());
        if (component == null) {
            if (damager instanceof Player && Areas.getInstance().getAreaFromLocation(e.getEntity().getLocation()) != null && !Permission.EDIT.checkPermissionAndSendMsg((Player) damager)) {
                e.setCancelled(true);
            }
            return;
        }

        e.setCancelled(true);

        if (!(damager instanceof Player)) {
            return;
        }
        Player player = (Player) damager;
        LEPlayers.getInstance().findLEPlayer(player).onLeftClick(component);
    }
}
