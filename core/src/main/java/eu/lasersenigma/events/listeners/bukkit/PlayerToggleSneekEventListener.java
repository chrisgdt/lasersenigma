package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * EventListener for PlayerToggleSneakEvent
 */
public class PlayerToggleSneekEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerToggleSneekEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a PlayerToggleSneakEvent
     *
     * @param e a PlayerToggleSneakEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerToggleSneakEvent(PlayerToggleSneakEvent e) {
        if (e.getPlayer().isSneaking()) {
            return;
        }
        LEPlayers.getInstance().findLEPlayer(e.getPlayer()).onPlayerSneek();
    }
}
