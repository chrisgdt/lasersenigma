package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.logging.Level;

/**
 * EventListener for PlayerInteractEvent
 */
public class InventoryCloseEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public InventoryCloseEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each PlayerInteractEvent
     *
     * @param e a PlayerInteractEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onInventoryCloseEventListener(InventoryCloseEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onInventoryCloseEventListener");
        if (!(e.getPlayer() instanceof Player)) {
            return;
        }
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer((Player) e.getPlayer());
        lePlayer.getInventoryManager().onCloseInventory();
    }
}
