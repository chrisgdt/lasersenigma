package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.logging.Level;

/**
 * EventListener for InventoryClickEvent
 */
public class InventoryClickEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public InventoryClickEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each InvetoryClickEvent
     *
     * @param e a InvetoryClickEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onInventoryClickEvent(InventoryClickEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onInventoryClickEvent");
        HumanEntity human = e.getWhoClicked();
        if (!(human instanceof Player)) {
            return;
        }
        LEPlayers.getInstance().findLEPlayer((Player) human).onInventoryClick(e);
    }
}
