package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Permission;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.logging.Level;

/**
 * EventListener for BlockBreakEvent
 */
public class BlockBreakEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public BlockBreakEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a BlockBreakEvent
     *
     * @param e a BlockBreakEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onBlockBreakEvent(BlockBreakEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onBlockBreakEvent");
        Location loc = e.getBlock().getLocation();

        Area area = Areas.getInstance().getAreaFromLocation(loc);
        if (area == null) {
            return;
        }
        area.onBlockBreakEvent(e);
        IComponent component = area.getComponentFromLocation(loc);
        if (component != null) {
            e.setCancelled(true);
            return;
        }
        if (e.getPlayer() == null) {
            e.setCancelled(true);
            return;
        }
        if (!Permission.EDIT.hasPermission(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
}
