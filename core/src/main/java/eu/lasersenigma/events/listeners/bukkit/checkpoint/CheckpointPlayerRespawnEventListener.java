package eu.lasersenigma.events.listeners.bukkit.checkpoint;

import eu.lasersenigma.config.GetBackToCheckpointOnDeath;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class CheckpointPlayerRespawnEventListener implements Listener {

    private final GetBackToCheckpointOnDeath backToCheckpointOnDeath;

    public CheckpointPlayerRespawnEventListener(GetBackToCheckpointOnDeath backToCheckpointOnDeath) {
        this.backToCheckpointOnDeath = backToCheckpointOnDeath;
    }

    public void onPlayerRespawnEvent(PlayerRespawnEvent e) {
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        Location checkpoint = lePlayer.getOnRespawnCheckpoint(e.getPlayer().getWorld(), backToCheckpointOnDeath);
        if (checkpoint == null) {
            return;
        }
        e.setRespawnLocation(checkpoint);
    }

}
