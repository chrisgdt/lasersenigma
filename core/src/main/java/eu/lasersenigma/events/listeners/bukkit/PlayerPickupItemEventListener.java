package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.player.PlayerInventoryManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

import java.util.logging.Level;

/**
 * EventListener for PlayerPickupItemEventListener
 */
public class PlayerPickupItemEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerPickupItemEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a PlayerPickupItemEventListener
     *
     * @param e a PlayerPickupItemEventListener
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerPickupItemEvent(PlayerPickupItemEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerPickupItemEvent");
        PlayerInventoryManager pim = LEPlayers.getInstance().findLEPlayer(e.getPlayer()).getInventoryManager();
        if (pim.isRotationShortcutBarOpened()) {
            pim.closeRotationShortcutBarInventory(false);
        }
    }
}
