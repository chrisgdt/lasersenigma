package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Areas;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

import java.util.logging.Level;

/**
 * EventListener for WorldLoadEvent
 */
public class WorldLoadEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public WorldLoadEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each WorldLoadEvent
     *
     * @param e a WorldLoadEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onInventoryClickEvent(WorldLoadEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onWorldLoadEvent");
        Areas.getInstance().onWorldLoad(e);
    }
}
