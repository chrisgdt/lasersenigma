package eu.lasersenigma.events.listeners.bukkit.keylocks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class KeyPlayerRespawnEventListener implements Listener {

    @SuppressWarnings("LeakingThisInConstructor")
    public KeyPlayerRespawnEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        if (plugin.getConfig().getBoolean("clear_keys_on_death", false)) {
            plugin.getServer().getPluginManager().registerEvents(this, plugin);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerRespawnEvent(PlayerRespawnEvent e) {
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        lePlayer.removeWorldKeys(e.getRespawnLocation().getWorld());
    }

}
