package eu.lasersenigma.events.listeners.custom;

import com.xxmicloxx.NoteBlockAPI.event.SongStoppedEvent;
import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.songs.PlayersListSongManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;
import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class SongStopListener implements Listener {

    private final PlayersListSongManager playersListSongManager;

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public SongStopListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        playersListSongManager = PlayersListSongManager.getInstance();
    }

    /**
     * executed on a SongEndEvent
     *
     * @param e a SongEndEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onSongStopEvent(SongStoppedEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onSongEndEvent");
        UUID playerUUID = e.getSongPlayer().getPlayerUUIDs().stream().findFirst().orElse(null);
        if (playerUUID == null) {
            return;
        }
        Player p = Bukkit.getPlayer(playerUUID);
        playersListSongManager.onSongEnd(p);
    }
}
