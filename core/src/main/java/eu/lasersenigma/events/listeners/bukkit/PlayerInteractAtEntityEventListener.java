package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

import java.util.logging.Level;

/**
 * EventListener for PlayerInteractAtEntityEvent
 */
public class PlayerInteractAtEntityEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerInteractAtEntityEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each PlayerInteractAtEntityEvent
     *
     * @param e a PlayerInteractAtEntityEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onPlayerInteractAtEntityEvent(PlayerInteractAtEntityEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerInteractAtEntityEvent");
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInteractAtEntityEventLister");
        IComponent component = Areas.getInstance().getComponentFromEntity(e.getRightClicked().getUniqueId());
        Player p = e.getPlayer();
        if (component == null) {
            if (Areas.getInstance().getAreaFromLocation(e.getRightClicked().getLocation()) != null && !Permission.EDIT.checkPermissionAndSendMsg(p)) {
                e.setCancelled(true);
            }
            return;
        }
        e.setCancelled(true);
        LEPlayers.getInstance().findLEPlayer(p).onRightClick(component);
    }
}
