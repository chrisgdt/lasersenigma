package eu.lasersenigma.events.listeners.custom;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.VictoryArea;
import eu.lasersenigma.events.areas.PlayerEnteredAreaLEEvent;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.logging.Level;

public class PlayerEnteredAreaStatsInitializationListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerEnteredAreaStatsInitializationListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerEnteredArea(PlayerEnteredAreaLEEvent e) {
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerEnteredArea");
        switch (e.getPlayer().getGameMode()) {
            case SPECTATOR:
            case CREATIVE:
                return;
            default:
                break;
        }
        if (lePlayer.getInventoryManager().isInEditionMode()) {
            return;
        }
        //Checking if the players enters by the area's end (one of the victory areas)
        VictoryArea vArea = null;
        for (VictoryArea tmpVArea : e.getArea().getVictoryAreas()) {
            if (tmpVArea.containsLocation(e.getPlayer().getLocation())) {
                vArea = tmpVArea;
                break;
            }
        }
        if (vArea != null) {
            lePlayer.setCurrentVictoryCheckpoint(e.getPlayer().getLocation().clone());
            return; //if a player enters an area by its end (one of the victory areas), we will not start statistics recording.
        }
        lePlayer.setCurrentCheckpoint(e.getArea().getCheckpoint());
        e.getArea().getStats().addListener(e.getPlayer().getUniqueId(), new PlayerAreaStatsListener(e.getArea(), e.getPlayer()));
    }

}
