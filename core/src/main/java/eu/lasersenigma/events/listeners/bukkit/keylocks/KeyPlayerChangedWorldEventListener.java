package eu.lasersenigma.events.listeners.bukkit.keylocks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

public class KeyPlayerChangedWorldEventListener implements Listener {

    @SuppressWarnings("LeakingThisInConstructor")
    public KeyPlayerChangedWorldEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        if (plugin.getConfig().getBoolean("clear_keys_on_change_world", false)) {
            plugin.getServer().getPluginManager().registerEvents(this, plugin);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent e) {
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        lePlayer.removeWorldKeys(e.getFrom());
    }

}
