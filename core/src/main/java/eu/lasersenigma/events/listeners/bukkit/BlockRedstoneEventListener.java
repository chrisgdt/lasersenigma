package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;

/**
 * EventListener for BlockRedstoneEvent
 */
public class BlockRedstoneEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public BlockRedstoneEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a ChunkUnloadEvent
     *
     * @param e a ChunkUnloadEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onBlockRedstoneEvent(BlockRedstoneEvent e) {
        Location loc = e.getBlock().getLocation();
        Area area = Areas.getInstance().getAreaFromLocation(loc);
        if (area != null) {
            area.onBlockRedstoneEvent(e, loc);
        }
    }
}
