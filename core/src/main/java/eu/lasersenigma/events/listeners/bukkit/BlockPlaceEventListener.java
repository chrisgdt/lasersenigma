package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.logging.Level;

/**
 * EventListener for BlockPlaceEvent
 */
public class BlockPlaceEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public BlockPlaceEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each BlockPlaceEvent
     *
     * @param e a BlockPlaceEvent
     */
    @EventHandler(ignoreCancelled = true)
    public void onBlockPlaceEvent(BlockPlaceEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onBlockPlaceEvent");
        Block blockPlaced = e.getBlockPlaced();
        Area area = Areas.getInstance().getAreaFromLocation(blockPlaced.getLocation());
        Item item;
        if (area == null) {
            item = ItemsFactory.getInstance().getSimilarItem(e.getItemInHand());
            if (item != null && (!item.isPlacableOutsideAnArea() || !item.isPlacableAsBlock())) {
                e.setCancelled(true);
            }
            return;
        }
        area.onBlockBreakEvent(e);

        if (!Permission.EDIT.hasPermission(e.getPlayer())) {
            e.setCancelled(true);
            return;
        }
        item = ItemsFactory.getInstance().getSimilarItem(e.getItemInHand());
        if (item == null) {
            return;
        }
        if (!item.isPlacableAsBlock()) {
            e.setCancelled(true);
        }
    }
}
