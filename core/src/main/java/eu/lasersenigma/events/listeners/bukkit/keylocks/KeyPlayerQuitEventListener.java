package eu.lasersenigma.events.listeners.bukkit.keylocks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * EventListener for BlockBreakEvent
 */
public class KeyPlayerQuitEventListener implements Listener {

    @SuppressWarnings("LeakingThisInConstructor")
    public KeyPlayerQuitEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        if (plugin.getConfig().getBoolean("clear_keys_on_quit", false)) {
            plugin.getServer().getPluginManager().registerEvents(this, plugin);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        LEPlayer lePlayer = LEPlayers.getInstance().findLEPlayer(e.getPlayer());
        lePlayer.removeAllKeys();
    }
}
