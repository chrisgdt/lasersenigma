package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;

import java.util.logging.Level;

/**
 * EventListener for PlayerInteractEvent
 */
public class PlayerAnimationEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerAnimationEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on each PlayerInteractEvent
     *
     * @param e a PlayerInteractEvent
     */
    @EventHandler(ignoreCancelled = false)
    public void onPlayerAnimationEvent(PlayerAnimationEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerAnimationEvent");
        if (!e.getAnimationType().equals(PlayerAnimationType.ARM_SWING)) {
            return;
        }
        Player p = e.getPlayer();
        if (!p.getGameMode().equals(GameMode.ADVENTURE)) {
            return;
        }
        LEPlayers.getInstance().findLEPlayer(p).onLeftClick();
    }

}
