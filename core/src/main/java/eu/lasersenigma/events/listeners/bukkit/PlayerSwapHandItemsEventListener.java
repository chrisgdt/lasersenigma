package eu.lasersenigma.events.listeners.bukkit;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.player.PlayerInventoryManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

import java.util.logging.Level;

/**
 * EventListener for PlayerSwapHandItemsEvent
 */
public class PlayerSwapHandItemsEventListener implements Listener {

    /**
     * Constructor
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerSwapHandItemsEventListener() {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed on a PlayerSwapHandItemsEvent
     *
     * @param e a PlayerSwapHandItemsEvent
     */
    @EventHandler(ignoreCancelled = false, priority = EventPriority.HIGHEST)
    public void onPlayerSwapHandItemsEvent(PlayerSwapHandItemsEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerPickupItemEvent");
        PlayerInventoryManager pim = LEPlayers.getInstance().findLEPlayer(e.getPlayer()).getInventoryManager();
        if (pim.isRotationShortcutBarOpened()) {
            pim.closeRotationShortcutBarInventory(false);
            e.setCancelled(true);
        }
    }
}
