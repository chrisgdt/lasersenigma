package eu.lasersenigma.events.listeners.custom;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.VictoryArea;
import eu.lasersenigma.events.areas.PlayerLeavedAreaLEEvent;
import eu.lasersenigma.events.areas.PlayerWonAndLeavedAreaLEEvent;
import eu.lasersenigma.exceptions.PlayerStatsNotFoundException;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.stats.AreaStats;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.logging.Level;

/**
 * EventListener for SongEndEvent
 */
public class PlayerAreaStatsListener implements Listener {

    private static final String SAVE_VICTORY_EVEN_IF_PLAYER_CHANGED_WORLD_CONFIG_PATH = "save_victory_even_if_player_changed_world";

    private static final String SAVE_VICTORY_EVEN_IF_PLAYER_TELEPORTED_CONFIG_PATH = "save_victory_even_if_player_teleported";

    private final Area area;

    private final UUID playerUUID;
    private final LocalDateTime playerEnteredAreaDateTime;
    private int taskId;
    private int nbStep;
    private int nbAction;
    private Location currentLocation;
    private double lastDistance;

    /**
     * Constructor
     *
     * @param area   the area to follow
     * @param player the player to follow
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PlayerAreaStatsListener(Area area, Player player) {
        LasersEnigmaPlugin plugin = LasersEnigmaPlugin.getInstance();
        this.area = area;
        this.playerUUID = player.getUniqueId();
        this.nbStep = 0;
        this.nbAction = 0;
        this.playerEnteredAreaDateTime = LocalDateTime.now();
        this.currentLocation = player.getLocation();
        this.lastDistance = 0;
        this.taskId = -1;
        this.taskId = LasersEnigmaPlugin.getInstance().getServer().getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), () -> {
            Location playerNewLocation = player.getLocation();
            World previousWorld = currentLocation.getWorld();
            World newWorld = playerNewLocation.getWorld();
            if (previousWorld == null || newWorld == null || !previousWorld.getUID().equals(newWorld.getUID())) {
                if (this.taskId != -1) {
                    LasersEnigmaPlugin.getInstance().getServer().getScheduler().cancelTask(taskId);
                    taskId = -1;
                }
                return;
            }
            double distance = playerNewLocation.distance(currentLocation) + lastDistance;
            if (distance >= 1) {
                nbStep += Math.floor(distance);
                lastDistance = distance % 1;
                currentLocation = player.getLocation();
            }
        }, 0, 30).getTaskId();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * executed when a player leaves the area.
     *
     * @param e a PlayerLeavedAreaLEEvent
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerLeavedAreaLEEvent(PlayerLeavedAreaLEEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINEST, "onPlayerLeavedAreaLEEvent");
        final Player player = e.getPlayer();

        if (!player.getUniqueId().equals(playerUUID)) {
            return;
        }
        if (taskId != -1) {
            LasersEnigmaPlugin.getInstance().getServer().getScheduler().cancelTask(taskId);
            taskId = -1;
        }
        PlayerLeavedAreaLEEvent.getHandlerList().unregister(this);
        endStatsListening(e, player);
    }

    private void endStatsListening(PlayerLeavedAreaLEEvent e, Player player) {
        AreaStats stats = area.getStats();
        if ((e.getArea() != area)
                || (!player.isOnline())
                || (LasersEnigmaPlugin.getInstance().getConfig().getBoolean(SAVE_VICTORY_EVEN_IF_PLAYER_CHANGED_WORLD_CONFIG_PATH) && !area.getMinLocation().getWorld().getUID().equals(player.getLocation().getWorld().getUID()))
                || (LasersEnigmaPlugin.getInstance().getConfig().getBoolean(SAVE_VICTORY_EVEN_IF_PLAYER_TELEPORTED_CONFIG_PATH) && e.getLastPlayerLocationInsideArea().distance(player.getLocation()) > 50)) {
            stats.removeListener(player.getUniqueId());
            return;
        }
        //Checking if the players enters by the area's end (one of the victory areas)
        switch (area.getVictoryCriteria()) {
            case DETECTION_VICTORY_AREA:
                VictoryArea vArea = null;
                for (VictoryArea tmpVArea : area.getVictoryAreas()) {
                    if (tmpVArea.containsLocation(e.getLastPlayerLocationInsideArea())) {
                        vArea = tmpVArea;
                        break;
                    }
                }
                if (vArea == null) {
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                break;
            case DETECTION_LASER_RECEIVERS:
                int nbActivatedLaserReceivers = area.getNbActivatedLaserReceivers();
                if (area.getMinimumRange() > nbActivatedLaserReceivers || nbActivatedLaserReceivers > area.getMaximumRange()) {
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                break;
            case DETECTION_REDSTONE_SENSORS:
                int nbActivatedRedstoneSensors = area.getNbActivatedRedstoneSensor();
                if (area.getMinimumRange() > nbActivatedRedstoneSensors || nbActivatedRedstoneSensors > area.getMaximumRange()) {
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                break;
            case DETECTION_PLAYERS:
                int nbPlayersInsideArea = area.getNbPlayersInside();
                if (area.getMinimumRange() > nbPlayersInsideArea || nbPlayersInsideArea > area.getMaximumRange()) {
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                break;
            case DETECTION_LOCKS:
                int nbActivatedLocks = area.getNbActivatedLocks();
                if (area.getMinimumRange() > nbActivatedLocks || nbActivatedLocks > area.getMaximumRange()) {
                    stats.removeListener(player.getUniqueId());
                    return;
                }
                break;
            default:
                throw new IllegalStateException();
        }
        //The player won the area

        LEPlayers.getInstance().findLEPlayer(player).setCurrentVictoryCheckpoint(e.getLastPlayerLocationInsideArea().clone());
        Duration duration = Duration.between(playerEnteredAreaDateTime, LocalDateTime.now());
        PlayerStats oldRecord;
        try {
            oldRecord = stats.getStats(playerUUID);
        } catch (PlayerStatsNotFoundException ex) {
            oldRecord = null;
        }
        PlayerStats playerStats = stats.addStats(playerUUID, duration, nbAction, nbStep);
        PlayerStats newRecord;
        try {
            newRecord = stats.getStats(playerUUID);
        } catch (PlayerStatsNotFoundException ex) {
            //should never happen since we just saved stats
            newRecord = null;
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, null, ex);
        }

        //send a PlayerWonAndLeavedAreaLEEvent
        Bukkit.getServer().getPluginManager().callEvent(new PlayerWonAndLeavedAreaLEEvent(
                        player,
                        e.getArea(),
                        playerStats,
                        oldRecord,
                        newRecord,
                        e.getLastPlayerLocationInsideArea()
                )
        );
        stats.removeListener(player.getUniqueId());
        if (LasersEnigmaPlugin.getInstance().getConfig().getBoolean("show_stats_on_player_leaved_area")) {
            AreaStats.showStats(player, player.getName(), playerStats);
        }
    }

    public void increamentNbAction() {
        nbAction++;
    }

}
