package eu.lasersenigma.events.components;

import eu.lasersenigma.components.AttractionRepulsionSphere;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeAttractionRepulsionSphereSizeLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final AttractionRepulsionSphere component;

    private final int oldSize;

    private final int newSize;

    public PlayerTryToChangeAttractionRepulsionSphereSizeLEEvent(Player player, AttractionRepulsionSphere component, int oldSize, int newSize) {
        super();
        this.player = player;
        this.component = component;
        this.oldSize = oldSize;
        this.newSize = newSize;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public AttractionRepulsionSphere getAttractionRepulsionSphereComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public int getOldSize() {
        return oldSize;
    }

    public int getNewSize() {
        return newSize;
    }

}
