package eu.lasersenigma.events.components;

import eu.lasersenigma.components.MusicBlock;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToogleMusicBlockStopOnExitLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final MusicBlock component;

    public PlayerTryToToogleMusicBlockStopOnExitLEEvent(Player player, MusicBlock component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public MusicBlock getMusicBlock() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
