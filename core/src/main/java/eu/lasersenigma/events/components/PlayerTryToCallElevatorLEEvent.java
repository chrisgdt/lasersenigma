package eu.lasersenigma.events.components;

import eu.lasersenigma.components.CallButton;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToCallElevatorLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final CallButton component;

    public PlayerTryToCallElevatorLEEvent(Player player, CallButton component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public CallButton getCallButton() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
