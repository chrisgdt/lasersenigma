package eu.lasersenigma.events.components;

import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.events.parents.ABeforeActionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToPlaceMirrorLEEvent extends ABeforeActionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final IMirrorContainer component;

    private boolean bypassPermissions;

    public PlayerTryToPlaceMirrorLEEvent(Player player, IMirrorContainer component) {
        super();
        this.player = player;
        this.component = component;
        this.bypassPermissions = false;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IMirrorContainer getMirrorContainer() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public boolean getBypassPermissions() {
        return bypassPermissions;
    }

    public void setBypassPermissions(boolean bypassPermissions) {
        this.bypassPermissions = bypassPermissions;
    }

}
