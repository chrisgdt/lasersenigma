package eu.lasersenigma.events.components;

import eu.lasersenigma.components.AttractionRepulsionSphere;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeAttractionRepulsionSphereModeLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final AttractionRepulsionSphere component;

    public PlayerTryToChangeAttractionRepulsionSphereModeLEEvent(Player player, AttractionRepulsionSphere component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public AttractionRepulsionSphere getAttractionRepulsionSphere() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
