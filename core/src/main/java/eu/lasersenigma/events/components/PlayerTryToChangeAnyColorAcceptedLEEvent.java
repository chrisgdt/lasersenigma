package eu.lasersenigma.events.components;

import eu.lasersenigma.components.LaserReceiver;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeAnyColorAcceptedLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final LaserReceiver component;

    public PlayerTryToChangeAnyColorAcceptedLEEvent(Player player, LaserReceiver component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public LaserReceiver getLaserReceiver() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
