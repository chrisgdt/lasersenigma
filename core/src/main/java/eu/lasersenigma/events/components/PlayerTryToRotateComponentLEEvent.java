package eu.lasersenigma.events.components;

import eu.lasersenigma.components.attributes.RotationType;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IRotatableComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToRotateComponentLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final IRotatableComponent component;

    private final RotationType rotationType;

    public PlayerTryToRotateComponentLEEvent(Player player, IRotatableComponent component, RotationType rotationType) {
        super();
        this.player = player;
        this.component = component;
        this.rotationType = rotationType;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IRotatableComponent getRotatableComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

    public RotationType getRotationType() {
        return rotationType;
    }

}
