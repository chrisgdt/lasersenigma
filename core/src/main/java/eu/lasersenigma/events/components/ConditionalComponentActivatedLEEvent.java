package eu.lasersenigma.events.components;

import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;

public class ConditionalComponentActivatedLEEvent extends AAfterActionLEEvent implements IComponentLEEvent {

    private final IDetectionComponent component;

    public ConditionalComponentActivatedLEEvent(IDetectionComponent component) {
        super();
        this.component = component;
    }

    @Override
    public IDetectionComponent getComponent() {
        return component;
    }

}
