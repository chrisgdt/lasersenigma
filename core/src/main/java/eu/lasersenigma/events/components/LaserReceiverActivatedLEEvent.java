package eu.lasersenigma.events.components;

import eu.lasersenigma.components.LaserReceiver;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;

public class LaserReceiverActivatedLEEvent extends AAfterActionLEEvent implements IComponentLEEvent {

    private final LaserReceiver component;

    public LaserReceiverActivatedLEEvent(LaserReceiver component) {
        super();
        this.component = component;
    }

    @Override
    public LaserReceiver getComponent() {
        return component;
    }

}
