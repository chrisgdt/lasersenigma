package eu.lasersenigma.events.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentFace;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerTryToCreateComponentLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IAreaLEEvent {

    private final Player player;

    private final Area area;

    private final ComponentType componentType;

    private final Location location;

    private final ComponentFace face;

    public PlayerTryToCreateComponentLEEvent(Player player, Area area, ComponentType componentType, Location location, ComponentFace face) {
        super();
        this.player = player;
        this.area = area;
        this.componentType = componentType;
        this.location = location;
        this.face = face;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public Location getLocation() {
        return location;
    }

    public ComponentFace getClickedBlockFace() {
        return face;
    }

}
