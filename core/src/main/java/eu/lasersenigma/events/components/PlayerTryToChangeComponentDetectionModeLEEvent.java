package eu.lasersenigma.events.components;

import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeComponentDetectionModeLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final IDetectionComponent component;

    public PlayerTryToChangeComponentDetectionModeLEEvent(Player player, IDetectionComponent component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IDetectionComponent getDetectionComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
