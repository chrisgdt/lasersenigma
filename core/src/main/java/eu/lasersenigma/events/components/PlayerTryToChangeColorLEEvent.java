package eu.lasersenigma.events.components;

import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeColorLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final IColorableComponent component;

    public PlayerTryToChangeColorLEEvent(Player player, IColorableComponent component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public IColorableComponent getColorableComponent() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
