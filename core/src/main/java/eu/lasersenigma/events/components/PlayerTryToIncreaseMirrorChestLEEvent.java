package eu.lasersenigma.events.components;

import eu.lasersenigma.components.MirrorChest;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToIncreaseMirrorChestLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final MirrorChest mirrorChest;

    public PlayerTryToIncreaseMirrorChestLEEvent(Player player, MirrorChest mirrorChest) {
        super();
        this.player = player;
        this.mirrorChest = mirrorChest;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public MirrorChest getMirrorChest() {
        return mirrorChest;
    }

    @Override
    public IComponent getComponent() {
        return mirrorChest;
    }

}
