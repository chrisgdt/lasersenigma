package eu.lasersenigma.events.components;

import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToChangeMirrorSupportModeLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IComponentLEEvent {

    private final Player player;

    private final MirrorSupport component;

    public PlayerTryToChangeMirrorSupportModeLEEvent(Player player, MirrorSupport component) {
        super();
        this.player = player;
        this.component = component;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public MirrorSupport getMirrorSupport() {
        return component;
    }

    @Override
    public IComponent getComponent() {
        return component;
    }

}
