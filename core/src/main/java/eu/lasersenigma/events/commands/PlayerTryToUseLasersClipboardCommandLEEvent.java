package eu.lasersenigma.events.commands;

import eu.lasersenigma.events.parents.ACommandLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

public class PlayerTryToUseLasersClipboardCommandLEEvent extends ACommandLEEvent implements IPlayerLEEvent {

    private final Player player;

    public PlayerTryToUseLasersClipboardCommandLEEvent(Player player, Command cmd, String label, String[] args) {
        super(player, cmd, label, args);
        this.player = player;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

}
