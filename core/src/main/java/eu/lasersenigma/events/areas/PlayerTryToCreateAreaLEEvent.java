package eu.lasersenigma.events.areas;

import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerTryToCreateAreaLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent {

    private final Player player;

    private final Location firstLocation;

    private final Location secondLocation;

    public PlayerTryToCreateAreaLEEvent(Player player, Location firstLocation, Location secondLocation) {
        super();
        this.player = player;
        this.firstLocation = firstLocation;
        this.secondLocation = secondLocation;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public Location getFirstLocation() {
        return firstLocation;
    }

    public Location getSecondLocation() {
        return secondLocation;
    }

}
