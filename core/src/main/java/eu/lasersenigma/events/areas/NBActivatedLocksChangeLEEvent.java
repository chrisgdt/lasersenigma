package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;

public class NBActivatedLocksChangeLEEvent extends AAfterActionLEEvent implements IAreaLEEvent {

    private final Area area;

    private final int oldNbActivatedLocks;

    private final int newNbActivatedLocks;

    public NBActivatedLocksChangeLEEvent(Area area, int oldNbActivatedLocks, int newNbActivatedLocks) {
        super();
        this.area = area;
        this.oldNbActivatedLocks = oldNbActivatedLocks;
        this.newNbActivatedLocks = newNbActivatedLocks;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public int getOldNbActivatedLocks() {
        return oldNbActivatedLocks;
    }

    public int getNewNbActivatedLocks() {
        return newNbActivatedLocks;
    }

}
