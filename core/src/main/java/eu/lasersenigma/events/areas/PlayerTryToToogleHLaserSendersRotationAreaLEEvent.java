package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToogleHLaserSendersRotationAreaLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IAreaLEEvent {

    private final Player player;

    private final Area area;

    private final boolean newHRotationAuthorizationState;

    public PlayerTryToToogleHLaserSendersRotationAreaLEEvent(Player player, Area area, boolean newHRotationAuthorizationState) {
        super();
        this.player = player;
        this.area = area;
        this.newHRotationAuthorizationState = newHRotationAuthorizationState;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public boolean getNewHRotationAuthorizationState() {
        return newHRotationAuthorizationState;
    }

}
