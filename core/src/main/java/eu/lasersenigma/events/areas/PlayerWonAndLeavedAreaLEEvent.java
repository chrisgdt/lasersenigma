package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerWonAndLeavedAreaLEEvent extends AAfterActionLEEvent implements IPlayerLEEvent, IAreaLEEvent {

    private final Player player;

    private final Area area;

    private final Location lastPlayerLocationInsideArea;

    private final PlayerStats stats;
    private final PlayerStats oldRecord;
    private final PlayerStats newRecord;

    public PlayerWonAndLeavedAreaLEEvent(Player player, Area area, PlayerStats stats, PlayerStats oldRecord, PlayerStats newRecord, Location lastPlayerLocationInsideArea) {
        super();
        this.player = player;
        this.area = area;
        this.stats = stats;
        this.oldRecord = oldRecord;
        this.newRecord = newRecord;
        this.lastPlayerLocationInsideArea = lastPlayerLocationInsideArea;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public PlayerStats getStats() {
        return stats;
    }

    public PlayerStats getOldRecord() {
        return oldRecord;
    }

    public PlayerStats getNewRecord() {
        return newRecord;
    }

    public Location getLastPlayerLocationInsideArea() {
        return lastPlayerLocationInsideArea;
    }

}
