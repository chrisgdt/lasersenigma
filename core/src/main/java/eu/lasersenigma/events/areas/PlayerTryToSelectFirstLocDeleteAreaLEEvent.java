package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToSelectFirstLocDeleteAreaLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IAreaLEEvent {

    private final Player player;

    private final Area area;

    public PlayerTryToSelectFirstLocDeleteAreaLEEvent(Player player, Area area) {
        super();
        this.player = player;
        this.area = area;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

}
