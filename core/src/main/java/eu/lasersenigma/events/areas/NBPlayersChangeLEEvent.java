package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;

public class NBPlayersChangeLEEvent extends AAfterActionLEEvent implements IAreaLEEvent {

    private final Area area;

    private final int oldNbPlayersInsideArea;

    private final int newNbPlayersInsideArea;

    public NBPlayersChangeLEEvent(Area area,
                                  int oldNbPlayersInsideArea, int newNbPlayersInsideArea) {
        super();
        this.area = area;
        this.oldNbPlayersInsideArea = oldNbPlayersInsideArea;
        this.newNbPlayersInsideArea = newNbPlayersInsideArea;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public int getOldNbPlayersInsideArea() {
        return oldNbPlayersInsideArea;
    }

    public int getNewNbPlayersInsideArea() {
        return newNbPlayersInsideArea;
    }

}
