package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.AAfterActionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerLeavedAreaLEEvent extends AAfterActionLEEvent implements IPlayerLEEvent, IAreaLEEvent {

    private final Player player;

    private final Area area;

    private final Location lastPlayerLocationInsideArea;

    public PlayerLeavedAreaLEEvent(Player player, Area area, Location lastPlayerLocationInsideArea) {
        super();
        this.player = player;
        this.area = area;
        this.lastPlayerLocationInsideArea = lastPlayerLocationInsideArea;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public Location getLastPlayerLocationInsideArea() {
        return lastPlayerLocationInsideArea;
    }

}
