package eu.lasersenigma.events.areas;

import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class PlayerTryToSelectFirstLocCreateAreaLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent {

    private final Player player;

    private final Location firstLocation;

    public PlayerTryToSelectFirstLocCreateAreaLEEvent(Player player, Location firstLocation) {
        super();
        this.player = player;
        this.firstLocation = firstLocation;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    public Location getFirstLocation() {
        return firstLocation;
    }

}
