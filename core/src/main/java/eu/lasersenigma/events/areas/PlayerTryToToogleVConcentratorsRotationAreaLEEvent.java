package eu.lasersenigma.events.areas;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.ABeforeActionPermissionLEEvent;
import eu.lasersenigma.events.parents.IAreaLEEvent;
import eu.lasersenigma.events.parents.IPlayerLEEvent;
import org.bukkit.entity.Player;

public class PlayerTryToToogleVConcentratorsRotationAreaLEEvent extends ABeforeActionPermissionLEEvent implements IPlayerLEEvent, IAreaLEEvent {

    private final Player player;

    private final Area area;

    private final boolean newVRotationAuthorizationState;

    public PlayerTryToToogleVConcentratorsRotationAreaLEEvent(Player player, Area area, boolean newVRotationAuthorizationState) {
        super();
        this.player = player;
        this.area = area;
        this.newVRotationAuthorizationState = newVRotationAuthorizationState;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public Area getArea() {
        return area;
    }

    public boolean getNewVRotationAuthorizationState() {
        return newVRotationAuthorizationState;
    }

}
