package eu.lasersenigma.events.laserparticles;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.ALaserParticleLEEvent;
import eu.lasersenigma.particles.LaserParticle;

public class ParticleTryToMoveThroughCrossableMaterialsLEEvent extends ALaserParticleLEEvent {

    public ParticleTryToMoveThroughCrossableMaterialsLEEvent(Area area, LaserParticle particle) {
        super(particle, area);
    }

}
