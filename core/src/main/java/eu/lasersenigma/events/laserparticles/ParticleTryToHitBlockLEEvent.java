package eu.lasersenigma.events.laserparticles;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.events.parents.ALaserParticleLEEvent;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.block.Block;

public class ParticleTryToHitBlockLEEvent extends ALaserParticleLEEvent {

    private final Block block;

    public ParticleTryToHitBlockLEEvent(Area area, Block block, LaserParticle particle) {
        super(particle, area);
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }

}
