package eu.lasersenigma.events.laserparticles;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.events.parents.ALaserParticleLEEvent;
import eu.lasersenigma.events.parents.IComponentLEEvent;
import eu.lasersenigma.particles.LaserParticle;

public class ParticleTryToHitComponentLEEvent extends ALaserParticleLEEvent implements IComponentLEEvent {

    private final IComponent component;

    private LaserReceptionResult laserReceptionResult;

    public ParticleTryToHitComponentLEEvent(Area area, LaserParticle particle, IComponent component, LaserReceptionResult laserReceptionResult) {
        super(particle, area);
        this.component = component;
        this.laserReceptionResult = laserReceptionResult;
    }

    @Override
    public final IComponent getComponent() {
        return component;
    }

    public final LaserReceptionResult getLaserReceptionResult() {
        return laserReceptionResult;
    }

    public final void setLaserReceptionResult(LaserReceptionResult laserReceptionResult) {
        this.laserReceptionResult = laserReceptionResult;
    }
}
