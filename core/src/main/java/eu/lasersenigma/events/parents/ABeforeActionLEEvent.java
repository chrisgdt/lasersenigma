package eu.lasersenigma.events.parents;

import org.bukkit.event.Cancellable;

public abstract class ABeforeActionLEEvent extends ALEEvent implements Cancellable {

    private boolean cancelled = false;

    @Override
    public final boolean isCancelled() {
        return cancelled;
    }

    @Override
    public final void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

}
