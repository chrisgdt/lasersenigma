package eu.lasersenigma.events.parents;

import eu.lasersenigma.components.parents.IComponent;

public interface IComponentLEEvent {

    public IComponent getComponent();

}
