package eu.lasersenigma.events.parents;

public abstract class ABeforeActionPermissionLEEvent extends ABeforeActionLEEvent {

    private boolean bypassPermissions = false;

    public final boolean getBypassPermissions() {
        return bypassPermissions;
    }

    public final void setBypassPermissions(boolean byPassPermissions) {
        this.bypassPermissions = byPassPermissions;
    }

}
