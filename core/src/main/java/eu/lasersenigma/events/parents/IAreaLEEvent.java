package eu.lasersenigma.events.parents;

import eu.lasersenigma.areas.Area;

public interface IAreaLEEvent {

    public Area getArea();

}
