package eu.lasersenigma.events;

import eu.lasersenigma.events.parents.ABeforeActionLEEvent;
import eu.lasersenigma.sound.PlaySoundCause;
import org.bukkit.Location;

public class PlaySoundLEEvent extends ABeforeActionLEEvent {

    private final Location location;
    private final String soundName;
    private final float volume;
    private final float pitch;
    private final PlaySoundCause playSoundCause;

    public PlaySoundLEEvent(Location location, String soundName, float volume, float pitch, PlaySoundCause playSoundCause) {
        super();
        this.location = location;
        this.soundName = soundName;
        this.volume = volume;
        this.pitch = pitch;
        this.playSoundCause = playSoundCause;
    }

    public Location getLocation() {
        return location;
    }

    public String getSoundName() {
        return soundName;
    }

    public float getVolume() {
        return volume;
    }

    public float getPitch() {
        return pitch;
    }

    public PlaySoundCause getPlaySoundCause() {
        return playSoundCause;
    }
}
