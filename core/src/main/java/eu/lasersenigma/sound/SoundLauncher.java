package eu.lasersenigma.sound;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.events.PlaySoundLEEvent;
import eu.lasersenigma.nms.NMSManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class SoundLauncher {


    private static final String SOUND_CATEGORY = LasersEnigmaPlugin.getInstance().getConfig().getString("other_sounds_sound_category");

    private SoundLauncher() {
    }

    public static void playSound(Location location, String soundName, float volume, float pitch, PlaySoundCause playSoundCause) {
        PlaySoundLEEvent playSoundLEEvent = new PlaySoundLEEvent(location, soundName, volume, pitch, playSoundCause);
        Bukkit.getServer().getPluginManager().callEvent(playSoundLEEvent);
        if (playSoundLEEvent.isCancelled()) {
            return;
        }
        NMSManager.getNMS().playSound(location, soundName, SOUND_CATEGORY, volume, pitch);
    }
}
