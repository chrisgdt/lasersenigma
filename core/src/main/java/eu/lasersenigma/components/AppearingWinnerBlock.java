package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.parents.AComponent;
import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

/**
 * A block that appears when enough laser receivers are activated
 */
public class AppearingWinnerBlock extends AComponent implements IDetectionComponent {

    /**
     * the minimum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     */
    private int min;

    /**
     * the maximum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     */
    private int max;

    /**
     * The detection mode of the component ( number of activated laser receivers
     * or number of players inside the area)
     */
    private DetectionMode mode;

    /**
     * The actual number of (laser receivers activated | players inside the area)
     */
    private int nbActivated;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this component
     * @param componentId the id of the component inside the database
     * @param location    the component location
     * @param min         The minimum number of activated laser receivers | players needed to make
     *                    this block appear
     * @param max         The maximum number of activated laser receivers | players needed to make
     *                    this block appear
     * @param mode        the detection mode
     */
    public AppearingWinnerBlock(Area area, int componentId, Location location, int min, int max, DetectionMode mode) {
        super(area, componentId, location, ComponentType.APPEARING_WINNER_BLOCK);
        this.min = min;
        this.max = max;
        this.nbActivated = -1;
        this.mode = mode;
    }

    /**
     * constructor
     *
     * @param area     the area containing this component
     * @param location the component location
     */
    public AppearingWinnerBlock(Area area, Location location) {
        super(area, location, ComponentType.APPEARING_WINNER_BLOCK);
        this.min = 1;
        this.max = 10;
        this.nbActivated = -1;
        this.mode = DetectionMode.DETECTION_LASER_RECEIVERS;
        dbCreate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.mode = mode;
        dbUpdate();
        updateDisplay();
    }

    @Override
    public DetectionMode getMode() {
        return mode;
    }

    /**
     * let you know if lasers can pass through this component
     *
     * @return true if the lasers can pass through this component
     */
    public boolean isCrossable() {
        return !isActivated();
    }

    @Override
    public boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    /**
     * sets the number of laser receivers currently activated
     *
     * @param nbActivated the number of laser receivers currently activated
     */
    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated) {
            this.nbActivated = nbActivated;
            updateDisplay();
        }
    }

    /**
     * Resets this component
     */
    @Override
    public void reset() {
        nbActivated = 0;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(!isCrossable());
    }

    /**
     * updates this component
     */
    @Override
    public void updateDisplay() {
        final Block block = getLocation().getBlock();
        if (isActivated()) {
            block.setType(Material.OBSIDIAN);
        } else {
            block.setType(Material.AIR);
        }
        block.getState().update();

    }

}
