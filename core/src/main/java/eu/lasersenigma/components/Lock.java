package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.tasks.ParticleSpiralAnimationTask;
import eu.lasersenigma.tasks.VictorySoundTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;

public class Lock extends AArmorStandComponent {

    private final HashSet<KeyChest> keyChests;
    private final HashMap<KeyChest, LEPlayer> insertedKeysAndOwner;
    /**
     * The armorstand representing the lock
     */
    private ArmorStand armorStandLock = null;
    private boolean opened = false;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this component
     * @param componentId the id of the component inside the database
     * @param location    the location of this component
     * @param face        the face the component is on
     */
    public Lock(Area area, int componentId, Location location, ComponentFace face) {
        super(area, componentId, location, ComponentType.LOCK, face, face.getDefaultRotation(ComponentType.LOCK));
        keyChests = new HashSet<>();
        insertedKeysAndOwner = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this component
     * @param location the location of this component
     * @param face     the face the component is on
     */
    public Lock(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.LOCK, face);
        keyChests = new HashSet<>();
        insertedKeysAndOwner = new HashMap<>();
        dbCreate();
    }

    public void onKeyChestCreated(KeyChest keyChest) {
        this.keyChests.add(keyChest);
    }

    public void onKeyChestRemoved(KeyChest keyChest) {
        this.keyChests.remove(keyChest);
    }

    public boolean isOpened() {
        return this.opened;
    }

    public HashSet<KeyChest> getKeyChests() {
        return keyChests;
    }

    public boolean insertKeys(HashSet<KeyChest> playerKeys, LEPlayer player) {
        if (opened == true) {
            return false;
        }
        HashSet<Integer> requiredKeysNumber = keyChests.stream()
                .map(keyChest -> keyChest.getKeyNumber())
                .distinct()
                .collect(Collectors.toCollection(HashSet::new));
        playerKeys.stream()
                .filter(keyChest -> keyChest.getLock() == this)
                .filter(keyToInsert -> this.insertedKeysAndOwner.keySet().stream().noneMatch(keyPreviouslyInserted -> keyPreviouslyInserted.getKeyNumber() == keyToInsert.getKeyNumber()))
                .forEach(keyToInsert -> {
                    this.insertedKeysAndOwner.put(keyToInsert, player);
                });
        this.insertedKeysAndOwner.keySet().stream()
                .forEach(insertedKey -> requiredKeysNumber.remove(insertedKey.getKeyNumber()));
        opened = requiredKeysNumber.isEmpty();
        if (opened) {
            startLockOpeningAnimations(player);
            updateDisplay();
            if (LasersEnigmaPlugin.getInstance().getConfig().getBoolean("opening_locks_consumes_keys")) {
                this.insertedKeysAndOwner.entrySet().stream()
                        .forEach(e -> e.getValue().removeKey(e.getKey()));
            }
            return true;
        }
        return false;
    }

    public int getNbRequiredKeys() {
        return (int) keyChests.stream()
                .map(keyChest -> keyChest.getKeyNumber())
                .distinct()
                .count();
    }

    public int getNbRequiredKeysLeft(HashSet<KeyChest> playerKeys) {
        HashSet<Integer> requiredKeysNumber = keyChests.stream()
                .map(keyChest -> keyChest.getKeyNumber())
                .distinct()
                .collect(Collectors.toCollection(HashSet::new));
        HashSet<Integer> playerObtainedKeysNumber = playerKeys.stream()
                .filter(keyChest -> keyChest.getLock() == this)
                .map(keyChest -> keyChest.getKeyNumber())
                .distinct()
                .collect(Collectors.toCollection(HashSet::new));
        requiredKeysNumber.removeAll(playerObtainedKeysNumber);
        return requiredKeysNumber.size();

    }

    public int getNbInsertedKeys() {
        return insertedKeysAndOwner.size();
    }

    public int getNbChestsForNumber(int keyNumber) {
        return (int) keyChests.stream().filter(keyChest -> keyChest.getKeyNumber() == keyNumber).count();
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandLock != null) {
            Areas.getInstance().removeEntity(armorStandLock.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandLock.getUniqueId())) {
                    armorStandLock = (ArmorStand) entity;
                }
            }
            armorStandLock.remove();
            armorStandLock = null;
        }
    }

    /**
     * Removes the component from the database
     */
    @Override
    public void remove() {
        new HashSet<>(keyChests).forEach(keyChest -> keyChest.getArea().removeComponent(keyChest));
        hide();
        dbRemove();
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandLock = findArmorStandBack(armorStandLock);
        if (armorStandLock == null) {
            createLockArmorStand();
        } else {
            armorStandLock.setHelmet(ItemsFactory.getInstance().getItemStack(getHelmetItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    private Item getHelmetItem() {
        if (opened) {
            return Item.LOCK_ACTIVATED;
        } else {
            return Item.LOCK_DEACTIVATED;
        }
    }

    /**
     * Creates the lock armorstand
     */
    private void createLockArmorStand() {
        Location armorStandLockLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandLock = createArmorStand(armorStandLockLoc, rotation, getHelmetItem());
        Areas.getInstance().addEntity(armorStandLock.getUniqueId(), this);
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandLock = null;
    }

    @Override
    public void reset() {
        this.opened = false;
        this.insertedKeysAndOwner.clear();
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void startLockOpeningAnimations(LEPlayer player) {
        new VictorySoundTask(getArea());
        new ParticleSpiralAnimationTask(player.getBukkitPlayer());
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
