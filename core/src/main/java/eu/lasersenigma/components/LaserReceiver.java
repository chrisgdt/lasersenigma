package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.events.components.LaserReceiverActivatedLEEvent;
import eu.lasersenigma.events.components.LaserReceiverDeactivatedLEEvent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Laser receiver
 */
public final class LaserReceiver extends AArmorStandComponent implements IColorableComponent, ITaskComponent, IRotatableComponent, ILightComponent, IPlayerModifiableComponent {

    public static final double MAX_ACCEPTED_INPUT_RADIAL_ANGLE = 0.8;
    /**
     * the colors that are received in this laser receiver associated with an
     * integer value representing the moment they were received
     */
    private final HashMap<LasersColor, Integer> receivedColorsLifeTime;
    /**
     * The 4 corners of the front face of the ArmorStand Head representing this
     * laser receiver
     */
    protected List<Location> frontCorners;
    /**
     * the color of this component as saved in database
     */
    private LasersColor color;
    /**
     * current color of this component (not saved in the database)
     */
    private LasersColor currentColor;
    /**
     * The armorStand representing the laser receiver
     */
    private ArmorStand armorStandLaserReceiver = null;
    /**
     * the id of the task used to scan the lasers input
     */
    private Integer taskId;
    /**
     * the state of the laser receiver
     *
     * @see LaserReceiver#startTask() that updates this status
     */
    private boolean activated;
    private Rotation currentRotation;

    private boolean anyColorAccepted;

    private int lightLevel;

    /**
     * Constructor used for creation from the database
     *
     * @param area        the area containing this laser receiver
     * @param componentId the id of the component inside the database
     * @param location    the location of the laser receiver
     * @param face        the blockface of the component
     * @param rotation    the rotation of the component
     * @param color       the color of the laser receiver
     */
    public LaserReceiver(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, LasersColor color, int lightLevel) {
        super(area, componentId, location, ComponentType.LASER_RECEIVER, face, rotation);
        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        this.color = color != null ? color : LasersColor.WHITE;
        currentColor = color;
        this.taskId = null;
        this.activated = false;
        this.lightLevel = lightLevel;
        receivedColorsLifeTime = new HashMap<>();
        currentRotation = getRotation();
        this.anyColorAccepted = color == null; //TODO : BDD (cru) + Factory
    }

    /**
     * Constructor
     *
     * @param area     the area containing this laser receiver
     * @param location the location of the laser receiver
     * @param face     the BlockFace of the component
     */
    public LaserReceiver(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.LASER_RECEIVER, face);
        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        color = LasersColor.WHITE;
        currentColor = color;
        this.taskId = null;
        this.activated = false;
        this.anyColorAccepted = false;
        this.lightLevel = LasersEnigmaPlugin.getInstance().getConfig().getInt("laser_default_light_level");
        receivedColorsLifeTime = new HashMap<>();
        currentRotation = getRotation();
        dbCreate();
    }

    public Rotation getCurrentRotation() {
        return currentRotation;
    }

    public List<Location> getFrontCorners() {
        return frontCorners;
    }

    /**
     * starts the scanning task which checks received lasers
     */
    @Override
    public final void startTask() {
        if (!getArea().isActivated()) {
            return;
        }
        if (taskId != null) {
            return;
        }
        final LaserReceiver instance = this;
        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (() -> {
            if (!getArea().isActivated()) {
                cancelTask();
                return;
            }
            receivedColorsLifeTime.replaceAll((k, v) -> v + 1); //increment LifeTime
            receivedColorsLifeTime.entrySet().removeIf(e -> e.getValue() >= 2);
            boolean oldActivated = activated;
            if (anyColorAccepted) {
                activated = receivedColorsLifeTime.entrySet().stream().anyMatch((e) -> e.getValue() == 1);
                changeColor(false);
            } else {
                Set<LasersColor> colors = receivedColorsLifeTime.entrySet().stream().filter((e) -> e.getValue() == 1).map(Map.Entry::getKey).collect(Collectors.toSet());
                activated = currentColor.isComposedOf(colors, LasersEnigmaPlugin.getInstance().getConfig().getBoolean(LasersColor.ONLY_NEED_COLORS_CONFIG_PATH));
            }
            if (oldActivated != activated) {
                if (activated) {
                    Bukkit.getServer().getPluginManager().callEvent(new LaserReceiverActivatedLEEvent(instance));
                } else {
                    Bukkit.getServer().getPluginManager().callEvent(new LaserReceiverDeactivatedLEEvent(instance));
                }
                updateDisplay();
                updateRedstone();
            }
        }), 0, LaserParticle.LASERS_FREQUENCY);
        taskId = task.getTaskId();
    }

    /**
     * Cancels the scanning task which checks the received lasers
     */
    @Override
    public void cancelTask() {
        if (taskId == null) {
            return;
        }
        Bukkit.getScheduler().cancelTask(taskId);
        receivedColorsLifeTime.clear();
        taskId = null;
    }

    /**
     * checks if the component is currently activated (if it receives the good
     * color(s))
     *
     * @return true if the component is activated
     */
    public boolean isActivated() {
        return activated;
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandLaserReceiver != null) {
            Areas.getInstance().removeEntity(armorStandLaserReceiver.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandLaserReceiver.getUniqueId())) {
                    armorStandLaserReceiver = (ArmorStand) entity;
                }
            }
            armorStandLaserReceiver.remove();
            armorStandLaserReceiver = null;
        }
        cancelTask();
        currentRotation = rotation;
        currentColor = color;
        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        this.activated = false;
        this.receivedColorsLifeTime.clear();
        getArea().setPower(getLocation(), false);
    }

    /**
     * updates this laser receiver
     */
    @Override
    public void updateDisplay() {
        armorStandLaserReceiver = findArmorStandBack(armorStandLaserReceiver);
        if (armorStandLaserReceiver == null) {
            createLaserReceiverArmorStand();
        } else {
            armorStandLaserReceiver.setHeadPose(currentRotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
            armorStandLaserReceiver.teleport(nextLoc);
            armorStandLaserReceiver.setHelmet(ItemsFactory.getInstance().getItemStack(getItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    /**
     * Creates the armor stand corresponding to the mirror support
     */
    private void createLaserReceiverArmorStand() {
        Location armorStandLaserReceiverLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                currentRotation
        );
        armorStandLaserReceiver = createArmorStand(armorStandLaserReceiverLoc, currentRotation, getItem());
        Areas.getInstance().addEntity(armorStandLaserReceiver.getUniqueId(), this);
    }

    /**
     * Rotates the laser receiver
     *
     * @param rotationType the type of the rotation
     * @param save         if the rotation must be saved or not
     */
    @Override
    public void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType, save, true);
    }

    /**
     * Rotates the mirror on this laser receiver
     *
     * @param rotationType the type of the rotation
     * @param save         if the rotation must be saved or not
     * @param playSound    if the rotation sound should be played
     */
    @Override
    public void rotate(RotationType rotationType, boolean save, boolean playSound) {
        currentRotation = currentRotation.getNextRotation(rotationType);
        this.frontCorners = currentRotation.getFrontCorners(getASHeadCenterLocation());
        armorStandLaserReceiver.setHeadPose(currentRotation);
        Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
        armorStandLaserReceiver.teleport(nextLoc);
        if (playSound)
            SoundLauncher.playSound(armorStandLaserReceiver.getLocation(), "ENTITY_IRONGOLEM_ATTACK", 1f, 0.7f, PlaySoundCause.ROTATE_LASER_RECEIVER);
        if (save) {
            rotation = currentRotation;
            dbUpdate();
        }
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        if (laserParticle.getDirection().isAcceptedInputAngleFor(currentRotation.toEyeDirection(), MAX_ACCEPTED_INPUT_RADIAL_ANGLE)) {
            this.receivedColorsLifeTime.put(laserParticle.getColor(), 0);
        }
        return new LaserReceptionResult(true);
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(true);
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of FilteringSpheres is defined by
     *             its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(false), save);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of FilteringSpheres is defined by
     *              its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }
        this.currentColor = color;
        if (save) {
            this.color = color;
        }
        if (save) {
            dbUpdate();
        }
        updateDisplay();
    }

    /**
     * gets the color of the laser receiver
     *
     * @return the color of the laser receiver
     */
    @Override
    public LasersColor getColor() {
        return this.currentColor;
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, true);
    }

    @Override
    public LasersColor getSavedColor() {
        return color;
    }

    /**
     * Resets the component (when the last player leaves the area)
     */
    @Override
    public void reset() {
        if (!rotation.equals(currentRotation)) {
            armorStandLaserReceiver.setHeadPose(currentRotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
            armorStandLaserReceiver.teleport(nextLoc);
        }
        currentRotation = rotation;
        currentColor = color;
        this.frontCorners = rotation.getFrontCorners(getASHeadCenterLocation());
        this.activated = false;
        getArea().setPower(getLocation(), false);
        this.receivedColorsLifeTime.clear();
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandLaserReceiver = null;
    }

    public boolean isAnyColorAccepted() {
        return anyColorAccepted;
    }

    public void setAnyColorAccepted(boolean anyColorAccepted) {
        this.anyColorAccepted = anyColorAccepted;
        dbUpdate();
        updateDisplay();
    }

    private Item getItem() {
        String enumName = "LASERS_RECEIVER_";
        enumName += anyColorAccepted ? "ANY" : color.name();
        enumName += activated ? "_ON" : "_OFF";
        if (getArea().isHLaserReceiversRotationAllowed() && getArea().isVLaserReceiversRotationAllowed()) {
            enumName += "_HV";
        } else if (getArea().isHLaserReceiversRotationAllowed()) {
            enumName += "_H";
        } else if (getArea().isVLaserReceiversRotationAllowed()) {
            enumName += "_V";
        }
        return Item.valueOf(enumName);
    }

    private void updateRedstone() {
        getArea().setPower(getLocation(), activated);
    }

    @Override
    public int getLightLevel() {
        return lightLevel;
    }

    @Override
    public void setLightLevel(int lightLevel) {
        this.lightLevel = lightLevel;
    }
}
