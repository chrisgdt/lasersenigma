package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IPlayerModifiableComponent;
import eu.lasersenigma.components.parents.IRotatableComponent;
import eu.lasersenigma.components.parents.ITaskComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Concentrator
 */
public final class Concentrator extends AArmorStandComponent implements ITaskComponent, IRotatableComponent, IPlayerModifiableComponent {

    /**
     * the colors that are received by this concentrator associated with an
     * integer value representing the moment they were received
     */
    private final HashMap<Integer, Set<LasersColor>> receivedColorsLifeTime;
    /**
     * the light levels that are received by this concentrator associated with an
     * integer value representing the moment they were received
     */
    private final HashMap<Integer, Set<Integer>> receivedLightsLifeTime;
    /**
     * The armorStand representing the concentrator
     */
    private ArmorStand armorStandConcentrator = null;
    /**
     * the id of the task used to scan the lasers input
     */
    private Integer taskId;

    private Rotation currentRotation;

    /**
     * Constructor used for creation from the database
     *
     * @param area        the area containing this concentrator
     * @param componentId the id of the component inside the database
     * @param location    the location of the concentrator
     * @param face        the blockface of the concentrator
     * @param rotation    the rotation of the concentrator
     */
    public Concentrator(Area area, int componentId, Location location, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, ComponentType.CONCENTRATOR, face, rotation);
        this.taskId = null;
        receivedColorsLifeTime = new HashMap<>();
        receivedLightsLifeTime = new HashMap<>();
        currentRotation = getRotation();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this concentrator
     * @param location the location of the concentrator
     * @param face     the blockface of the concentrator
     */
    public Concentrator(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.CONCENTRATOR, face);
        this.taskId = null;
        receivedColorsLifeTime = new HashMap<>();
        receivedLightsLifeTime = new HashMap<>();
        currentRotation = getRotation();
        dbCreate();
    }

    public Rotation getCurrentRotation() {
        return currentRotation;
    }

    /**
     * starts the scanning task which checks received lasers
     */
    @Override
    public final void startTask() {
        if (!getArea().isActivated()) {
            return;
        }
        if (taskId != null) {
            return;
        }
        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), () -> {
            if (!getArea().isActivated()) {
                cancelTask();
                return;
            }
            for (int i = 0; i < 3; ++i) {
                receivedColorsLifeTime.put(i + 1, receivedColorsLifeTime.get(i));
                receivedLightsLifeTime.put(i + 1, receivedLightsLifeTime.get(i));
            }
            receivedColorsLifeTime.put(0, new HashSet<>());
            receivedLightsLifeTime.put(0, new HashSet<>());

        }, 0, LaserParticle.LASERS_FREQUENCY);
        taskId = task.getTaskId();
    }

    /**
     * Cancels the scanning task which checks the received lasers
     */
    @Override
    public void cancelTask() {
        if (taskId == null) {
            return;
        }
        Bukkit.getScheduler().cancelTask(taskId);
        receivedColorsLifeTime.clear();
        receivedLightsLifeTime.clear();
        taskId = null;
    }

    /**
     * checks if the concentrator is currently activated and retrieves the
     * recomposed color
     *
     * @return the resulting laser color
     */
    public LasersColor getResultingColor() {
        return LasersColor.merge(receivedColorsLifeTime.get(1));
    }

    /**
     * checks if the concentrator is currently activated and retrieves the
     * recalculated lightLevel
     *
     * @return the resulting light level
     */
    public int getResultingLightLevel() {
        return Collections.max(receivedLightsLifeTime.get(1));
    }

    /**
     * Deletes the concentrator
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandConcentrator != null) {
            Areas.getInstance().removeEntity(armorStandConcentrator.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandConcentrator.getUniqueId())) {
                    armorStandConcentrator = (ArmorStand) entity;
                }
            }
            armorStandConcentrator.remove();
            armorStandConcentrator = null;
        }
        cancelTask();
        currentRotation = rotation;
        this.receivedColorsLifeTime.clear();
    }

    /**
     * updates this Concentrator display
     */
    @Override
    public void updateDisplay() {
        armorStandConcentrator = findArmorStandBack(armorStandConcentrator);
        if (armorStandConcentrator == null) {
            createConcentratorArmorStand();
        } else {
            armorStandConcentrator.setHeadPose(currentRotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
            armorStandConcentrator.teleport(nextLoc);
            armorStandConcentrator.setHelmet(ItemsFactory.getInstance().getItemStack(getHelmetItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    private Item getHelmetItem() {
        if (getArea().isHConcentratorsRotationAllowed() && getArea().isVConcentratorsRotationAllowed()) {
            return Item.CONCENTRATOR_HV;
        } else if (getArea().isHConcentratorsRotationAllowed()) {
            return Item.CONCENTRATOR_H;
        } else if (getArea().isVConcentratorsRotationAllowed()) {
            return Item.CONCENTRATOR_V;
        } else {
            return Item.CONCENTRATOR;
        }
    }

    /**
     * Creates the armor stand corresponding to the concentrator
     */
    private void createConcentratorArmorStand() {
        Location armorStandLaserReceiverLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                currentRotation
        );
        armorStandConcentrator = createArmorStand(armorStandLaserReceiverLoc, currentRotation, Item.CONCENTRATOR);
        Areas.getInstance().addEntity(armorStandConcentrator.getUniqueId(), this);
    }

    /**
     * Rotates the concentrator
     *
     * @param rotationType the type of the rotation
     * @param save         if the rotation must be saved or not
     */
    @Override
    public void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType, save, true);
    }

    /**
     * Rotates the concentrator
     *
     * @param rotationType the type of the rotation
     * @param save         if the rotation must be saved or not
     * @param playSound    if the rotation sound should be played
     */
    @Override
    public void rotate(RotationType rotationType, boolean save, boolean playSound) {
        currentRotation = currentRotation.getNextRotation(rotationType);
        armorStandConcentrator.setHeadPose(currentRotation);
        Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
        armorStandConcentrator.teleport(nextLoc);
        if (playSound)
            SoundLauncher.playSound(armorStandConcentrator.getLocation(), "ENTITY_IRONGOLEM_ATTACK", 1f, 0.7f, PlaySoundCause.ROTATE_CONCENTRATOR);
        if (save) {
            rotation = currentRotation;
            dbUpdate();
        }
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        receivedColorsLifeTime.get(0).add(laserParticle.getColor());
        receivedLightsLifeTime.get(0).add(laserParticle.getLightLevel());
        return new LaserReceptionResult(true);
    }

    /**
     * Resets the component (when the last player leaves the area)
     */
    @Override
    public void reset() {
        if (!rotation.equals(currentRotation)) {
            armorStandConcentrator.setHeadPose(currentRotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, currentRotation);
            armorStandConcentrator.teleport(nextLoc);
        }
        currentRotation = rotation;
        this.receivedColorsLifeTime.clear();
        this.receivedLightsLifeTime.clear();
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandConcentrator = null;
    }
}
