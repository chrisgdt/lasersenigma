package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.parents.AComponent;
import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * A block that transforms itself in redstone block if enough laser receivers
 * are activated
 */
public class WinnerBlock extends AComponent implements IDetectionComponent {

    /**
     * the minimum number of [activated laser receivers | players inside the
     * area] needed to activate this component
     */
    private int min;

    /**
     * the maximum number of [activated laser receivers | players inside the
     * area] needed to activate this component
     */
    private int max;

    /**
     * The detection mode of the component ( number of activated laser receivers
     * or number of players inside the area)
     */
    private DetectionMode mode;

    /**
     * The actual number of (laser receivers activated | players inside the
     * area)
     */
    private int nbActivated;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this winner block
     * @param componentId the id of the component inside the database
     * @param location    the location of this winner block
     * @param min         The minimum number of activated laser receivers | players
     *                    needed to make this block transform into a redstone block
     * @param max         The maximum number of activated laser receivers | players
     *                    needed to make this block transform into a redstone block
     * @param mode        the detection mode
     */
    public WinnerBlock(Area area, int componentId, Location location, int min, int max, DetectionMode mode) {
        super(area, componentId, location, ComponentType.REDSTONE_WINNER_BLOCK);
        this.min = min;
        this.max = max;
        this.nbActivated = -1;
        this.mode = mode;
    }

    /**
     * Constructor
     *
     * @param area     the area containing this winner block
     * @param location the location of this winner block
     */
    public WinnerBlock(Area area, Location location) {
        super(area, location, ComponentType.REDSTONE_WINNER_BLOCK);
        this.min = 1;
        this.max = 10;
        this.nbActivated = -1;
        this.mode = DetectionMode.DETECTION_LASER_RECEIVERS;
        dbCreate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.mode = mode;
        dbUpdate();
        updateDisplay();
    }

    @Override
    public DetectionMode getMode() {
        return mode;
    }

    @Override
    public boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    /**
     * sets the current number of laser receivers activated
     *
     * @param nbActivated the current number of laser receivers activated
     */
    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated) {
            this.nbActivated = nbActivated;
            updateDisplay();
        }

    }

    /**
     * resets this winner block
     */
    @Override
    public void reset() {
        this.nbActivated = 0;
    }

    /**
     * updates this winner block
     */
    @Override
    public void updateDisplay() {
        if (isActivated()) {
            getLocation().getBlock().setType(Material.REDSTONE_BLOCK);
            getLocation().getBlock().getState().update(true, true);
        } else {
            getLocation().getBlock().setType(Material.COAL_BLOCK);
            getLocation().getBlock().getState().update(true, true);
        }
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }

}
