package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

/**
 * Redstone Sensor component
 */
public final class RedstoneSensor extends AArmorStandComponent {

    /**
     * The armorStand representing the sensor
     */
    private ArmorStand armorStandSensor = null;

    private boolean powered = false;

    /**
     * Constructor used for creation from the database
     *
     * @param area        the area containing this redstone sensor
     * @param componentId the id of the component inside the database
     * @param location    the location of the redstone sensor
     * @param face        the blockface of the component
     * @param rotation    the rotation of the component
     */
    public RedstoneSensor(Area area, int componentId, Location location, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, ComponentType.REDSTONE_SENSOR, face, rotation);
    }

    /**
     * Constructor
     *
     * @param area     the area containing this
     * @param location the location of the component
     * @param face     the blockface of the component
     */
    public RedstoneSensor(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.REDSTONE_SENSOR, face);
        dbCreate();
    }

    public boolean isPowered() {
        return powered;
    }

    public void setPowered(boolean powered) {
        if (powered != this.powered) {
            this.powered = powered;
            updateDisplay();
        }
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandSensor != null) {
            Areas.getInstance().removeEntity(armorStandSensor.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandSensor.getUniqueId())) {
                    armorStandSensor = (ArmorStand) entity;
                }
            }
            armorStandSensor.remove();
            armorStandSensor = null;
        }
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandSensor = findArmorStandBack(armorStandSensor);
        if (armorStandSensor == null) {
            createSensorArmorStand();
        } else {
            armorStandSensor.setHelmet(ItemsFactory.getInstance().getItemStack(getHelmetItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    private Item getHelmetItem() {
        if (powered) {
            return Item.REDSTONE_SENSOR_ACTIVATED;
        } else {
            return Item.REDSTONE_SENSOR_DEACTIVATED;
        }
    }

    /**
     * Creates the redstone sensor armorstand
     */
    private void createSensorArmorStand() {
        Location armorStandSensorLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandSensor = createArmorStand(armorStandSensorLoc, rotation, getHelmetItem());
        Areas.getInstance().addEntity(armorStandSensor.getUniqueId(), this);
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandSensor = null;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
