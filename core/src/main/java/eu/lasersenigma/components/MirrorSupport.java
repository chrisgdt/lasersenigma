package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.*;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.nms.v1_13_R2.ParticleMapping;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.particles.ReflectionResultType;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import eu.lasersenigma.tasks.GetMirrorFromSupportTask;
import eu.lasersenigma.tasks.PlaceMirrorOnSupportTask;
import org.apache.commons.lang.NotImplementedException;
import org.apache.maven.settings.Mirror;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Mirror support component
 */
public class MirrorSupport extends AArmorStandComponent implements IColorableComponent, IMirrorContainer, IRotatableComponent, IPlayerModifiableComponent {

    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.275d;
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;
    private static final double MIRROR_SIZE = 0.42;
    private static final double MIRROR_DEPTH = 0.23;


    public static ReflectionData reflect(MirrorSupport ms, ReflectionData laserParticleData) {
        /* Reflexion Direction calculation */
        //https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
        //https://stackoverflow.com/questions/5666222/3d-line-plane-intersection
        //http://www.3dkingdoms.com/weekly/weekly.php?a=2
        Vector mirrorPlaneNormal = ms.getRotation().toEyeDirection();
        Vector mirrorHeadDirection = ms.getRotation().toHeadDirection();
        Vector mirrorSideDirection = ms.getRotation().toSideDirection();
        Location mirrorCenterLocation = new Location(ms.getLocation().getWorld(), ms.getLocation().getBlockX() + 0.5, ms.getLocation().getBlockY() + 0.5, ms.getLocation().getBlockZ() + 0.5);
        double dotProd = laserParticleData.getDirection().dot(mirrorPlaneNormal);
        if (dotProd == 0) { //No Intersection can be calculated.
            //Calculate the distance between the laser particle and the mirror center but only on the axis defined by the mirrorPlaneNormal Vector.
            double distanceBetweenLaserAndMirror = Math.abs((laserParticleData.getLocation().getX() - mirrorCenterLocation.getX()) * mirrorPlaneNormal.getX())
                    + Math.abs((laserParticleData.getLocation().getY() - mirrorCenterLocation.getY()) * mirrorPlaneNormal.getY())
                    + Math.abs((laserParticleData.getLocation().getZ() - mirrorCenterLocation.getZ()) * mirrorPlaneNormal.getZ());
            //System.out.println(distanceBetweenLaserAndMirror + " < " + MIRROR_DEPTH);
            //Check if the laser is really touching the mirror
            if (distanceBetweenLaserAndMirror < MIRROR_DEPTH) {
                return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.ORTHOGONAL);
            } else {
                return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
            }
        } else { //intersection calculation

            double param = (-mirrorPlaneNormal.getX() * (laserParticleData.getLocation().getX() - mirrorCenterLocation.getX())
                    - mirrorPlaneNormal.getY() * (laserParticleData.getLocation().getY() - mirrorCenterLocation.getY())
                    - mirrorPlaneNormal.getZ() * (laserParticleData.getLocation().getZ() - mirrorCenterLocation.getZ()))
                    / (mirrorPlaneNormal.getX() * laserParticleData.getDirection().getX()
                    + mirrorPlaneNormal.getY() * laserParticleData.getDirection().getY()
                    + mirrorPlaneNormal.getZ() * laserParticleData.getDirection().getZ());
            Location intersection = new Location(
                    laserParticleData.getLocation().getWorld(),
                    laserParticleData.getLocation().getX() + laserParticleData.getDirection().getX() * param,
                    laserParticleData.getLocation().getY() + laserParticleData.getDirection().getY() * param,
                    laserParticleData.getLocation().getZ() + laserParticleData.getDirection().getZ() * param);

            //Calculate the distance between the intersection and the mirror center but only on the axis defined by the mirrorHeadDirection Vector.
            double distanceBetweenLaserAndMirrorOnHeadAxis = Math.abs((intersection.getX() - mirrorCenterLocation.getX()) * mirrorHeadDirection.getX())
                    + Math.abs((intersection.getY() - mirrorCenterLocation.getY()) * mirrorHeadDirection.getY())
                    + Math.abs((intersection.getZ() - mirrorCenterLocation.getZ()) * mirrorHeadDirection.getZ());

            //Calculate the distance between the intersection and the mirror center but only on the axis defined by the mirrorHeadDirection Vector.
            double distanceBetweenLaserAndMirrorOnSideAxis = Math.abs((intersection.getX() - mirrorCenterLocation.getX()) * mirrorSideDirection.getX())
                    + Math.abs((intersection.getY() - mirrorCenterLocation.getY()) * mirrorSideDirection.getY())
                    + Math.abs((intersection.getZ() - mirrorCenterLocation.getZ()) * mirrorSideDirection.getZ());

            if (distanceBetweenLaserAndMirrorOnHeadAxis < MIRROR_SIZE
                    && distanceBetweenLaserAndMirrorOnSideAxis < MIRROR_SIZE) {
                Direction resultDirection = new Direction(
                        laserParticleData.getDirection().getX() - (2 * dotProd * mirrorPlaneNormal.getX()),
                        laserParticleData.getDirection().getY() - (2 * dotProd * mirrorPlaneNormal.getY()),
                        laserParticleData.getDirection().getZ() - (2 * dotProd * mirrorPlaneNormal.getZ()));
                return new ReflectionData(resultDirection, intersection, ReflectionResultType.REFLECTED);
            } else {
                return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.NO_INTERSECTION);
            }
        }
    }

    /**
     * Cache for lasers reflexion calculation
     */
    private final HashMap<ReflectionData, ReflectionData> reflectionCache;
    /**
     * does this mirror support has a mirror on it
     */
    private boolean hasMirror;
    /**
     * the armor stand representing the mirror
     */
    private ArmorStand armorStandMirror;
    /**
     * the armor stand representing the mirror support
     */
    private ArmorStand armorStandMirrorSupport;
    /**
     * is this mirror support currently showing an animation
     */
    private boolean onGoingAnimation;
    /**
     * the color of this mirror support
     */
    private LasersColor color;

    /**
     * The saved color of this component's mirror. Used only on reset
     */
    private LasersColor savedColor;

    /**
     * Saved mirror rotation
     */
    private Rotation savedMirrorRotation;

    private MirrorSupportMode mode;

    private PlaceMirrorOnSupportTask placeMirrorOnSupportTask;

    private GetMirrorFromSupportTask getMirrorFromSupportTask;

    /**
     * Constructor used for creation from database
     *
     * @param area          the area containing this mirror support
     * @param componentId   the id of the component inside the database
     * @param location      the location of this mirror support
     * @param face          the face the component is on
     * @param savedColor    the color of this component on reset
     * @param savedRotation the rotation of this component on reset
     */
    public MirrorSupport(Area area, int componentId, Location location, ComponentFace face, LasersColor savedColor, Rotation savedRotation, MirrorSupportMode mode) {
        super(area, componentId, location, ComponentType.MIRROR_SUPPORT, face, face.getDefaultRotation(ComponentType.MIRROR_SUPPORT));
        this.onGoingAnimation = false;
        this.savedColor = savedColor;
        this.color = savedColor == null ? LasersColor.WHITE : savedColor;
        this.rotation = savedColor == null ? rotation : savedRotation;
        this.savedMirrorRotation = savedRotation;
        this.hasMirror = savedColor != null;
        this.mode = mode;
        this.reflectionCache = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this mirror support
     * @param location the location of this mirror support
     * @param face     the face the component is on
     */
    public MirrorSupport(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.MIRROR_SUPPORT, face);
        this.hasMirror = true;
        this.onGoingAnimation = false;
        this.savedColor = LasersColor.WHITE;
        this.savedMirrorRotation = rotation;
        this.color = LasersColor.WHITE;
        this.mode = MirrorSupportMode.FREE;
        this.reflectionCache = new HashMap<>();
        updateDisplay();
        dbCreate();
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return armorStandMirror;
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return armorStandMirrorSupport;
    }

    public HashMap<ReflectionData, ReflectionData> getReflectionCache() {
        return reflectionCache;
    }

    /**
     * deletes this mirror support
     */
    @Override
    public void hide() {
        super.hide();
        if (onGoingAnimation && getMirrorFromSupportTask != null) {
            getMirrorFromSupportTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSupportTask != null) {
            placeMirrorOnSupportTask.cancel();
        }
        onGoingAnimation = false;
        hasMirror = false;
        armorStandMirrorSupport = findArmorStandBack(armorStandMirrorSupport);
        armorStandMirror = findArmorStandBack(armorStandMirror);
        if (armorStandMirror != null) {
            armorStandMirror.remove();
            armorStandMirror = null;
        }
        if (armorStandMirrorSupport != null) {
            armorStandMirrorSupport.remove();
            armorStandMirrorSupport = null;
        }
    }

    /**
     * checks if an animation is currently on going
     *
     * @return true if an animation is currently shown
     */
    public boolean getOnGoingAnimation() {
        return onGoingAnimation;
    }

    /**
     * sets if this mirror support is currently animated or not
     *
     * @param onGoingAnimation if this mirror support is currently animated or not
     */
    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    /**
     * does this mirror support currently has a mirror on it
     *
     * @return true if this mirror support has a mirror on it
     */
    @Override
    public boolean hasMirror() {
        return hasMirror;
    }

    public boolean hasSavedMirror() {
        return savedMirrorRotation != null;
    }

    /**
     * Sets if this mirror support currently has a mirror on it
     *
     * @param hasMirror if this mirror support currently has a mirror on it or not
     * @param save should the presence/lack of mirror be saved in database
     */
    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        if (onGoingAnimation) {
            return;
        }
        this.hasMirror = hasMirror;
        updateDisplay();
        if (save) {
            savedColor = hasMirror ? color : null;
            savedMirrorRotation = hasMirror ? rotation : face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
            dbUpdate();
        }
    }
    /**
     * retrieves the mirror from this mirror support
     *
     * @return true if the mirror has been retrieved successfully
     * @param save should the presence/lack of mirror be saved in database
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean getMirror(boolean save) {
        if (!onGoingAnimation && hasMirror && !this.isRemoved()) {
            onGoingAnimation = true;
            // Animation
            getMirrorFromSupportTask = new GetMirrorFromSupportTask(this, save);
            return true;
        }
        return false;
    }

    /**
     * place a mirror on this mirror support
     *
     * @param color the color of the mirror retrieved
     * @return true is the mirror has been placed successfully
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        if (!onGoingAnimation && !hasMirror && !this.isRemoved()) {
            onGoingAnimation = true;
            this.color = color;
            reflectionCache.clear();
            rotation = face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
            createMirrorArmorStand();
            placeMirrorOnSupportTask = new PlaceMirrorOnSupportTask(this, save);
            return true;
        }
        return false;
    }

    /**
     * updates this mirror support
     */
    @Override
    public void updateDisplay() {

        //Component hidden block
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }

        //Retrieving armorstands if required
        armorStandMirrorSupport = findArmorStandBack(armorStandMirrorSupport);
        armorStandMirror = findArmorStandBack(armorStandMirror);

        //Creation/Deletion of armorstands
        if (armorStandMirrorSupport == null) {
            createMirrorSupportArmorStand();
        }
        if (hasMirror && armorStandMirror == null) {
            createMirrorArmorStand();
        } else if (!hasMirror && armorStandMirror != null) {
            armorStandMirror.remove();
            Areas.getInstance().removeEntity(armorStandMirror.getUniqueId());
            armorStandMirror = null;
        }

        //Rotation, head and location of mirror support armor stand
        Rotation supportRotation = face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);

        armorStandMirrorSupport.setHeadPose(supportRotation);
        armorStandMirrorSupport.setHelmet(ItemsFactory.getInstance().getItemStack(mode.getCorrespondingSupportSkin()));

        Location supportLoc = getDefaultArmorStandBaseLocation(getLocation(), face);
        if (hasMirror) {
            supportLoc.add(getAnimationVector().multiply(-1d));
        }
        supportLoc = getArmorStandLocationWithOffsets(
                supportLoc,
                ArmorStandItemOffset.HEAD,
                supportRotation);
        armorStandMirrorSupport.teleport(supportLoc);

        //Rotation, head and location of mirror armor stand
        if (hasMirror) {
            armorStandMirror.setHeadPose(rotation);
            armorStandMirror.setHelmet(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
            Location mirrorLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation()), ArmorStandItemOffset.GLASS_PANE, rotation);
            armorStandMirror.teleport(mirrorLoc);
        }
    }
    /**
     * Creates the armor stand corresponding to the mirror support
     */
    private void createMirrorSupportArmorStand() {
        Rotation supportRotation = face.getDefaultRotation(ComponentType.MIRROR_SUPPORT);
        Location supportLoc = getDefaultArmorStandBaseLocation(getLocation(), face);
        if (hasMirror) {
            supportLoc.add(getAnimationVector().multiply(-1d));
        }
        supportLoc = getArmorStandLocationWithOffsets(
                supportLoc,
                ArmorStandItemOffset.HEAD,
                supportRotation);
        armorStandMirrorSupport = createArmorStand(supportLoc,  face.getDefaultRotation(ComponentType.MIRROR_SUPPORT), Item.MIRROR_SUPPORT);
        Areas.getInstance().addEntity(armorStandMirrorSupport.getUniqueId(), this);
    }

    /**
     * Creates the armor stand corresponding to the mirror
     */
    private void createMirrorArmorStand() {
        Location mirrorLoc = getDefaultArmorStandBaseLocation(getLocation());
        if (!hasMirror)
            mirrorLoc = mirrorLoc.add(getAnimationClipVector()).add(getAnimationVector());
        Location armorStandMirrorLoc = getArmorStandLocationWithOffsets(
                mirrorLoc,
                ArmorStandItemOffset.GLASS_PANE,
                rotation
        );
        armorStandMirror = createArmorStand(armorStandMirrorLoc, rotation, Item.getMirror(color));
        Areas.getInstance().addEntity(armorStandMirror.getUniqueId(), this);
    }

    @Override
    public final Vector getAnimationVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public final Vector getAnimationClipVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    /**
     * Rotates the mirror on this mirror support
     * @param rotationType the type of the rotation
     */
    @Override
    public void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType, save, true);
    }

    /**
     * Rotates the mirror on this mirror support
     * @param rotationType the type of the rotation
     * @param playSound    if the rotation sound should be played
     */
    @Override
    public void rotate(RotationType rotationType, boolean save, boolean playSound) {
        if (onGoingAnimation || !hasMirror) {
            return;
        }
        reflectionCache.clear();
        this.rotation = rotation.getNextRotation(rotationType);
        updateDisplay();
        if (playSound)
            SoundLauncher.playSound(armorStandMirrorSupport.getLocation(), "ENTITY_IRONGOLEM_ATTACK", 1f, 0.7f, PlaySoundCause.ROTATE_MIRROR_SUPPORT);
        if (save) {
            savedMirrorRotation = rotation;
            savedColor = color;
            dbUpdate();
        }
    }

    /**
     * Resets the mirror support
     */
    @Override
    public void reset() {
        if (onGoingAnimation && getMirrorFromSupportTask != null) {
            getMirrorFromSupportTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSupportTask != null) {
            placeMirrorOnSupportTask.cancel();
        }
        onGoingAnimation = false;
        this.color = savedColor == null ? LasersColor.WHITE : savedColor;
        this.rotation = savedColor == null ? face.getDefaultRotation(ComponentType.MIRROR_SUPPORT) : savedMirrorRotation;
        reflectionCache.clear();
        updateDisplay();
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of MirrorSupports is defined by its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(color.getNextColor(false), save);
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(false);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, false);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of MirrorSupports is defined by its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (!hasMirror || onGoingAnimation) {
            return;
        }
        this.color = color;
        updateDisplay();
        if (save) {
            savedColor = color;
            savedMirrorRotation = rotation;
            dbUpdate();
        }
    }

    /**
     * gets the color of this mirror support
     *
     * @return the color of this mirror support
     */
    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getSavedColor() {
        return savedColor;
    }

    @Override
    public Rotation getSavedRotation() {
        return savedMirrorRotation;
    }

    public MirrorSupportMode getMode() {
        return mode;
    }

    public void changeMode(MirrorSupportMode mode) {
        this.mode = mode;
        updateDisplay();
        dbUpdate();
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandMirror = null;
        armorStandMirrorSupport = null;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }

        Set<LaserParticle> newParticles = new HashSet<>();
        // It has no mirror inside, it is an opaque component
        if (hasMirror() && !getOnGoingAnimation()) {
            ReflectionData inputRD = new ReflectionData(laserParticle.getDirection(), laserParticle.getLocation());

            //Compute reflected direction (using a reflection cache to avoid recomputing the same reflection two times)
            ReflectionData outputRD = getReflectionCache().get(inputRD);
            if (outputRD == null) {
                outputRD = reflect(this, inputRD);
                getReflectionCache().put(inputRD, outputRD);
            }

            //Compute reflected color
            HashMap<LasersColor.FilterResult, LasersColor> filterResult;
            LasersColor reflectedColor = null;
            LasersColor passThroughColor = null;
            switch (outputRD.getReflexionResult()) {
                case NO_INTERSECTION:
                    newParticles.add(new LaserParticle(
                            this,
                            laserParticle.getLocation(),
                            laserParticle.getDirection(),
                            laserParticle.getColor(),
                            getArea(),
                            laserParticle.getLightLevel()
                    ));
                    break;
                case ORTHOGONAL:
                    filterResult = laserParticle.getColor().filterBy(color);
                    passThroughColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                    if (color == LasersColor.WHITE) {
                        passThroughColor = filterResult.get(LasersColor.FilterResult.PASS_THROUGH);
                    }
                    break;
                case REFLECTED:
                    filterResult = laserParticle.getColor().filterBy(color);
                    passThroughColor = filterResult.get(LasersColor.FilterResult.PASS_THROUGH);
                    reflectedColor = filterResult.get(LasersColor.FilterResult.REFLECTED);
                    if (color == LasersColor.WHITE) {
                        reflectedColor = passThroughColor;
                        passThroughColor = null;
                    }
                    break;
                default:
                    throw new NotImplementedException("Unknown reflexion type");
            }
            if (passThroughColor != null) {
                newParticles.add(new LaserParticle(
                        this,
                        outputRD.getLocation(),
                        laserParticle.getDirection(),
                        passThroughColor,
                        getArea(),
                        laserParticle.getLightLevel()
                ));
            }
            if (reflectedColor != null) {
                newParticles.add(new LaserParticle(
                        this,
                        outputRD.getLocation(),
                        outputRD.getDirection(),
                        reflectedColor,
                        getArea(),
                        laserParticle.getLightLevel()

                ));
            }
        }
        return new LaserReceptionResult(true, newParticles);
    }
}
