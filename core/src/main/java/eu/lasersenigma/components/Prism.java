package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.*;

/**
 * Prism component
 */
public final class Prism extends AArmorStandComponent {

    private final HashMap<Direction, HashMap<Integer, ArrayList<Direction>>> cache;
    /**
     * The armorStand representing the prism
     */
    private ArmorStand armorStandPrism = null;

    /**
     * Constructor used for creation from the database
     *
     * @param area        the area containing this laser receiver
     * @param componentId the id of the component inside the database
     * @param location    the location of the laser receiver
     * @param face        the blockface of the component
     * @param rotation    the rotation of the component
     */
    public Prism(Area area, int componentId, Location location, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, ComponentType.PRISM, face, rotation);
        cache = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this
     * @param location the location of the component
     * @param face     the blockface of the component
     */
    public Prism(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.PRISM, face);
        cache = new HashMap<>();
        dbCreate();
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandPrism != null) {
            Areas.getInstance().removeEntity(armorStandPrism.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandPrism.getUniqueId())) {
                    armorStandPrism = (ArmorStand) entity;
                }
            }
            armorStandPrism.remove();
            armorStandPrism = null;
        }
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandPrism = findArmorStandBack(armorStandPrism);
        if (armorStandPrism == null) {
            createPrismArmorStand();
        } else {
            armorStandPrism.setHeadPose(rotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, rotation);
            armorStandPrism.teleport(nextLoc);
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        ArrayList<LasersColor> newColors = laserParticle.getColor().decompose();
        ArrayList<Direction> newDirections = getNewDirection(laserParticle.getDirection(), newColors.size());
        Set<LaserParticle> newParticles = new HashSet<>();
        for (int i = 0; i < newColors.size(); ++i) {
            newParticles.add(new LaserParticle(
                    this,
                    laserParticle.getLocation().clone().add(newDirections.get(i)),
                    newDirections.get(i),
                    newColors.get(i),
                    getArea(),
                    laserParticle.getLightLevel()
            ));
        }
        return new LaserReceptionResult(true, newParticles);
    }

    /**
     * Creates the prism armorstand
     */
    private void createPrismArmorStand() {
        Location armorStandPrismLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandPrism = createArmorStand(armorStandPrismLoc, rotation, Item.PRISM);
        Areas.getInstance().addEntity(armorStandPrism.getUniqueId(), this);
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    public ArrayList<Direction> getNewDirection(Direction direction, int size) {
        HashMap<Integer, ArrayList<Direction>> cacheBySize = cache.get(direction);
        ArrayList<Direction> cachedResult;
        if (cacheBySize != null) {
            cachedResult = cacheBySize.get(size);
            if (cachedResult != null) {
                return cachedResult;
            }
        }
        LinkedList<Direction> res = new LinkedList<>();
        if (size > 0) {
            boolean even = size % 2 == 0;
            Vector diffVector = direction
                    .clone()
                    .crossProduct(getRotation().toHeadDirection())
                    .normalize().multiply(0.1);
            Vector minusDiffVector = diffVector.clone().multiply(-1);
            Vector lastDiffVector, minusLastDiffVector;
            int nbRemaining = size;
            if (even) {
                Vector halfDiffVector = diffVector.clone().multiply(0.5);
                lastDiffVector = halfDiffVector;
                minusLastDiffVector = halfDiffVector.clone().multiply(-1);
                res.add(new Direction(direction.clone().add(minusLastDiffVector)));
                res.add(new Direction(direction.clone().add(lastDiffVector)));
                nbRemaining -= 2;
            } else {
                lastDiffVector = new Vector(0, 0, 0);
                minusLastDiffVector = lastDiffVector;
                res.add(direction.clone());
                nbRemaining--;
            }
            while (nbRemaining > 0) {
                lastDiffVector = lastDiffVector.add(diffVector);
                minusLastDiffVector = minusLastDiffVector.add(minusDiffVector);
                res.add(0, new Direction(direction.clone().add(minusLastDiffVector)));
                res.add(res.size(), new Direction(direction.clone().add(lastDiffVector)));
                nbRemaining -= 2;
            }
        }
        cachedResult = new ArrayList<>(res);
        if (cacheBySize == null) {
            cacheBySize = new HashMap<>();
            cacheBySize.put(size, cachedResult);
        }
        cache.put(direction.clone(), cacheBySize);
        return cachedResult;
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandPrism = null;
    }
}
