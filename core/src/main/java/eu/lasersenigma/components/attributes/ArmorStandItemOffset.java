package eu.lasersenigma.components.attributes;

public enum ArmorStandItemOffset {
    GLASS_PANE(0.275, 0.755d),
    HEAD(0, 0.3d);

    private final double offsetItemToHead;
    private final double offsetNeckToHead;

    private ArmorStandItemOffset(double offsetItemToHead, double offsetNeckToHead) {
        this.offsetNeckToHead = offsetNeckToHead;
        this.offsetItemToHead = offsetItemToHead;
    }

    public double getOffsetItemToHead() {
        return offsetItemToHead;
    }

    public double getOffsetNeckToHead() {
        return offsetNeckToHead;
    }
}
