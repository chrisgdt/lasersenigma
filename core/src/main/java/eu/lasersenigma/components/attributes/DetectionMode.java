package eu.lasersenigma.components.attributes;

import eu.lasersenigma.config.Message;

public enum DetectionMode {
    PERMANENTLY_ENABLED(true, false, Message.DETECTION_MODE_ALWAYS_ENABLED),
    DETECTION_LASER_RECEIVERS(false, false, Message.DETECTION_MODE_LASER_RECEIVERS),
    DETECTION_REDSTONE(true, false, Message.DETECTION_MODE_REDSTONE),
    DETECTION_PLAYERS(false, false, Message.DETECTION_MODE_PLAYERS),
    DETECTION_REDSTONE_SENSORS(false, false, Message.DETECTION_MODE_REDSTONE_SENSOR),
    DETECTION_LOCKS(false, false, Message.DETECTION_MODE_LOCKS),
    DETECTION_VICTORY_AREA(false, true, Message.DETECTION_MODE_VICTORY_AREA);

    private final boolean specificToLaserSenders;

    private final boolean specificToAreas;

    private final Message correspondingMessage;

    private DetectionMode(boolean specificToLaserSenders, boolean specificToAreas, Message correspondingMessage) {
        this.specificToLaserSenders = specificToLaserSenders;
        this.correspondingMessage = correspondingMessage;
        this.specificToAreas = specificToAreas;
    }

    public DetectionMode next(boolean isLaserSender, boolean isArea) {
        DetectionMode tmpMode = this;
        while (true) {
            tmpMode = tmpMode.next();
            if (!isLaserSender && tmpMode.specificToLaserSenders == true) {
                continue;
            }
            if (!isArea && tmpMode.specificToAreas == true) {
                continue;
            }
            return tmpMode;
        }
    }

    public Message getCorrespondingMessage() {
        return this.correspondingMessage;
    }

    private DetectionMode next() {
        int nextModeIndex = this.ordinal() + 1;
        for (DetectionMode curMode : DetectionMode.values()) {
            if (curMode.ordinal() == nextModeIndex) {
                return curMode;
            }
        }
        return DetectionMode.values()[0];
    }

    public boolean isRangeModificationCompatible() {
        return !specificToLaserSenders && !specificToAreas;
    }

    public boolean isSpecificToLaserSender() {
        return specificToLaserSenders;
    }

    public boolean isSpecificToAreas() {
        return specificToAreas;
    }
}
