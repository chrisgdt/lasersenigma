package eu.lasersenigma.components.attributes;

import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

public enum ComponentFace {
    UP(BlockFace.UP, new Rotation(0, 0), new Vector(0, 1, 0)),
    DOWN(BlockFace.DOWN, new Rotation(Math.toRadians(90), 0), new Vector(0, -1, 0)),
    SOUTH(BlockFace.SOUTH, new Rotation(0, 0), new Vector(0, 0, 1)),
    NORTH(BlockFace.NORTH, new Rotation(0, Math.toRadians(180)), new Vector(0, 0, -1)),
    EAST(BlockFace.EAST, new Rotation(0, Math.toRadians(-90)), new Vector(1, 0, 0)),
    WEST(BlockFace.WEST, new Rotation(0, Math.toRadians(90)), new Vector(-1, 0, 0));

    private final BlockFace blockFace;
    private final Rotation rotation;
    private final Vector vector;
    ComponentFace(BlockFace blockface, Rotation rotation, Vector vector) {
        this.blockFace = blockface;
        this.rotation = rotation;
        this.vector = vector.normalize();
    }

    public static ComponentFace from(BlockFace blockFace) {
        for (ComponentFace face : values()) {
            if (blockFace == face.blockFace) {
                return face;
            }
        }
        return null;
    }

    public Rotation getRotation() {
        return rotation;
    }

    public Vector getVector() {
        return vector.clone();
    }

    public BlockFace getBlockFace() {
        return blockFace;
    }

    public Rotation getDefaultRotation(ComponentType type) {
        switch (type) {
            case FILTERING_SPHERE:
            case REFLECTING_SPHERE:
            case MIRROR_SUPPORT:
            case PRISM:
            case CALL_BUTTON:
                switch (this) {
                    case DOWN:
                        return new Rotation(Math.toRadians(180), 0);
                    case SOUTH:
                        return new Rotation(Math.toRadians(90), 0);
                    case NORTH:
                        return new Rotation(Math.toRadians(90), Math.toRadians(180));
                    case EAST:
                        return new Rotation(Math.toRadians(90), Math.toRadians(-90));
                    case WEST:
                        return new Rotation(Math.toRadians(90), Math.toRadians(90));
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        return rotation;
    }

}
