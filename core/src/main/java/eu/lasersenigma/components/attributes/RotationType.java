package eu.lasersenigma.components.attributes;

public enum RotationType {
    LEFT,
    RIGHT,
    UP,
    DOWN;
}
