package eu.lasersenigma.components.attributes;

import org.bukkit.Location;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Rotation extends EulerAngle {

    private static final double ANGLE_DIFF = Math.toRadians(22.5);

    private static final double AS_HEAD_RADIUS = 0.28;

    public Rotation(double x, double y) {
        super(x, y, 0);
    }

    public Rotation(EulerAngle eulerAngle) {
        super(eulerAngle.getX(), eulerAngle.getY(), 0);
    }

    /**
     * Retrieves the next rotation from an Euler Angle and the type of the
     * rotation done
     *
     * @param rotationType the type of the rotation which must be performed
     * @return the new euler angle of the armorstand head
     */
    public Rotation getNextRotation(RotationType rotationType) {
        switch (rotationType) {
            case RIGHT:
                return new Rotation(getX(), (getY() + ANGLE_DIFF));
            case LEFT:
                return new Rotation(getX(), (getY() - ANGLE_DIFF));
            case DOWN:
                return new Rotation((getX() + ANGLE_DIFF), getY());
            case UP:
                return new Rotation((getX() - ANGLE_DIFF), getY());
            default:
                throw new UnsupportedOperationException("What type of rotation is that ?");
        }
    }

    /**
     * Retrieves the previous rotation from an Euler Angle and the type of the
     * rotation done
     *
     * @param rotationType the type of the rotation which must be performed
     * @return the new euler angle of the armorstand head
     */
    public Rotation getPrevRotation(RotationType rotationType) {
        switch (rotationType) {
            case RIGHT:
                return new Rotation(getX(), (getY() - ANGLE_DIFF));
            case LEFT:
                return new Rotation(getX(), (getY() + ANGLE_DIFF));
            case DOWN:
                return new Rotation((getX() - ANGLE_DIFF), getY());
            case UP:
                return new Rotation((getX() + ANGLE_DIFF), getY());
            default:
                throw new UnsupportedOperationException("What type of rotation is that ?");
        }
    }

    public Vector toEyeDirection() {

        //head rotation offset
        double doubleRotX = getX() + Math.toRadians(90);
        double doubleRotY = getY();
        double x = Math.sin(doubleRotY) * Math.sin(doubleRotX + Math.toRadians(180));
        double y = Math.cos(doubleRotX);
        double z = Math.cos(doubleRotY) * Math.sin(doubleRotX);

        //rounding
        x = (double) (Math.round(x * 100000d) / 100000d);
        y = (double) (Math.round(y * 100000d) / 100000d);
        z = (double) (Math.round(z * 100000d) / 100000d);

        Vector v = new Vector(x, y, z).normalize();

        return v;
    }

    public Vector toHeadDirection() {

        //head rotation offset
        double doubleRotX = getX();
        double doubleRotY = getY();
        double x = Math.sin(doubleRotY) * Math.sin(doubleRotX + Math.toRadians(180));
        double y = Math.cos(doubleRotX);
        double z = Math.cos(doubleRotY) * Math.sin(doubleRotX);

        //rounding
        x = (double) (Math.round(x * 100000d) / 100000d);
        y = (double) (Math.round(y * 100000d) / 100000d);
        z = (double) (Math.round(z * 100000d) / 100000d);

        Vector v = new Vector(x, y, z).normalize();

        return v;
    }

    public Vector toSideDirection() {
        return toEyeDirection().crossProduct(toHeadDirection()).normalize();
    }

    public List<Location> getFrontCorners(Location armorStandHeadCenterLoc) {
        List<Location> result = new ArrayList<>();
        armorStandHeadCenterLoc.add(toEyeDirection().multiply(AS_HEAD_RADIUS));
        Vector top = toHeadDirection().multiply(AS_HEAD_RADIUS);
        Vector side = toSideDirection().multiply(AS_HEAD_RADIUS);
        Vector minusSide = side.clone().multiply(-1);
        Vector minusTop = top.clone().multiply(-1);
        result.add(armorStandHeadCenterLoc.clone().add(top).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(top).add(minusSide));
        result.add(armorStandHeadCenterLoc.clone().add(minusTop).add(side));
        result.add(armorStandHeadCenterLoc.clone().add(minusTop).add(minusSide));
        return result;
    }
}
