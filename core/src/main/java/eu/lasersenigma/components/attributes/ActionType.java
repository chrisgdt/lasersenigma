package eu.lasersenigma.components.attributes;

import eu.lasersenigma.components.AttractionRepulsionSphere;
import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.components.parents.IRotatableComponent;
import eu.lasersenigma.items.Item;

import java.util.Arrays;

public enum ActionType {
    WAIT(Item.SCHEDULED_ACTIONS_WAIT),
    ROTATE_LEFT(Item.COMPONENT_SHORTCUTBAR_ROTATION_LEFT),
    ROTATE_RIGHT(Item.COMPONENT_SHORTCUTBAR_ROTATION_RIGHT),
    ROTATE_UP(Item.COMPONENT_SHORTCUTBAR_ROTATION_UP),
    ROTATE_DOWN(Item.COMPONENT_SHORTCUTBAR_ROTATION_DOWN),
    COLOR_LOOP(Item.COMPONENT_SHORTCUTBAR_COLOR_LOOPER),
    COLOR_RED(Item.RED),
    COLOR_GREEN(Item.GREEN),
    COLOR_BLUE(Item.BLUE),
    COLOR_MAGENTA(Item.MAGENTA),
    COLOR_YELLOW(Item.YELLOW),
    COLOR_LIGHT_BLUE(Item.LIGHT_BLUE),
    COLOR_WHITE(Item.WHITE),
    COLOR_BLACK(Item.BLACK),
    SPHERE_DECREASE(Item.COMPONENT_SHORTCUTBAR_DECREASE_SPHERE_SIZE),
    SPHERE_INCREASE(Item.COMPONENT_SHORTCUTBAR_INCREASE_SPHERE_SIZE);

    private final Item item;

    ActionType(Item item) {
        this.item = item;
    }

    public static ActionType getFromColor(LasersColor color) {
        switch (color) {
            case WHITE:
                return COLOR_WHITE;
            case RED:
                return COLOR_RED;
            case GREEN:
                return COLOR_GREEN;
            case BLUE:
                return COLOR_BLUE;
            case MAGENTA:
                return COLOR_MAGENTA;
            case YELLOW:
                return COLOR_YELLOW;
            case CYAN:
                return COLOR_LIGHT_BLUE;
            case BLACK:
                return COLOR_BLACK;
            default:
                return null;
        }
    }

    public static ActionType getFromRotation(RotationType rotationType) {
        switch (rotationType) {
            case DOWN:
                return ROTATE_DOWN;
            case UP:
                return ROTATE_UP;
            case LEFT:
                return ROTATE_LEFT;
            case RIGHT:
                return ROTATE_RIGHT;
            default:
                return null;
        }
    }

    public static ActionType getFromItem(Item item) {
        return Arrays.asList(ActionType.values()).stream().filter(actionType -> actionType.getItem() == item).findAny().orElse(null);
    }

    public Item getItem() {
        return this.item;
    }

    public boolean isDelayAvailable() {
        return (this == WAIT);
    }

    public boolean isAvailable(IComponent component) {
        switch (this) {
            case ROTATE_DOWN:
            case ROTATE_LEFT:
            case ROTATE_RIGHT:
            case ROTATE_UP:
                return (component instanceof IRotatableComponent);
            case COLOR_RED:
            case COLOR_GREEN:
            case COLOR_BLUE:
            case COLOR_MAGENTA:
            case COLOR_YELLOW:
            case COLOR_LIGHT_BLUE:
            case COLOR_WHITE:
            case COLOR_LOOP:
                return (component instanceof IColorableComponent);
            case COLOR_BLACK:
                return (component instanceof MirrorSupport);
            case SPHERE_DECREASE:
            case SPHERE_INCREASE:
                return (component instanceof AttractionRepulsionSphere);
            case WAIT:
                return true;
            default:
                throw new IllegalStateException("Unsupported action");
        }
    }

    public boolean isDoableNow(IComponent component) {
        if (!component.getArea().isActivated()) {
            return false;
        }
        switch (this) {
            case ROTATE_DOWN:
            case ROTATE_LEFT:
            case ROTATE_RIGHT:
            case ROTATE_UP:
                if (component instanceof IRotatableComponent) {
                    if (component instanceof IMirrorContainer) {
                        return ((IMirrorContainer) component).hasMirror();
                    }
                    return true;
                }
                return false;
            case COLOR_RED:
            case COLOR_GREEN:
            case COLOR_BLUE:
            case COLOR_MAGENTA:
            case COLOR_YELLOW:
            case COLOR_LIGHT_BLUE:
            case COLOR_WHITE:
            case COLOR_LOOP:
                if (component instanceof IColorableComponent) {
                    if (component instanceof IMirrorContainer) {
                        return ((IMirrorContainer) component).hasMirror();
                    }
                    return true;
                }
                return false;
            case COLOR_BLACK:
                if (!(component instanceof MirrorSupport)) {
                    return false;
                }
                return ((MirrorSupport) component).hasMirror();
            case SPHERE_DECREASE:
            case SPHERE_INCREASE:
                return (component instanceof AttractionRepulsionSphere);
            case WAIT:
                return true;
            default:
                throw new IllegalStateException("Unsupported action");
        }
    }

    public LasersColor getColor() {
        switch (this) {
            case COLOR_RED:
                return LasersColor.RED;
            case COLOR_GREEN:
                return LasersColor.GREEN;
            case COLOR_BLUE:
                return LasersColor.BLUE;
            case COLOR_MAGENTA:
                return LasersColor.MAGENTA;
            case COLOR_YELLOW:
                return LasersColor.YELLOW;
            case COLOR_LIGHT_BLUE:
                return LasersColor.CYAN;
            case COLOR_WHITE:
                return LasersColor.WHITE;
            case COLOR_BLACK:
                return LasersColor.BLACK;
            default:
                return null;
        }
    }

    public RotationType getRotationType() {
        switch (this) {
            case ROTATE_UP:
                return RotationType.UP;
            case ROTATE_DOWN:
                return RotationType.DOWN;
            case ROTATE_LEFT:
                return RotationType.LEFT;
            case ROTATE_RIGHT:
                return RotationType.RIGHT;
            default:
                return null;
        }
    }
}
