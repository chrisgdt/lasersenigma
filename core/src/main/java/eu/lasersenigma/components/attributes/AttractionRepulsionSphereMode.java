package eu.lasersenigma.components.attributes;

import eu.lasersenigma.config.Message;

public enum AttractionRepulsionSphereMode {
    ATTRACTION(Message.ATTRACTION_MODE),
    REPULSION(Message.REPULSION_MODE);

    private final Message correspondingMessage;

    private AttractionRepulsionSphereMode(Message correspondingMessage) {
        this.correspondingMessage = correspondingMessage;
    }

    public Message getCorrespondingMessage() {
        return correspondingMessage;
    }

    public AttractionRepulsionSphereMode next() {
        int nextModeIndex = this.ordinal() + 1;
        for (AttractionRepulsionSphereMode curMode : AttractionRepulsionSphereMode.values()) {
            if (curMode.ordinal() == nextModeIndex) {
                return curMode;
            }
        }
        return AttractionRepulsionSphereMode.values()[0];
    }
}
