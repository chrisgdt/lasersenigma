package eu.lasersenigma.components.attributes;

import org.bukkit.util.Vector;

public enum ElevatorDirection {
    UP(new Vector(0, 1, 0)),
    DOWN(new Vector(0, -1, 0));

    private final Vector vector;

    private ElevatorDirection(Vector vector) {
        this.vector = vector;
    }

    public Vector getVector() {
        return this.vector.clone();
    }
}
