package eu.lasersenigma.components.attributes;

import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.items.Item;

public enum MirrorSupportMode {
    FREE(Message.MIRROR_SUPPORT_MODE_FREE, Item.MIRROR_SUPPORT, false),
    BLOCKED(Message.MIRROR_SUPPORT_MODE_BLOCKED, Item.MIRROR_SUPPORT_BLOCKED, true);

    private final Message correspondingMessage;

    private final boolean mirrorBlocked;

    private final Item correspondingSupportSkin;

    private MirrorSupportMode(Message correspondingMessage, Item correspondingSupportSkin, boolean mirrorBlocked) {
        this.correspondingMessage = correspondingMessage;
        this.correspondingSupportSkin = correspondingSupportSkin;
        this.mirrorBlocked = mirrorBlocked;
    }

    public Message getCorrespondingMessage() {
        return correspondingMessage;
    }

    public MirrorSupportMode next() {
        int nextModeIndex = this.ordinal() + 1;
        for (MirrorSupportMode curMode : MirrorSupportMode.values()) {
            if (curMode.ordinal() == nextModeIndex) {
                return curMode;
            }
        }
        return MirrorSupportMode.values()[0];
    }

    public boolean isMirrorRotatable() { return !mirrorBlocked; }

    public boolean isMirrorRetrievable() {
        return !mirrorBlocked;
    }

    public Item getCorrespondingSupportSkin() {
        return correspondingSupportSkin;
    }

    public static MirrorSupportMode from(String modeStr) {
        MirrorSupportMode mode;
        try {
            mode = MirrorSupportMode.valueOf(modeStr);
        } catch (IllegalArgumentException iae) {
            mode = FREE;
        }
        return mode;
    }
}
