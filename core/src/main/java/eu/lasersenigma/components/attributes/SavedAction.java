package eu.lasersenigma.components.attributes;

import eu.lasersenigma.components.AttractionRepulsionSphere;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IRotatableComponent;

public class SavedAction implements Comparable {

    private final IComponent component;
    private int scheduledActionId;
    private ActionType type;
    private Integer delay;

    public SavedAction(IComponent component, ActionType type, Integer delay) {
        this.component = component;
        this.type = type;
        this.delay = delay;
    }

    public SavedAction(IComponent component) {
        this.component = component;
        this.type = ActionType.WAIT;
        this.delay = 20;
    }

    public int getScheduledActionId() {
        return scheduledActionId;
    }

    public IComponent getComponent() {
        return component;
    }

    public Integer getDelay() {
        return this.delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    public int getIndex() {
        return this.component.getScheduledActions().indexOf(this);
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public void doAction() {
        switch (type) {
            case COLOR_BLACK:
            case COLOR_WHITE:
            case COLOR_RED:
            case COLOR_GREEN:
            case COLOR_BLUE:
            case COLOR_MAGENTA:
            case COLOR_YELLOW:
            case COLOR_LIGHT_BLUE:
                ((IColorableComponent) component).setColor(type.getColor(), false);
                break;
            case COLOR_LOOP:
                ((IColorableComponent) component).changeColor(false);
                break;
            case ROTATE_DOWN:
            case ROTATE_LEFT:
            case ROTATE_RIGHT:
            case ROTATE_UP:
                ((IRotatableComponent) component).rotate(type.getRotationType(), false, false);
                break;
            case SPHERE_DECREASE:
            case SPHERE_INCREASE:
                AttractionRepulsionSphere sphere = ((AttractionRepulsionSphere) component);
                int size = sphere.getCurrentSize();
                int newSize = size;
                if (type == ActionType.SPHERE_DECREASE) {
                    newSize -= 1;
                    if (newSize < 1) {
                        return;
                    }
                } else {
                    newSize += 1;
                    if (newSize > 9) {
                        return;
                    }
                }
                sphere.setSize(newSize, false);
                break;
            default:
                throw new IllegalStateException("ActionType not supported yet.");
        }
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof SavedAction)) {
            return -1;
        }
        SavedAction secondSavedAction = (SavedAction) o;
        if (this.getComponent() != secondSavedAction.getComponent()) {
            return -1;
        }
        return Integer.compare(this.getIndex(), secondSavedAction.getIndex());
    }
}
