package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentFace;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.parents.AComponent;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * A chest containing (colored) mirror(s)
 */
public class MirrorChest extends AComponent implements IColorableComponent {

    /**
     * the facing of the chest
     */
    private final ComponentFace face;
    /**
     * the number of mirrors this component contains
     */
    private int nbMirrors;
    /**
     * the color of this component as saved in database
     */
    private LasersColor color;
    /**
     * current color of this component (not saved in the database)
     */
    private LasersColor currentColor;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this mirror chest
     * @param componentId the id of the component inside the database
     * @param location    the location of this mirror chest
     * @param nbMirrors   the number of mirrors this mirror chest contains
     * @param color       the color of this mirror chest
     * @param face        the component facing
     */
    public MirrorChest(Area area, int componentId, Location location, int nbMirrors, LasersColor color, ComponentFace face) {
        super(area, componentId, location, ComponentType.MIRROR_CHEST);
        this.nbMirrors = nbMirrors;
        this.color = color;
        currentColor = color;
        this.face = face;
    }

    /**
     * Constructor
     *
     * @param area      the area containing this mirror chest
     * @param location  the location of this mirror chest
     * @param nbMirrors the number of mirrors this mirror chest contains
     * @param color     the color of this mirror chest
     * @param face      the component facing
     */
    public MirrorChest(Area area, Location location, int nbMirrors, LasersColor color, ComponentFace face) {
        super(area, location, ComponentType.MIRROR_CHEST);
        this.nbMirrors = nbMirrors;
        this.color = color;
        currentColor = color;
        this.face = face;
        dbCreate();
    }

    /**
     * Constructor
     *
     * @param area      the area containing this mirror chest
     * @param location  the location of this mirror chest
     * @param nbMirrors the number of mirrors this mirror chest contains
     * @param face      the component facing
     */
    public MirrorChest(Area area, Location location, int nbMirrors, ComponentFace face) {
        this(area, location, nbMirrors, LasersColor.WHITE, face);
    }

    public ComponentFace getFace() {
        return face;
    }

    /**
     * gets the number of mirrors this chest contains
     *
     * @return the number of mirrors this chest contains
     */
    public int getNbMirrors() {
        return nbMirrors;
    }

    /**
     * sets the number of mirrors this chest will contain
     *
     * @param nbMirrors the number of mirrors this chest will contain
     */
    public void setNbMirrors(int nbMirrors) {
        this.nbMirrors = nbMirrors;
        updateDisplay();
        dbUpdate();
    }

    /**
     * updates this mirror chest
     */
    @Override
    public void updateDisplay() {
        Block b = getLocation().getBlock();
        ItemStack item = ItemsFactory.getInstance().getItemStack(Item.getMirror(currentColor));
        if (b.getType() != Material.CHEST) {
            b.setType(Material.CHEST);
        }
        b.getState().update(true);
        Chest chest = (Chest) b.getState();
        chest.setData(new org.bukkit.material.Chest(face.getBlockFace()));
        //chest.setCustomName(Item.MIRROR_CHEST.getTranslatedName());
        chest.update();
        Chest chestTmp = (Chest) b.getState();
        Inventory inv = chestTmp.getInventory();
        inv.clear();
        for (int i = 0; i < nbMirrors; i++) {
            inv.addItem(item.clone());

        }
        b.getState().update();
    }

    /**
     * deletes this mirror chest
     */
    @Override
    public void hide() {
        Block b = getLocation().getBlock();
        BlockState state = b.getState();
        if (state instanceof Chest) {
            Chest chest = (Chest) state;
            chest.getBlockInventory().clear();
            b.getState().update();
        }
        b.setType(Material.AIR);
        b.getState().update();
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(true);
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of FilteringSpheres is defined by
     *             its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(true), save);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of FilteringSpheres is defined by
     *              its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        this.currentColor = color;
        if (save) {
            this.color = color;
        }
        updateDisplay();
        if (save) {
            dbUpdate();
        }
    }

    /**
     * gets the color of this mirror chest
     *
     * @return the color of this mirror chest
     */
    @Override
    public LasersColor getColor() {
        return this.currentColor;
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, true);
    }

    @Override
    public LasersColor getSavedColor() {
        return color;
    }

    /**
     * Resets the component (when the last player leaves the area)
     */
    @Override
    public void reset() {
        this.currentColor = this.color;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
