package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IPlayerModifiableComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Attraction Repulsion Sphere component
 */
public final class AttractionRepulsionSphere extends AArmorStandComponent implements IPlayerModifiableComponent {

    private final HashMap<Location, HashMap<Direction, Vector>> cache;
    /**
     * The armorstand representing the sphere
     */
    private ArmorStand armorStandSphere = null;
    private AttractionRepulsionSphereMode mode;

    private int size;

    private int currentSize;

    /**
     * Constructor used for creation from the database
     *
     * @param area        the area containing this laser receiver
     * @param componentId the id of the component inside the database
     * @param location    the location of the laser receiver
     * @param face        the blockface of the component
     * @param rotation    the rotation of the component
     * @param mode        the mode (attraction or repulsion)
     * @param size        the size of the sphere where laser particles will be affected
     */
    public AttractionRepulsionSphere(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, AttractionRepulsionSphereMode mode, int size) {
        super(area, componentId, location, ComponentType.ATTRACTION_REPULSION_SPHERE, face, rotation);
        cache = new HashMap<>();
        this.size = size;
        this.currentSize = size;
        this.mode = mode;
        getArea().clearAttractionRepulsionCache();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this
     * @param location the location of the component
     * @param face     the blockface of the component
     */
    public AttractionRepulsionSphere(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.ATTRACTION_REPULSION_SPHERE, face);
        cache = new HashMap<>();
        mode = AttractionRepulsionSphereMode.ATTRACTION;
        size = 4;
        this.currentSize = size;
        getArea().clearAttractionRepulsionCache();
        dbCreate();
    }

    public int getSize() {
        return size;
    }

    public int getCurrentSize() {
        return currentSize;
    }

    public void setSize(int size, boolean save) {
        this.currentSize = size;
        showSphere();
        cache.clear();
        getArea().clearAttractionRepulsionCache();
        if (save) {
            this.size = currentSize;
            dbUpdate();
        }
    }

    public void changeMode(AttractionRepulsionSphereMode mode) {
        this.mode = mode;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
        updateDisplay();
        showSphere();
        dbUpdate();
    }

    public AttractionRepulsionSphereMode getMode() {
        return this.mode;
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandSphere != null) {
            Areas.getInstance().removeEntity(armorStandSphere.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandSphere.getUniqueId())) {
                    armorStandSphere = (ArmorStand) entity;
                }
            }
            armorStandSphere.remove();
            armorStandSphere = null;
        }
        currentSize = size;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandSphere = findArmorStandBack(armorStandSphere);
        if (armorStandSphere == null) {
            createSphereArmorStand();
        } else {
            armorStandSphere.setHeadPose(rotation);
            Location nextLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.HEAD, rotation);
            armorStandSphere.teleport(nextLoc);
            armorStandSphere.setHelmet(ItemsFactory.getInstance().getItemStack(getItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    /**
     * Creates the component armorstand
     */
    private void createSphereArmorStand() {
        Location armorStandPrismLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandSphere = createArmorStand(armorStandPrismLoc, rotation, getItem());
        Areas.getInstance().addEntity(armorStandSphere.getUniqueId(), this);
    }

    private Item getItem() {
        switch (mode) {
            case ATTRACTION:
                return Item.ATTRACTION_SPHERE;
            case REPULSION:
                return Item.REPULSION_SPHERE;
            default:
                return null;
        }
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    /**
     * Resets the component (when the last player leaves the area)
     */
    @Override
    public void reset() {
        currentSize = size;
        cache.clear();
        getArea().clearAttractionRepulsionCache();
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }

    public Vector getTrajectoryModicationVector(Location location, Direction direction) {

        Vector trajectoryModicationVector;

        double cx = getASHeadCenterLocation().getX();
        double cy = getASHeadCenterLocation().getY();
        double cz = getASHeadCenterLocation().getZ();
        double px = location.getX();
        double py = location.getY();
        double pz = location.getZ();

        double distance = getASHeadCenterLocation().distance(location);
        if (mode == AttractionRepulsionSphereMode.REPULSION) {
            trajectoryModicationVector = new Vector(
                    (px - cx),
                    (py - cy),
                    (pz - cz)
            );
        } else {
            trajectoryModicationVector = new Vector(
                    (cx - px),
                    (cy - py),
                    (cz - pz)
            );
        }
        double gravityMultiplier = LaserParticle.LASERS_SPEED * 0.5 * ((currentSize - distance) / currentSize);
        return trajectoryModicationVector.normalize().multiply(gravityMultiplier);
    }

    public void showSphere() {
        Location baseLocation = getASHeadCenterLocation();
        double nbParticlesSquared = 50;
        double TAU = 2 * Math.PI;
        double increment = TAU / nbParticlesSquared;
        for (double angle1 = 0; angle1 < TAU; angle1 += increment) {
            double y = (Math.cos(angle1) * currentSize);
            double tmpRadius = Math.sin(angle1);
            for (double angle2 = 0; angle2 < TAU; angle2 += increment) {
                double x = Math.cos(angle2) * tmpRadius * currentSize;
                double z = Math.sin(angle2) * tmpRadius * currentSize;
                NMSManager.getNMS().playEffect(
                        getArea().getPlayers(),
                        new Location(baseLocation.getWorld(), baseLocation.getX() + x, baseLocation.getY() + y, baseLocation.getZ() + z),
                        "REDSTONE",
                        LasersColor.BLACK.getBukkitColor());
            }
        }

    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandSphere = null;
    }
}
