package eu.lasersenigma.components;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.parents.AComponent;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.ITaskComponent;
import eu.lasersenigma.events.components.MeltableClayIgnitedLEEvent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.tasks.MeltDownTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * colored clay block that will melt and explode if a laser of the good color
 * touch it
 */
public final class MeltableClayBlock extends AComponent implements IComponent, ITaskComponent, IColorableComponent {

    /**
     * the colors that are received in this laser receiver associated with an
     * integer value representing the moment they were received
     */
    private final HashMap<LasersColor, Integer> receivedColorsLifeTime;
    /**
     * the color of this component as saved in database
     */
    private LasersColor color;
    /**
     * current color of this component (not saved in the database)
     */
    private LasersColor currentColor;
    /**
     * did the clay already melt
     */
    private boolean melted;
    /**
     * the id of the task scanning received lasers
     */
    private Integer taskId;
    /**
     * is the clay currently melting
     */
    private boolean isMelting;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this meltable clay block
     * @param componentId the id of the component inside the database
     * @param location    the location of this meltable clay block
     * @param color       the color of this meltable clay block
     */
    public MeltableClayBlock(Area area, int componentId, Location location, LasersColor color) {
        super(area, componentId, location, ComponentType.MELTABLE_CLAY);
        receivedColorsLifeTime = new HashMap<>();
        melted = false;
        isMelting = false;
        this.color = color;
        currentColor = color;
        this.taskId = null;
    }

    /**
     * @param area     the area containing this meltable clay block
     * @param location the location of this meltable clay block
     * @param color    the color of this meltable clay block
     */
    public MeltableClayBlock(Area area, Location location, LasersColor color) {
        super(area, location, ComponentType.MELTABLE_CLAY);
        receivedColorsLifeTime = new HashMap<>();
        melted = false;
        isMelting = false;
        this.color = color;
        currentColor = color;
        this.taskId = null;
        dbCreate();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this meltable clay block
     * @param location the location containing this meltable clay block
     */
    public MeltableClayBlock(Area area, Location location) {
        this(area, location, NMSManager.getNMS().getColorFromBlock(location.getBlock()));
    }

    /**
     * Starts the task scanning the laser received
     */
    @Override
    public final void startTask() {
        if (!getArea().isActivated()) {
            return;
        }
        if (taskId != null) {
            return;
        }
        BukkitTask task = Bukkit.getScheduler().runTaskTimer(LasersEnigmaPlugin.getInstance(), (new Runnable() {
            @Override
            public void run() {
                if (!getArea().isActivated()) {
                    cancelTask();
                    return;
                }
                receivedColorsLifeTime.replaceAll((k, v) -> v + 1); //increment LifeTime
                receivedColorsLifeTime.entrySet().removeIf(e -> e.getValue() >= 2);
            }
        }), 0, LaserParticle.LASERS_FREQUENCY);
        taskId = task.getTaskId();
    }

    /**
     * cancels the task scanning the laser received
     */
    @Override
    public void cancelTask() {
        if (taskId == null) {
            return;
        }
        Bukkit.getScheduler().cancelTask(taskId);
        receivedColorsLifeTime.clear();
        taskId = null;
    }

    /**
     * is this meltable clay block already melted
     *
     * @return true if this meltable clay block is melted
     */
    public boolean isMelted() {
        return this.melted;
    }

    /**
     * sets if this meltable clay block is melted
     *
     * @param melted true to set this meltable clay block melted
     */
    public void setMelted(boolean melted) {
        this.melted = melted;
        updateDisplay();
    }

    /**
     * Checks if this meltable clay block is currently melting
     *
     * @param isMelting true to set this meltable clay block melting
     */
    public void setIsMelting(boolean isMelting) {
        this.isMelting = isMelting;
    }

    /**
     * resets the meltable clay block
     */
    @Override
    public final void reset() {
        isMelting = false;
        this.melted = false;
        this.currentColor = color;
        this.receivedColorsLifeTime.clear();
        updateDisplay();
    }

    /**
     * deletes this meltable clay block
     */
    @Override
    public void hide() {
        super.hide();
        cancelTask();
        this.receivedColorsLifeTime.clear();
    }

    /**
     * updates this meltable clay block
     */
    @Override
    public void updateDisplay() {
        if (melted) {
            Block b = getLocation().getBlock();
            b.setType(Material.AIR);
            b.getState().update();
            return;
        }
        if (!shouldMeltDown()) {
            Block b = getLocation().getBlock();
            NMSManager.getNMS().setBlock(b, Item.getMeltableClay(currentColor));
            b.getState().update();
        }
    }

    @Override
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        if (isMelted()) {
            return new LaserReceptionResult(false);
        } else {
            if (!isMelting && shouldMeltDown()) {
                isMelting = true;
                Bukkit.getServer().getPluginManager().callEvent(new MeltableClayIgnitedLEEvent(this));
                new MeltDownTask(this);
            } else {
                this.receivedColorsLifeTime.put(laserParticle.getColor(), 0);
            }
            return new LaserReceptionResult(true);
        }
    }

    /**
     * checks if this meltable clay block should melt down
     *
     * @return true if this meltable clay block should melt down
     */
    private boolean shouldMeltDown() {
        Set<LasersColor> colors = receivedColorsLifeTime.entrySet().stream()
                .filter((e) -> e.getValue() == 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
        return currentColor.isComposedOf(colors, LasersEnigmaPlugin.getInstance().getConfig().getBoolean(LasersColor.ONLY_NEED_COLORS_CONFIG_PATH));
    }

    /**
     * gets the color of this meltable clay block
     *
     * @return color
     */
    @Override
    public LasersColor getColor() {
        return currentColor;
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, true);
    }

    @Override
    public LasersColor getSavedColor() {
        return color;
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        changeColor(true);
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of FilteringSpheres is defined by
     *             its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(currentColor.getNextColor(false), save);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is ignored since the color of FilteringSpheres is defined by
     *              its mirror so it's never saved in database.
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }
        if (isMelting || melted) {
            return;
        }
        this.currentColor = color;
        if (save) {
            this.color = color;
        }
        updateDisplay();
        if (save) {
            dbUpdate();
        }
    }

}
