package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.particles.ReflectionData;
import eu.lasersenigma.tasks.GetMirrorFromSphereTask;
import eu.lasersenigma.tasks.PlaceMirrorOnSphereTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

import java.util.HashMap;

/**
 * Reflecting sphere component
 */
public class ReflectingSphere extends AArmorStandComponent implements IColorableComponent, IMirrorContainer {

    public static final double SPHERE_RADIUS = 0.42;

    /**
     * The difference between the location where the mirror appears and the
     * location where it starts entering inside the sphere
     */
    public static final double MIRROR_ANIMATION_CLIP_LOCATION_DIFF = 0.6d;
    /**
     * The difference between the location where the mirror starts entering the
     * sphere and the location where it stops and disappears
     */
    public static final double MIRROR_ANIMATION_LOCATION_DIFF = 0.7d;
    /**
     * Cache for lasers reflection calculation
     */
    private final HashMap<ReflectionData, ReflectionData> reflectionCache;
    /**
     * does this reflecting sphere has a mirror inside
     */
    private boolean hasMirror;
    /**
     * the armor stand representing the mirror
     */
    private ArmorStand armorStandMirror;
    /**
     * The armorstand representing the reflecting sphere
     */
    private ArmorStand armorStandReflectingSphere;
    /**
     * Is this reflecting sphere currently showing an animation
     */
    private boolean onGoingAnimation;
    /**
     * The color of this reflecting sphere
     */
    private LasersColor color;

    /**
     * The saved color of this component's mirror. Used only on reset.
     */
    private LasersColor savedColor;


    private PlaceMirrorOnSphereTask placeMirrorOnSphereTask;
    private GetMirrorFromSphereTask getMirrorFromSphereTask;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this component
     * @param componentId the id of the component inside the database
     * @param location    the location of this component
     * @param face        the face the component is on
     */
    public ReflectingSphere(Area area, int componentId, Location location, ComponentFace face, LasersColor savedColor) {
        super(area, componentId, location, ComponentType.REFLECTING_SPHERE, face, face.getDefaultRotation(ComponentType.REFLECTING_SPHERE));
        hasMirror = savedColor != null;
        color = savedColor != null ? savedColor : LasersColor.BLACK;
        this.savedColor = savedColor;
        onGoingAnimation = false;
        reflectionCache = new HashMap<>();
    }

    /**
     * Constructor
     *
     * @param area     the area containing this component
     * @param location the location of this component
     * @param face     the face the component is on
     */
    public ReflectingSphere(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.REFLECTING_SPHERE, face);
        hasMirror = true;
        onGoingAnimation = false;
        color = LasersColor.WHITE;
        savedColor = null;
        reflectionCache = new HashMap<>();
        updateDisplay();
        dbCreate();
    }

    @Override
    public ArmorStand getArmorStandMirror() {
        return armorStandMirror;
    }

    @Override
    public ArmorStand getArmorStandMirrorContainer() {
        return armorStandReflectingSphere;
    }

    public HashMap<ReflectionData, ReflectionData> getReflectionCache() {
        return reflectionCache;
    }

    /**
     * deletes this component
     */
    @Override
    public void hide() {
        super.hide();
        if (onGoingAnimation && getMirrorFromSphereTask != null) {
            getMirrorFromSphereTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSphereTask != null) {
            placeMirrorOnSphereTask.cancel();
        }
        onGoingAnimation = false;
        //Retrieving armorstands if required
        armorStandReflectingSphere = findArmorStandBack(armorStandReflectingSphere);
        armorStandMirror = findArmorStandBack(armorStandMirror);
        if (armorStandReflectingSphere != null) {
            armorStandReflectingSphere.remove();
            armorStandReflectingSphere = null;
        }
        if (armorStandMirror != null) {
            armorStandMirror.remove();
            armorStandMirror = null;
        }
    }

    /**
     * checks if an animation is currently on going
     *
     * @return true if an animation is currently shown
     */
    public boolean getOnGoingAnimation() {
        return onGoingAnimation;
    }

    /**
     * sets if this component is currently animated or not
     *
     * @param onGoingAnimation if this component is currently animated
     */
    @Override
    public void setOnGoingAnimation(boolean onGoingAnimation) {
        this.onGoingAnimation = onGoingAnimation;
    }

    /**
     * does this component currently has a mirror on it
     *
     * @return true if this component has a mirror on it
     */
    @Override
    public boolean hasMirror() {
        return hasMirror;
    }

    /**
     * Sets if this component currently has a mirror on it
     *
     * @param save      should the lack/presence of mirror be saved in database
     * @param hasMirror if this component currently has a mirror on it or not
     */
    @Override
    public void setHasMirror(boolean hasMirror, boolean save) {
        if (onGoingAnimation) {
            return;
        }
        this.hasMirror = hasMirror;
        if (!hasMirror) {
            color = LasersColor.BLACK;
        }
        updateDisplay();
        if (save) {
            savedColor = hasMirror ? color : null;
            dbUpdate();
        }
    }

    @Override
    public boolean hasSavedMirror() {
        return savedColor != null;
    }

    /**
     * retrieves the mirror from this component
     *
     * @param save should the lack of mirror be saved in database
     * @return true if the mirror has been retrieved successfully
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean getMirror(boolean save) {
        if (!onGoingAnimation && hasMirror && !this.isRemoved()) {
            onGoingAnimation = true;
            // Animation
            getMirrorFromSphereTask = new GetMirrorFromSphereTask(this, Item.REFLECTING_SPHERE, save);
            return true;
        }
        return false;
    }

    /**
     * place a mirror on this component
     *
     * @param color the color of the mirror retrieved
     * @param save  should the presence of mirror be saved in database
     * @return true is the mirror has been placed successfully
     */
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    @Override
    public boolean placeMirror(LasersColor color, boolean save) {
        if (!onGoingAnimation && !hasMirror && !this.isRemoved() && armorStandMirror == null) {
            onGoingAnimation = true;
            this.color = color;
            createMirrorArmorStand();
            placeMirrorOnSphereTask = new PlaceMirrorOnSphereTask(this, Item.getReflectingSphereItem(color), save);
            return true;
        }
        return false;
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
        armorStandReflectingSphere = findArmorStandBack(armorStandReflectingSphere);
        armorStandMirror = findArmorStandBack(armorStandMirror);
        if (armorStandReflectingSphere == null) {
            createReflectingSphereArmorStand();
        }
        if (hasMirror && armorStandMirror == null) {
            createMirrorArmorStand();
        } else if (!hasMirror && armorStandMirror != null) {
            armorStandMirror.remove();
            Areas.getInstance().removeEntity(armorStandMirror.getUniqueId());
            armorStandMirror = null;
        }
        //Rotation, head and location of mirror support armor stand
        Rotation sphereRotation = face.getDefaultRotation(ComponentType.REFLECTING_SPHERE);

        armorStandReflectingSphere.setHeadPose(sphereRotation);
        armorStandReflectingSphere.setHelmet(ItemsFactory.getInstance().getItemStack(Item.getReflectingSphereItem(color)));

        Location sphereLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                sphereRotation);
        armorStandReflectingSphere.teleport(sphereLoc);

        //Rotation, head and location of mirror armor stand
        if (hasMirror) {
            armorStandMirror.setHeadPose(rotation);
            armorStandMirror.setHelmet(ItemsFactory.getInstance().getItemStack(Item.getMirror(color)));
            Location mirrorLoc = getArmorStandLocationWithOffsets(getDefaultArmorStandBaseLocation(getLocation(), face), ArmorStandItemOffset.GLASS_PANE, face.getDefaultRotation(getType()));
            armorStandMirror.teleport(mirrorLoc);
        }
    }

    /**
     * Creates the armorstand corresponding to the component
     */
    private void createReflectingSphereArmorStand() {
        Location armorStandReflectingSphereLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                face.getDefaultRotation(getType())
        );
        armorStandReflectingSphere = createArmorStand(armorStandReflectingSphereLoc, rotation, Item.getReflectingSphereItem(color));
        Areas.getInstance().addEntity(armorStandReflectingSphere.getUniqueId(), this);
    }

    /**
     * Creates the armorstand corresponding to the component
     */
    private void createMirrorArmorStand() {
        Location armorStandMirrorLoc = getDefaultArmorStandBaseLocation(getLocation(), face);
        if (!hasMirror()) {
            armorStandMirrorLoc.add(getAnimationClipVector()).add(getAnimationVector());
        }
        armorStandMirrorLoc = getArmorStandLocationWithOffsets(
                armorStandMirrorLoc,
                ArmorStandItemOffset.GLASS_PANE,
                face.getDefaultRotation(getType())
        );
        armorStandMirror = createArmorStand(armorStandMirrorLoc, rotation, Item.getMirror(color));
        Areas.getInstance().addEntity(armorStandMirror.getUniqueId(), this);
    }

    @Override
    public final Vector getAnimationVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_LOCATION_DIFF);
    }

    @Override
    public final Vector getAnimationClipVector() {
        return face.getVector().multiply(MIRROR_ANIMATION_CLIP_LOCATION_DIFF);
    }

    /**
     * Rotates the mirror on this component
     */
    @Override
    @Deprecated
    public void rotate(RotationType rotationType) {
    }

    /**
     * Resets the component
     */
    @Override
    public void reset() {
        if (onGoingAnimation && getMirrorFromSphereTask != null) {
            getMirrorFromSphereTask.cancel();
        }
        if (onGoingAnimation && placeMirrorOnSphereTask != null) {
            placeMirrorOnSphereTask.cancel();
        }
        onGoingAnimation = false;
        color = hasSavedMirror() ? savedColor : LasersColor.BLACK;
        hasMirror = hasSavedMirror();
        updateDisplay();
    }

    /**
     * Changes the color of this component with/without saving it
     *
     * @param save is ignored since the color of ReflectingSpheres is defined by its mirror so it's never saved in database.
     */
    @Override
    public void changeColor(boolean save) {
        setColor(color.getNextColor(false), save);
    }

    /**
     * Changes the color of this component
     */
    @Override
    public void changeColor() {
        setColor(color.getNextColor(false));
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     */
    @Override
    public void setColor(LasersColor color) {
        setColor(color, false);
    }

    /**
     * Set the color of this component
     *
     * @param color The new color of this component
     * @param save  is this mirror color saved in database
     */
    @Override
    public void setColor(LasersColor color, boolean save) {
        if (color == LasersColor.BLACK) {
            throw new UnsupportedOperationException("Black color is not authorized for this component");
        }
        if (!hasMirror || onGoingAnimation) {
            return;
        }
        this.color = color;
        updateDisplay();
        if (save) {
            this.savedColor = color;
            dbUpdate();
        }
    }

    /**
     * gets the color of this component
     *
     * @return the color of this component
     */
    @Override
    public LasersColor getColor() {
        return color;
    }

    @Override
    public LasersColor getSavedColor() {
        return savedColor;
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandReflectingSphere = null;
        armorStandMirror = null;
    }

    @Override
    public boolean canBeDeleted() {
        return !onGoingAnimation;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        // Grace period (reprieve) during which the particle is potentially still in the component that made it appear.
        // In such cases, the particle is allowed a few iterations to leave the component.
        // Once this delay is over, the particle will be checked as any other particle.
        if (this.equals(laserParticle.getLastComponent())) {
            return new LaserReceptionResult(false);
        }
        // It has no mirror inside, it is an opaque component
        if (hasMirror() && !getOnGoingAnimation()) {
            LasersColor reflectedColor;
            if (color == LasersColor.WHITE) {
                reflectedColor = laserParticle.getColor();
            } else {
                reflectedColor = laserParticle.getColor().filterBy(color).get(LasersColor.FilterResult.REFLECTED);
            }
            if (reflectedColor != null) {
                ReflectionData inputRD = new ReflectionData(laserParticle.getDirection(), laserParticle.getLocation());
                //Using a reflection cache to avoid recomputing the same reflection two times
                ReflectionData outputRD = getReflectionCache().get(inputRD);
                if (outputRD == null) {
                    outputRD = reflect(this, inputRD);
                    getReflectionCache().put(inputRD, outputRD);
                }
                return new LaserReceptionResult(
                        true,
                        new LaserParticle(this, outputRD.getLocation(), outputRD.getDirection(), reflectedColor, getArea(), laserParticle.getLightLevel())
                );
            }
        }
        return new LaserReceptionResult(true);
    }

    public static ReflectionData reflect(ReflectingSphere rs, ReflectionData laserParticleData) {

        // http://www.codeproject.com/Articles/19799/Simple-Ray-Tracing-in-C-Part-II-Triangles-Intersec
        Location sphereCenter = rs.getASHeadCenterLocation();

        double cx = sphereCenter.getX();
        double cy = sphereCenter.getY();
        double cz = sphereCenter.getZ();

        double px = laserParticleData.getLocation().getX();
        double py = laserParticleData.getLocation().getY();
        double pz = laserParticleData.getLocation().getZ();

        double vx = laserParticleData.getDirection().getX();
        double vy = laserParticleData.getDirection().getY();
        double vz = laserParticleData.getDirection().getZ();

        double a = vx * vx + vy * vy + vz * vz;
        double b = 2.0 * (px * vx + py * vy + pz * vz - vx * cx - vy * cy - vz * cz);
        double c = px * px - 2 * px * cx + cx * cx + py * py - 2 * py * cy + cy * cy
                + pz * pz - 2 * pz * cz + cz * cz - SPHERE_RADIUS * SPHERE_RADIUS;

        // discriminant
        double d = b * b - 4 * a * c;

        if (d < 0) {
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation());
        }

        double t1 = (-b - Math.sqrt(d)) / (2.0 * a);

        Location solution1 = new Location(
                laserParticleData.getLocation().getWorld(),
                px * (1 - t1) + t1 * (px + vx),
                py * (1 - t1) + t1 * (py + vy),
                pz * (1 - t1) + t1 * (pz + vz));
        Location finalIntersection;
        if (d == 0) {
            finalIntersection = solution1;
        } else {

            double t2 = (-b + Math.sqrt(d)) / (2.0 * a);
            Location solution2 = new Location(
                    laserParticleData.getLocation().getWorld(),
                    px * (1 - t2) + t2 * (px + vx),
                    py * (1 - t2) + t2 * (py + vy),
                    pz * (1 - t2) + t2 * (pz + vz));

            // prefer a solution that's the first intersection between the laser and the sphere.
            Location prevParticleLoc = new Location(
                    laserParticleData.getLocation().getWorld(),
                    px - vx,
                    py - vy,
                    pz - vz
            );

            if (solution1.distance(prevParticleLoc) < solution2.distance(prevParticleLoc)) {
                finalIntersection = solution1;
            } else {
                finalIntersection = solution2;
            }

        }

        double normalVectorX = cx - finalIntersection.getX();
        double normalVectorY = cy - finalIntersection.getY();
        double normalVectorZ = cz - finalIntersection.getZ();
        Vector normalVector = new Vector(normalVectorX, normalVectorY, normalVectorZ).normalize();
        double dotProd = laserParticleData.getDirection().clone().normalize().dot(normalVector);
        if (dotProd == 0) {
            return new ReflectionData(new Direction(laserParticleData.getDirection().clone().multiply(-1)), laserParticleData.getLocation());
        }
        Direction resultDirection = new Direction(
                vx - (2 * dotProd * normalVectorX),
                vy - (2 * dotProd * normalVectorY),
                vz - (2 * dotProd * normalVectorZ));
        return new ReflectionData(resultDirection, finalIntersection);

    }

}
