package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.player.LEPlayer;
import eu.lasersenigma.player.LEPlayers;
import eu.lasersenigma.tasks.ParticleSpiralAnimationTask;
import eu.lasersenigma.tasks.VictorySoundTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

/**
 * A chest containing a key relative to a specific lock
 */
public class KeyChest extends AArmorStandComponent {

    /**
     * the corresponding lock
     */
    private final Lock lock;
    /**
     * The armorstand representing the key chest
     */
    private ArmorStand armorStandKeyChest = null;
    /**
     * the number of the key
     */
    private int keyNumber;

    /**
     * Constructor used for creation from database
     *
     * @param area        the area containing this mirror chest
     * @param componentId the id of the component inside the database
     * @param location    the location of this mirror chest
     * @param keyNumber   the number of the key
     * @param lock        the corresponding lock
     * @param face        the component facing
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public KeyChest(Area area, int componentId, Location location, int keyNumber, Lock lock, ComponentFace face) {
        super(area, componentId, location, ComponentType.KEY_CHEST, face, face.getDefaultRotation(ComponentType.KEY_CHEST));
        this.keyNumber = keyNumber;
        this.lock = lock;
        this.lock.onKeyChestCreated(this);
    }

    /**
     * Constructor
     *
     * @param area     the area containing this mirror chest
     * @param location the location of this mirror chest
     * @param lock     the corresponding lock
     * @param face     the component facing
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public KeyChest(Area area, Location location, Lock lock, ComponentFace face) {
        super(area, location, ComponentType.KEY_CHEST, face);
        this.keyNumber = 1;
        this.lock = lock;
        this.lock.onKeyChestCreated(this);
        dbCreate();
    }

    /**
     * gets the key number
     *
     * @return the key number
     */
    public int getKeyNumber() {
        return keyNumber;
    }

    /**
     * sets the key number
     *
     * @param keyNumber the key number
     */
    public void setKeyNumber(int keyNumber) {
        if (keyNumber > 25) {
            keyNumber = 25;
        } else if (keyNumber < 1) {
            keyNumber = 1;
        }
        this.keyNumber = keyNumber;
        this.lock.reset();
        dbUpdate();
        updateDisplay();
    }

    public Lock getLock() {
        return this.lock;
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandKeyChest != null) {
            Areas.getInstance().removeEntity(armorStandKeyChest.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandKeyChest.getUniqueId())) {
                    armorStandKeyChest = (ArmorStand) entity;
                }
            }
            armorStandKeyChest.remove();
            armorStandKeyChest = null;
        }
    }

    /**
     * Removes the component from the database
     */
    @Override
    public void remove() {
        lock.onKeyChestRemoved(this);
        LEPlayers.getInstance().removeKeyChestKeys(this);
        hide();
        dbRemove();
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandKeyChest = findArmorStandBack(armorStandKeyChest);
        if (armorStandKeyChest == null) {
            createKeyChestArmorStand();
        } else {
            armorStandKeyChest.setHelmet(ItemsFactory.getInstance().getItemStack(getHelmetItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    public Item getHelmetItem() {
        switch (keyNumber) {
            case 1:
                return Item.KEY_CHEST_1;
            case 2:
                return Item.KEY_CHEST_2;
            case 3:
                return Item.KEY_CHEST_3;
            case 4:
                return Item.KEY_CHEST_4;
            case 5:
                return Item.KEY_CHEST_5;
            case 6:
                return Item.KEY_CHEST_6;
            case 7:
                return Item.KEY_CHEST_7;
            case 8:
                return Item.KEY_CHEST_8;
            case 9:
                return Item.KEY_CHEST_9;
            case 10:
                return Item.KEY_CHEST_10;
            case 11:
                return Item.KEY_CHEST_11;
            case 12:
                return Item.KEY_CHEST_12;
            case 13:
                return Item.KEY_CHEST_13;
            case 14:
                return Item.KEY_CHEST_14;
            case 15:
                return Item.KEY_CHEST_15;
            case 16:
                return Item.KEY_CHEST_16;
            case 17:
                return Item.KEY_CHEST_17;
            case 18:
                return Item.KEY_CHEST_18;
            case 19:
                return Item.KEY_CHEST_19;
            case 20:
                return Item.KEY_CHEST_20;
            case 21:
                return Item.KEY_CHEST_21;
            case 22:
                return Item.KEY_CHEST_22;
            case 23:
                return Item.KEY_CHEST_23;
            case 24:
                return Item.KEY_CHEST_24;
            default:
                return Item.KEY_CHEST_25;
        }
    }

    /**
     * Creates the redstone sensor armorstand
     */
    private void createKeyChestArmorStand() {
        Location armorStandKeyChestLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandKeyChest = createArmorStand(armorStandKeyChestLoc, rotation, getHelmetItem());
        Areas.getInstance().addEntity(armorStandKeyChest.getUniqueId(), this);
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandKeyChest = null;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void startKeyObtainingAnimations(LEPlayer player) {
        new VictorySoundTask(getArea());
        new ParticleSpiralAnimationTask(player.getBukkitPlayer());
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
