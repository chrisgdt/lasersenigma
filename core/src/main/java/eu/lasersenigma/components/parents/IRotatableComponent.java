package eu.lasersenigma.components.parents;

import eu.lasersenigma.components.attributes.Rotation;
import eu.lasersenigma.components.attributes.RotationType;

public interface IRotatableComponent extends IComponent {

    public Rotation getRotation();

    public void rotate(RotationType rotationType, boolean save);

    public void rotate(RotationType rotationType, boolean save, boolean playSound);

}
