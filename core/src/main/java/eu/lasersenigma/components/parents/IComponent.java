package eu.lasersenigma.components.parents;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.attributes.SavedAction;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Location;

import javax.annotation.Nullable;
import java.util.ArrayList;

public interface IComponent {

    /**
     * gets the location of the component
     *
     * @return the location of the component
     */
    Location getLocation();

    /**
     * gets the type of the component
     *
     * @return the type of the component
     */
    ComponentType getType();

    /**
     * gets the area containing the component
     *
     * @return the area containing the component
     */
    Area getArea();

    /**
     * Hide the component (= removes visible elements)
     */
    void hide();

    /**
     * Show (or update the visible elements of) the component
     */
    void updateDisplay();

    /**
     * Resets the component (when the last player leaves the area)
     */
    void reset();

    /**
     * Removes the component
     */
    void remove();

    /**
     * Removes the component from the database
     */
    void dbRemove();

    /**
     * Creates the component into the database
     */
    void dbCreate();

    /**
     * Updates the component in the database
     */
    void dbUpdate();

    /**
     * Retrieves the component id
     *
     * @return int the component id
     */
    int getComponentId();

    /**
     * To know if the component have been removed
     *
     * @return true if the component still exists, false otherwise
     */
    boolean isRemoved();

    /**
     * Can this component currently be deleted
     *
     * @return true if the component can  be deleted now
     */
    boolean canBeDeleted();

    /**
     * Retrieves the scheduled actions which will be executed in a loop on this component
     *
     * @return the scheduled actions which will be executed in a loop on this component
     */
    ArrayList<SavedAction> getScheduledActions();

    /**
     * Notify the system that this component scheduled actions has been modified.
     */
    void updateActions();

    void startScheduledActions();

    void stopScheduledActions();

    /**
     * Receives a laser particle
     *
     * @param laserParticle the received laser particle
     * @return the result of the particle processing.
     */
     LaserReceptionResult receiveLaser(LaserParticle laserParticle);
}
