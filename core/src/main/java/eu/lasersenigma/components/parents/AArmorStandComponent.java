package eu.lasersenigma.components.parents;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Used for components that can be rotated
 */
public abstract class AArmorStandComponent extends AComponent {

    public static final String ARMORSTAND_CUSTOM_NAME = "lasersenigma-as";
    /**
     * The face of the component
     */
    protected final ComponentFace face;
    /**
     * The rotation of the head (vertically and horizontally)
     */
    protected Rotation rotation;

    /**
     * Constructor
     *
     * @param area        the area containing the component
     * @param componentId the id of the component
     * @param location    the location of the component
     * @param type        the type of the component
     * @param face        the direction the component is facing
     *                    (up/down/north/south/east/west)
     * @param rotation    the current rotation of the component
     */
    public AArmorStandComponent(Area area, int componentId, Location location, ComponentType type, ComponentFace face, Rotation rotation) {
        super(area, componentId, location, type);
        this.face = face;
        this.rotation = rotation;
    }

    /**
     * Constructor
     *
     * @param area     the area containing the component
     * @param location the location of the component
     * @param type     the type of the component
     * @param face     the direction the component is facing
     *                 (up/down/north/south/east/west)
     */
    public AArmorStandComponent(Area area, Location location, ComponentType type, ComponentFace face) {
        super(area, location, type);
        this.face = face;
        this.rotation = face.getDefaultRotation(type);
    }

    public static Location getDefaultArmorStandBaseLocation(Location location, ComponentFace face) {
        return getDefaultArmorStandBaseLocation(location).add(face.getVector().multiply(-0.16));
    }

    public static Location getDefaultArmorStandBaseLocation(Location location) {
        return location.clone().add(0.5, -1, 0.5);
    }

    final public Location getASHeadCenterLocation() {
        return getLocation().clone().add(0.5, 0.5, 0.5).add(face.getVector().multiply(-0.19));
    }

    /**
     * Gets the direction the component is facing
     * (up/down/north/south/east/west)
     *
     * @return the direction the component is facing
     * (up/down/north/south/east/west)
     */
    public final ComponentFace getFace() {
        return face;
    }

    /**
     * Gets the rotation of the component
     *
     * @return the rotation of the component
     */
    public final Rotation getRotation() {
        return this.rotation;
    }

    /**
     * Retrieve the saved rotation of this component
     * @return the saved rotation
     */
    public Rotation getSavedRotation() {
        return getRotation();

    }

    public void rotate(RotationType rotationType, boolean save) {
        rotate(rotationType);
    }

    /**
     * Rotates the component
     *
     * @param rotationType the type of the rotation (vertical/horizontal)
     */
    protected void rotate(RotationType rotationType) {
        if (canRotate()) {
            rotation = rotation.getNextRotation(rotationType);
            updateDisplay();
        }
    }

    protected ArmorStand findArmorStandBack(ArmorStand as) {
        if (as != null) {
            if (as.isDead()) {
                as = null;
            } else {
                boolean found = false;
                Chunk asChunk = as.getLocation().getChunk();
                Chunk componentChunk = getLocation().getChunk();
                Entity[] entities = asChunk.getEntities();
                HashSet<Entity> entitiesSet = new HashSet<>(Arrays.asList(entities));
                if (asChunk != componentChunk) {
                    entitiesSet.addAll(Arrays.asList(componentChunk.getEntities()));
                }
                for (Entity entity : entitiesSet) {
                    if (entity.getUniqueId().equals(as.getUniqueId())) {
                        as = (ArmorStand) entity;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    as = null;
                }
            }
        }
        return as;
    }

    /**
     * Creates and initialize an armorStand
     *
     * @param location the location of the created ArmorStand
     * @param rotation the rotation of the created ArmorStand
     * @param item     the item used as head for the created ArmorStand
     * @return the ArmorStand
     */
    protected ArmorStand createArmorStand(Location location, Rotation rotation, Item item) {
        ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        armorStand.setHeadPose(rotation);
        armorStand.setVisible(false);
        armorStand.setHelmet(ItemsFactory.getInstance().getItemStack(item));
        armorStand.setGravity(false);
        armorStand.setCustomNameVisible(false);
        armorStand.setCustomName(ARMORSTAND_CUSTOM_NAME);
        armorStand.setAI(false);
        return armorStand;
    }

    /**
     * checks if the component can be rotated
     *
     * @return true if the component can be rotated
     */
    public boolean canRotate() {
        return true;
    }

    /**
     * adds an offset on a location corresponding to its rotation (used for
     * armorstand with decentered head block)
     *
     * @param baseLocation The origin location of the armorstand
     * @param offset       the offset between the item on the armorstand head and the
     *                     armorstand head center.
     * @param rot          the current rotation of the armorstand
     * @return the new armorstand location where the item is well centered on
     * the baseLocation
     */
    public Location getArmorStandLocationWithOffsets(Location baseLocation, ArmorStandItemOffset offset, Rotation rot) {
        double x = baseLocation.getX();
        double y = baseLocation.getY();
        double z = baseLocation.getZ();

        //Calculate radial form or yaw and pitch
        //head item offset
        double offsetValue = offset.getOffsetItemToHead();
        if (offsetValue > 0) {
            Vector eye = rot.toEyeDirection().multiply(offsetValue);
            x += eye.getX();
            y += eye.getY();
            z += eye.getZ();
        }

        //head rotation offset
        Vector head = rot.toHeadDirection().multiply(offset.getOffsetNeckToHead());
        x -= head.getX();
        y -= head.getY();
        z -= head.getZ();

        //normalization
        x = (double) (Math.round(x * 100000d) / 100000d);
        y = (double) (Math.round(y * 100000d) / 100000d);
        z = (double) (Math.round(z * 100000d) / 100000d);
        return new Location(baseLocation.getWorld(), x, y, z, baseLocation.getYaw(), baseLocation.getPitch());
    }

    /**
     * Set all ArmorStand entities to null
     */
    public abstract void clearEntitiesAttribute();
}
