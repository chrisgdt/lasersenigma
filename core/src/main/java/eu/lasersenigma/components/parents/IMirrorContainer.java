package eu.lasersenigma.components.parents;

import eu.lasersenigma.components.attributes.ArmorStandItemOffset;
import eu.lasersenigma.components.attributes.ComponentFace;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.attributes.Rotation;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;

public interface IMirrorContainer extends IColorableComponent {

    public boolean getMirror(boolean save);

    public boolean placeMirror(LasersColor color, boolean save);

    public boolean hasMirror();

    public void setHasMirror(boolean hasMirror, boolean save);

    public boolean hasSavedMirror();

    public Vector getAnimationClipVector();

    public Vector getAnimationVector();

    public ComponentFace getFace();

    public boolean getOnGoingAnimation();

    public void setOnGoingAnimation(boolean onGoingAnimation);

    public Location getArmorStandLocationWithOffsets(Location baseLocation, ArmorStandItemOffset offset, Rotation rot);

    public ArmorStand getArmorStandMirror();

    public ArmorStand getArmorStandMirrorContainer();

}
