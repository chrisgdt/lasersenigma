package eu.lasersenigma.components.parents;

import eu.lasersenigma.components.attributes.LasersColor;

/**
 * For the components that can be colored
 */
public interface IColorableComponent extends IComponent {

    /**
     * Changes the current color of the component
     */
    public void changeColor();

    public void changeColor(boolean save);

    public void setColor(LasersColor color, boolean save);

    /**
     * gets the current color of the component
     *
     * @return the current color of the component
     */
    public LasersColor getColor();

    public void setColor(LasersColor color);

    public LasersColor getSavedColor();

}
