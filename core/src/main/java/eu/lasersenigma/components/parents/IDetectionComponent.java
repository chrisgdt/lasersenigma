package eu.lasersenigma.components.parents;

import eu.lasersenigma.components.attributes.DetectionMode;

/**
 * Used for components that reacts to the number of laser receiver activated
 */
public interface IDetectionComponent extends IComponent {

    /**
     * gets the maximum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     *
     * @return the maximum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     */
    public int getMax();

    /**
     * sets the maximum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     *
     * @param max the maximum number of [activated laser receivers | players
     *            inside the area] needed to activate this component
     */
    public void setMax(int max);

    /**
     * gets the minimum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     *
     * @return the minimum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     */
    public int getMin();

    /**
     * sets the minimum number of [activated laser receivers | players inside
     * the area] needed to activate this component
     *
     * @param min the minimum number of [activated laser receivers | players
     *            inside the area] needed to activate this component
     */
    public void setMin(int min);

    /**
     * sets the actual number of [activated laser receivers | players inside the
     * area]
     *
     * @param nbActivated the actual number of [activated laser receivers |
     *                    players inside the area]
     */
    public void setNbActivated(int nbActivated);

    /**
     * changes the detection mode
     *
     * @param mode the new detection mode
     */
    public void changeMode(DetectionMode mode);

    /**
     * gets the detection mode
     *
     * @return the actual detection mode
     */
    public DetectionMode getMode();

    /**
     * check if the component is currently activated or not
     *
     * @return true if the component is activated, false otherwise
     */
    public boolean isActivated();
}
