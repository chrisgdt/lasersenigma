package eu.lasersenigma.components.parents;

import org.bukkit.Location;

public interface IAreaComponent extends IComponent {

    public Location getMinLocation();

    public Location getMaxLocation();
}
