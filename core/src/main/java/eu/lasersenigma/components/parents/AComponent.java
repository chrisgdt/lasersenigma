package eu.lasersenigma.components.parents;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.attributes.ActionType;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LaserReceptionResult;
import eu.lasersenigma.components.attributes.SavedAction;
import eu.lasersenigma.particles.LaserParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Level;

/**
 * Parent abstract class for all components
 */
public abstract class AComponent implements IComponent {

    /**
     * The location of the component
     */
    private final Location location;

    /**
     * The type of the component
     */
    private final ComponentType type;

    /**
     * The area containing this component
     */
    private final Area area;

    /**
     * The id of the component
     */
    protected int componentId;

    /**
     * The scheduled actions which will be executed in a loop
     */
    protected ArrayList<SavedAction> scheduledActions;

    /**
     * The id of the task handling the next scheduledAction;
     */
    protected Integer scheduledActionsTaskId;

    private Integer dbUpdateComponentScheduledActionsTaskId;

    /**
     * Constructor
     *
     * @param area     The area containing this component
     * @param location The location of the component
     * @param type     The type of the component
     */
    public AComponent(Area area, Location location, ComponentType type) {
        this.location = location;
        this.type = type;
        this.area = area;
        this.scheduledActions = new ArrayList<>();
        this.scheduledActionsTaskId = null;
    }

    /**
     * Constructor used for creation from database
     *
     * @param area        The area containing this component
     * @param componentId the id of the component
     * @param location    The location of the component
     * @param type        The type of the component
     */
    public AComponent(Area area, int componentId, Location location, ComponentType type) {
        this(area, location, type);
        this.componentId = componentId;
    }

    @Override
    public final int getComponentId() {
        return componentId;
    }

    /**
     * gets the location of the component
     *
     * @return the location of the component
     */
    @Override
    public final Location getLocation() {
        return location.clone();
    }

    /**
     * gets the type of the component
     *
     * @return the type of the component
     */
    @Override
    public final ComponentType getType() {
        return type;
    }

    /**
     * gets the area containing this component
     *
     * @return the area containing this component
     */
    @Override
    public final Area getArea() {
        return area;
    }

    /**
     * Deletes the visible elements corresponding to this component
     */
    @Override
    public void hide() {
        Block b = location.getBlock();
        b.setType(Material.AIR);
        b.getState().update();
    }

    /**
     * Resets the component (when the last player leaves the area)
     */
    @Override
    public void reset() {
    }

    /**
     * Removes a component from the database
     */
    @Override
    public final void dbRemove() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removeComponent(this);
    }

    /**
     * Updates the component into the database
     */
    @Override
    public final void dbUpdate() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().updateComponent(this);
    }

    /**
     * Creates the component into the database
     */
    @Override
    public final void dbCreate() {
        this.componentId = LasersEnigmaPlugin.getInstance().getPluginDatabase().createComponent(this);
    }

    /**
     * Removes the component from the database
     */
    @Override
    public void remove() {
        hide();
        dbRemove();
    }

    @Override
    public final boolean isRemoved() {
        return getArea().getComponent(this.getComponentId()) != this;
    }

    @Override
    public boolean canBeDeleted() {
        return true;
    }

    @Override
    public ArrayList<SavedAction> getScheduledActions() {
        return scheduledActions;
    }

    @Override
    public void startScheduledActions() {
        if (scheduledActionsTaskId != null) {
            Bukkit.getScheduler().cancelTask(scheduledActionsTaskId);
        } else if (area.isActivated() && !scheduledActions.isEmpty()) {
            scheduledActionsTaskId = Bukkit.getScheduler().runTask(LasersEnigmaPlugin.getInstance(), () -> {
                callAction(0);
            }).getTaskId();
        }
    }

    @Override
    public void stopScheduledActions() {
        if (scheduledActionsTaskId != null) {
            Bukkit.getScheduler().cancelTask(scheduledActionsTaskId);
            scheduledActionsTaskId = null;
        }
    }

    @Override
    public void updateActions() {
        startScheduledActions();
        if (dbUpdateComponentScheduledActionsTaskId != null) {
            Bukkit.getScheduler().cancelTask(dbUpdateComponentScheduledActionsTaskId);
        }
        //The saving is done asynchronously 10 seconds later. This avoid constant saving when modifying scheduled tasks quickly.
        dbUpdateComponentScheduledActionsTaskId = Bukkit.getScheduler().runTaskLaterAsynchronously(LasersEnigmaPlugin.getInstance(), () -> {
            dbUpdateComponentScheduledActionsTaskId = null;
            LasersEnigmaPlugin.getInstance().getPluginDatabase().updateComponentScheduledActions(this);
        }, 200).getTaskId();
    }

    private void callAction(int i) {
        if (scheduledActions.isEmpty() || (!area.isActivated())) {
            scheduledActionsTaskId = null;
            return;
        }
        int delay = scheduledActions.stream().filter(action -> action.getType() == ActionType.WAIT).map(action -> action.getDelay()).reduce(Integer::sum).orElse(0);
        if (delay < 10) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.WARNING, "Scheduled actions disallowed whithout at least 10 ticks delay");
            scheduledActionsTaskId = null;
            return;
        }
        if (i >= scheduledActions.size()) {
            i = 0;
        }
        SavedAction action = scheduledActions.get(i);
        int newIndex = i + 1;

        if (action.getType() == ActionType.WAIT) {
            scheduledActionsTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                this.callAction(newIndex);
            }, action.getDelay()).getTaskId();
        } else {
            if (action.getType().isDoableNow(this)) {
                action.doAction();
            }
            scheduledActionsTaskId = Bukkit.getScheduler().runTask(LasersEnigmaPlugin.getInstance(), () -> {
                callAction(newIndex);
            }).getTaskId();
        }
    }
}
