package eu.lasersenigma.components.parents;

/**
 * Used for components that have a taskTimer
 */
public interface ITaskComponent {

    /**
     * Starts the taskTimer
     */
    public void startTask();

    /**
     * Cancels the taskTimer
     */
    public void cancelTask();
}
