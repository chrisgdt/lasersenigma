package eu.lasersenigma.components;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.attributes.*;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IDetectionComponent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.particles.LaserParticle;
import eu.lasersenigma.songs.PlayersListSongManager;
import eu.lasersenigma.songs.SongsListManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Music block component
 */
public final class MusicBlock extends AArmorStandComponent implements IDetectionComponent {

    private static final SongsListManager SONG_LIST_MANAGER = SongsListManager.getInstance();
    private static final PlayersListSongManager PLAYERS_LIST_SONG_MANAGER = PlayersListSongManager.getInstance();

    /**
     * The armorstand representing the music block
     */
    private ArmorStand armorStandMusicBlock = null;

    private boolean loop;
    private boolean stopOnExit;
    private String songFileName;

    /**
     * the minimum number of [activated laser receivers | players inside the
     * area] needed to activate this component
     */
    private int min;

    /**
     * the maximum number of [activated laser receivers | players inside the
     * area] needed to activate this component
     */
    private int max;

    /**
     * The actual number of (laser receivers activated | players inside the
     * area)
     */
    private int nbActivated;

    /**
     * The detection mode of the component ( number of activated laser receivers
     * or number of players inside the area)
     */
    private DetectionMode mode;

    /**
     * Constructor used for creation from the database
     *
     * @param area         the area containing this laser receiver
     * @param componentId  the id of the component inside the database
     * @param location     the location of the laser receiver
     * @param face         the blockface of the component
     * @param rotation     the rotation of the component
     * @param min          the minimun number of activated laser receivers | players
     *                     needed for this block to be activated
     * @param max          the maximum number of activated laser receivers | players
     *                     needed for this block to be activated
     * @param songFileName the name of the song file.
     * @param loop         if the loop mode is activated or not
     * @param stopOnExit   if the stop on exit mode is activated or not
     * @param mode         the detection mode
     */
    public MusicBlock(Area area, int componentId, Location location, ComponentFace face, Rotation rotation, int min, int max, String songFileName, boolean loop, boolean stopOnExit, DetectionMode mode) {
        super(area, componentId, location, ComponentType.MUSIC_BLOCK, face, rotation);
        this.min = min;
        this.max = max;
        this.songFileName = songFileName;
        this.loop = loop;
        this.stopOnExit = stopOnExit;
        this.nbActivated = -1;
        this.mode = mode;
    }

    /**
     * Constructor
     *
     * @param area     the area containing this
     * @param location the location of the component
     * @param face     the blockface of the component
     */
    public MusicBlock(Area area, Location location, ComponentFace face) {
        super(area, location, ComponentType.MUSIC_BLOCK, face);
        this.min = 1;
        this.max = 10;
        this.songFileName = SONG_LIST_MANAGER.getSongFileName(0);
        this.loop = false;
        this.stopOnExit = true;
        this.nbActivated = -1;
        this.mode = DetectionMode.DETECTION_LASER_RECEIVERS;
        dbCreate();
    }

    @Override
    public int getMin() {
        return min;
    }

    @Override
    public void setMin(int min) {
        this.min = min;
        dbUpdate();
    }

    @Override
    public int getMax() {
        return max;
    }

    @Override
    public void setMax(int max) {
        this.max = max;
        dbUpdate();
    }

    @Override
    public void changeMode(DetectionMode mode) {
        if (mode.isSpecificToLaserSender() || mode.isSpecificToAreas()) {
            throw new UnsupportedOperationException();
        }
        this.mode = mode;
        dbUpdate();
        updateDisplay();
    }

    @Override
    public DetectionMode getMode() {
        return mode;
    }

    public String getSongFileName() {
        return songFileName;
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
        updateDisplay();
        startSong();
        dbUpdate();
    }

    public boolean isStopOnExit() {
        return stopOnExit;
    }

    public void setStopOnExit(boolean stopOnExit) {
        this.stopOnExit = stopOnExit;
        updateDisplay();
        startSong();
        dbUpdate();
    }

    public void nextSong(Player player) {
        if (!SONG_LIST_MANAGER.isActivated()) {
            return;
        }
        int index = SONG_LIST_MANAGER.getIndex(songFileName);
        int newIndex = SONG_LIST_MANAGER.getNextIndex(index);
        songFileName = SONG_LIST_MANAGER.getSongFileName(newIndex);
        SONG_LIST_MANAGER.showScoreboard(newIndex, player);
        startSong();
        dbUpdate();
    }

    public void prevSong(Player player) {
        if (!SONG_LIST_MANAGER.isActivated()) {
            return;
        }
        int index = SONG_LIST_MANAGER.getIndex(songFileName);
        int newIndex = SONG_LIST_MANAGER.getPrevIndex(index);
        songFileName = SONG_LIST_MANAGER.getSongFileName(newIndex);
        SONG_LIST_MANAGER.showScoreboard(newIndex, player);
        startSong();
        dbUpdate();
    }

    /**
     * Deletes the component
     */
    @Override
    public void hide() {
        super.hide();
        if (armorStandMusicBlock != null) {
            Areas.getInstance().removeEntity(armorStandMusicBlock.getUniqueId());
            for (Entity entity : getLocation().getChunk().getEntities()) {
                if (entity.getUniqueId().equals(armorStandMusicBlock.getUniqueId())) {
                    armorStandMusicBlock = (ArmorStand) entity;
                }
            }
            armorStandMusicBlock.remove();
            armorStandMusicBlock = null;
        }
        getArea().getPlayers()
                .stream()
                .forEach(p -> PLAYERS_LIST_SONG_MANAGER.fadeOut(p, 50));
    }

    /**
     * updates this component display
     */
    @Override
    public void updateDisplay() {
        armorStandMusicBlock = findArmorStandBack(armorStandMusicBlock);
        if (armorStandMusicBlock == null) {
            createMusicBlockArmorStand();
        } else {

            armorStandMusicBlock.setHelmet(ItemsFactory.getInstance().getItemStack(getArmorStandItem()));
        }
        Block b = getLocation().getBlock();
        if (b.getType() != Material.BARRIER) {
            b.setType(Material.BARRIER);
            b.getState().update();
        }
    }

    /**
     * Creates the armorstand
     */
    private void createMusicBlockArmorStand() {
        Location armorStandPrismLoc = getArmorStandLocationWithOffsets(
                getDefaultArmorStandBaseLocation(getLocation(), face),
                ArmorStandItemOffset.HEAD,
                rotation
        );
        armorStandMusicBlock = createArmorStand(armorStandPrismLoc, rotation, getArmorStandItem());
        Areas.getInstance().addEntity(armorStandMusicBlock.getUniqueId(), this);
    }

    /**
     * Gets the correct head item
     *
     * @return the head item
     */
    private Item getArmorStandItem() {
        if (loop && !stopOnExit) {
            return Item.MUSIC_BLOCK_LOOP_N_KEEP_PLAYING_ON_EXIT;
        }
        if (loop) {
            return Item.MUSIC_BLOCK_LOOP;
        }
        if (!stopOnExit) {
            return Item.MUSIC_BLOCK_KEEP_PLAYING_ON_EXIT;
        }
        return Item.MUSIC_BLOCK;
    }

    /**
     * Rotates the component (disabled)
     */
    @Deprecated
    @Override
    public void rotate(RotationType rotationType) {
    }

    @Override
    public boolean isActivated() {
        return (this.min <= this.nbActivated && this.nbActivated <= this.max);
    }

    /**
     * sets the number of laser receivers currently activated
     *
     * @param nbActivated the number of laser receivers currently activated
     */
    @Override
    public void setNbActivated(int nbActivated) {
        if (nbActivated != this.nbActivated) {
            this.nbActivated = nbActivated;
            if (isActivated()) {
                startSong();
            }
        }
    }

    public void onPlayerEnteredArea(Player p) {
        if (isActivated()) {
            startSong(p);
        }
    }

    /**
     * Resets this component
     */
    @Override
    public void reset() {
        nbActivated = 0;
    }

    private void startSong(Player p, boolean loop, boolean stopOnExit) {
        PLAYERS_LIST_SONG_MANAGER.startSong(p, songFileName, loop, stopOnExit);
    }

    private void startSong(Player p) {
        startSong(p, loop, stopOnExit);
    }

    private void startSong(boolean loop, boolean stopOnExit) {
        getArea().getPlayers()
                .stream()
                .forEach(p -> startSong(p, loop, stopOnExit));
    }

    private void startSong() {
        getArea().getPlayers()
                .stream()
                .forEach(p -> startSong(p));
    }

    @Override
    public void clearEntitiesAttribute() {
        armorStandMusicBlock = null;
    }

    @Override
    public LaserReceptionResult receiveLaser(LaserParticle laserParticle) {
        return new LaserReceptionResult(true);
    }
}
