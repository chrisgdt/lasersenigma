package eu.lasersenigma.player;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.MirrorSupport;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.attributes.MirrorSupportMode;
import eu.lasersenigma.components.parents.IColorableComponent;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IPlayerModifiableComponent;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.exceptions.NoAreaFoundException;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import eu.lasersenigma.items.inventory.armors.LEArmorActionMenuInventory;
import eu.lasersenigma.items.inventory.armors.LEArmorOptionsMenuInventory;
import eu.lasersenigma.items.inventory.component.*;
import eu.lasersenigma.items.inventory.parents.ALEInventory;
import eu.lasersenigma.items.inventory.parents.ALEOpenableInventory;
import eu.lasersenigma.items.inventory.parents.ALEShortcutBarInventory;
import eu.lasersenigma.items.inventory.parents.LEInventoryType;
import eu.lasersenigma.items.inventory.player.LEPlayerModifyingComponentShortcutBarInventory;
import eu.lasersenigma.items.inventory.saving.PlayerInventorySaveManager;
import eu.lasersenigma.nms.NMSManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.Cancellable;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.logging.Level;

public class PlayerInventoryManager {

    public static boolean sendComponentBlockedMsg = LasersEnigmaPlugin.getInstance().getConfig().getBoolean("component_blocked_message", true);

    private final LEPlayer player;

    private final PlayerInventorySaveManager playerInventorySaveManager;
    private final boolean isInClearedInventoryPlayMode;
    private final boolean isInArea;
    private ALEOpenableInventory currentLEOpenedInventory;
    private ALEShortcutBarInventory currentLEShortcutBarInventory;
    private ComponentType clickedComponentType;
    private Block clickedBlock;
    private BlockFace clickedBlockFace;
    private Material clickedMaterial;
    private LEInventoryType colorSelectionContext;
    private IComponent component;
    private Integer antispamTaskId;
    private boolean isRotationShortcutBarOpened;
    private boolean isInEditionMode;
    private ArmorAction selectedArmorAction;

    public PlayerInventoryManager(LEPlayer player) {
        this.player = player;
        isRotationShortcutBarOpened = false;
        isInEditionMode = false;
        isInClearedInventoryPlayMode = false;
        isInArea = false;
        playerInventorySaveManager = new PlayerInventorySaveManager(player);
    }

    public boolean isInArea() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.isInArea:{0}", isInArea);
        return isInArea;
    }

    public boolean isRotationShortcutBarOpened() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.isRotationShortcutbarOpened:{0}", isRotationShortcutBarOpened);
        return isRotationShortcutBarOpened;
    }

    public boolean isInClearedInventoryPlayMode() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.isInClearedInventoryPlayMode:{0}", isInClearedInventoryPlayMode);
        return isInClearedInventoryPlayMode;
    }

    public boolean isInEditionMode() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.isInEditionMode:{0}", isInEditionMode);
        return isInEditionMode;
    }

    public PlayerInventorySaveManager getInventorySaveManager() {
        return this.playerInventorySaveManager;
    }

    public void onRightClick(Item item, IComponent component) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick(Item,IComponent)");
        if (isRotationShortcutBarOpened) {
            if (currentLEShortcutBarInventory instanceof LEPlayerModifyingComponentShortcutBarInventory) {
                IComponent rotatableComponent = ((LEPlayerModifyingComponentShortcutBarInventory) currentLEShortcutBarInventory).getComponent();
                double distance = this.player.getBukkitPlayer().getLocation().distance(rotatableComponent.getLocation());
                if (rotatableComponent != component || distance > 5) {
                    closeRotationShortcutBarInventory();
                    return;
                }
                currentLEShortcutBarInventory.onClick(item);
            }
            return;
        }

        if (currentLEShortcutBarInventory instanceof LEComponentShortcutBarInventory) {
            LEComponentShortcutBarInventory currentLEComponentShortcutBarInventory = (LEComponentShortcutBarInventory) currentLEShortcutBarInventory;
            if (currentLEComponentShortcutBarInventory.getComponent() != component) {
                openLEInventory(new LEComponentShortcutBarInventory(player, component));
                return;
            }
        } else {
            openLEInventory(new LEComponentShortcutBarInventory(player, component));
        }
        if (item != null) {
            onRightClick(item);
        }
    }

    public LEPlayer getPlayer() {
        return this.player;
    }

    public boolean onRightClick(Item item) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick(Item)");
        boolean shouldEventBeCancelled = false;
        final boolean isItemInCurrentLEOpenedInventory = (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item));
        final boolean isItemInCurrentLEShortcutBarInventory = (!isItemInCurrentLEOpenedInventory && currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item));
        if (isItemInCurrentLEOpenedInventory || isItemInCurrentLEShortcutBarInventory) {
            shouldEventBeCancelled = true;
        }
        if (antispamTaskId == null) {
            antispamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
                antispamTaskId = null;
                if (item != null) {
                    if (isItemInCurrentLEOpenedInventory) {
                        currentLEOpenedInventory.onClick(item);
                    } else if (isItemInCurrentLEShortcutBarInventory) {
                        currentLEShortcutBarInventory.onClick(item);
                    }
                }
            }, 1).getTaskId();
        }
        return shouldEventBeCancelled;
    }

    public void onRightClick(IComponent component) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick(IComponent)");
        if (antispamTaskId != null) {
            return;
        }
        antispamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antispamTaskId = null;
            onRightClick(player.getCurrentItem(), component);
        }, 1).getTaskId();
    }

    public boolean onRightClick() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onRightClick()");
        return onRightClick(player.getCurrentItem());
    }

    public void onInventoryClick(ItemStack itemStack) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onInventoryClick");
        if (antispamTaskId != null) {
            return;
        }
        antispamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antispamTaskId = null;
            if (currentLEOpenedInventory == null && currentLEShortcutBarInventory == null) {
                return;
            }
            Item item = ItemsFactory.getInstance().getSimilarItem(itemStack);
            if (item == null) {
                if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(itemStack)) {
                    currentLEOpenedInventory.onClick(itemStack);
                }
                return;
            }
            if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item)) {
                currentLEOpenedInventory.onClick(item);
            } else if (currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item)) {
                currentLEShortcutBarInventory.onClick(item);
            }
        }, 1).getTaskId();
    }

    public void onInventoryDrag(InventoryDragEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onInventoryDrag");
        if (currentLEOpenedInventory == null && currentLEShortcutBarInventory == null) {
            return;
        }
        Item item = ItemsFactory.getInstance().getSimilarItem(e.getOldCursor());
        if (item == null) {
            return;
        }
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item)) {
            e.setCancelled(true);
        }
        if (currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item)) {
            e.setCancelled(true);
        }
    }

    public void onPlayerDropItem(ItemStack itemStack, Cancellable event) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onPlayerDropItem");
        if (currentLEOpenedInventory == null && currentLEShortcutBarInventory == null) {
            return;
        }
        Item item = ItemsFactory.getInstance().getSimilarItem(itemStack);
        if (item == null) {
            return;
        }
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.contains(item) && currentLEOpenedInventory.isDropable(item)) {
            return;
        }
        if (currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.contains(item) && currentLEShortcutBarInventory.isDropable(item)) {
            return;
        }
        event.setCancelled(true);
    }

    public void onArmorMenu() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onArmourMenu");
        List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(LEPlayer.TRANSPARENT_MATERIALS, 20);
        if (blocks.size() < 2) {
            return;
        }
        clickedBlock = blocks.get(1);
        clickedBlockFace = clickedBlock.getFace(blocks.get(0));
        openLEInventory(new LEArmorActionMenuInventory(player));
    }

    public void onSelectedArmor(ArmorAction armorAction) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onSelectedArmor");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);

        this.selectedArmorAction = armorAction;

        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> openLEInventory(new LEArmorOptionsMenuInventory(player)), 2);
    }

    public void onArmorMenuCompleted(boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onArmourMenuCompleted");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);

        Location newMaterialLocation = clickedBlock.getLocation().clone().add(clickedBlockFace.getModX(), clickedBlockFace.getModY(), clickedBlockFace.getModZ());
        clickedBlock = null;
        clickedBlockFace = null;

        ArmorAction action = selectedArmorAction;
        selectedArmorAction = null;

        PlayerController.armorCreation(this.getPlayer(), action, newMaterialLocation, noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber);

    }

    public void onPlaceComponent() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onPlaceComponent");
        Area area = Areas.getInstance().getAreaFromLocation(player.getBukkitPlayer().getLocation());
        if (area == null || !area.isActivated()) {
            ErrorsConfig.showError(player.getBukkitPlayer(), new NoAreaFoundException());
            return;
        }
        List<Block> blocks = player.getBukkitPlayer().getLastTwoTargetBlocks(LEPlayer.TRANSPARENT_MATERIALS, 20);
        if (blocks.size() < 2) {
            return;
        }
        clickedBlock = blocks.get(1);
        clickedBlockFace = clickedBlock.getFace(blocks.get(0));
        openLEInventory(new LEComponentSelectorInventory(player, area));
    }

    public void onChangeComponentColor(IComponent component) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onChangeComponentColor");
        colorSelectionContext = LEInventoryType.COMPONENT_MENU;
        this.component = component;
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            switch (component.getType()) {
                case FILTERING_SPHERE:
                case MELTABLE_CLAY:
                case LASER_RECEIVER:
                case LASER_SENDER:
                case REFLECTING_SPHERE:
                    colorSelectionContext = LEInventoryType.COMPONENT_MENU;
                    openLEInventory(new LEColorSelectorInventory(player, Message.SELECT_COMPONENT_COLOR, false, component, component.getArea()));
                    break;
                case MIRROR_SUPPORT:
                case MIRROR_CHEST:
                    colorSelectionContext = LEInventoryType.COMPONENT_MENU;
                    openLEInventory(new LEColorSelectorInventory(player, Message.SELECT_COMPONENT_COLOR, true, component, component.getArea()));
                    break;
                default:
                    throw new UnsupportedOperationException("Color selection has no context.");
            }
        }, 2);
    }

    public void onComponentSelected(ComponentType componentType) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onComponentSelected(ComponentType)");
        clickedComponentType = componentType;
        clickedMaterial = null;
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            switch (componentType) {
                case FILTERING_SPHERE:
                case MELTABLE_CLAY:
                case LASER_RECEIVER:
                case LASER_SENDER:
                case REFLECTING_SPHERE:
                    colorSelectionContext = LEInventoryType.NEW_COMPONENT_MENU;
                    openLEInventory(new LEColorSelectorInventory(player, Message.SELECT_COMPONENT_COLOR, false, null, Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
                    break;
                case MIRROR_SUPPORT:
                case MIRROR_CHEST:
                    colorSelectionContext = LEInventoryType.NEW_COMPONENT_MENU;
                    openLEInventory(new LEColorSelectorInventory(player, Message.SELECT_COMPONENT_COLOR, true, null, Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
                    break;
                case ELEVATOR:
                    Location elevatorFirstLocation = clickedBlock.getLocation();
                    Area area = Areas.getInstance().getAreaFromLocation(elevatorFirstLocation);
                    if (area == null) {
                        ErrorsConfig.showError(getPlayer().getBukkitPlayer(), new NoAreaFoundException());
                    } else {
                        openLEInventory(new LEElevatorSecondLocationShortcutBarInventory(player, area, elevatorFirstLocation));
                    }
                    clickedComponentType = null;
                    clickedMaterial = null;
                    clickedBlock = null;
                    clickedBlockFace = null;
                    break;
                default:
                    IComponent newComponent = ComponentController.addComponentFromBlockFace(componentType, clickedBlock, clickedBlockFace, player);
                    if (newComponent != null) {
                        this.component = newComponent;
                        openLEInventory(new LEComponentShortcutBarInventory(player, newComponent));
                    }
                    clickedBlock = null;
                    clickedBlockFace = null;
                    break;
            }
        }, 2);
    }

    public void onComponentSelected(Item item) {
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            switch (item) {
                case WHITE_CONCRETE_POWDER:
                case WHITE_GLASS_PANE:
                case WHITE_GLASS:
                    clickedMaterial = NMSManager.getNMS().getItemMaterial(item);
                    clickedComponentType = null;
                    break;
                default:
                    throw new UnsupportedOperationException("This item is not supported");
            }
            colorSelectionContext = LEInventoryType.NEW_COMPONENT_MENU;
            openLEInventory(new LEColorSelectorInventory(player, Message.SELECT_COMPONENT_COLOR, true, null, Areas.getInstance().getAreaFromLocation(clickedBlock.getLocation())));
        }, 2);
    }

    @SuppressWarnings("null")
    public void onColorSelected(LasersColor color) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onColorSelected");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), this::closeOpenedInventory, 1);
        if (colorSelectionContext == null) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            switch (colorSelectionContext) {
                case NEW_COMPONENT_MENU:
                    if (clickedMaterial == null) {
                        IComponent newComponent = ComponentController.addComponentFromBlockFace(clickedComponentType, clickedBlock, clickedBlockFace, player);
                        ((IColorableComponent) newComponent).setColor(color, true);
                        component = newComponent;
                        openLEInventory(new LEComponentShortcutBarInventory(player, newComponent));
                    } else {
                        ComponentController.addColoredMaterialFromBlockFace(clickedMaterial, clickedBlock, clickedBlockFace, player, color);
                    }
                    clickedComponentType = null;
                    clickedMaterial = null;
                    clickedBlock = null;
                    clickedBlockFace = null;
                    break;
                case COMPONENT_MENU:
                    ComponentController.setColor(component.getArea(), (IColorableComponent) component, player, color);
                    openLEInventory(new LEComponentMenuInventory(player, component));
                    break;
                default:
                    throw new UnsupportedOperationException("Color selection has no context.");
            }
        }, 2);
    }

    public void closeOpenedInventory() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.closeOpenedInventory");
        player.getBukkitPlayer().closeInventory();
        if (currentLEOpenedInventory != null) {
            currentLEOpenedInventory.onClose();
            currentLEOpenedInventory = null;
        }
    }

    public void closeShortcutBarInventory() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.closeShortcutBarInventory");
        if (currentLEShortcutBarInventory != null) {
            currentLEShortcutBarInventory.onClose();
            currentLEShortcutBarInventory = null;
        }
    }

    public void openLEInventory(ALEInventory inventory) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.openLEInventory");
        Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            AreaController.clearTemporaryLocations(player);
            closeOpenedInventory();
            ItemStack[] items = inventory.getContents();
            if (inventory instanceof ALEOpenableInventory) {
                if (currentLEOpenedInventory != null) {
                    closeOpenedInventory();
                }
                currentLEOpenedInventory = (ALEOpenableInventory) inventory;
                Inventory bukkitInventory = currentLEOpenedInventory.getBukkitInventory();
                player.getBukkitPlayer().openInventory(bukkitInventory);
            } else if (inventory instanceof ALEShortcutBarInventory) {
                if (currentLEShortcutBarInventory != null) {
                    currentLEShortcutBarInventory.onClose();
                }
                currentLEShortcutBarInventory = (ALEShortcutBarInventory) inventory;
                Inventory bukkitInventory = player.getBukkitPlayer().getInventory();
                for (int i = 0; i < items.length; i++) {
                    bukkitInventory.setItem(i, items[i]);
                }
            }
        }, 1);
    }

    public boolean isUsing(IComponent component) {
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.getComponent() == component) {
            return true;
        }
        return currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.getComponent() == component;
    }

    public boolean isUsing(Area area) {
        if (currentLEOpenedInventory != null && currentLEOpenedInventory.getArea() == area) {
            return true;
        }
        return currentLEShortcutBarInventory != null && currentLEShortcutBarInventory.getArea() == area;
    }

    public void updateInventories() {
        if (currentLEOpenedInventory != null) {
            currentLEOpenedInventory.updateInventory();
        }
        if (currentLEShortcutBarInventory != null) {
            currentLEShortcutBarInventory.updateInventory();
        }
    }

    public void setSavedInventory(Inventory inventoryFromDatabase, boolean isInEditionMode, boolean isRotationShortcutBarOpened) {
        this.isInEditionMode = isInEditionMode;
        this.isRotationShortcutBarOpened = isRotationShortcutBarOpened;
        this.getInventorySaveManager().addInventoryFromDatabase(inventoryFromDatabase.getContents());
    }

    public void onCloseInventory() {
        if (currentLEOpenedInventory != null) {
            currentLEOpenedInventory.onClose();
            currentLEOpenedInventory = null;
        }
    }


    public void toggleEditionMode() {
        if (isRotationShortcutBarOpened) {
            return;
        }
        if (isInEditionMode()) {
            exitEditionMode();
        } else {
            openEditionMode();
        }
    }

    public void exitEditionMode() {
        if (isInEditionMode()) {
            closeOpenedInventory();
            closeShortcutBarInventory();
            playerInventorySaveManager.onExitEditionMode();
            isInEditionMode = false;
            isRotationShortcutBarOpened = false;
        }
    }

    public void openEditionMode() {
        if (!isInEditionMode()) {
            playerInventorySaveManager.onOpenEditionMode();
            isInEditionMode = true;
            isRotationShortcutBarOpened = false;
            openLEInventory(new LEMainShortcutBarInventory(player));
        }
    }

    public void closeRotationShortcutBarInventory() {
        closeRotationShortcutBarInventory(false);
    }

    public void closeRotationShortcutBarInventory(boolean delay) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.closeRotationShortcutbarInventory");
        Runnable doCloseRotation = () -> {
            closeShortcutBarInventory();
            playerInventorySaveManager.onExitRotationShortcutBar();
            isRotationShortcutBarOpened = false;
            isInEditionMode = false;
        };
        if (delay) {
            Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), doCloseRotation, 1);
        } else {
            doCloseRotation.run();
        }
    }

    public void openRotationShortcutBarInventory(IPlayerModifiableComponent playerModifiableComponent) {
        if (isInEditionMode()) {
            return;
        }
        if (isRotationShortcutBarOpened()) {
            closeRotationShortcutBarInventory();
            return;
        }
        Area area = playerModifiableComponent.getArea();
        boolean canRotateHorizontally = true;
        boolean canRotateVertically = true;
        boolean canResizeGravitationalSphere = false;
        switch (playerModifiableComponent.getType()) {
            case LASER_SENDER:
                if (!area.isHLaserSendersRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVLaserSendersRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case LASER_RECEIVER:
                if (!area.isHLaserReceiversRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVLaserReceiversRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case CONCENTRATOR:
                if (!area.isHConcentratorsRotationAllowed()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVConcentratorsRotationAllowed()) {
                    canRotateVertically = false;
                }
                break;
            case MIRROR_SUPPORT:
                MirrorSupportMode mode = ((MirrorSupport) playerModifiableComponent).getMode();
                if (!area.isHMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateHorizontally = false;
                }
                if (!area.isVMirrorsRotationAllowed() || !mode.isMirrorRotatable()) {
                    canRotateVertically = false;
                }
                break;
            case ATTRACTION_REPULSION_SPHERE:
                canResizeGravitationalSphere = area.isAttractionRepulsionSpheresResizingAllowed();
                canRotateVertically = false;
                canRotateHorizontally = false;
                break;
            default:
                canRotateHorizontally = false;
                canRotateVertically = false;
                canResizeGravitationalSphere = false;
                break;
        }
        if (!(canResizeGravitationalSphere || canRotateHorizontally || canRotateVertically)) {
            if (sendComponentBlockedMsg) {
                Message.CANNOT_INTERACT_WITH_COMPONENT.sendMessage(getPlayer().getBukkitPlayer());
            }
            return;
        }
        this.playerInventorySaveManager.onOpenRotationShortcutBar();
        isRotationShortcutBarOpened = true;
        isInEditionMode = false;
        openLEInventory(new LEPlayerModifyingComponentShortcutBarInventory(player, playerModifiableComponent));
    }

    public void onEnterArea() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onEnterArea");
        getInventorySaveManager().onEnterArea();
    }

    public void onLeaveArea() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onLeaveArea");
        if (isRotationShortcutBarOpened) {
            closeRotationShortcutBarInventory();
        }
        getInventorySaveManager().onLeaveArea();
    }

    public void onQuit() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onQuit");
        onCloseInventory();
        getInventorySaveManager().onQuit();
    }

    public void onJoin() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "PlayerInventoryManager.onJoin");
        getInventorySaveManager().onJoin();
    }
}
