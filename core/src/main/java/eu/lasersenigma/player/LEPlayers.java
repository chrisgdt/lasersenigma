package eu.lasersenigma.player;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.KeyChest;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.components.parents.IRotatableComponent;
import eu.lasersenigma.items.inventory.LEMainShortcutBarInventory;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Lasers-Enigma player list container
 */
public class LEPlayers {

    private static LEPlayers instance;
    private static HashMap<UUID, LEPlayer> players;

    public LEPlayers() {
        players = new HashMap<>();
    }

    public static LEPlayers getInstance() {
        if (instance == null) {
            instance = new LEPlayers();
        }
        return instance;
    }

    private List<PlayerInventoryManager> getInventoriesManagers(IComponent component) {
        return players.values().stream()
                .map(LEPlayer::getInventoryManager)
                .filter((pim) -> pim.isUsing(component))
                .collect(Collectors.toList());
    }

    private List<PlayerInventoryManager> getInventoriesManagers(Area area) {
        return players.values().stream()
                .map(LEPlayer::getInventoryManager)
                .filter((pim) -> pim.isUsing(area))
                .collect(Collectors.toList());
    }

    public HashMap<UUID, LEPlayer> getPlayers() {
        return players;
    }

    public void onComponentUpdated(IComponent component) {
        getInventoriesManagers(component)
                .forEach(PlayerInventoryManager::updateInventories);
    }

    public void onMirrorRetrieved(IMirrorContainer component) {
        if (component instanceof IRotatableComponent) {
            getInventoriesManagers(component).stream()
                    .filter(PlayerInventoryManager::isRotationShortcutBarOpened)
                    .forEach(PlayerInventoryManager::closeRotationShortcutBarInventory);
        }
    }

    public void onComponentDeleted(IComponent component) {
        players.values().stream()
                .filter((p) -> p.getInventoryManager().isUsing(component))
                .forEach(LEPlayer::forceStopSavingActions);
        getInventoriesManagers(component)
                .forEach(pim -> {
                    pim.openLEInventory(new LEMainShortcutBarInventory(pim.getPlayer()));
                });
    }

    public void onAreaUpdated(Area area) {
        getInventoriesManagers(area)
                .forEach(PlayerInventoryManager::updateInventories);
    }

    public void onAreaDeleted(Area area) {
        getInventoriesManagers(area)
                .forEach(pim -> {
                    pim.openLEInventory(new LEMainShortcutBarInventory(pim.getPlayer()));
                });
    }

    public LEPlayer findLEPlayer(UUID playerUUID) {
        LEPlayer lePlayer = players.get(playerUUID);
        if (lePlayer == null) {
            lePlayer = new LEPlayer(playerUUID);
            players.put(playerUUID, lePlayer);
        }
        return lePlayer;
    }

    public LEPlayer findLEPlayer(Player player) {
        return findLEPlayer(player.getUniqueId());
    }

    public void removeLockKeys(Lock lock) {
        players.values()
                .forEach(p -> p.removeLockKeys(lock));
    }

    public void removeKeyChestKeys(KeyChest keyChest) {
        players.values()
                .forEach(p -> p.removeKey(keyChest));
    }
}
