package eu.lasersenigma.player;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.items.AArmorActionProcessor;
import eu.lasersenigma.items.ArmorAction;
import eu.lasersenigma.nms.NMSManager;
import org.bukkit.Location;

public class PlayerController {

    private PlayerController() {
    }

    /**
     * remove the keys corresponding to the lock passed as parameter from every
     * players key storage
     *
     * @param lock   the lock whose keys must be removed from every player key
     *               storage
     * @param player the player that started this action
     */
    public static void removeLockKeysFromEveryPlayers(LEPlayer player, Lock lock) {
        if (!player.getInventoryManager().isInEditionMode() && !Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }
        LEPlayers.getInstance().removeLockKeys(lock);
        Message.PLAYERS_KEYS_REMOVED_FOR_THIS_LOCK.sendMessage(player.getBukkitPlayer());
    }

    public static void removePlayerLockKeys(LEPlayer player, Lock lock) {
        player.removeLockKeys(lock);
        Message.MY_KEYS_REMOVED_FOR_THIS_LOCK.sendMessage(player.getBukkitPlayer());
    }

    static void armorCreation(LEPlayer player, ArmorAction selectedArmorAction, Location newMaterialLocation, boolean noBurnCheckbox, boolean noDamageCheckbox, boolean noKnockbackCheckbox, boolean reflectionCheckbox, boolean focusCheckbox, boolean prismCheckbox, int durabilityNumber) {
        if (!Permission.EDIT.checkPermissionAndSendMsg(player.getBukkitPlayer())) {
            return;
        }

        String armor_no_knockback_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_knockback_tag");
        String armor_no_burn_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_burn_tag");
        String armor_no_damage_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_damage_tag");
        String armor_reflect_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_reflect_tag");
        String armor_prism_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_prism_tag");
        String armor_focus_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_focus_tag");
        String armor_lasers_durability_regex = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_lasers_reduces_durability_tag");

        AArmorActionProcessor armorActionProcessor = NMSManager.getNMS().getArmorActionProcessor(
                player.getBukkitPlayer(), selectedArmorAction, newMaterialLocation,
                noBurnCheckbox, noDamageCheckbox, noKnockbackCheckbox, reflectionCheckbox, focusCheckbox, prismCheckbox, durabilityNumber,
                armor_no_knockback_tag, armor_no_burn_tag, armor_no_damage_tag, armor_reflect_tag, armor_prism_tag, armor_focus_tag, armor_lasers_durability_regex);

        armorActionProcessor.process();

    }
}
