package eu.lasersenigma.player;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.areas.ComponentController;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.ActionType;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.attributes.SavedAction;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.components.parents.IPlayerModifiableComponent;
import eu.lasersenigma.config.*;
import eu.lasersenigma.exceptions.AbstractLasersException;
import eu.lasersenigma.exceptions.PlayerStatsNotFoundException;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemAttribute;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.stats.AreaStats;
import eu.lasersenigma.stats.PlayerStats;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;

import java.time.Duration;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Lasers-Enigma representation of a player
 */
public class LEPlayer {

    private static final String[] TRANSPARENT_MATERIALS_STR_ARRAY = {"AIR", "WATER", "LEGACY_STATIONARY_WATER"};

    @SuppressWarnings("SetReplaceableByEnumSet")
    public static final Set<Material> TRANSPARENT_MATERIALS = NMSManager.getNMS().getCrossableMaterials(TRANSPARENT_MATERIALS_STR_ARRAY);
    private final UUID playerUUID;

    private final PlayerInventoryManager inventoryManager;
    private final HashMap<String, Boolean> perWorldIsCheckpointReachedAVictoryCheckpoint;
    private final HashMap<String, Location> perWorldLastCheckpoint;
    private final HashMap<String, Location> perWorldLastVictoryCheckpoint;
    private final HashSet<KeyChest> keysChestObtained;
    private Integer antiSpamTaskId;
    private Location lastCheckpointReached;
    private Location lastVictoryCheckpointReached;
    private boolean isLastCheckpointReachedAVictoryCheckpoint;

    private PlayerActionsSaver actionSaver;

    private boolean inArea;

    public LEPlayer(UUID playerUUID) {
        this.playerUUID = playerUUID;
        inventoryManager = new PlayerInventoryManager(this);
        isLastCheckpointReachedAVictoryCheckpoint = false;
        this.perWorldIsCheckpointReachedAVictoryCheckpoint = new HashMap<>();
        this.perWorldLastCheckpoint = new HashMap<>();
        this.perWorldLastVictoryCheckpoint = new HashMap<>();
        this.keysChestObtained = new HashSet<>();
        this.actionSaver = null;
        inArea = false;
    }

    private static boolean isSafeLocation(Location location) {
        Block feet = location.getBlock();
        if (!feet.getType().isTransparent() && !feet.getLocation().add(0, 1, 0).getBlock().getType().isTransparent()) {
            return false; // not transparent (will suffocate)
        }
        Block head = feet.getRelative(BlockFace.UP);
        if (!head.getType().isTransparent()) {
            return false; // not transparent (will suffocate)
        }
        Block ground = feet.getRelative(BlockFace.DOWN);
        return ground.getType().isSolid();
    }

    public boolean isInArea() {
        return inArea;
    }

    public void teleportToNearestSafeLocation(Location location) {
        boolean safeLocationFound = isSafeLocation(location);
        Location tpLoc = location;
        for (int x = 1; x <= 5 && -5 <= x && !safeLocationFound; ) {
            for (int z = 1; z <= 5 && -5 <= z && !safeLocationFound; ) {
                for (int y = 1; y <= 5 && -5 <= y && !safeLocationFound; ) {
                    tpLoc = location.clone().add(x, y, z);
                    if (isSafeLocation(tpLoc)) {
                        safeLocationFound = true;
                    }
                    if (y == 5) {
                        y = -1;
                    } else if (y > 0) {
                        y++;
                    } else if (y < 0) {
                        y--;
                    }
                }
                if (z == 5) {
                    z = -1;
                } else if (z > 0) {
                    z++;
                } else if (z < 0) {
                    z--;
                }
            }
            if (x == 5) {
                x = -1;
            } else if (x > 0) {
                x++;
            } else if (x < 0) {
                x--;
            }
        }
        getBukkitPlayer().teleport(tpLoc);
    }

    public void addKeyFromDB(KeyChest keyChest) {
        this.keysChestObtained.add(keyChest);
    }

    public void addKey(KeyChest keyChest) {
        if (!keysChestObtained.contains(keyChest)) {
            this.keysChestObtained.add(keyChest);
            LasersEnigmaPlugin.getInstance().getPluginDatabase().addPlayerKey(playerUUID, keyChest);
        }
    }

    public void removeWorldKeys(World world) {
        HashSet<KeyChest> toRemove = keysChestObtained.stream()
                .filter(keyChest -> Objects.equals(keyChest.getLocation().getWorld(), world))
                .collect(Collectors.toCollection(HashSet::new));
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(playerUUID, toRemove);
        keysChestObtained.removeIf(keyChest -> Objects.equals(keyChest.getLocation().getWorld(), world));
    }

    public HashSet<KeyChest> getKeysChestObtained() {
        return this.keysChestObtained;
    }

    public void setWorldCheckpointsFromDB(Location lastCheckpointReached, Location lastVictoryCheckpointReached, boolean isLastCheckpointReachedAVictoryCheckpoint) {
        if (lastCheckpointReached == null && lastVictoryCheckpointReached == null) {
            throw new IllegalArgumentException("lastCheckpointReached & lastVictoryCheckpointReached are both null.");
        }
        String worldStr;
        if (lastCheckpointReached == null) {
            worldStr = Objects.requireNonNull(lastVictoryCheckpointReached.getWorld()).getName();
        } else {
            worldStr = Objects.requireNonNull(lastCheckpointReached.getWorld()).getName();
        }
        perWorldIsCheckpointReachedAVictoryCheckpoint.put(worldStr, isLastCheckpointReachedAVictoryCheckpoint);
        perWorldLastCheckpoint.put(worldStr, lastCheckpointReached);
        perWorldLastVictoryCheckpoint.put(worldStr, lastVictoryCheckpointReached);
    }

    public void setCheckpointsFromDB(World world, World vworld, boolean isLastCheckpointReachedAVictoryCheckpoint) {
        this.isLastCheckpointReachedAVictoryCheckpoint = isLastCheckpointReachedAVictoryCheckpoint;
        if (world == null) {
            this.lastCheckpointReached = null;
        } else {
            this.lastCheckpointReached = this.perWorldLastCheckpoint.get(world.getName());
        }
        if (vworld == null) {
            this.lastVictoryCheckpointReached = null;
        } else {
            this.lastVictoryCheckpointReached = this.perWorldLastVictoryCheckpoint.get(vworld.getName());
        }
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(playerUUID);
    }

    public UUID getUniqueId() {
        return playerUUID;
    }

    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(playerUUID);
    }

    public PlayerInventoryManager getInventoryManager() {
        return this.inventoryManager;
    }

    public void onInventoryDrag(InventoryDragEvent e) {
        getInventoryManager().onInventoryDrag(e);
    }

    public void onPlayerQuit(PlayerQuitEvent e) {
        getInventoryManager().onQuit();
    }

    public void onPlayerDropItem(PlayerDropItemEvent e) {
        getInventoryManager().onPlayerDropItem(e.getItemDrop().getItemStack(), e);
    }

    public void onInventoryClick(InventoryClickEvent e) {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onInventoryClick");
        switch (e.getClick()) {
            case WINDOW_BORDER_LEFT:
            case WINDOW_BORDER_RIGHT:
            case UNKNOWN:
            case NUMBER_KEY:
                break;
            case CONTROL_DROP:
            case DROP:
                getInventoryManager().onPlayerDropItem(e.getCurrentItem(), e);
                break;
            case CREATIVE:
                if (this.getInventoryManager().isInEditionMode()) {
                    boolean isCursorItemPluginRelated = (e.getCursor() != null && ItemsFactory.getInstance().getSimilarItem(e.getCursor()) != null);
                    boolean isCurrentItemPluginRelated = (e.getCurrentItem() != null && ItemsFactory.getInstance().getSimilarItem(e.getCurrentItem()) != null);
                    if (isCursorItemPluginRelated || isCurrentItemPluginRelated) {
                        e.setCancelled(true);
                        if (isCursorItemPluginRelated) {
                            getInventoryManager().onInventoryClick(e.getCursor());
                        } else {
                            getInventoryManager().onInventoryClick(e.getCurrentItem());
                        }
                    }
                }
                break;
            case MIDDLE:
            default:
                if (this.getInventoryManager().isInEditionMode()) {
                    getInventoryManager().onInventoryClick(e.getCurrentItem());
                    e.setCancelled(true);
                }
                if (this.getInventoryManager().isRotationShortcutBarOpened()) {
                    e.setCancelled(true);
                }
                break;
        }
    }

    public void onLeftClick(IComponent component) {
        if (antiSpamTaskId != null) {
            return;
        }
        antiSpamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antiSpamTaskId = null;
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onLeftClick({0}, {1}, {2}, {3}", component.getType().toString(), Double.toString(component.getLocation().getX()), Double.toString(component.getLocation().getY()), Double.toString(component.getLocation().getZ()));
            if (!inventoryManager.isInEditionMode()) {
                if (component instanceof IMirrorContainer) {
                    IMirrorContainer mirrorContainer = (IMirrorContainer) component;
                    if (mirrorContainer.hasMirror()) {
                        ComponentController.getMirror((IMirrorContainer) component, this);
                        return;
                    }
                    Item item = getCurrentItem();
                    if (item != null) {
                        if ((component instanceof MirrorSupport && item.isMirror(true)) || item.isMirror(false)) {
                            ComponentController.placeMirror(mirrorContainer, this, (LasersColor) item.getAttributes().get(ItemAttribute.COLOR));
                        }
                    }
                }
            } else {
                ComponentController.removeComponent(component.getArea(), this, component);
            }
        }, 1).getTaskId();
    }

    public boolean onLeftClick() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onLeftClick()");
        Player bukkitPlayer = getBukkitPlayer();
        if (bukkitPlayer == null) {
            return false; // player is disconnected
        }
        Block block = bukkitPlayer.getTargetBlock(TRANSPARENT_MATERIALS, 5);
        Location blockLocation = block.getLocation();
        Area area = Areas.getInstance().getAreaFromLocation(blockLocation);
        if (area == null) {
            return false;
        }
        IComponent component = area.getComponentFromLocation(blockLocation);
        if (component == null) {
            component = getInvisibleTargetedComponent(area, block);
        }
        if (component != null) {
            onLeftClick(component);
            return true;
        }
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onLeftClick() component==null block");
        return false;
    }

    public void onRightClick(IComponent component) {
        if (antiSpamTaskId != null) {
            return;
        }
        if (!component.getArea().isActivated()) {
            return;
        }
        antiSpamTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            antiSpamTaskId = null;
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onRightClick({0}, {1}, {2}, {3}", component.getType().toString(), Double.toString(component.getLocation().getX()), Double.toString(component.getLocation().getY()), Double.toString(component.getLocation().getZ()));
            if (inventoryManager.isInEditionMode()) {
                inventoryManager.onRightClick(component);
                return;
            }
            boolean isPlayerModifiableComponent = component instanceof IPlayerModifiableComponent;
            if (!isPlayerModifiableComponent && getInventoryManager().isRotationShortcutBarOpened()) {
                getInventoryManager().closeRotationShortcutBarInventory();
            }
            if (component instanceof IMirrorContainer) {
                IMirrorContainer mirrorContainer = (IMirrorContainer) component;
                if (!mirrorContainer.hasMirror()) {
                    Item item = getCurrentItem();
                    if (item != null) {
                        if ((component instanceof MirrorSupport && item.isMirror(true)) || item.isMirror(false)) {
                            ComponentController.placeMirror(mirrorContainer, this, (LasersColor) item.getAttributes().get(ItemAttribute.COLOR));
                        }
                    }
                    return;
                }
            }
            if (component instanceof CallButton) {
                CallButton callButton = (CallButton) component;
                ComponentController.elevatorCallButton(this, callButton);
            }
            if (component instanceof Lock) {
                Lock lock = (Lock) component;
                if (lock.isOpened()) {
                    Message.LOCK_ALREADY_OPENED.sendMessage(this.getBukkitPlayer());
                } else {
                    boolean opened = lock.insertKeys(keysChestObtained, this);
                    if (opened) {
                        Message.LOCK_OPENED.sendMessage(this.getBukkitPlayer());
                    } else {
                        String nbRequiredKeys = String.valueOf(lock.getNbRequiredKeys());
                        String nbRequiredKeysInserted = String.valueOf(lock.getNbInsertedKeys());
                        Message.LOCK_NEEDS_MORE_KEYS.sendMessage(this.getBukkitPlayer(), new String[]{nbRequiredKeysInserted, nbRequiredKeys});
                    }
                }
                return;
            }
            if (component instanceof KeyChest) {
                KeyChest keyChest = (KeyChest) component;
                if (this.keysChestObtained.contains(keyChest)) {
                    Message.KEY_ALREADY_POSSESED.sendMessage(this.getBukkitPlayer());
                } else {
                    keysChestObtained.add(keyChest);
                    keyChest.startKeyObtainingAnimations(this);
                    Message.KEY_OBTAINED.sendMessage(this.getBukkitPlayer());
                    String nbRequiredKeys = String.valueOf(keyChest.getLock().getNbRequiredKeys());
                    int nbRequiredKeysLeft = keyChest.getLock().getNbRequiredKeysLeft(keysChestObtained);
                    if (nbRequiredKeysLeft > 0) {
                        Message.KEY_S_LOCK_NEEDS_MORE_KEYS.sendMessage(this.getBukkitPlayer(), new String[]{String.valueOf(nbRequiredKeysLeft), nbRequiredKeys});
                    } else {
                        Message.KEY_S_LOCK_NEEDS_NO_MORE_KEYS.sendMessage(this.getBukkitPlayer());
                    }
                }
                return;
            }
            if (isPlayerModifiableComponent) {
                Item item = getCurrentItem();
                IPlayerModifiableComponent playerModifiableComponent = (IPlayerModifiableComponent) component;
                boolean isRotationShortcutBarOpened = getInventoryManager().isRotationShortcutBarOpened();
                if (isRotationShortcutBarOpened) {
                    if (item != null) {
                        getInventoryManager().onRightClick(item, component);
                    }
                } else if (ComponentController.canModify(this, playerModifiableComponent)) {
                    getInventoryManager().openRotationShortcutBarInventory(playerModifiableComponent);
                }
            }
        }, 1).getTaskId();
    }

    public boolean onRightClick() {
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onRightClick()");
        Player bukkitPlayer = getBukkitPlayer();
        if (bukkitPlayer == null) {
            return true; // player is disconnected
        }
        Block block = bukkitPlayer.getTargetBlock(TRANSPARENT_MATERIALS, 5);
        Location blockLocation = block.getLocation();
        Area area = Areas.getInstance().getAreaFromLocation(blockLocation);
        if (area != null) {
            IComponent component = area.getComponentFromLocation(blockLocation);
            if (component == null) {
                component = getInvisibleTargetedComponent(area, block);
            }
            if (component != null) {
                onRightClick(component);
                return !(component instanceof MirrorChest);
            }
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "LEPlayer.onRightClick() component==null");
        }
        if (inventoryManager.isInEditionMode()) {
            return inventoryManager.onRightClick();
        }
        if (inventoryManager.isRotationShortcutBarOpened()) {
            inventoryManager.closeRotationShortcutBarInventory();
            return true;
        }
        return false;
    }

    public Item getCurrentItem() {
        return ItemsFactory.getInstance().getSimilarItem(getBukkitPlayer().getInventory().getItemInMainHand());
    }

    /* Intermediary functions */
    private IComponent getInvisibleTargetedComponent(Area a, Block clickedBlock) {
        Vector eyeDirection = getBukkitPlayer().getLocation().getDirection().normalize().multiply(0.05);
        Location currentSearchLocation = getBukkitPlayer().getEyeLocation();
        double distance = 0.1;
        while (distance < 5) {
            currentSearchLocation = currentSearchLocation.add(eyeDirection);
            distance = distance + 0.05;
            IComponent component = a.getComponentFromLocation(currentSearchLocation);
            if (component != null) {
                return component;
            }
        }
        return null;
    }

    public void onPlayerJoin(PlayerJoinEvent e) {
        inArea = false;
        this.getInventoryManager().onJoin();
        if (Permission.ADMIN.hasPermission(getBukkitPlayer())) {
            LEUpdateNotifier.getInstance().getUpdateMessages().forEach(msg -> getBukkitPlayer().sendMessage(msg));
        }
    }

    private Location getLastCheckpointReached() {
        return lastCheckpointReached == null ? null : lastCheckpointReached.clone();
    }

    private Location getLastVictoryCheckpointReached() {
        return lastVictoryCheckpointReached == null ? null : lastVictoryCheckpointReached.clone();
    }

    public boolean getIsLastCheckpointReachedAVictoryCheckpoint() {
        return isLastCheckpointReachedAVictoryCheckpoint;
    }

    private void setLastCheckpoint(Location checkpointLocation, boolean isVictoryCheckpoint) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return;
        }
        if (checkpointLocation == null) {
            return;
        }
        if (isVictoryCheckpoint) {
            if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("checkpoint_in_victory_area")) {
                return;
            }
            this.lastVictoryCheckpointReached = checkpointLocation;
            this.perWorldLastVictoryCheckpoint.put(Objects.requireNonNull(checkpointLocation.getWorld()).getName(), checkpointLocation);
        } else {
            this.lastCheckpointReached = checkpointLocation;
            this.perWorldLastCheckpoint.put(Objects.requireNonNull(checkpointLocation.getWorld()).getName(), checkpointLocation);
        }
        this.isLastCheckpointReachedAVictoryCheckpoint = isVictoryCheckpoint;
        this.perWorldIsCheckpointReachedAVictoryCheckpoint.put(checkpointLocation.getWorld().getName(), isVictoryCheckpoint);
        LasersEnigmaPlugin.getInstance().getPluginDatabase().updatePlayerCheckpoint(this.getUniqueId(), checkpointLocation, isLastCheckpointReachedAVictoryCheckpoint);
    }

    public void setCurrentVictoryCheckpoint(Location playerLocation) {
        setLastCheckpoint(playerLocation, true);
    }

    public void setCurrentCheckpoint(Location checkpointLocation) {
        setLastCheckpoint(checkpointLocation, false);
    }

    public void clearCheckpoints() {
        this.lastCheckpointReached = null;
        this.lastVictoryCheckpointReached = null;
        this.perWorldIsCheckpointReachedAVictoryCheckpoint.clear();
        this.perWorldLastCheckpoint.clear();
        this.perWorldLastVictoryCheckpoint.clear();
        LasersEnigmaPlugin.getInstance().getPluginDatabase().clearAllCheckpoints(this.getUniqueId());
    }

    public void clearWorldCheckpoints(World from) {
        String worldName = from.getName();
        if (this.lastCheckpointReached != null && Objects.requireNonNull(this.lastCheckpointReached.getWorld()).getName().equals(worldName)) {
            this.lastCheckpointReached = null;
            this.isLastCheckpointReachedAVictoryCheckpoint = true;
            LasersEnigmaPlugin.getInstance().getPluginDatabase().deleteLastCheckpointReached(this.getUniqueId());
        }
        if (this.lastVictoryCheckpointReached != null && Objects.requireNonNull(this.lastVictoryCheckpointReached.getWorld()).getName().equals(worldName)) {
            this.lastVictoryCheckpointReached = null;
            this.isLastCheckpointReachedAVictoryCheckpoint = false;
            LasersEnigmaPlugin.getInstance().getPluginDatabase().deleteLastVictoryCheckpointReached(this.getUniqueId());
        }
        this.perWorldIsCheckpointReachedAVictoryCheckpoint.remove(worldName);
        this.perWorldLastCheckpoint.remove(worldName);
        this.perWorldLastVictoryCheckpoint.remove(worldName);
        LasersEnigmaPlugin.getInstance().getPluginDatabase().deleteWorldCheckpointData(this.getUniqueId(), worldName);
    }

    public Location getOnChangeWorldCheckpoint(World from, World to, GetBackToCheckpointOnChangeWorld getBackToCheckpointOnChangeWorld) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return null;
        }
        switch (getBackToCheckpointOnChangeWorld) {
            case FALSE:
                return null;
            case ONLY_TO_VICTORY_CHECKPOINTS:
                return this.perWorldLastVictoryCheckpoint.get(to.getName());
            case ONLY_TO_NORMAL_CHECKPOINTS:
                return this.perWorldLastCheckpoint.get(to.getName());
            case TRUE:
                Boolean b = this.perWorldIsCheckpointReachedAVictoryCheckpoint.get(to.getName());
                if (b != null) {
                    if (b) {
                        return this.perWorldLastVictoryCheckpoint.get(to.getName());
                    } else {
                        return this.perWorldLastCheckpoint.get(to.getName());
                    }
                }

            default:
                return null;
        }
    }

    public Location getOnJoinCheckpoint(World to, GetBackToCheckpointOnJoin getBackToCheckpointOnJoin) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return null;
        }
        switch (getBackToCheckpointOnJoin) {
            case FALSE:
                return null;
            case PER_WORLD_ONLY_TO_VICTORY_CHECKPOINTS:
                return this.perWorldLastVictoryCheckpoint.get(to.getName());
            case PER_WORLD_ONLY_TO_NORMAL_CHECKPOINTS:
                return this.perWorldLastCheckpoint.get(to.getName());
            case PER_WORLD_TRUE:
                Boolean b = this.perWorldIsCheckpointReachedAVictoryCheckpoint.get(to.getName());
                if (b != null) {
                    if (b) {
                        return this.perWorldLastVictoryCheckpoint.get(to.getName());
                    } else {
                        return this.perWorldLastCheckpoint.get(to.getName());
                    }
                }
                return null;
            case ONLY_TO_NORMAL_CHECKPOINTS:
                return this.lastCheckpointReached;
            case ONLY_TO_VICTORY_CHECKPOINTS:
                return this.lastVictoryCheckpointReached;
            case TRUE:
                if (this.isLastCheckpointReachedAVictoryCheckpoint) {
                    return this.lastVictoryCheckpointReached;
                }
                return this.lastCheckpointReached;
            default:
                return null;
        }

    }

    public Location getOnRespawnCheckpoint(World to, GetBackToCheckpointOnDeath getBackToCheckpointOnDeath) {
        if (!LasersEnigmaPlugin.getInstance().getConfig().getBoolean("use_checkpoint_system")) {
            return null;
        }
        switch (getBackToCheckpointOnDeath) {
            case FALSE:
                return null;
            case ONLY_TO_VICTORY_CHECKPOINTS:
                return this.perWorldLastVictoryCheckpoint.get(to.getName());
            case ONLY_TO_NORMAL_CHECKPOINTS:
                return this.perWorldLastCheckpoint.get(to.getName());
            case TRUE:
                Boolean b = this.perWorldIsCheckpointReachedAVictoryCheckpoint.get(to.getName());
                if (b != null) {
                    if (b) {
                        return this.perWorldLastVictoryCheckpoint.get(to.getName());
                    } else {
                        return this.perWorldLastCheckpoint.get(to.getName());
                    }
                }
                return null;
            default:
                return null;
        }
    }

    public void removeLockKeys(Lock lock) {
        HashSet<KeyChest> keyChestsToRemove = keysChestObtained.stream()
                .filter(keyChest -> keyChest.getLock() == lock)
                .collect(Collectors.toCollection(HashSet::new));
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(playerUUID, keyChestsToRemove);
        keysChestObtained.removeIf(keyChest -> keyChest.getLock() == lock);
    }

    public void removeKey(KeyChest keyChest) {
        if (keysChestObtained.remove(keyChest)) {
            LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(playerUUID, new HashSet<>(Collections.singletonList(keyChest)));
        }
    }

    public void removeAllKeys() {
        LasersEnigmaPlugin.getInstance().getPluginDatabase().removePlayerKeys(playerUUID);
        keysChestObtained.clear();
    }

    public void onPlayerSneek() {
        Area area = Areas.getInstance().getAreaFromLocation(getBukkitPlayer().getLocation());
        if (area == null) {
            return;
        }
        IComponent component = area.getComponentFromLocation(getBukkitPlayer().getLocation());
        if (!(component instanceof Elevator)) {
            return;
        }

        ComponentController.elevatorGoDown(this, (Elevator) component);
    }

    public void onPlayerJump() {
        Area area = Areas.getInstance().getAreaFromLocation(getBukkitPlayer().getLocation());
        if (area == null) {
            return;
        }
        IComponent component = area.getComponentFromLocation(getBukkitPlayer().getLocation());
        if (!(component instanceof Elevator)) {
            return;
        }

        ComponentController.elevatorGoUp(this, (Elevator) component);
    }

    /**
     * Retrieves the player records for each area he has solved
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the player's records for each area solved
     */
    public HashMap<Area, PlayerStats> getPerAreaRecords(World world) {
        HashMap<Area, PlayerStats> result = new HashMap<>();
        Areas.getInstance().getAreaSet().forEach(area -> {
            if (world != null) {
                if (!world.getUID().equals(Objects.requireNonNull(area.getMinLocation().getWorld()).getUID())) {
                    return; //equivalent to continue in java 8 forEach.
                }
            }
            AreaStats areaStats = area.getStats();

            //count multiple linked area only one
            Area curLinkedArea = area;
            while (curLinkedArea.getStats().isLinked()) {
                curLinkedArea = curLinkedArea.getStats().getLinkedArea();
            }
            if (result.containsKey(areaStats.getLinkedArea())) {
                return;
            }

            PlayerStats playerStatsForThisArea = null;
            try {
                playerStatsForThisArea = areaStats.getStats(getUniqueId());
            } catch (PlayerStatsNotFoundException ignored) {
            }
            if (playerStatsForThisArea != null) {
                result.put(area, playerStatsForThisArea);
            }
        });
        return result;
    }

    /**
     * Retrieves the number of area solved by this player
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the number of area solved by this player
     */
    public int getNbAreaSolved(World world) {
        return getPerAreaRecords(world).size();
    }

    /**
     * Retrieves the total number of step done amongst solved puzzles records
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the total number of step done amongst solved puzzles records
     */
    public long getTotalNbStepsInSolvedAreasRecords(World world) {
        return getPerAreaRecords(world).values().stream()
                .map(PlayerStats::getNbStep).mapToLong(Integer::toUnsignedLong).sum();
    }

    /**
     * Retrieves the total time spent in solved puzzles records
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the total time spent in solved puzzles records
     */
    public Duration getTotalTimeSpentInSolvedAreasRecords(World world) {
        return getPerAreaRecords(world).values().stream()
                .map(PlayerStats::getDuration)
                .reduce(Duration.ZERO, Duration::plus);
    }

    /**
     * Retrieves the total number of actions done amongst solved puzzles records
     *
     * @param world to only retrieve this for a specific world. Can be set to
     *              null to retrieve this for every worlds.
     * @return the total number of actions done amongst solved puzzles records
     */
    public long getTotalNbActionDoneInSolvedAreasRecords(World world) {
        return getPerAreaRecords(world).values().stream()
                .map(PlayerStats::getNbAction).mapToLong(Integer::toUnsignedLong).sum();
    }

    public boolean isSavingActions() {
        return this.actionSaver != null;
    }

    public void startSavingActions(Area area, IComponent component) {
        if (this.actionSaver != null) {
            try {
                this.actionSaver.stopSavingActions();
            } catch (AbstractLasersException ex) {
                ErrorsConfig.showError(this.getBukkitPlayer(), ex);
                this.actionSaver = null;
            }
        }
        this.actionSaver = new PlayerActionsSaver(area, component);
    }

    public void saveActions(IComponent component, ActionType actionType) {
        if (this.actionSaver != null) {
            if (!getInventoryManager().isInEditionMode() || !Permission.EDIT.hasPermission(this.getBukkitPlayer())) {
                forceStopSavingActions();
                return;
            }
            try {
                this.actionSaver.saveAction(component, actionType);
            } catch (AbstractLasersException ex) {
                ErrorsConfig.showError(this.getBukkitPlayer(), ex);
                this.actionSaver = null;
            }
        }
    }

    public ArrayList<SavedAction> stopSavingActions() {
        try {
            return this.actionSaver == null ? null : this.actionSaver.stopSavingActions();
        } catch (AbstractLasersException ex) {
            ErrorsConfig.showError(this.getBukkitPlayer(), ex);
            this.actionSaver = null;
            return null;
        }
    }

    public void forceStopSavingActions() {
        this.actionSaver = null;
    }

    public void onLeaveArea() {
        inArea = false;
        getInventoryManager().onLeaveArea();
    }

    public void onEnterArea() {
        inArea = true;
        getInventoryManager().onEnterArea();
    }
}
