package eu.lasersenigma.songs;

import com.xxmicloxx.NoteBlockAPI.RadioSongPlayer;
import com.xxmicloxx.NoteBlockAPI.SoundCategory;
import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;
import java.util.logging.Level;

public class PlayerSongManager {

    private static final int FADE_DURATION = 40;
    private static final int LOOP_DELAY = 40;
    private static final SoundCategory SOUND_CATEGORY = SoundCategory.valueOf(LasersEnigmaPlugin.getInstance().getConfig().getString("music_blocks_sound_category"));

    private final UUID playerUUID;

    private int fadingOutTaskId;
    private int startSongLaterTaskId;

    private RadioSongPlayer radioSongPlayer;
    private String songFileName;
    private boolean loop;
    private boolean stopWhenPlayerLeavedArea;

    private String nextSongFileName;
    private boolean nextLoop;
    private boolean nextStopWhenPlayerLeavedArea;

    protected PlayerSongManager(UUID playerUUID, String nextSongFileName, boolean nextLoop, boolean nextStopWhenPlayerLeavedArea) {
        this.nextSongFileName = nextSongFileName;
        this.nextLoop = nextLoop;
        this.nextStopWhenPlayerLeavedArea = nextStopWhenPlayerLeavedArea;
        this.fadingOutTaskId = -1;
        this.startSongLaterTaskId = -1;
        this.playerUUID = playerUUID;
        this.radioSongPlayer = null;
        startSongNow();
    }

    protected void startSong(String songFileName, boolean loop, boolean stopWhenPlayerLeavedArea) {
        if (!SongsListManager.getInstance().isActivated()) {
            return;
        }
        this.nextSongFileName = songFileName;
        this.nextLoop = loop;
        this.nextStopWhenPlayerLeavedArea = stopWhenPlayerLeavedArea;

        //If a song (and from now: this one) is already going to be played soon
        if (startSongLaterTaskId != -1) {
            return;
        }

        //If a song is fading out we have to start the playing of this song later
        if (fadingOutTaskId != -1) {
            startSongLater(FADE_DURATION);
            return;
        }

        //if no song is currently played 
        if (!(radioSongPlayer != null && radioSongPlayer.isPlaying())) {
            startSongNow();
            return;
        }

        //If the song currently played is the same one
        if (this.songFileName.equals(nextSongFileName)) {
            this.loop = loop;
            this.stopWhenPlayerLeavedArea = stopWhenPlayerLeavedArea;
            return;
        }
        fadeOut(FADE_DURATION);
        startSongLater(FADE_DURATION);
    }

    private void startSongLater(int delay) {
        if (startSongLaterTaskId >= 0) {
            return;
        }
        startSongLaterTaskId = Bukkit.getScheduler().runTaskLater(LasersEnigmaPlugin.getInstance(), () -> {
            startSongNow();
            startSongLaterTaskId = -1;
        }, delay).getTaskId();
    }

    private void startSongNow() {
        if (!SongsListManager.getInstance().isActivated()) {
            return;
        }
        if (fadingOutTaskId != -1) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "Canceling song fading task in startSongNow method");
            Bukkit.getScheduler().cancelTask(fadingOutTaskId);
            fadingOutTaskId = -1;
        }
        if (startSongLaterTaskId != -1) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINER, "Canceling song starting task in startSongNow method");
            Bukkit.getScheduler().cancelTask(startSongLaterTaskId);
            startSongLaterTaskId = -1;
        }
        if (radioSongPlayer != null && radioSongPlayer.isPlaying()) {
            radioSongPlayer.setPlaying(false);
            radioSongPlayer = null;
        }
        Player player = Bukkit.getPlayer(playerUUID);
        if (player == null) {
            return;
        }
        songFileName = nextSongFileName;
        loop = nextLoop;
        stopWhenPlayerLeavedArea = nextStopWhenPlayerLeavedArea;
        radioSongPlayer = new RadioSongPlayer(SongsListManager.getInstance().getSong(songFileName));
        radioSongPlayer.addPlayer(player);
        radioSongPlayer.setAutoDestroy(true);
        radioSongPlayer.setPlaying(true);
        radioSongPlayer.setVolume((byte) 70);
        radioSongPlayer.setCategory(SOUND_CATEGORY);
    }

    protected void fadeOut(int maximumFadeDuration) {
        if (fadingOutTaskId >= 0 || radioSongPlayer == null) {
            return;
        }
        if (!radioSongPlayer.isPlaying()) {
            radioSongPlayer = null;
            return;
        }
        loop = false;
        double currentVolume = radioSongPlayer.getVolume();
        byte finalVolume = 0;
        double delay = maximumFadeDuration;
        double diffVolumeDouble = -Math.floor(currentVolume / delay);
        final byte diffVolume = (byte) diffVolumeDouble;
        BukkitRunnable fadeTask = new BukkitRunnable() {

            private int nbIteration = 0;

            @Override
            public void run() {
                nbIteration++;
                if (nbIteration >= delay || radioSongPlayer == null || !radioSongPlayer.isPlaying() || !SongsListManager.getInstance().isActivated()) {
                    if (radioSongPlayer != null) {
                        radioSongPlayer.setPlaying(false);
                    }
                    radioSongPlayer = null;
                    fadingOutTaskId = -1;
                    this.cancel();
                    return;
                }
                int newVolume = (radioSongPlayer.getVolume() + diffVolume);
                if (newVolume + diffVolume <= finalVolume) {
                    radioSongPlayer.setPlaying(false);
                    radioSongPlayer = null;
                    fadingOutTaskId = -1;
                    this.cancel();
                    return;
                }
                radioSongPlayer.setVolume((byte) newVolume);

            }
        };
        if (!LasersEnigmaPlugin.getInstance().isDisabling()) {
            fadingOutTaskId = fadeTask.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 0).getTaskId();
        }
    }

    protected void onSongEnd() {
        if (loop && SongsListManager.getInstance().isActivated()) {
            radioSongPlayer = null;
            nextSongFileName = songFileName;
            nextLoop = loop;
            nextStopWhenPlayerLeavedArea = stopWhenPlayerLeavedArea;
            startSongNow();
            return;
        }
        radioSongPlayer = null;
    }

    protected void onPlayerLeavedArea() {
        if (startSongLaterTaskId >= 0) {
            if (nextStopWhenPlayerLeavedArea) {
                Bukkit.getScheduler().cancelTask(startSongLaterTaskId);
                startSongLaterTaskId = -1;
            }
            return;
        }
        if (radioSongPlayer == null || !radioSongPlayer.isPlaying()) {
            return;
        }
        if (!stopWhenPlayerLeavedArea) {
            loop = false;
            return;
        }
        if (fadingOutTaskId >= 0) {
            return;
        }
        fadeOut(FADE_DURATION);
    }

    protected void onReload() {
        if (startSongLaterTaskId >= 0) {
            Bukkit.getScheduler().cancelTask(startSongLaterTaskId);
        }
        if (fadingOutTaskId >= 0) {
            Bukkit.getScheduler().cancelTask(fadingOutTaskId);
        }
        if (radioSongPlayer != null) {
            if (radioSongPlayer.isPlaying()) {
                radioSongPlayer.setPlaying(false);
            }
        }
    }
}
