package eu.lasersenigma.songs;

import com.xxmicloxx.NoteBlockAPI.Song;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class SongsListManager {

    private static SongsListManager instance;
    private ArrayList<Song> songs;
    private SongsScoreboardManager scoreboardManager;

    private SongsListManager() {
        songs = SongsLoader.getInstance().getSongs();
    }

    public static SongsListManager getInstance() {
        if (instance == null) {
            instance = new SongsListManager();
        }
        return instance;
    }

    protected void reload() {
        SongsLoader songLoader = SongsLoader.getInstance();
        songLoader.reload();
        songs = songLoader.getSongs();
        if (scoreboardManager != null) {
            scoreboardManager.reload(songs.size(), getMaxSongFileName());
        }
    }

    public int getNbSongs() {
        return songs.size();
    }

    public int getIndex(String songFileName) {
        if (!SongsLoader.getInstance().isActivated()) {
            return -1;
        }
        for (int i = 0; i < songs.size(); i++) {
            if (SongsLoader.removeExtension(songs.get(i).getPath().getName()).equals(songFileName)) {
                return i;
            }
        }
        return -1;
    }

    public Song getSong(String songFileName) {
        if (!SongsLoader.getInstance().isActivated()) {
            return null;
        }
        return songs
                .stream()
                .filter(s -> SongsLoader.removeExtension(s.getPath().getName()).equals(songFileName))
                .findAny()
                .orElse(null);
    }

    public int getNextIndex(int index) {
        if (!SongsLoader.getInstance().isActivated()) {
            return -1;
        }
        int newIndex = index + 1;
        if (newIndex < 0 || newIndex >= songs.size()) {
            newIndex = 0;
        }
        return newIndex;
    }

    public int getPrevIndex(int index) {
        if (!SongsLoader.getInstance().isActivated()) {
            return -1;
        }
        int newIndex = index - 1;
        if (newIndex < 0 || newIndex >= songs.size()) {
            newIndex = songs.size() - 1;
        }
        return newIndex;
    }

    public String getSongFileName(int index) {
        if (!SongsLoader.getInstance().isActivated()) {
            return "";
        }
        if (index < 0 || index >= songs.size()) {
            return SongsLoader.removeExtension(songs.get(0).getPath().getName());
        }
        //System.out.println(index + "--name->" + SongsLoader.removeExtension(songs.get(index).getPath().getName()));
        return SongsLoader.removeExtension(songs.get(index).getPath().getName());
    }

    public Song getSong(int index) {
        if (!SongsLoader.getInstance().isActivated()) {
            return null;
        }
        if (index < 0 || index >= songs.size()) {
            return songs.get(0);
        }
        return songs.get(index);
    }

    public final boolean isActivated() {
        return SongsLoader.getInstance().isActivated();
    }

    public void showScoreboard(int index, Player p) {
        if (!SongsLoader.getInstance().isActivated()) {
            return;
        }
        if (index == -1 || index >= songs.size()) {
            index = 0;
        }
        if (scoreboardManager == null) {
            scoreboardManager = new SongsScoreboardManager(songs.size(), getMaxSongFileName());
        }
        scoreboardManager.showScoreboard(index, p);
    }

    private int getMaxSongFileName() {
        int max = 0;
        for (Song s : songs) {
            int length = SongsLoader.removeExtension(s.getPath().getName()).length();
            if (length > max) {
                max = length;
            }
        }
        return max;
    }
}
