package eu.lasersenigma.config;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.exceptions.AbstractLasersException;
import eu.lasersenigma.exceptions.AreaOverlapException;
import eu.lasersenigma.exceptions.SelectionOverlapPartiallyAreaException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.logging.Level;

public class ErrorsConfig {

    /**
     * the path to errors translations in config
     */
    public static String BASE_TRANSLATION_PATH = "errors.";

    /**
     * Constructor
     */
    private ErrorsConfig() {
    }

    /**
     * Show an error in the console
     *
     * @param e the exception corresponding to the message we want to show
     */
    public static void logError(AbstractLasersException e) {
        String msg = getErrorMessage(e);
        LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, msg);
    }

    public static void showError(Player p, AbstractLasersException e) {
        p.sendMessage(getErrorMessage(e));
    }

    public static void showError(CommandSender cs, AbstractLasersException e) {
        cs.sendMessage(getErrorMessage(e));
    }

    /**
     * gets an error message from an exception
     *
     * @param e the exception corresponding to the message we want to show
     * @return the message corresponding to this exception
     */
    private static String getErrorMessage(AbstractLasersException e) {
        if (e instanceof SelectionOverlapPartiallyAreaException) {
            String translatedMessage = getErrorMessage(e.getTranslationCode());
            String[] args = Message.toArray(((SelectionOverlapPartiallyAreaException) e).getPointOutsideSelection());
            for (int i = 1; i <= args.length; ++i) {
                translatedMessage = translatedMessage.replace("%" + i + "%", args[i - 1]);
            }
            return translatedMessage;
        }
        if (e instanceof AreaOverlapException) {
            String translatedMessage = getErrorMessage(e.getTranslationCode());
            ArrayList<String> args2 = new ArrayList<>();
            Area overlappedArea = ((AreaOverlapException) e).getOverlappedArea();
            args2.add(String.valueOf(overlappedArea.getMinLocation().getBlockX()));
            args2.add(String.valueOf(overlappedArea.getMinLocation().getBlockY()));
            args2.add(String.valueOf(overlappedArea.getMinLocation().getBlockZ()));
            args2.add(String.valueOf(overlappedArea.getMaxLocation().getBlockX()));
            args2.add(String.valueOf(overlappedArea.getMaxLocation().getBlockY()));
            args2.add(String.valueOf(overlappedArea.getMaxLocation().getBlockZ()));
            for (int i = 1; i <= args2.size(); ++i) {
                translatedMessage = translatedMessage.replace("%" + i + "%", args2.get(i - 1));
            }
            return translatedMessage;
        }
        return getErrorMessage(e.getTranslationCode());
    }

    /**
     * gets an error message from a relative configuration path
     *
     * @param message_code the relative path of the message
     * @return the message
     */
    private static String getErrorMessage(String message_code) {
        return LasersEnigmaPlugin.getInstance().getTranslationConfig().getCustomConfig().getString(BASE_TRANSLATION_PATH + message_code);
    }
}
