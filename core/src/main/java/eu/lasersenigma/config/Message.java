package eu.lasersenigma.config;

import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;

/**
 * Enum used to retrieve translations from the configuration
 */
public enum Message {
    CREATE_AREA_FIRST_LOCATION("create_area_first_location"),
    CREATE_AREA_SECOND_LOCATION("create_area_second_location"),
    CREATE_AREA_SUCCESS("create_area_success"),
    DELETE_AREA("delete_area"),
    ARMOR_CONFIG_VALIDATE("armor_config_validate"),
    DELETE_AREA_CONFIRM("delete_area_confirm"),
    TOGGLE_AREA_VROTATION("toggle_area_vrotation"),
    TOGGLE_AREA_HROTATION("toggle_area_hrotation"),
    TOGGLE_CONCENTRATORS_VROTATION("toggle_concentrators_vrotation"),
    TOGGLE_CONCENTRATORS_HROTATION("toggle_concentrators_hrotation"),
    TOGGLE_LASER_SENDERS_VROTATION("toggle_laser_senders_vrotation"),
    TOGGLE_LASER_SENDERS_HROTATION("toggle_laser_senders_hrotation"),
    TOGGLE_LASER_RECEIVERS_VROTATION("toggle_laser_receivers_vrotation"),
    TOGGLE_LASER_RECEIVERS_HROTATION("toggle_laser_receivers_hrotation"),
    TOGGLE_SPHERES_SIZING("toggle_spheres_sizing"),
    MIRROR_CHEST_DECREASE("mirror_chest_decrease"),
    MIRROR_CHEST_INCREASE("mirror_chest_increase"),
    CONFIG_RELOADED("config_reloaded"),
    COLOR_CHANGED("color_changed"),
    RANGE_CHANGED("range_changed"),
    MUSIC_LOOP_TRUE("music_block_loop_true"),
    MUSIC_LOOP_FALSE("music_block_mode_false"),
    MUSIC_STOP_ON_EXIT_TRUE("music_block_stop_on_exit_area_true"),
    MUSIC_STOP_ON_EXIT_FALSE("music_block_stop_on_exit_area_false"),
    DETECTION_MODE_ALWAYS_ENABLED("detection_mode_always_enabled"),
    DETECTION_MODE_LASER_RECEIVERS("detection_mode_laser_receivers"),
    DETECTION_MODE_REDSTONE("detection_mode_redstone"),
    DETECTION_MODE_PLAYERS("detection_mode_players"),
    DETECTION_MODE_REDSTONE_SENSOR("detection_mode_redstone_sensor"),
    DETECTION_MODE_LOCKS("detection_mode_locks"),
    DETECTION_MODE_VICTORY_AREA("detection_mode_victory_area"),
    ATTRACTION_MODE("attraction_mode"),
    REPULSION_MODE("repulsion_mode"),
    ATTRACTION_REPULSION_SPHERE_RESIZED("attraction_repulsion_sphere_resized"),
    MIRROR_SUPPORT_MODE_FREE("mirror_support_mode_free"),
    MIRROR_SUPPORT_MODE_BLOCKED("mirror_support_mode_blocked"),
    ACTIVATED("activated"),
    DEACTIVATED("deactivated"),
    COPY_SUCCESS("copy_success"),
    PASTE_SUCCESS("paste_success"),
    SAVE_SUCCESS("save_success"),
    LOAD_SUCCESS("load_success"),
    STEPS("steps"),
    ACTIONS("actions"),
    DURATION("duration"),
    NBSTEPS("nbsteps"),
    NBACTIONS("nbactions"),
    STATS_MERGE_AND_LINKED("stats_merged_and_linked"),
    STATS_UNLINKED("stats_unlinked"),
    AREA_CONF_MENU_TITLE("area_conf_menu_title"),
    STATS_MENU_TITLE("stats_menu_title"),
    COMPONENT_MENU_TITLE("component_menu_title"),
    COMPONENT_SELECTOR_TITLE("component_selector_title"),
    ARMOR_OPTION_MENU_TITLE("armor_options_menu_inventory_title"),
    ARMOR_ACTION_MENU_TITLE("armor_action_menu_inventory_title"),
    TELEPORTATION_MENU_TITLE("teleportation_menu_title"),
    KEY_CHEST_SELECTOR_MENU_TITLE("key_chest_selector_menu_title"),
    LOCK_S_KEY_CHEST_DELETION("lock_s_key_chest_deletion"),
    LOCK_S_PLAYERS_KEYS_DELETION("lock_s_players_keys_deletion"),
    AREA_DELETE_CONFIRM("area_delete_confirm"),
    SELECT_COMPONENT_COLOR("component_select_color"),
    AREA_STATS_CLEAR_CONFIRM("area_stats_clear_confirm_menu_title"),
    AREA_STATS_LINK_CONFIRM("area_stats_link_confirm_menu_title"),
    AREA_STATS_UNLINK_CONFIRM("area_stats_unlink_confirm_menu_title"),
    STATS_ACTION_LIST_MENU_TITLE("area_stats_action_menu_title"),
    STATS_TIME_LIST_MENU_TITLE("area_stats_time_menu_title"),
    STATS_STEP_LIST_MENU_TITLE("area_stats_step_menu_title"),
    CHECKPOINT_SELECTED("checkpoint_selected"),
    CHECKPOINT_DELETED("checkpoint_deleted"),
    VICTORY_AREAS_DELETED("victory_areas_deleted"),
    VICTORY_AREA_FIRST_LOCATION("victory_area_first_location"),
    VICTORY_AREA_SECOND_LOCATION("victory_area_second_location"),
    VICTORY_AREA_CREATED("victory_area_created"),
    ALL_LOCK_S_KEY_CHESTS_DELETED("all_lock_s_key_chests_deleted"),
    PLAYERS_KEYS_REMOVED_FOR_THIS_LOCK("players_keys_removed_for_this_lock"),
    MY_KEYS_REMOVED_FOR_THIS_LOCK("my_keys_removed_for_this_lock"),
    KEY_REQUIRED("key_required"),
    KEY_OPTIONNAL("key_optionnal"),
    LOCK_ALREADY_OPENED("lock_already_opened"),
    LOCK_OPENED("lock_opened"),
    LOCK_NEEDS_MORE_KEYS("lock_need_more_key"),
    KEY_ALREADY_POSSESED("key_already_possed"),
    KEY_OBTAINED("key_obtained"),
    KEY_S_LOCK_NEEDS_MORE_KEYS("key_s_lock_needs_more_keys"),
    KEY_S_LOCK_NEEDS_NO_MORE_KEYS("key_s_lock_needs_no_more_keys"),
    INCOMPATIBLE_ARMOR_TAGS("incompatible_armor_tags"),
    ELEVATOR_FLOOR_CREATED("elevator_floor_created"),
    ELEVATOR_FLOORS_DELETED("elevator_floors_deleted"),
    ELEVATOR_CALL_BUTTONS_DELETED("elevator_call_buttons_deleted"),
    SCHEDULED_ACTIONS_MAIN_MENU_TITLE("scheduled_actions_main_menu"),
    SCHEDULED_ACTIONS_EDITION_MENU_TITLE("scheduled_actions_edition_menu_title"),
    SCHEDULED_ACTIONS_ACTION_EDIT_MENU_TITLE("scheduled_actions_action_edit_menu_title"),
    SCHEDULED_ACTIONS_DELAY_REQUIRED("scheduled_actions_delay_required"),
    LIGHT_LEVEL_CHANGED("light_level_changed"),
    LIGHT_LEVEL_MENU_TITLE("light_level_menu"),
    AREA_SIZE_UPDATE("area_size_updated"),
    SHOW_AREA("show_area"),
    CANNOT_INTERACT_WITH_COMPONENT("cannot_interact_with_component");

    /**
     * base path to all messages
     */
    private static final String BASE_PATH = "messages.";
    private final String config_path;

    Message(String config_path) {
        this.config_path = config_path;
    }

    public static String[] toArray(Location l) {
        return new String[]{Integer.toString(l.getBlockX()), Integer.toString(l.getBlockY()), Integer.toString(l.getBlockZ())};
    }

    public static String[] toArray(boolean b) {
        if (b) {
            return new String[]{ACTIVATED.getMessage()};
        }
        return new String[]{DEACTIVATED.getMessage()};
    }

    private String getConfigPath() {
        return this.config_path;
    }

    public String getMessage() {
        String defaultValue = BASE_PATH + getConfigPath();
        String message = LasersEnigmaPlugin.getInstance().getTranslationConfig().getCustomConfig().getString(BASE_PATH + getConfigPath(), defaultValue);
        if (message.equals(defaultValue)) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "The translation does not contain the following key: " + defaultValue);
        }
        return message;
    }

    public String getMessage(String[] args) {
        String translatedMessage = getMessage();
        for (int i = 1; i <= args.length; ++i) {
            translatedMessage = translatedMessage.replace("%" + i + "%", args[i - 1]);
        }
        return translatedMessage;
    }

    public void sendMessage(CommandSender commandSender, String[] args) {
        commandSender.sendMessage(getMessage(args));
    }

    public void sendMessage(Player player, String[] args) {
        player.sendMessage(getMessage(args));
    }

    public void sendMessage(CommandSender commandSender) {
        commandSender.sendMessage(getMessage());
    }

    public void sendMessage(Player player) {
        player.sendMessage(getMessage());
    }

}
