package eu.lasersenigma.config;

import eu.lasersenigma.LasersEnigmaPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.logging.Level;

/**
 * Custom Config manager
 */
public class ConfigManager {

    /**
     * the instance of the plugin
     */
    private final LasersEnigmaPlugin plugin;
    /**
     * the name of the configuration file
     */
    private final String configFileName;
    /**
     * The file configuration we area building
     */
    private FileConfiguration customConfig = null;
    /**
     * the file containing the configuration
     */
    private File customConfigFile = null;

    /**
     * Constructor
     *
     * @param lasersEnigmaPlugin the instance of the plugin
     * @param configFileName     the name of the configuration file
     */
    public ConfigManager(LasersEnigmaPlugin lasersEnigmaPlugin, String configFileName) {
        this.plugin = lasersEnigmaPlugin;
        this.configFileName = configFileName;
    }

    /**
     * reloads the custom config
     */
    public void reloadCustomConfig() {
        if (customConfigFile == null) {
            customConfigFile = new File(plugin.getDataFolder(), configFileName);
        }
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        // Look for defaults in the jar
        try {
            Reader defConfigStream = new InputStreamReader(plugin.getResource(configFileName), "UTF8");
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            customConfig.setDefaults(defConfig);
        } catch (UnsupportedEncodingException ex) {
            plugin.getBetterLogger().log(Level.SEVERE, "Could not read config " + customConfigFile, ex);
        }
    }

    /**
     * gets the custom config
     *
     * @return the custom config
     */
    public FileConfiguration getCustomConfig() {
        if (customConfig == null) {
            reloadCustomConfig();
        }
        return customConfig;
    }

    /**
     * saves the customconfig
     */
    public void saveCustomConfig() {
        if (customConfig == null || customConfigFile == null) {
            return;
        }
        try {
            getCustomConfig().save(customConfigFile);
        } catch (IOException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
        }
    }

    /**
     * if doesn't exists already, save the default configuration file
     */
    public void saveDefaultCustomConfig() {
        if (customConfigFile == null) {
            customConfigFile = new File(plugin.getDataFolder(), configFileName);
        }
        if (!customConfigFile.exists()) {
            plugin.saveResource(configFileName, false);
        }
    }

}
