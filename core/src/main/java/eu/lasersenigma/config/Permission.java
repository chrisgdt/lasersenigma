package eu.lasersenigma.config;

import eu.lasersenigma.events.permissions.PlayerPermissionsCheckLEEvent;
import eu.lasersenigma.exceptions.NoPermissionException;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public enum Permission {
    EDIT("lasers.edit"),
    ADMIN("lasers.admin");

    private final String permissionPath;

    Permission(String permissionPath) {
        this.permissionPath = permissionPath;
    }

    public String getPath() {
        return this.permissionPath;
    }

    public boolean hasPermission(Player p) {
        boolean checkResult = p.hasPermission(permissionPath);
        PlayerPermissionsCheckLEEvent permEvent = new PlayerPermissionsCheckLEEvent(p, checkResult);
        Bukkit.getServer().getPluginManager().callEvent(permEvent);
        return checkResult;
    }

    public boolean hasPermission(CommandSender cs) {
        return cs.hasPermission(permissionPath);
    }

    public void checkPermission(Player p) throws NoPermissionException {
        if (!hasPermission(p)) {
            throw new NoPermissionException();
        }
    }

    public boolean checkPermissionAndSendMsg(CommandSender cs) {
        if (hasPermission(cs)) {
            return true;
        }
        ErrorsConfig.showError(cs, new NoPermissionException());
        return false;

    }
}
