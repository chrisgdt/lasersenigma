package eu.lasersenigma.config;

public enum GetBackToCheckpointOnDeath {
    TRUE,
    ONLY_TO_NORMAL_CHECKPOINTS,
    ONLY_TO_VICTORY_CHECKPOINTS,
    FALSE;

    public static GetBackToCheckpointOnDeath fromString(String str) {
        return GetBackToCheckpointOnDeath.valueOf(str.toUpperCase());
    }
}
