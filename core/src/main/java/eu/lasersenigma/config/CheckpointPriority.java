package eu.lasersenigma.config;

import org.bukkit.event.EventPriority;

public enum CheckpointPriority {
    LOWEST,
    LOW,
    NORMAL,
    HIGH,
    HIGHEST;

    public static CheckpointPriority fromString(String str) {
        return CheckpointPriority.valueOf(str.toUpperCase());
    }

    public EventPriority getPriority() {
        return EventPriority.valueOf(this.toString());
    }

}
