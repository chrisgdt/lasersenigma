package eu.lasersenigma.config;

public enum GetBackToCheckpointOnChangeWorld {
    TRUE,
    ONLY_TO_NORMAL_CHECKPOINTS,
    ONLY_TO_VICTORY_CHECKPOINTS,
    FALSE;

    public static GetBackToCheckpointOnChangeWorld fromString(String str) {
        return GetBackToCheckpointOnChangeWorld.valueOf(str.toUpperCase());
    }
}
