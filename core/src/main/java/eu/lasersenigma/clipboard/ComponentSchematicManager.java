package eu.lasersenigma.clipboard;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.ComponentFactory;
import eu.lasersenigma.components.*;
import eu.lasersenigma.components.attributes.ComponentType;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.components.attributes.Rotation;
import eu.lasersenigma.components.parents.*;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.LinkedList;

/**
 * @author Benjamin (alias bZx) ben.nbld@gmail.com
 */
public class ComponentSchematicManager {

    private ComponentSchematicManager() {

    }

    public static ComponentSchematic createSchematic(IComponent component, Location playerLocation) {
        ComponentSchematic componentSchematic = new ComponentSchematic();
        componentSchematic.setType(component.getType().toString());
        Vector locDiff = component.getLocation().toVector().subtract(playerLocation.toVector());
        componentSchematic.setLocDiffX(locDiff.getX());
        componentSchematic.setLocDiffY(locDiff.getY());
        componentSchematic.setLocDiffZ(locDiff.getZ());
        if (component instanceof AArmorStandComponent) {
            AArmorStandComponent rotatableComponent = ((AArmorStandComponent) component);
            componentSchematic.setRotationX(rotatableComponent.getRotation().getX());
            componentSchematic.setRotationY(rotatableComponent.getRotation().getY());
            componentSchematic.setFace(rotatableComponent.getFace().toString());
        } else if (component instanceof MirrorChest) {
            componentSchematic.setFace(((MirrorChest) component).getFace().toString());
        }
        if (component.getType() == ComponentType.MIRROR_CHEST) {
            componentSchematic.setMin(((MirrorChest) component).getNbMirrors());
        }
        if (component instanceof IColorableComponent) {
            int color = ((IColorableComponent) component).getSavedColor().getInt();
            if (component instanceof LaserReceiver) {
                if (!((LaserReceiver) component).isAnyColorAccepted()) {
                    color = 16;
                }
            }
            componentSchematic.setColor(color);
        }
        if (component instanceof MusicBlock) {
            MusicBlock musicBlock = (MusicBlock) component;
            componentSchematic.setSongFileName(musicBlock.getSongFileName());
            componentSchematic.setLoop(musicBlock.isLoop());
            componentSchematic.setStopOnExit(musicBlock.isStopOnExit());
        }
        if (component instanceof IDetectionComponent) {
            componentSchematic.setMode(((IDetectionComponent) component).getMode().toString());
            componentSchematic.setMin(((IDetectionComponent) component).getMin());
            componentSchematic.setMax(((IDetectionComponent) component).getMax());
        }
        if (component instanceof ILightComponent) {
            componentSchematic.setLightLevel(((ILightComponent) component).getLightLevel());
        }
        if (component instanceof AttractionRepulsionSphere) {
            componentSchematic.setMode(((AttractionRepulsionSphere) component).getMode().toString());
            componentSchematic.setMin(((AttractionRepulsionSphere) component).getSize());
        }
        if (component instanceof MirrorSupport) {
            componentSchematic.setMode(((MirrorSupport) component).getMode().toString());
        }
        return componentSchematic;
    }

    public static IComponent createComponent(ComponentSchematic componentSchematic, Area area, Location playerLocation) {
        IComponent component = ComponentFactory.createComponentFromDatabase(
                -1,
                area,
                ComponentType.valueOf(componentSchematic.getType()),
                playerLocation.clone().add(new Vector(
                        componentSchematic.getLocDiffX(),
                        componentSchematic.getLocDiffY(),
                        componentSchematic.getLocDiffZ()
                )),
                componentSchematic.getFace(),
                new Rotation(componentSchematic.getRotationX(), componentSchematic.getRotationY()),
                componentSchematic.getMin(),
                componentSchematic.getMax(),
                componentSchematic.getColor() == 16 ? null : LasersColor.fromInt(componentSchematic.getColor()),
                componentSchematic.getSongFileName(),
                componentSchematic.getLoop(),
                componentSchematic.getStopOnExit(),
                componentSchematic.getMode(),
                null,
                new LinkedList<>(),
                null,
                componentSchematic.getLightLevel());
        area.addComponent(component, false);
        component.dbCreate();
        return component;
    }
}
