package eu.lasersenigma.clipboard;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.exceptions.*;
import eu.lasersenigma.tasks.CreateAreaComponentsFromSchematicTask;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class AreasSchematicManager {

    private AreasSchematicManager() {

    }

    public static AreasSchematic createSchematic(Location playerLocationTmp, Location minLocationTmp, Location maxLocationTmp) throws SelectionOverlapPartiallyAreaException {
        Location playerLocation = roundLocation(playerLocationTmp);
        Location minLocation = roundLocation(minLocationTmp);
        Location maxLocation = roundLocation(maxLocationTmp);

        AreasSchematic areasSchematic = new AreasSchematic();
        Vector minDiff = minLocation.toVector().subtract(playerLocation.toVector());
        Vector maxDiff = maxLocation.toVector().subtract(playerLocation.toVector());
        areasSchematic.setMinDiffX(minDiff.getX());
        areasSchematic.setMinDiffY(minDiff.getY());
        areasSchematic.setMinDiffZ(minDiff.getZ());
        areasSchematic.setMaxDiffX(maxDiff.getX());
        areasSchematic.setMaxDiffY(maxDiff.getY());
        areasSchematic.setMaxDiffZ(maxDiff.getZ());
        areasSchematic.setAreaSchematicList(createAreaSchematicList(minLocation, maxLocation, playerLocation));
        return areasSchematic;
    }

    public static List<CreateAreaComponentsFromSchematicTask> createAreas(AreasSchematic areasSchematic, Location playerLocationTmp) throws PasteOutsideWorldException, AreaOverlapException, VictoryAreaMustHaveCommonWallException {
        Location playerLocation = roundLocation(playerLocationTmp);
        Location min = getMinLocationFromRoundedPlayerLoc(areasSchematic, playerLocation);
        Location max = getMaxLocationFromRoundedPlayerLoc(areasSchematic, playerLocation);
        if (playerLocation.getWorld().getMaxHeight() < max.getBlockY() || min.getBlockY() < 0) {
            throw new PasteOutsideWorldException();
        }
        for (AreaSchematic areaSchematic : areasSchematic.getAreaSchematicList()) {
            Vector minLocDiff = new Vector(
                    areaSchematic.getMinLocDiffX(),
                    areaSchematic.getMinLocDiffY(),
                    areaSchematic.getMinLocDiffZ()
            );
            Vector maxLocDiff = new Vector(
                    areaSchematic.getMaxLocDiffX(),
                    areaSchematic.getMaxLocDiffY(),
                    areaSchematic.getMaxLocDiffZ()
            );

            Location areaSchematicMinLoc = playerLocation.clone().add(minLocDiff);
            Location areaSchematicMaxLoc = playerLocation.clone().add(maxLocDiff);
            for (Area area : Areas.getInstance().getAreaSet()) {
                if (isOverlapping(area, areaSchematicMinLoc, areaSchematicMaxLoc)) {
                    throw new AreaOverlapException(area);
                }
            }
        }
        List<CreateAreaComponentsFromSchematicTask> createAreaComponentsTask = new ArrayList<>();
        for (AreaSchematic areaSchematic : areasSchematic.getAreaSchematicList()) {
            try {
                createAreaComponentsTask.add(AreaSchematicManager.createArea(areaSchematic, playerLocation));
            } catch (AreaCrossWorldsException | AreaNoDepthException | NoAreaFoundException ex) {
                LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "Normally this should never happen. Is the schematic file corrupted ? Else contact plugin developper please.", ex);
            }
        }
        return createAreaComponentsTask;
    }

    public static Location getMinLocation(AreasSchematic areasSchematic, Location playerLocationTmp) {
        return getMinLocationFromRoundedPlayerLoc(areasSchematic, roundLocation(playerLocationTmp));
    }

    public static Location getMaxLocation(AreasSchematic areasSchematic, Location playerLocationTmp) {
        return getMaxLocationFromRoundedPlayerLoc(areasSchematic, roundLocation(playerLocationTmp));
    }

    private static Location getMinLocationFromRoundedPlayerLoc(AreasSchematic areasSchematic, Location playerLocation) {
        Vector minDiff = new Vector(
                areasSchematic.getMinDiffX(),
                areasSchematic.getMinDiffY(),
                areasSchematic.getMinDiffZ()
        );
        return playerLocation.clone().add(minDiff);
    }

    private static Location getMaxLocationFromRoundedPlayerLoc(AreasSchematic areasSchematic, Location playerLocation) {
        Vector maxDiff = new Vector(
                areasSchematic.getMaxDiffX(),
                areasSchematic.getMaxDiffY(),
                areasSchematic.getMaxDiffZ()
        );
        return playerLocation.clone().add(maxDiff);
    }

    private static List<AreaSchematic> createAreaSchematicList(Location min, Location max, Location playerLocation) throws SelectionOverlapPartiallyAreaException {
        //retrieving all areas that are at least partially inside this selection
        List<Area> areasPartiallyInside = Areas.getInstance().getAreaSet()
                .stream()
                .filter(area -> isOverlapping(area, min, max))
                .collect(Collectors.toList());
        //retrieving those area corners
        List<Location> areasPartiallyInsideCorners = new ArrayList<>();
        areasPartiallyInside.forEach(area -> {
            areasPartiallyInsideCorners.addAll(getCorners(area.getMinLocation(), area.getMaxLocation()));
        });
        //checking if those area are fully inside selection
        Location cornerOutsideSelection = areasPartiallyInsideCorners.stream()
                .filter(corner -> !containsLocation(min, max, corner))
                .findAny()
                .orElse(null);
        if (cornerOutsideSelection != null) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.FINE, MessageFormat.format("min: {0} {1} {2} max: {3} {4} {5}", min.getBlockX(), min.getBlockY(), min.getBlockZ(), max.getBlockX(), max.getBlockY(), max.getBlockZ()));
            throw new SelectionOverlapPartiallyAreaException(cornerOutsideSelection);
        }
        //creating AreaSchematics
        return areasPartiallyInside.stream()
                .map(area -> AreaSchematicManager.createSchematic(area, playerLocation))
                .collect(Collectors.toList());
    }

    private static List<Location> getCorners(Location min, Location max) {
        World world = min.getWorld();
        List<Location> corners = new ArrayList<>();
        double minX = min.getBlockX();
        double minY = min.getBlockY();
        double minZ = min.getBlockZ();
        double maxX = max.getBlockX();
        double maxY = max.getBlockY();
        double maxZ = max.getBlockZ();
        //bottom corners
        corners.add(min.clone());
        corners.add(new Location(world, minX, minY, maxZ));
        corners.add(new Location(world, maxX, minY, minZ));
        corners.add(new Location(world, maxX, minY, maxZ));
        //top corners
        corners.add(new Location(world, minX, maxY, minZ));
        corners.add(new Location(world, minX, maxY, maxZ));
        corners.add(new Location(world, maxX, maxY, minZ));
        corners.add(max.clone());
        return corners;
    }

    private static boolean containsLocation(Location min, Location max, Location location) {

        if (location.getBlockX() > max.getBlockX() || location.getBlockX() < min.getBlockX()) {
            return false;
        }
        if (location.getBlockY() > max.getBlockY() || location.getBlockY() < min.getBlockY()) {
            return false;
        }
        return !(location.getBlockZ() > max.getBlockZ() || location.getBlockZ() < min.getBlockZ());
    }

    private static boolean isOverlapping(Area area, Location min, Location max) {
        if (!area.getMinLocation().getWorld().getName().equals(min.getWorld().getName())) {
            return false;
        }

        if (!intersectsDimension(min.getBlockX(), max.getBlockX(), area.getMinLocation().getBlockX(), area.getMaxLocation().getBlockX())) {
            return false;
        }

        if (!intersectsDimension(min.getBlockY(), max.getBlockY(), area.getMinLocation().getBlockY(), area.getMaxLocation().getBlockY())) {
            return false;
        }

        return intersectsDimension(min.getBlockZ(), max.getBlockZ(), area.getMinLocation().getBlockZ(), area.getMaxLocation().getBlockZ());
    }

    private static boolean intersectsDimension(double aMin, double aMax, double bMin, double bMax) {
        return aMin <= bMax && aMax >= bMin;
    }

    private static Location roundLocation(Location l) {
        return new Location(l.getWorld(), l.getBlockX(), l.getBlockY(), l.getBlockZ());
    }

}
