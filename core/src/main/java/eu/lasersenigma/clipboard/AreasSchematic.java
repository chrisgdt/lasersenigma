package eu.lasersenigma.clipboard;

import java.io.Serializable;
import java.util.List;

public class AreasSchematic implements Serializable {

    public static final long serialVersionUID = 1;

    private List<AreaSchematic> areaSchematicList = null;

    private double minDiffX = 0;
    private double minDiffY = 0;
    private double minDiffZ = 0;
    private double maxDiffX = 0;
    private double maxDiffY = 0;
    private double maxDiffZ = 0;

    public AreasSchematic() {

    }

    public List<AreaSchematic> getAreaSchematicList() {
        return areaSchematicList;
    }

    public void setAreaSchematicList(List<AreaSchematic> areaSchematicList) {
        this.areaSchematicList = areaSchematicList;
    }

    public double getMinDiffX() {
        return minDiffX;
    }

    public void setMinDiffX(double minDiffX) {
        this.minDiffX = minDiffX;
    }

    public double getMinDiffY() {
        return minDiffY;
    }

    public void setMinDiffY(double minDiffY) {
        this.minDiffY = minDiffY;
    }

    public double getMinDiffZ() {
        return minDiffZ;
    }

    public void setMinDiffZ(double minDiffZ) {
        this.minDiffZ = minDiffZ;
    }

    public double getMaxDiffX() {
        return maxDiffX;
    }

    public void setMaxDiffX(double maxDiffX) {
        this.maxDiffX = maxDiffX;
    }

    public double getMaxDiffY() {
        return maxDiffY;
    }

    public void setMaxDiffY(double maxDiffY) {
        this.maxDiffY = maxDiffY;
    }

    public double getMaxDiffZ() {
        return maxDiffZ;
    }

    public void setMaxDiffZ(double maxDiffZ) {
        this.maxDiffZ = maxDiffZ;
    }

}
