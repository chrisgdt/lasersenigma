package eu.lasersenigma.clipboard;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.VictoryArea;
import eu.lasersenigma.exceptions.*;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class VictoryAreaSchematicManager {

    private VictoryAreaSchematicManager() {

    }

    public static VictoryAreaSchematic createSchematic(VictoryArea va, Location playerLocation) {
        VictoryAreaSchematic areaSchematic = new VictoryAreaSchematic();
        Vector vMin = va.getMin().toVector().subtract(playerLocation.toVector());
        Vector vMax = va.getMax().toVector().subtract(playerLocation.toVector());
        areaSchematic.setMinLocDiffX(vMin.getX());
        areaSchematic.setMinLocDiffY(vMin.getY());
        areaSchematic.setMinLocDiffZ(vMin.getZ());
        areaSchematic.setMaxLocDiffX(vMax.getX());
        areaSchematic.setMaxLocDiffY(vMax.getY());
        areaSchematic.setMaxLocDiffZ(vMax.getZ());
        return areaSchematic;
    }

    public static void createVictoryArea(Area area, VictoryAreaSchematic vaSchematic, Location playerLocation) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException, VictoryAreaMustHaveCommonWallException {
        area.addVictoryArea(new VictoryArea(
                area,
                playerLocation.clone().add(new Vector(
                        vaSchematic.getMinLocDiffX(),
                        vaSchematic.getMinLocDiffY(),
                        vaSchematic.getMinLocDiffZ()
                )),
                playerLocation.clone().add(new Vector(
                        vaSchematic.getMaxLocDiffX(),
                        vaSchematic.getMaxLocDiffY(),
                        vaSchematic.getMaxLocDiffZ()
                ))
        ));
    }
}
