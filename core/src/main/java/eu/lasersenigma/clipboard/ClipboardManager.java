package eu.lasersenigma.clipboard;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.config.ErrorsConfig;
import eu.lasersenigma.config.Message;
import eu.lasersenigma.exceptions.AbstractLasersException;
import eu.lasersenigma.exceptions.InvalidSelectionException;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

public class ClipboardManager {

    private static ClipboardManager instance;
    private final HashMap<UUID, LESchematic> playersLESchematicClipboard;

    private ClipboardManager() {
        playersLESchematicClipboard = new HashMap<>();
    }

    public static ClipboardManager getInstance() {
        if (instance == null) {
            instance = new ClipboardManager();
        }
        return instance;
    }

    public void copy(Player player) {
        try {
            LESchematic schematic = LESchematicManager.createSchematic(player);
            playersLESchematicClipboard.put(player.getUniqueId(), schematic);
            Message.COPY_SUCCESS.sendMessage(player);
        } catch (AbstractLasersException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "An error occured while saving schematic.", ex);
            ErrorsConfig.showError(player, ex);
            ErrorsConfig.logError(ex);
        }
    }

    public void paste(Player player) {
        try {
            LESchematic schematic = playersLESchematicClipboard.get(player.getUniqueId());
            if (schematic == null) {
                throw new InvalidSelectionException();
            }
            LESchematicManager.pasteSchematic(player, schematic);
            Message.PASTE_SUCCESS.sendMessage(player);
        } catch (AbstractLasersException ex) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, "An error occured while restoring schematic.", ex);
            ErrorsConfig.showError(player, ex);
            ErrorsConfig.logError(ex);
        }
    }

    public void load(Player player, String schematicName) {
        try {
            LESchematic schematic = LESchematicManager.importSchematic(schematicName);
            playersLESchematicClipboard.put(player.getUniqueId(), schematic);
            Message.LOAD_SUCCESS.sendMessage(player, new String[]{LESchematicManager.getSchematicDisplayName(schematicName)});
        } catch (AbstractLasersException ex) {
            ErrorsConfig.showError(player, ex);
            ErrorsConfig.logError(ex);
        }
    }

    public void save(Player player, String schematicName) {
        try {
            LESchematic schematic = playersLESchematicClipboard.get(player.getUniqueId());
            if (schematic == null) {
                throw new InvalidSelectionException();
            }
            LESchematicManager.exportSchematic(schematic, schematicName);
            Message.SAVE_SUCCESS.sendMessage(player, new String[]{LESchematicManager.getSchematicDisplayName(schematicName)});
        } catch (AbstractLasersException ex) {
            ErrorsConfig.showError(player, ex);
            ErrorsConfig.logError(ex);
        }
    }
}
