package eu.lasersenigma.clipboard;

import java.io.Serializable;
import java.util.List;

public class AreaSchematic implements Serializable {

    public static final long serialVersionUID = 1;

    private List<ComponentSchematic> componentSchematicList = null;
    private List<VictoryAreaSchematic> victoryAreaSchematicList = null;

    private double minLocDiffX = 0;
    private double minLocDiffY = 0;
    private double minLocDiffZ = 0;
    private double maxLocDiffX = 0;
    private double maxLocDiffY = 0;
    private double maxLocDiffZ = 0;

    private boolean vrotation = false;
    private boolean hrotation = false;
    private boolean cvrotation = false;
    private boolean chrotation = false;

    private boolean lsvrotation = false;
    private boolean lshrotation = false;
    private boolean lrvrotation = false;
    private boolean lrhrotation = false;

    private double checkpointX = 0;
    private double checkpointY = 0;
    private double checkpointZ = 0;
    private float checkpointYaw = Float.MIN_VALUE;
    private float checkpointPitch = Float.MIN_VALUE;

    private String mode = null;
    private int minRange = 0;
    private int maxRange = 0;

    private boolean arspheresizing = false;

    public AreaSchematic() {

    }

    public List<ComponentSchematic> getComponentSchematicList() {
        return componentSchematicList;
    }

    public void setComponentSchematicList(List<ComponentSchematic> componentSchematicList) {
        this.componentSchematicList = componentSchematicList;
    }

    public List<VictoryAreaSchematic> getVictoryAreaSchematicList() {
        return victoryAreaSchematicList;
    }

    public void setVictoryAreaSchematicList(List<VictoryAreaSchematic> victoryAreaSchematicList) {
        this.victoryAreaSchematicList = victoryAreaSchematicList;
    }

    public double getMinLocDiffX() {
        return minLocDiffX;
    }

    public void setMinLocDiffX(double minLocDiffX) {
        this.minLocDiffX = minLocDiffX;
    }

    public double getMinLocDiffY() {
        return minLocDiffY;
    }

    public void setMinLocDiffY(double minLocDiffY) {
        this.minLocDiffY = minLocDiffY;
    }

    public double getMinLocDiffZ() {
        return minLocDiffZ;
    }

    public void setMinLocDiffZ(double minLocDiffZ) {
        this.minLocDiffZ = minLocDiffZ;
    }

    public double getMaxLocDiffX() {
        return maxLocDiffX;
    }

    public void setMaxLocDiffX(double maxLocDiffX) {
        this.maxLocDiffX = maxLocDiffX;
    }

    public double getMaxLocDiffY() {
        return maxLocDiffY;
    }

    public void setMaxLocDiffY(double maxLocDiffY) {
        this.maxLocDiffY = maxLocDiffY;
    }

    public double getMaxLocDiffZ() {
        return maxLocDiffZ;
    }

    public void setMaxLocDiffZ(double maxLocDiffZ) {
        this.maxLocDiffZ = maxLocDiffZ;
    }

    public boolean getVrotation() {
        return vrotation;
    }

    public void setVrotation(boolean vrotation) {
        this.vrotation = vrotation;
    }

    public boolean getHrotation() {
        return hrotation;
    }

    public void setHrotation(boolean hrotation) {
        this.hrotation = hrotation;
    }

    public boolean getCvrotation() {
        return cvrotation;
    }

    public void setCvrotation(boolean cvrotation) {
        this.cvrotation = cvrotation;
    }

    public boolean getChrotation() {
        return chrotation;
    }

    public void setChrotation(boolean chrotation) {
        this.chrotation = chrotation;
    }

    public boolean getLsvrotation() {
        return lsvrotation;
    }

    public void setLsvrotation(boolean lsvrotation) {
        this.lsvrotation = lsvrotation;
    }

    public boolean getLshrotation() {
        return lshrotation;
    }

    public void setLshrotation(boolean lshrotation) {
        this.lshrotation = lshrotation;
    }

    public boolean getLrvrotation() {
        return lrvrotation;
    }

    public void setLrvrotation(boolean lrvrotation) {
        this.lrvrotation = lrvrotation;
    }

    public boolean getLrhrotation() {
        return lrhrotation;
    }

    public void setLrhrotation(boolean lrhrotation) {
        this.lrhrotation = lrhrotation;
    }

    public boolean getArspheresizing() {
        return arspheresizing;
    }

    public void setArspheresizing(boolean arspheresizing) {
        this.arspheresizing = arspheresizing;
    }

    public double getCheckpointX() {
        return checkpointX;
    }

    public void setCheckpointX(double checkpointX) {
        this.checkpointX = checkpointX;
    }

    public double getCheckpointY() {
        return checkpointY;
    }

    public void setCheckpointY(double checkpointY) {
        this.checkpointY = checkpointY;
    }

    public double getCheckpointZ() {
        return checkpointZ;
    }

    public void setCheckpointZ(double checkpointZ) {
        this.checkpointZ = checkpointZ;
    }

    public float getCheckpointYaw() {
        return checkpointYaw;
    }

    public void setCheckpointYaw(float checkpointYaw) {
        this.checkpointYaw = checkpointYaw;
    }

    public float getCheckpointPitch() {
        return checkpointPitch;
    }

    public void setCheckpointPitch(float checkpointPitch) {
        this.checkpointPitch = checkpointPitch;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public int getMinRange() {
        return minRange;
    }

    public void setMinRange(int minRange) {
        this.minRange = minRange;
    }

    public int getMaxRange() {
        return maxRange;
    }

    public void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

}
