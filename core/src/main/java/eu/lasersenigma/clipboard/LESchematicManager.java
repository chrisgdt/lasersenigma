package eu.lasersenigma.clipboard;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.exceptions.*;
import eu.lasersenigma.nms.IWECopy;
import eu.lasersenigma.nms.IWEPaste;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.tasks.CreateAreaComponentsFromSchematicTask;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.io.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Level;

public class LESchematicManager {

    public static final String SCHEMATIC_VERSION = "1.1";

    public static final int NB_MAXIMUM_BLOCK = 1000000000;

    public static final String SCHEMATIC_FORLDER_NAME = "schematics";

    public static final String SCHEMATIC_EXTENSION = ".leschem";

    public static LESchematic createSchematic(Player player) throws WorldEditNotAvailableException, SelectionNotCuboidException, SelectionOverlapPartiallyAreaException, InvalidSelectionException, SchematicSaveException {
        LESchematic schematic = new LESchematic();
        IWECopy copiedData = NMSManager.getNMS().worldEditCopy(player);
        schematic.setVersion(SCHEMATIC_VERSION);
        schematic.setAreasSchematic(AreasSchematicManager.createSchematic(copiedData.getPlayerLocation(), copiedData.getMin(), copiedData.getMax()));
        schematic.setWorleditSchematic(copiedData.getBytes());
        return schematic;
    }

    public static void pasteSchematic(Player player, LESchematic schematic) throws WorldEditNotAvailableException, PasteOutsideWorldException, AreaOverlapException, InvalidSelectionException, SchematicInvalidException, VictoryAreaMustHaveCommonWallException {
        IWEPaste wePaste = NMSManager.getNMS().worldEditPaste(player, schematic.getWorleditSchematic());
        wePaste.initializePaste();
        List<CreateAreaComponentsFromSchematicTask> createAreasComponentsTasks = AreasSchematicManager.createAreas(schematic.getAreasSchematic(), wePaste.getPlayerLocation());
        wePaste.finalizePaste();
        cleanArmorStandEntities(schematic.getAreasSchematic(), wePaste.getPlayerLocation());
        createComponents(createAreasComponentsTasks);
    }

    public static LESchematic importSchematic(String schematicFileName) throws SchematicInvalidException {
        try {
            FileInputStream fis = new FileInputStream(getSchematicFile(schematicFileName, true, false));
            BufferedInputStream bis = new BufferedInputStream(fis);
            LESchematic schematic;
            try (ObjectInputStream ois = new ObjectInputStream(bis)) {
                try {
                    schematic = (LESchematic) ois.readObject();
                    if (!schematic.getVersion().equals(SCHEMATIC_VERSION)) {
                        throw new SchematicInvalidException();
                    }
                } catch (ClassNotFoundException ex) {
                    LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, null, ex);
                    throw new SchematicInvalidException();
                }
            }
            return schematic;
        } catch (IOException e) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, MessageFormat.format("Error writting schematic file ({0}):", schematicFileName), e);
            throw new SchematicInvalidException();
        }
    }

    public static void exportSchematic(LESchematic schematic, String schematicFileName) throws SchematicSaveException, SchematicInvalidException {

        try (FileOutputStream fos = new FileOutputStream(getSchematicFile(schematicFileName, false, true));
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             ObjectOutputStream oos = new ObjectOutputStream(bos);) {
            oos.writeObject(schematic);

        } catch (IOException e) {
            LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, MessageFormat.format("Error writting schematic file ({0}):", schematicFileName), e);
            throw new SchematicSaveException();
        }
    }

    private static File getSchematicFolder() {
        File schematicFolder = new File(LasersEnigmaPlugin.getInstance().getDataFolder(), SCHEMATIC_FORLDER_NAME);
        if (!schematicFolder.exists()) {
            schematicFolder.mkdirs();
        }
        return schematicFolder;
    }

    private static File getSchematicFile(String schematicFileName, boolean checkExist, boolean create) throws SchematicInvalidException {
        File schematic = new File(getSchematicFolder(), schematicFileName + SCHEMATIC_EXTENSION);
        if (schematic.exists() && schematic.isDirectory()) {
            throw new SchematicInvalidException();
        }
        if (checkExist) {
            if (!schematic.exists()) {
                throw new SchematicInvalidException();
            }
        }
        if (create) {
            try {
                schematic.createNewFile();
            } catch (IOException ex) {
                LasersEnigmaPlugin.getInstance().getBetterLogger().log(Level.SEVERE, MessageFormat.format("Error creating schematic file ({0}):", schematicFileName), ex);
                throw new SchematicInvalidException();
            }
        }
        return schematic;
    }

    public static String getSchematicDisplayName(String schematicFileName) {
        return new StringBuilder(SCHEMATIC_FORLDER_NAME)
                .append(File.separator)
                .append(schematicFileName)
                .append(SCHEMATIC_EXTENSION).toString();
    }

    private static void cleanArmorStandEntities(AreasSchematic schematic, Location playerLocation) {
        Location min = AreasSchematicManager.getMinLocation(schematic, playerLocation);
        Location max = AreasSchematicManager.getMaxLocation(schematic, playerLocation);
        playerLocation.getWorld().getEntities()
                .stream()
                .filter(e -> e.getCustomName() != null && e.getCustomName() != null && e.getCustomName().equals(AArmorStandComponent.ARMORSTAND_CUSTOM_NAME) && Area.containsLocation(e.getLocation(), min, max))
                .forEach(e -> e.remove());
    }

    private static void createComponents(List<CreateAreaComponentsFromSchematicTask> createAreasComponentsTasks) {
        createAreasComponentsTasks.forEach(task -> task.runTask(LasersEnigmaPlugin.getInstance()));
    }
}
