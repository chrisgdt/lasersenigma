package eu.lasersenigma.clipboard;

import eu.lasersenigma.areas.Area;
import eu.lasersenigma.areas.Areas;
import eu.lasersenigma.components.CallButton;
import eu.lasersenigma.components.Elevator;
import eu.lasersenigma.components.KeyChest;
import eu.lasersenigma.components.Lock;
import eu.lasersenigma.components.attributes.DetectionMode;
import eu.lasersenigma.exceptions.*;
import eu.lasersenigma.tasks.CreateAreaComponentsFromSchematicTask;
import org.bukkit.Location;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class AreaSchematicManager {

    private AreaSchematicManager() {

    }

    public static AreaSchematic createSchematic(Area area, Location playerLocation) {
        AreaSchematic areaSchematic = new AreaSchematic();
        Vector vMin = area.getMinLocation().toVector().subtract(playerLocation.toVector());
        Vector vMax = area.getMaxLocation().toVector().subtract(playerLocation.toVector());
        areaSchematic.setMinLocDiffX(vMin.getX());
        areaSchematic.setMinLocDiffY(vMin.getY());
        areaSchematic.setMinLocDiffZ(vMin.getZ());
        areaSchematic.setMaxLocDiffX(vMax.getX());
        areaSchematic.setMaxLocDiffY(vMax.getY());
        areaSchematic.setMaxLocDiffZ(vMax.getZ());
        areaSchematic.setVrotation(area.isVMirrorsRotationAllowed());
        areaSchematic.setHrotation(area.isHMirrorsRotationAllowed());
        areaSchematic.setCvrotation(area.isVConcentratorsRotationAllowed());
        areaSchematic.setChrotation(area.isHConcentratorsRotationAllowed());
        areaSchematic.setLsvrotation(area.isVLaserSendersRotationAllowed());
        areaSchematic.setLshrotation(area.isHLaserSendersRotationAllowed());
        areaSchematic.setLrvrotation(area.isVLaserReceiversRotationAllowed());
        areaSchematic.setLrhrotation(area.isHLaserReceiversRotationAllowed());
        areaSchematic.setArspheresizing(area.isAttractionRepulsionSpheresResizingAllowed());
        areaSchematic.setMode(area.getVictoryCriteria().toString());
        areaSchematic.setMinRange(area.getMinimumRange());
        areaSchematic.setMaxRange(area.getMaximumRange());
        if (area.getCheckpoint() != null) {
            Vector vCheckpoint = area.getCheckpoint().toVector().subtract(playerLocation.toVector());
            areaSchematic.setCheckpointX(vCheckpoint.getX());
            areaSchematic.setCheckpointY(vCheckpoint.getY());
            areaSchematic.setCheckpointZ(vCheckpoint.getZ());
            areaSchematic.setCheckpointPitch(area.getCheckpoint().getPitch());
            areaSchematic.setCheckpointYaw(area.getCheckpoint().getYaw());
        }
        List<ComponentSchematic> componentSchematicList = new ArrayList<>();
        area.getComponents().stream()
                .filter(c -> (!(c instanceof KeyChest || c instanceof Lock || c instanceof Elevator || c instanceof CallButton)))
                .forEach(c -> componentSchematicList.add(ComponentSchematicManager.createSchematic(c, playerLocation)));
        areaSchematic.setComponentSchematicList(componentSchematicList);
        List<VictoryAreaSchematic> victoryAreaSchematicList = new ArrayList<>();
        area.getVictoryAreas().forEach(va -> victoryAreaSchematicList.add(VictoryAreaSchematicManager.createSchematic(va, playerLocation)));
        areaSchematic.setVictoryAreaSchematicList(victoryAreaSchematicList);
        return areaSchematic;
    }

    public static CreateAreaComponentsFromSchematicTask createArea(AreaSchematic areaSchematic, Location playerLocation) throws AreaOverlapException, AreaCrossWorldsException, AreaNoDepthException, NoAreaFoundException, VictoryAreaMustHaveCommonWallException {
        Location checkpointLocation = null;
        if (areaSchematic.getCheckpointPitch() != Float.MIN_VALUE) {
            checkpointLocation = playerLocation.clone().add(new Vector(
                    areaSchematic.getCheckpointX(),
                    areaSchematic.getCheckpointY(),
                    areaSchematic.getCheckpointZ()
            ));
            checkpointLocation.setPitch(areaSchematic.getCheckpointPitch());
            checkpointLocation.setYaw(areaSchematic.getCheckpointYaw());
        }
        final Area area = Areas.getInstance().createArea(
                playerLocation.clone().add(new Vector(
                        areaSchematic.getMinLocDiffX(),
                        areaSchematic.getMinLocDiffY(),
                        areaSchematic.getMinLocDiffZ()
                )),
                playerLocation.clone().add(new Vector(
                        areaSchematic.getMaxLocDiffX(),
                        areaSchematic.getMaxLocDiffY(),
                        areaSchematic.getMaxLocDiffZ()
                )),
                -1,
                areaSchematic.getVrotation(),
                areaSchematic.getHrotation(),
                areaSchematic.getCvrotation(),
                areaSchematic.getChrotation(),
                areaSchematic.getLsvrotation(),
                areaSchematic.getLshrotation(),
                areaSchematic.getLrvrotation(),
                areaSchematic.getLrhrotation(),
                areaSchematic.getArspheresizing(),
                checkpointLocation,
                DetectionMode.valueOf(areaSchematic.getMode()),
                areaSchematic.getMinRange(),
                areaSchematic.getMaxRange()
        );
        for (VictoryAreaSchematic vas : areaSchematic.getVictoryAreaSchematicList()) {
            VictoryAreaSchematicManager.createVictoryArea(area, vas, playerLocation);
        }
        return new CreateAreaComponentsFromSchematicTask(areaSchematic.getComponentSchematicList(), area, playerLocation);
    }
}
