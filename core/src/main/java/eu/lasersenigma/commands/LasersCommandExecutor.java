package eu.lasersenigma.commands;

import eu.lasersenigma.areas.AreaController;
import eu.lasersenigma.areas.AreaSizeModificationAction;
import eu.lasersenigma.areas.CardinalDirection;
import eu.lasersenigma.clipboard.ClipboardManager;
import eu.lasersenigma.config.Permission;
import eu.lasersenigma.events.commands.PlayerTryToUseLasersClipboardCommandLEEvent;
import eu.lasersenigma.events.commands.PlayerTryToUseLasersCommandLEEvent;
import eu.lasersenigma.player.LEPlayers;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * A command executor used for commands that can only be sent by players
 */
public class LasersCommandExecutor implements CommandExecutor {

    private static final String COMMAND_LASERS_SYNTAX_WORLDEDIT1 = "/lasers [copy|paste]";
    private static final String COMMAND_LASERS_SYNTAX_WORLDEDIT2 = "/lasers [save|load] [schematic]";
    private static final String COMMAND_LASERS_SYNTAX_AREA_CONTRACT_SUBSTRACT = "/lasers area [contract|expand] <amount> [direction]";
    private static final String COMMAND_LASERS_SYNTAX_AREA_SHOW = "/lasers area show";

    /**
     * Constructor
     */
    public LasersCommandExecutor() {
    }

    /**
     * executed when receiving commands
     *
     * @param sender the command sender
     * @param cmd    the command
     * @param label  the command label
     * @param args   the arguments of the command
     * @return true if the command was valid
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        //Only players can use this command
        if (!(sender instanceof Player)) {
            return false;
        }
        //"lasers" is the only command accepted
        if (!cmd.getName().equalsIgnoreCase("lasers")) {
            return false;
        }
        Player player = (Player) sender;
        if (args.length == 0) {
            return toggleEditionMode(sender, player, new PlayerTryToUseLasersCommandLEEvent(player, cmd, label, args));
        } else {
            String cmdArg = args[0];
            PlayerTryToUseLasersClipboardCommandLEEvent laserCmdEvent = new PlayerTryToUseLasersClipboardCommandLEEvent(player, cmd, label, args);
            switch (cmdArg) {
                case "menu":
                    return toggleEditionMode(sender, player, new PlayerTryToUseLasersCommandLEEvent(player, cmd, label, args));
                case "copy":
                    if (callClipboardCommandLEEvent(sender, laserCmdEvent)) return false;
                    ClipboardManager.getInstance().copy(player);
                    return true;
                case "paste":
                    if (callClipboardCommandLEEvent(sender, laserCmdEvent)) return false;
                    ClipboardManager.getInstance().paste(player);
                    return true;
                case "load":
                    if (callClipboardCommandLEEvent(sender, laserCmdEvent)) return false;
                    if (args.length == 2) {
                        ClipboardManager.getInstance().load(player, args[1]);
                    } else {
                        sendWorldEditSyntax(player);
                    }
                    return true;
                case "save":
                    if (callClipboardCommandLEEvent(sender, laserCmdEvent)) return false;
                    if (args.length == 2) {
                        ClipboardManager.getInstance().save(player, args[1]);
                    } else {
                        sendWorldEditSyntax(player);
                    }
                    return true;
                case "area":
                    if (args.length < 2) {
                        sendAreaSyntax(player);
                    } else {
                        switch (args[1]) {
                            case "show":
                                AreaController.showNearestArea(LEPlayers.getInstance().findLEPlayer(player));
                                return true;
                            case "contract":
                            case "expand":
                                if (args.length < 3 || 4 < args.length) {
                                    player.sendMessage(COMMAND_LASERS_SYNTAX_AREA_CONTRACT_SUBSTRACT);
                                    return true;
                                }
                                AreaSizeModificationAction action = "expand".equals(args[1]) ? AreaSizeModificationAction.EXPAND : AreaSizeModificationAction.CONTRACT;
                                int amount;
                                try {
                                    amount = Integer.parseInt(args[2]);
                                } catch (NumberFormatException e) {
                                    sendAreaSyntax(player);
                                    return true;
                                }
                                CardinalDirection direction = CardinalDirection.ALL;
                                if (args.length == 4) {
                                    direction = CardinalDirection.fromString(args[3]);
                                    if (direction == null) {
                                        sendAreaSyntax(player);
                                        return true;
                                    }
                                }
                                AreaController.modifyArea(LEPlayers.getInstance().findLEPlayer(player), action, amount, direction);
                                return true;
                            default:
                                sendAreaSyntax(player);
                                return true;
                        }
                    }
                default:
                    sendAllSyntax(player);
                    return true;
            }
        }
    }

    private boolean callClipboardCommandLEEvent(CommandSender sender, PlayerTryToUseLasersClipboardCommandLEEvent laserCmdEvent) {
        Bukkit.getServer().getPluginManager().callEvent(laserCmdEvent);
        if (laserCmdEvent.isCancelled()) {
            return true;
        }
        return !laserCmdEvent.getBypassPermissions() && !Permission.ADMIN.checkPermissionAndSendMsg(sender);
    }

    private boolean toggleEditionMode(CommandSender sender, Player player, PlayerTryToUseLasersCommandLEEvent laserCmdEvent) {
        Bukkit.getServer().getPluginManager().callEvent(laserCmdEvent);
        if (laserCmdEvent.isCancelled()) {
            return false;
        }
        if (!laserCmdEvent.getBypassPermissions() && !Permission.EDIT.checkPermissionAndSendMsg(sender)) {
            return false;
        }
        LEPlayers.getInstance().findLEPlayer(player).getInventoryManager().toggleEditionMode();
        return true;
    }

    private void sendAllSyntax(Player player) {
        sendAreaSyntax(player);
        sendWorldEditSyntax(player);
    }

    private void sendWorldEditSyntax(Player player) {
        player.sendMessage(COMMAND_LASERS_SYNTAX_WORLDEDIT1);
        player.sendMessage(COMMAND_LASERS_SYNTAX_WORLDEDIT2);
    }

    private void sendAreaSyntax(Player player) {
        player.sendMessage(COMMAND_LASERS_SYNTAX_AREA_SHOW);
        player.sendMessage(COMMAND_LASERS_SYNTAX_AREA_CONTRACT_SUBSTRACT);
    }
}
