package eu.lasersenigma.particles;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.attributes.Direction;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.events.laserparticles.ParticleTryToHitEntityLEEvent;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.AItemStackFlagsProcessor;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.LinkedList;

public class ParticleImpactProcessor {

    private static final short BASE_DURABILITY_DAMAGES = 1;
    private static boolean configIntialized = false;
    private static String armor_no_knockback_tag;
    private static String armor_no_burn_tag;
    private static String armor_no_damage_tag;
    private static String armor_reflect_tag;
    private static String armor_prism_tag;
    private static String armor_focus_tag;
    private static String armor_lasers_durability_regex;

    private static AItemStackFlagsProcessor itemStackFlagsProcessor;
    private final LaserParticle particle;
    private final LivingEntity lentity;
    private final ParticleTryToHitEntityLEEvent event;
    private boolean no_knockback = false;
    private boolean no_burn = false;
    private boolean no_damage = false;
    private boolean reflect = false;
    private boolean prism = false;
    private boolean focus = false;
    private int durabilityModifier = 7;
    private int burnsTickDuration;
    private double additionalDamage;
    private double knockbackMultiplier;
    private boolean entityStopsLasers;

    public ParticleImpactProcessor(LaserParticle particle, LivingEntity entity, ParticleTryToHitEntityLEEvent event) {
        this.particle = particle;
        this.lentity = entity;
        this.event = event;
        if (!configIntialized) {
            armor_no_knockback_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_knockback_tag");
            armor_no_burn_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_burn_tag");
            armor_no_damage_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_no_damage_tag");
            armor_reflect_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_reflect_tag");
            armor_prism_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_prism_tag");
            armor_focus_tag = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_focus_tag");
            armor_lasers_durability_regex = LasersEnigmaPlugin.getInstance().getConfig().getString("armor_lasers_reduces_durability_tag").replace("X", "([0-9])");
            itemStackFlagsProcessor = NMSManager.getNMS().getItemStackFlagsProcessor();
            itemStackFlagsProcessor.setNoKnockbackFlag(armor_no_knockback_tag);
            itemStackFlagsProcessor.setNoBurnFlag(armor_no_burn_tag);
            itemStackFlagsProcessor.setNoDamageFlag(armor_no_damage_tag);
            itemStackFlagsProcessor.setReflectFlag(armor_reflect_tag);
            itemStackFlagsProcessor.setPrismFlag(armor_prism_tag);
            itemStackFlagsProcessor.setFocusFlag(armor_focus_tag);
            itemStackFlagsProcessor.setDurabilityFlag(armor_lasers_durability_regex);
            configIntialized = true;
        }
    }

    private static ArrayList<Direction> getPrismNewDirection(Direction direction, int size) {
        ArrayList<Direction> result;
        LinkedList<Direction> res = new LinkedList<>();
        if (size > 0) {
            boolean even = size % 2 == 0;
            Vector diffVector = direction
                    .clone()
                    .crossProduct(new Vector(0, 1, 0))
                    .normalize().multiply(0.1);
            Vector minusDiffVector = diffVector.clone().multiply(-1);
            Vector lastDiffVector, minusLastDiffVector;
            int nbRemaining = size;
            if (even) {
                Vector halfDiffVector = diffVector.clone().multiply(0.5);
                lastDiffVector = halfDiffVector;
                minusLastDiffVector = halfDiffVector.clone().multiply(-1);
                res.add(new Direction(direction.clone().add(minusLastDiffVector)));
                res.add(new Direction(direction.clone().add(lastDiffVector)));
                nbRemaining -= 2;
            } else {
                lastDiffVector = new Vector(0, 0, 0);
                minusLastDiffVector = lastDiffVector;
                res.add(direction.clone());
                nbRemaining--;
            }
            while (nbRemaining > 0) {
                lastDiffVector = lastDiffVector.add(diffVector);
                minusLastDiffVector = minusLastDiffVector.add(minusDiffVector);
                res.add(0, new Direction(direction.clone().add(minusLastDiffVector)));
                res.add(res.size(), new Direction(direction.clone().add(lastDiffVector)));
                nbRemaining -= 2;
            }
        }
        result = new ArrayList<>(res);
        return result;
    }

    private static ReflectionData reflect(LivingEntity entity, ReflectionData laserParticleData) {
        /* Reflexion Direction calculation */
        //https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
        //https://stackoverflow.com/questions/5666222/3d-line-plane-intersection
        //http://www.3dkingdoms.com/weekly/weekly.php?a=2
        Vector mirrorPlaneNormal = new Direction(entity.getEyeLocation().getDirection());
        Location mirrorCenterLocation = entity.getLocation().clone();
        double dotProd = laserParticleData.getDirection().normalize().dot(mirrorPlaneNormal.normalize());
        if (dotProd == 0) { //No Intersection can be calculated.
            return new ReflectionData(laserParticleData.getDirection(), laserParticleData.getLocation(), ReflectionResultType.ORTHOGONAL);
        } else { //intersection calculation
            double param = (-mirrorPlaneNormal.getX() * (laserParticleData.getLocation().getX() - mirrorCenterLocation.getX())
                    - mirrorPlaneNormal.getY() * (laserParticleData.getLocation().getY() - mirrorCenterLocation.getY())
                    - mirrorPlaneNormal.getZ() * (laserParticleData.getLocation().getZ() - mirrorCenterLocation.getZ()))
                    / (mirrorPlaneNormal.getX() * laserParticleData.getDirection().getX()
                    + mirrorPlaneNormal.getY() * laserParticleData.getDirection().getY()
                    + mirrorPlaneNormal.getZ() * laserParticleData.getDirection().getZ());
            Location intersection = new Location(
                    laserParticleData.getLocation().getWorld(),
                    laserParticleData.getLocation().getX() + laserParticleData.getDirection().getX() * param,
                    laserParticleData.getLocation().getY() + laserParticleData.getDirection().getY() * param,
                    laserParticleData.getLocation().getZ() + laserParticleData.getDirection().getZ() * param);
            Direction resultDirection = new Direction(
                    laserParticleData.getDirection().getX() - (2 * dotProd * mirrorPlaneNormal.getX()),
                    laserParticleData.getDirection().getY() - (2 * dotProd * mirrorPlaneNormal.getY()),
                    laserParticleData.getDirection().getZ() - (2 * dotProd * mirrorPlaneNormal.getZ()));
            return new ReflectionData(resultDirection, intersection, ReflectionResultType.REFLECTED);
        }
    }

    public boolean process(boolean checkDamages) {
        if (lentity.getUniqueId().equals(particle.getLastEntityUUID())) {
            return false;
        }
        if (event.isCancelled() || event.getIgnoreDefaults()) {
            return event.getEntityStopLasers();
        }
        setDefaults();
        ItemStack chestPlate = null;
        if (lentity.getEquipment() != null) {
            chestPlate = lentity.getEquipment().getChestplate();
        }
        if (chestPlate != null && chestPlate.getType() != Material.AIR) {
            setFlags(chestPlate);
        }
        if (checkDamages) {
            applyDamages();
            modifyChestPlateDurability(chestPlate);
        }
        reflect();
        prism();
        focus();

        if (reflect || prism || focus) {
            return true;
        }
        if (no_burn && no_damage && no_knockback) {
            return true;
        }
        return entityStopsLasers;
    }

    private void applyDamages() {
        boolean burn = false, damage = false, knockback = false;
        if (!no_burn && burnsTickDuration > 0) {
            doLaserBurn(lentity, burnsTickDuration);
            burn = true;
        }
        if (!no_damage && additionalDamage > 0) {
            doLaserDamage(lentity, additionalDamage);
            damage = true;
        }
        if (burn || damage) {
            NMSManager.getNMS().playEffect(particle.getArea().getPlayers(), particle.getLocation().add(new Vector(0, 0.5, 0)), "LAVA", 0, 1, 0, 0.1, 1);
        }
        if (!no_knockback && knockbackMultiplier > 0) {
            NMSManager.getNMS().playEffect(particle.getArea().getPlayers(), particle.getLocation().add(new Vector(0, 0.5, 0)), "SMOKE_LARGE", 0, 0, 0, 0.1, 5);
            doLaserKnockback(lentity, knockbackMultiplier);
            knockback = true;
        }
        if (damage || burn || knockback) {
            SoundLauncher.playSound(particle.getLocation(), "ENTITY_GENERIC_EXTINGUISH_FIRE", 0.1f, 2, PlaySoundCause.LASER_PARTICLE_IMPACT_PLAYER);
        }
    }

    private void setDefaults() {
        burnsTickDuration = event.getLaserBurnsTickDuration();
        additionalDamage = event.getAdditionalDamage();
        knockbackMultiplier = event.getKnockbackMultiplier();
        entityStopsLasers = event.getEntityStopLasers();
    }

    private void setFlags(ItemStack chestPlate) {
        itemStackFlagsProcessor.process(chestPlate);
        no_knockback = itemStackFlagsProcessor.hasNoKnockback();
        no_burn = itemStackFlagsProcessor.hasNoBurn();
        no_damage = itemStackFlagsProcessor.hasNoDamage();
        reflect = itemStackFlagsProcessor.hasReflect();
        prism = itemStackFlagsProcessor.hasPrism();
        focus = itemStackFlagsProcessor.hasFocus();
        durabilityModifier = itemStackFlagsProcessor.getDurabilityModifier();
    }

    private void doLaserBurn(LivingEntity lentity, int laserBurnsDuration) {
        lentity.setFireTicks(laserBurnsDuration);
    }

    private void doLaserDamage(LivingEntity lentity, double additionnalDamage) {
        lentity.damage(additionnalDamage);
    }

    private void doLaserKnockback(LivingEntity lentity, double knockbackMultiplier) {
        lentity.setVelocity(particle.getDirection().clone().multiply(knockbackMultiplier));
    }

    private void modifyChestPlateDurability(ItemStack chestPlate) {
        if (durabilityModifier <= 0 || chestPlate == null) {
            return;
        }
        int flippedDurabilityModifier = 10 - durabilityModifier; // Takes user input, then flips value to match the users expected outcome 1 = strong 9 = weak in code, 9 = strong 1 = weak to avg person
        short newDurability = (short) (chestPlate.getDurability() + (BASE_DURABILITY_DAMAGES * flippedDurabilityModifier));
        if (newDurability > chestPlate.getType().getMaxDurability()) {
            lentity.getEquipment().setChestplate(ItemsFactory.getInstance().getItemStack(Item.EMPTY));
        } else {
            chestPlate.setDurability(newDurability);
        }
    }

    private void prism() {
        if (!prism) {
            return;
        }
        ArrayList<LasersColor> newColors = particle.getColor().decompose();
        ArrayList<Direction> newDirections = getPrismNewDirection(particle.getDirection(), newColors.size());
        for (int i = 0; i < newColors.size(); ++i) {
            particle.getArea().addLaserParticle(
                    new LaserParticle(lentity.getUniqueId(), particle.getLocation(), newDirections.get(i), newColors.get(i), particle.getArea(), particle.getLightLevel())
            );
        }
    }

    private void focus() {
        if (!focus) {
            return;
        }
        Direction newDirection = new Direction(lentity.getEyeLocation().getDirection());
        particle.getArea().addLaserParticle(
                new LaserParticle(lentity.getUniqueId(), lentity.getEyeLocation(), newDirection, particle.getColor(), particle.getArea(), particle.getLightLevel())
        );
    }

    private void reflect() {
        if (!reflect) {
            return;
        }
        ReflectionData inputRD = new ReflectionData(particle.getDirection(), particle.getLocation());
        ReflectionData outputRD = reflect(lentity, inputRD);
        switch (outputRD.getReflexionResult()) {
            case ORTHOGONAL:
                particle.getArea().addLaserParticle(
                        new LaserParticle(lentity.getUniqueId(), outputRD.getLocation(), outputRD.getDirection(), particle.getColor(), particle.getArea(), particle.getLightLevel())
                );
                break;
            case REFLECTED:
                particle.getArea().addLaserParticle(
                        new LaserParticle(lentity.getUniqueId(), outputRD.getLocation(), outputRD.getDirection(), particle.getColor(), particle.getArea(), particle.getLightLevel())
                );
                break;
            default:
                break;
        }
    }

}
