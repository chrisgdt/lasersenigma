package eu.lasersenigma.particles;

import eu.lasersenigma.components.attributes.Direction;
import org.bukkit.Location;

public final class ReflectionData {

    private final Direction direction;

    private final Location location;

    private final ReflectionResultType reflexionResult;

    public ReflectionData(Direction direction, Location location) {
        this.direction = direction;
        this.location = location;
        this.reflexionResult = null;
    }

    public ReflectionData(Direction direction, Location location, ReflectionResultType reflexionResult) {
        this.direction = direction.clone();
        this.location = location.clone();
        this.reflexionResult = reflexionResult;
    }

    public Direction getDirection() {
        return direction.clone();
    }

    public Location getLocation() {
        return location.clone();
    }

    public ReflectionResultType getReflexionResult() {
        return reflexionResult;
    }

    @Override
    public int hashCode() {
        int prime = 31;
        double result = reflexionResult == null ? 0 : reflexionResult.ordinal() + 1;
        result = result * prime + location.hashCode();
        result = result * prime + direction.hashCode();
        return new Double(result).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ReflectionData objCasted = (ReflectionData) obj;
        if (!direction.equals(objCasted.getDirection())) {
            return false;
        }
        if (!location.equals(objCasted.getLocation())) {
            return false;
        }
        return reflexionResult == objCasted.getReflexionResult();
    }
}
