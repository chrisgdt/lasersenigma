package eu.lasersenigma.particles;

public enum ReflectionResultType {
    /**
     * If the laser didn't touch the mirror
     */
    NO_INTERSECTION,
    /**
     * If the laser vector is orthogonal to the mirror
     */
    ORTHOGONAL,
    /**
     * If the laser was reflected;
     */
    REFLECTED;
}
