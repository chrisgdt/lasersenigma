package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.attributes.ArmorStandItemOffset;
import eu.lasersenigma.components.parents.AArmorStandComponent;
import eu.lasersenigma.components.parents.IMirrorContainer;
import eu.lasersenigma.items.Item;
import eu.lasersenigma.items.ItemsFactory;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

/**
 * The task runned when a players put a mirror on a mirror container
 */
public class PlaceMirrorOnSphereTask extends BukkitRunnable {

    /**
     * the mirror container
     */
    private final IMirrorContainer mirrorContainer;

    private final Vector clipVector;

    private final Vector enterInSphereVector;

    private final Item headItemAfterAnimation;

    /**
     * the number of times this task will currently be executed
     */
    private int nbTimesRemaining;

    private Location mirrorASBaseLocation;

    private boolean save;

    /**
     * Constructor
     *
     * @param mirrorContainer        the IMirrorContainer
     * @param headItemAfterAnimation the item used for the ArmorStand head
     * @param save should the presence of mirror be saved in database
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public PlaceMirrorOnSphereTask(IMirrorContainer mirrorContainer, Item headItemAfterAnimation, boolean save) {
        this.mirrorContainer = mirrorContainer;
        this.headItemAfterAnimation = headItemAfterAnimation;
        this.nbTimesRemaining = 30;
        this.save = save;
        Location mirrorContainerLoc = mirrorContainer.getLocation();
        SoundLauncher.playSound(mirrorContainerLoc, "BLOCK_PORTAL_AMBIENT", 1, 2f, PlaySoundCause.PLACE_MIRROR_ON_SPHERE_STEP_1);
        clipVector = mirrorContainer.getAnimationClipVector().multiply(-1d / 5);
        enterInSphereVector = mirrorContainer.getAnimationVector().multiply(-1d / 25);
        mirrorASBaseLocation = AArmorStandComponent.getDefaultArmorStandBaseLocation(mirrorContainerLoc, mirrorContainer.getFace()).add(mirrorContainer.getAnimationClipVector()).add(mirrorContainer.getAnimationVector());
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 1, 0);

    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        if (mirrorContainer.isRemoved()) {
            this.cancel();
            return;
        }
        if (nbTimesRemaining <= 0) {
            mirrorContainer.setOnGoingAnimation(false);
            mirrorContainer.setHasMirror(true, save);
            this.cancel();
            return;
        }

        if (nbTimesRemaining > 25) {
            mirrorASBaseLocation = mirrorASBaseLocation.add(clipVector);
            Location newLocation = mirrorContainer.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorContainer.getFace().getDefaultRotation(mirrorContainer.getType()));
            mirrorContainer.getArmorStandMirror().teleport(newLocation);
        } else {
            if (nbTimesRemaining == 25) {
                SoundLauncher.playSound(mirrorASBaseLocation, "BLOCK_WOODEN_DOOR_CLOSE", 1, 1.7f, PlaySoundCause.PLACE_MIRROR_ON_SPHERE_STEP_2);
                mirrorContainer.getArmorStandMirrorContainer().setHelmet(ItemsFactory.getInstance().getItemStack(headItemAfterAnimation));
            }
            mirrorASBaseLocation = mirrorASBaseLocation.add(enterInSphereVector);
            Location newLocation = mirrorContainer.getArmorStandLocationWithOffsets(
                    mirrorASBaseLocation,
                    ArmorStandItemOffset.GLASS_PANE,
                    mirrorContainer.getFace().getDefaultRotation(mirrorContainer.getType()));
            mirrorContainer.getArmorStandMirror().teleport(newLocation);
        }
        nbTimesRemaining--;
    }

}
