package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.areas.Area;
import eu.lasersenigma.components.Elevator;
import eu.lasersenigma.components.LaserReceiver;
import eu.lasersenigma.components.parents.IComponent;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Iterator;

/**
 * the task ran to play the defeat sound
 */
public class DefeatSoundTask extends BukkitRunnable {

    /**
     * the sound played
     */
    private static final String SOUND = "BLOCK_NOTE_BASS";
    /**
     * the pitch of the sound played
     */
    private static final float PITCH_BASE = 0.9f;
    /**
     * the modification applied to the pitch after each run
     */
    private static final float PITCH_MODIFICATION = -0.2f;
    /**
     * the area the sound must be played in
     */
    private final Area area;
    private final IComponent component;
    /**
     * the current pitch
     */
    private float pitch;
    /**
     * the current number of times the task must be ran
     */
    private int nbTimesRemaining;

    /**
     * Constructor
     *
     * @param area the area the sound must be played in
     */
    public DefeatSoundTask(Area area) {
        this.area = area;
        this.nbTimesRemaining = 3;
        this.pitch = PITCH_BASE;
        this.component = null;
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 3);
    }

    /**
     * Constructor
     *
     * @param area      the area the sound must be played in
     * @param component a component
     */
    public DefeatSoundTask(Area area, IComponent component) {
        this.area = area;
        this.nbTimesRemaining = 3;
        this.pitch = PITCH_BASE;
        this.component = component;
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 3);
    }

    /**
     * the method called on each repeated execution
     */
    @Override
    public void run() {
        if (nbTimesRemaining == 0) {
            this.cancel();
            return;
        }
        int nbLaserReceiver = 0;
        if (component == null) {
            Iterator<IComponent> it = area.getComponents().iterator();
            while (it.hasNext()) {
                IComponent ccomponent = it.next();
                if (ccomponent instanceof LaserReceiver) {
                    Location componentLoc = ccomponent.getLocation();
                    SoundLauncher.playSound(componentLoc, SOUND, 1, pitch, PlaySoundCause.DEFEAT);
                    nbLaserReceiver++;
                }
            }
        } else {
            Location componentLoc;
            if (component instanceof Elevator) {
                componentLoc = ((Elevator) component).getCageCenter();
            } else {
                componentLoc = component.getLocation();
            }
            SoundLauncher.playSound(componentLoc, SOUND, 1, pitch, PlaySoundCause.DEFEAT);
        }

        if (nbLaserReceiver == 0) {
            this.area.getPlayersNoCache().forEach(p -> {
                Location loc = p.getLocation();
                SoundLauncher.playSound(loc, SOUND, 1, pitch, PlaySoundCause.DEFEAT);
            });
        }
        pitch = pitch + PITCH_MODIFICATION;
        nbTimesRemaining--;
    }

}
