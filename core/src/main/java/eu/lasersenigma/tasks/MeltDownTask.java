package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.MeltableClayBlock;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashSet;

/**
 * Task used to melt down a meltable clay block
 */
public class MeltDownTask extends BukkitRunnable {

    /**
     * the meltable clay block
     */
    private final MeltableClayBlock meltableClayBlock;
    /**
     * the location of the meltable clay block
     */
    private final Location location;
    /**
     * the number of times this task must be ran
     */
    private int nbTimesRemaining = 50;

    /**
     * Constructor
     *
     * @param meltableClayBlock the meltable clay block that must melt down
     */
    public MeltDownTask(MeltableClayBlock meltableClayBlock) {
        this.meltableClayBlock = meltableClayBlock;
        this.location = meltableClayBlock.getLocation().clone().add(0.5, 0.5, 0.5);
        SoundLauncher.playSound(location, "BLOCK_LAVA_EXTINGUISH", 1, 0.3f, PlaySoundCause.MELTDOWN_STEP_1);
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 0);
    }

    /**
     * the method called on each execution of the task
     */
    @Override
    public void run() {
        HashSet<Player> players = meltableClayBlock.getArea().getPlayers();
        if (meltableClayBlock.isRemoved()) {
            this.cancel();
            return;
        }
        if (nbTimesRemaining <= 1) {
            meltableClayBlock.setIsMelting(false);
            meltableClayBlock.setMelted(true);
            NMSManager.getNMS().playEffect(players, location, "LAVA", 0.5, 0.5, 0.5, 0.1, 15);
            SoundLauncher.playSound(location, "ENTITY_LIGHTNING_IMPACT", 1, 1.5f, PlaySoundCause.MELTDOWN_STEP_2);
            Bukkit.getOnlinePlayers().forEach((player) -> {
                Location playerLocation = player.getLocation();
                if (player.getWorld() == location.getWorld()) {
                    double distance = playerLocation.distance(location);
                    if (distance < 3) {
                        Vector v = new Vector(
                                playerLocation.getX() - location.getX(),
                                playerLocation.getY() - location.getY(),
                                playerLocation.getZ() - location.getZ()
                        );
                        v.normalize().multiply(LasersEnigmaPlugin.getInstance().getConfig().getDouble("laser_knockback_multiplier")).multiply(1 / distance);
                        player.setVelocity(v);
                        player.damage(distance);
                    }
                }
            });
            this.cancel();
            return;
        }
        NMSManager.getNMS().playEffect(players, location, "SMOKE_LARGE", 0, 0, 0, 0.1, 5);
        NMSManager.getNMS().playEffect(players, location, "LAVA", 0, 0, 0, 0.1, 1);
        nbTimesRemaining--;
    }

}
