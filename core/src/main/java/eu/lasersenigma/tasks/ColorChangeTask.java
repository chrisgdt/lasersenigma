package eu.lasersenigma.tasks;

import eu.lasersenigma.LasersEnigmaPlugin;
import eu.lasersenigma.components.attributes.LasersColor;
import eu.lasersenigma.nms.NMSManager;
import eu.lasersenigma.sound.PlaySoundCause;
import eu.lasersenigma.sound.SoundLauncher;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;

/**
 * the task ran to play the defeat sound
 */
public class ColorChangeTask extends BukkitRunnable {

    private static final double TAU = 2 * Math.PI;
    private final Location baseLocation;
    private final Color color;
    private final HashSet<Player> areaPlayers;
    private double radius = 0.5;
    private int nbParticlesSquared = 4;
    private int nbTimesRemaining = 8;

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public ColorChangeTask(Location loc, LasersColor color, HashSet<Player> areaPlayers) {
        this.areaPlayers = areaPlayers;
        this.baseLocation = new Location(loc.getWorld(), loc.getBlockX() + 0.5, loc.getBlockY() + 0.5, loc.getBlockZ() + 0.5);
        SoundLauncher.playSound(baseLocation, "ENTITY_FIREWORK_LAUNCH", 1f, 0.7f, PlaySoundCause.COMPONENT_CHANGE_COLOR);
        this.color = color.getBukkitColor();
        this.runTaskTimer(LasersEnigmaPlugin.getInstance(), 0, 1);
    }

    /**
     * the method called on each repeated execution
     */
    @Override
    public void run() {
        if (nbTimesRemaining == 0) {
            this.cancel();
            return;
        }
        double increment = TAU / nbParticlesSquared;
        for (double angle1 = 0; angle1 < TAU; angle1 += increment) {
            double y = (Math.cos(angle1) * radius);
            double tmpRadius = Math.sin(angle1);
            for (double angle2 = 0; angle2 < TAU; angle2 += increment) {
                double x = Math.cos(angle2) * tmpRadius * radius;
                double z = Math.sin(angle2) * tmpRadius * radius;
                NMSManager.getNMS().playEffect(
                        areaPlayers,
                        new Location(baseLocation.getWorld(), baseLocation.getX() + x, baseLocation.getY() + y, baseLocation.getZ() + z),
                        "REDSTONE",
                        color);
            }
        }
        nbParticlesSquared += 1;
        nbTimesRemaining--;
        radius += 0.1;
    }

}
