package eu.lasersenigma.exceptions;

public class ActionSavingDisabledBecauseDifferentComponentException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "action_saving_disabled_because_different_component";

    /**
     * Constructor
     */
    public ActionSavingDisabledBecauseDifferentComponentException() {
        super(TRANSLATION_CODE);
    }

}
