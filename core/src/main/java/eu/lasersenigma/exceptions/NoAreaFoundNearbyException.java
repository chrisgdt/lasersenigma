package eu.lasersenigma.exceptions;

public class NoAreaFoundNearbyException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "no_areas_nearby";

    /**
     * Constructor
     */
    public NoAreaFoundNearbyException() {
        super(TRANSLATION_CODE);
    }

}
