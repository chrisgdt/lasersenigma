package eu.lasersenigma.exceptions;

public class NoPermissionException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    public static final String TRANSLATION_CODE = "no_permission";

    /**
     * Constructor
     */
    public NoPermissionException() {
        super(TRANSLATION_CODE);
    }

}
