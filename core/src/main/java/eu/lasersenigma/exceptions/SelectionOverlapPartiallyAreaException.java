package eu.lasersenigma.exceptions;

import org.bukkit.Location;

public class SelectionOverlapPartiallyAreaException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "selection_overlap_partially_area";

    protected final Location pointOutsideSelection;

    /**
     * Constructor
     *
     * @param pointOutsideSelection the point of the partially selected area
     *                              which is outside the selection
     */
    public SelectionOverlapPartiallyAreaException(Location pointOutsideSelection) {
        super(TRANSLATION_CODE);
        this.pointOutsideSelection = pointOutsideSelection;
    }

    public Location getPointOutsideSelection() {
        return pointOutsideSelection;
    }

}
