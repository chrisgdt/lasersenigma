package eu.lasersenigma.exceptions;

public class PasteOutsideWorldException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "paste_oustide_world";

    /**
     * Constructor
     */
    public PasteOutsideWorldException() {
        super(TRANSLATION_CODE);
    }

}
