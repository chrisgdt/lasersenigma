package eu.lasersenigma.exceptions;

public class VictoryAreaMustHaveCommonWallException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "victory_area_must_be_placed_against_area_walls";

    /**
     * Constructor
     */
    public VictoryAreaMustHaveCommonWallException() {
        super(TRANSLATION_CODE);
    }

}
