package eu.lasersenigma.exceptions;

public class AreaStatsNotLinkedException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "stats_not_linked";

    /**
     * Constructor
     */
    public AreaStatsNotLinkedException() {
        super(TRANSLATION_CODE);
    }

}
