package eu.lasersenigma.exceptions;

import eu.lasersenigma.components.parents.IComponent;

public class ComponentOutsideAreaException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "component_outside_area";

    private final IComponent component;

    /**
     * Constructor
     *
     * @param component the component that would be outside the area
     */
    public ComponentOutsideAreaException(IComponent component) {
        super(TRANSLATION_CODE);
        this.component = component;
    }

    public IComponent getComponent() {
        return component;
    }

}
