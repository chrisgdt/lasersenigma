package eu.lasersenigma.exceptions;

import eu.lasersenigma.areas.Area;

public class AreaOverlapException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "area_overlap";

    private final Area overlappedArea;

    /**
     * Constructor
     *
     * @param overlappedArea the area that is overlapping
     */
    public AreaOverlapException(Area overlappedArea) {
        super(TRANSLATION_CODE);
        this.overlappedArea = overlappedArea;
    }

    public Area getOverlappedArea() {
        return overlappedArea;
    }

}
