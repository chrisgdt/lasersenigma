package eu.lasersenigma.exceptions;

public class AreaNoDepthException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "area_has_no_depth";

    /**
     * Constructor
     */
    public AreaNoDepthException() {
        super(TRANSLATION_CODE);
    }

}
