package eu.lasersenigma.exceptions;

public class ActionSavingDisabledException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "action_saving_disabled";

    /**
     * Constructor
     */
    public ActionSavingDisabledException() {
        super(TRANSLATION_CODE);
    }

}
