package eu.lasersenigma.exceptions;

public class AreaCrossWorldsException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "area_cross_worlds";

    /**
     * Constructor
     */
    public AreaCrossWorldsException() {
        super(TRANSLATION_CODE);
    }

}
