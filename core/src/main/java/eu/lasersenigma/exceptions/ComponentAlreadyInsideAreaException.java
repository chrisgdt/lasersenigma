package eu.lasersenigma.exceptions;

public class ComponentAlreadyInsideAreaException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "component_already_inside_area";

    /**
     * Constructor
     */
    public ComponentAlreadyInsideAreaException() {
        super(TRANSLATION_CODE);
    }

}
