package eu.lasersenigma.exceptions;

public class NotSameAreaException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "not_same_area";

    /**
     * Constructor
     */
    public NotSameAreaException() {
        super(TRANSLATION_CODE);
    }

}
