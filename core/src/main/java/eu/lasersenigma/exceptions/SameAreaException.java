package eu.lasersenigma.exceptions;

public class SameAreaException extends AbstractLasersException {

    /**
     * The translation code for this error
     */
    private static final String TRANSLATION_CODE = "same_area";

    /**
     * Constructor
     */
    public SameAreaException() {
        super(TRANSLATION_CODE);
    }

}
