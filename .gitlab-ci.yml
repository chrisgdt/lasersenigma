image: maven:3-openjdk-16
stages:
  - build
  - deploy

variables:
  MAVEN_CLI_OPTS_ARTIFACTORY: "-s .m2/settings.xml --batch-mode"
  MAVEN_CLI_OPTS: "--batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  path: "/builds/$CI_PROJECT_PATH"
  

cache:
  paths:
    - .m2/repository/
    
build-branch:
  stage: build
  before_script:
    - cp $path/core/dev-example.properties $path/core/dev.properties
  except:
    - master@lasersenigma/lasersenigma
    - tags
  script:
    - mvn $MAVEN_CLI_OPTS clean install
    - mvn $MAVEN_CLI_OPTS generate-sources javadoc:aggregate
    - cp $path/core/target/LasersEnigma*.jar $path/
    - cp $path/core/target/classes/lasers-enigma-version.txt $path/
    - cp $path/core/target/classes/lasers-enigma-commit-hash.txt $path/
  artifacts:
    expire_in: "1 mos"
    paths:
      - $path/LasersEnigma*.jar
      - $path/lasers-enigma-commit-hash.txt
      - $path/lasers-enigma-version.txt
      
build-master:
  stage: build
  only:
    - master@lasersenigma/lasersenigma
  before_script:
    - cp $path/core/dev-example.properties $path/core/dev.properties
  script:
    - mvn $MAVEN_CLI_OPTS clean install
    - mvn $MAVEN_CLI_OPTS generate-sources javadoc:aggregate
    - cp $path/core/target/LasersEnigma*.jar $path/
    - cp $path/core/target/classes/lasers-enigma-version.txt $path/
    - cp $path/core/target/classes/lasers-enigma-commit-hash.txt $path/
  artifacts:
    expire_in: "6 mos"
    paths:
      - $path/LasersEnigma*.jar
      - $path/lasers-enigma-commit-hash.txt
      - $path/lasers-enigma-version.txt
      - $path/target/site/apidocs/
      
build-tags:
  stage: build
  only:
    - tags
  before_script:
    - cp $path/core/dev-example.properties $path/core/dev.properties
    - 'sed -i '':a;N;$!ba;s/\(\s*<revision>[^\n]*\)-SNAPSHOT/\1/g'' pom.xml'
  script:
    - mvn $MAVEN_CLI_OPTS clean install
    - mvn $MAVEN_CLI_OPTS generate-sources javadoc:aggregate
    - cp $path/core/target/LasersEnigma*.jar $path/
    - cp $path/core/target/classes/lasers-enigma-version.txt $path/
    - cp $path/core/target/classes/lasers-enigma-commit-hash.txt $path/
  artifacts:
    expire_in: "2 yrs"
    paths:
      - $path/LasersEnigma*.jar
      - $path/lasers-enigma-commit-hash.txt
      - $path/lasers-enigma-version.txt
      - $path/target/site/apidocs/

release-version-on-artifactory:
  stage: deploy
  only:
    - tags
  before_script:
    - cp $path/core/dev-example.properties $path/core/dev.properties
    - rm /usr/share/maven/boot/*.license
    - ls /usr/share/maven/boot/
    - 'sed -i '':a;N;$!ba;s/\(\s*<revision>[^\n]*\)-SNAPSHOT/\1/g'' pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' core/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-API/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_12_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_13_R2/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_14_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_15_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_16_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_16_R2/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_16_R3/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_17_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_18_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_18_R2/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${CI_COMMIT_TAG}"''/g'' nms/LE-NMS-v1_19_R1/pom.xml'
  script:
    - mvn $MAVEN_CLI_OPTS clean deploy

release-snapshot-on-artifactory:
  stage: deploy
  only:
    - master@lasersenigma/lasersenigma
  before_script:
    - cp $path/core/dev-example.properties $path/core/dev.properties
    - rm /usr/share/maven/boot/*.license
    - ls /usr/share/maven/boot/
    - 'SNAPSHOTVERSION=$(cat pom.xml | grep -e ''<revision>.*-SNAPSHOT<\/revision>'' | sed -En ''s/^.*revision>(.*)<\/revision.*$/\1/p'');'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' core/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-API/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_12_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_13_R2/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_14_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_15_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_16_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_16_R2/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_16_R3/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_17_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_18_R1/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_18_R2/pom.xml'
    - 'sed -i '':a;N;$!ba;s/${revision}/''"${SNAPSHOTVERSION}"''/g'' nms/LE-NMS-v1_19_R1/pom.xml'
  script:
    - mvn $MAVEN_CLI_OPTS clean deploy

upload-version-commit-hash-txt:
  image: maven:3.6-jdk-8
  stage: deploy
  only:
    - tags
  before_script:
    - apt-get update -qy
    - apt-get install -y libssl-dev
    - apt-get install -y sshpass
  script:
    - echo "Uploading lasers-enigma-commit-hash.txt to lasers-enigma.eu website."
    - sshpass -p "${www_pass}" scp -o PubkeyAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p $path/lasers-enigma-commit-hash.txt ${www_login}@lasers-enigma.eu:~/wordpress/
    
upload-snapshot-commit-hash-txt:
  image: maven:3.6-jdk-8
  stage: deploy
  only:
    - master@lasersenigma/lasersenigma
  before_script:
    - apt-get update -qy
    - apt-get install -y libssl-dev
    - apt-get install -y sshpass
  script:
    - echo "Uploading lasers-enigma-commit-hash.txt to lasers-enigma.eu website."
    - ls $path
    - sshpass -p "${www_pass}" scp -o PubkeyAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p $path/lasers-enigma-commit-hash.txt ${www_login}@lasers-enigma.eu:~/wordpress/

upload-version-txt:
  image: maven:3.6-jdk-8
  stage: deploy
  only:
    - tags
  before_script:
    - apt-get update -qy
    - apt-get install -y libssl-dev
    - apt-get install -y sshpass
  script:
    - echo "Uploading lasers-enigma-version.txt to lasers-enigma.eu website."
    - ls $path
    - sshpass -p "${www_pass}" scp -o PubkeyAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p $path/lasers-enigma-version.txt ${www_login}@lasers-enigma.eu:~/wordpress/
    
upload-version-javadoc:
  image: maven:3.6-jdk-8
  stage: deploy
  only:
    - tags
  before_script:
    - apt-get update -qy
    - apt-get install -y libssl-dev
    - apt-get install -y sshpass
    - apt-get install -y zip
  script:
    - project_version=$(cat lasers-enigma-version.txt)
    - echo "creating javadoc in https://lasers-enigma.eu/javadoc/${project_version}/"
    - sshpass -p "${www_pass}" ssh -oPubkeyAuthentication=no -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no ${www_login}@lasers-enigma.eu "cd /var/www/wordpress/javadoc; rm -R ${project_version}; rm ${project_version}.zip; mkdir ${project_version}"
    - zip $path/${project_version}.zip -r $path/target/site/apidocs/
    - ls $path
    - sshpass -p "${www_pass}" scp -o PubkeyAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p $path/${project_version}.zip ${www_login}@lasers-enigma.eu:~/wordpress/javadoc/
    - sshpass -p "${www_pass}" scp -r -o PubkeyAuthentication=no -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p $path/target/site/apidocs/* ${www_login}@lasers-enigma.eu:~/wordpress/javadoc/${project_version}

##deploy-version-on-servers:
##  image: maven:3.6-jdk-8
##  stage: deploy
##  only:
##    - tags
##  before_script:
##    - apt-get update -qy
##    - apt-get install -y libssl-dev
##    - apt-get install -y sshpass
##  script:
##    - echo "Stopping the servers"
##    - 'token_1_12=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_1_12}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'token_1_13=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_1_13}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'token_1_14=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_1_14}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'token_play=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_play}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'curl -k -H "Authorization: Bearer ${token_1_12}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_12_serverid}/stop?wait=true"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_13}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_13_serverid}/stop?wait=true"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_14}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_14_serverid}/stop?wait=true"'
##    - 'curl -k -H "Authorization: Bearer ${token_play}" "https://panel.lasers-enigma.eu/daemon/server/${pp_play_serverid}/stop?wait=true"'
##    - sleep 20
##    - echo "Deleting old jars from servers"
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_1_12_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_12@panel.lasers-enigma.eu
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_1_13_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_13@panel.lasers-enigma.eu
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_1_14_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_14@panel.lasers-enigma.eu
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_play_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|play@panel.lasers-enigma.eu
##    - echo "Uploading the jar to the servers"
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_1_12_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_12@panel.lasers-enigma.eu
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_1_13_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_13@panel.lasers-enigma.eu
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_1_14_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_14@panel.lasers-enigma.eu
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_play_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|play@panel.lasers-enigma.eu
##    - echo "Starting the servers"
##    - 'curl -k -H "Authorization: Bearer ${token_1_12}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_12_serverid}/start"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_13}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_13_serverid}/start"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_14}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_14_serverid}/start"'
##    - 'curl -k -H "Authorization: Bearer ${token_play}" "https://panel.lasers-enigma.eu/daemon/server/${pp_play_serverid}/start"'

##deploy-snapshot-on-servers:
##  image: maven:3.6-jdk-8
##  stage: deploy
##  only:
##    - master@lasersenigma/lasersenigma
##  before_script:
##    - apt-get update -qy
##    - apt-get install -y libssl-dev
##    - apt-get install -y sshpass
##  script:
##    - echo "Stopping the servers"
##    - 'token_1_12=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_1_12}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'token_1_13=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_1_13}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'token_1_14=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_1_14}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'token_1_14=$(curl -k -X POST "https://panel.lasers-enigma.eu/oauth2/token/request?grant_type=client_credentials&client_id=${pp_api_login}&client_secret=${pp_api_pass_play}" | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")'
##    - 'curl -k -H "Authorization: Bearer ${token_1_12}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_12_serverid}/stop?wait=true"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_13}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_13_serverid}/stop?wait=true"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_14}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_14_serverid}/stop?wait=true"'
##    - 'curl -k -H "Authorization: Bearer ${token_play}" "https://panel.lasers-enigma.eu/daemon/server/${pp_play_serverid}/stop?wait=true"'
##    - sleep 20
##    - echo "Deleting old jars from servers"
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_1_12_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_12@panel.lasers-enigma.eu
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_1_13_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_13@panel.lasers-enigma.eu
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_1_14_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_14@panel.lasers-enigma.eu
##    - echo "rm /plugins/LasersEnigma-*.jar" | sshpass -p "${pp_play_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|play@panel.lasers-enigma.eu
##    - echo "Uploading the jar to the servers"
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_1_12_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_12@panel.lasers-enigma.eu
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_1_13_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_13@panel.lasers-enigma.eu
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_1_14_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|1_14@panel.lasers-enigma.eu
##    - echo "put $path/LasersEnigma*.jar /plugins/" | sshpass -p "${pp_play_pass}" sftp -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no -oPort=5657 contact\@lasers-enigma.eu\|play@panel.lasers-enigma.eu
##    - echo "Starting the servers"
##    - 'curl -k -H "Authorization: Bearer ${token_1_12}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_12_serverid}/start"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_13}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_13_serverid}/start"'
##    - 'curl -k -H "Authorization: Bearer ${token_1_14}" "https://panel.lasers-enigma.eu/daemon/server/${pp_1_14_serverid}/start"'
##    - 'curl -k -H "Authorization: Bearer ${token_play}" "https://panel.lasers-enigma.eu/daemon/server/${pp_play_serverid}/start"'
